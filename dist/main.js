(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_admin_deatails_sucursal_deatails_sucursal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/admin/deatails-sucursal/deatails-sucursal.component */ "./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.ts");
/* harmony import */ var _components_admin_list_sucursales_list_sucursales_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/admin/list-sucursales/list-sucursales.component */ "./src/app/components/admin/list-sucursales/list-sucursales.component.ts");
/* harmony import */ var _components_users_login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/users/login/login.component */ "./src/app/components/users/login/login.component.ts");
/* harmony import */ var _components_users_profile_profile_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/users/profile/profile.component */ "./src/app/components/users/profile/profile.component.ts");
/* harmony import */ var _components_page404_page404_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/page404/page404.component */ "./src/app/components/page404/page404.component.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./guards/auth.guard */ "./src/app/guards/auth.guard.ts");
/* harmony import */ var _components_admin_home_admin_home_admin_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/admin/home-admin/home-admin.component */ "./src/app/components/admin/home-admin/home-admin.component.ts");
/* harmony import */ var _components_admin_list_repartidores_list_repartidores_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/admin/list-repartidores/list-repartidores.component */ "./src/app/components/admin/list-repartidores/list-repartidores.component.ts");
/* harmony import */ var _components_admin_list_administradores_list_administradores_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/admin/list-administradores/list-administradores.component */ "./src/app/components/admin/list-administradores/list-administradores.component.ts");
/* harmony import */ var _components_admin_list_clientes_list_clientes_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/admin/list-clientes/list-clientes.component */ "./src/app/components/admin/list-clientes/list-clientes.component.ts");
/* harmony import */ var _components_admin_list_transporte_list_transporte_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/admin/list-transporte/list-transporte.component */ "./src/app/components/admin/list-transporte/list-transporte.component.ts");
/* harmony import */ var _components_restaurante_list_menus_list_menus_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/restaurante/list-menus/list-menus.component */ "./src/app/components/restaurante/list-menus/list-menus.component.ts");
/* harmony import */ var _components_admin_list_restaurantes_list_restaurantes_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/admin/list-restaurantes/list-restaurantes.component */ "./src/app/components/admin/list-restaurantes/list-restaurantes.component.ts");
/* harmony import */ var _components_admin_list_servicios_list_servicios_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/admin/list-servicios/list-servicios.component */ "./src/app/components/admin/list-servicios/list-servicios.component.ts");
/* harmony import */ var _components_admin_detalle_pedido_detalle_pedido_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/admin/detalle-pedido/detalle-pedido.component */ "./src/app/components/admin/detalle-pedido/detalle-pedido.component.ts");
/* harmony import */ var _components_admin_list_hostorial_servicios_list_hostorial_servicios_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/admin/list-hostorial-servicios/list-hostorial-servicios.component */ "./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.ts");
/* harmony import */ var _components_admin_list_corte_list_corte_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/admin/list-corte/list-corte.component */ "./src/app/components/admin/list-corte/list-corte.component.ts");
/* harmony import */ var _components_admin_list_detail_servicios_list_detail_servicios_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/admin/list-detail-servicios/list-detail-servicios.component */ "./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.ts");
/* harmony import */ var _components_restaurante_list_pedidos_list_pedidos_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/restaurante/list-pedidos/list-pedidos.component */ "./src/app/components/restaurante/list-pedidos/list-pedidos.component.ts");
/* harmony import */ var _components_restaurante_detail_pedidos_convenio_detail_pedidos_convenio_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component */ "./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.ts");
/* harmony import */ var _components_restaurante_list_historial_pedidos_convenios_list_historial_pedidos_convenios_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component */ "./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.ts");
/* harmony import */ var _components_admin_config_servicio_config_servicio_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/admin/config-servicio/config-servicio.component */ "./src/app/components/admin/config-servicio/config-servicio.component.ts");
/* harmony import */ var _components_admin_config_banner_config_banner_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./components/admin/config-banner/config-banner.component */ "./src/app/components/admin/config-banner/config-banner.component.ts");
/* harmony import */ var _components_admin_list_category_convenio_list_category_convenio_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./components/admin/list-category-convenio/list-category-convenio.component */ "./src/app/components/admin/list-category-convenio/list-category-convenio.component.ts");
/* harmony import */ var _components_admin_modal_update_restaurant_modal_update_restaurant_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/admin/modal-update-restaurant/modal-update-restaurant.component */ "./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.ts");
/* harmony import */ var _components_admin_modal_update_repartidor_modal_update_repartidor_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./components/admin/modal-update-repartidor/modal-update-repartidor.component */ "./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.ts");







// import { RegisterComponent } from './components/users/register/register.component';























var routes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'user/login', component: _components_users_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"] },
    {
        path: 'user/profile',
        component: _components_users_profile_profile_component__WEBPACK_IMPORTED_MODULE_7__["ProfileComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/home-admin',
        component: _components_admin_home_admin_home_admin_component__WEBPACK_IMPORTED_MODULE_10__["HomeAdminComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-cortes',
        component: _components_admin_list_corte_list_corte_component__WEBPACK_IMPORTED_MODULE_20__["ListCorteComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'sucursal/:id',
        component: _components_admin_deatails_sucursal_deatails_sucursal_component__WEBPACK_IMPORTED_MODULE_4__["DeatailsSucursalComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-sucursal',
        component: _components_admin_list_sucursales_list_sucursales_component__WEBPACK_IMPORTED_MODULE_5__["ListSucursalesComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-repartidores',
        component: _components_admin_list_repartidores_list_repartidores_component__WEBPACK_IMPORTED_MODULE_11__["ListRepartidoresComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-administradores',
        component: _components_admin_list_administradores_list_administradores_component__WEBPACK_IMPORTED_MODULE_12__["ListAdministradoresComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-historial-servicios',
        component: _components_admin_list_hostorial_servicios_list_hostorial_servicios_component__WEBPACK_IMPORTED_MODULE_19__["ListHostorialServiciosComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'convenio/list-historial-servicios',
        component: _components_restaurante_list_historial_pedidos_convenios_list_historial_pedidos_convenios_component__WEBPACK_IMPORTED_MODULE_24__["ListHistorialPedidosConveniosComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'servicio-convenio/:uid',
        component: _components_restaurante_detail_pedidos_convenio_detail_pedidos_convenio_component__WEBPACK_IMPORTED_MODULE_23__["DetailPedidosConvenioComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/convenio-categoria',
        component: _components_admin_list_category_convenio_list_category_convenio_component__WEBPACK_IMPORTED_MODULE_27__["ListCategoryConvenioComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'restaurante/restaurante-list-menu',
        component: _components_restaurante_list_menus_list_menus_component__WEBPACK_IMPORTED_MODULE_15__["ListMenusComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-restaurantes',
        component: _components_admin_list_restaurantes_list_restaurantes_component__WEBPACK_IMPORTED_MODULE_16__["ListRestaurantesComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'list-servicios/:uid',
        component: _components_admin_list_detail_servicios_list_detail_servicios_component__WEBPACK_IMPORTED_MODULE_21__["ListDetailServiciosComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-transportes',
        component: _components_admin_list_transporte_list_transporte_component__WEBPACK_IMPORTED_MODULE_14__["ListTransporteComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/config-servicio',
        component: _components_admin_config_servicio_config_servicio_component__WEBPACK_IMPORTED_MODULE_25__["ConfigServicioComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/config-banner',
        component: _components_admin_config_banner_config_banner_component__WEBPACK_IMPORTED_MODULE_26__["ConfigBannerComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-servicios',
        component: _components_admin_list_servicios_list_servicios_component__WEBPACK_IMPORTED_MODULE_17__["ListServiciosComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-pedidos-res',
        component: _components_restaurante_list_pedidos_list_pedidos_component__WEBPACK_IMPORTED_MODULE_22__["ListPedidosComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'admin/list-clientes',
        component: _components_admin_list_clientes_list_clientes_component__WEBPACK_IMPORTED_MODULE_13__["ListClientesComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'servicio/:uid',
        component: _components_admin_detalle_pedido_detalle_pedido_component__WEBPACK_IMPORTED_MODULE_18__["DetallePedidoComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'updateRest/:uid',
        component: _components_admin_modal_update_restaurant_modal_update_restaurant_component__WEBPACK_IMPORTED_MODULE_28__["ModalUpdateRestaurantComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    {
        path: 'updateRepa/:uid',
        component: _components_admin_modal_update_repartidor_modal_update_repartidor_component__WEBPACK_IMPORTED_MODULE_29__["ModalUpdateRepartidorComponent"],
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_9__["AuthGuard"]]
    },
    { path: '**', component: _components_page404_page404_component__WEBPACK_IMPORTED_MODULE_8__["Page404Component"] }
    // { path: 'user/register', component: RegisterComponent, canActivate: [AuthGuard]  },
    // RESTAURANTES
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'BringmeWeb';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_admin_list_sucursales_list_sucursales_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/admin/list-sucursales/list-sucursales.component */ "./src/app/components/admin/list-sucursales/list-sucursales.component.ts");
/* harmony import */ var _components_admin_deatails_sucursal_deatails_sucursal_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/admin/deatails-sucursal/deatails-sucursal.component */ "./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.ts");
/* harmony import */ var _components_hero_hero_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/hero/hero.component */ "./src/app/components/hero/hero.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "./src/app/components/navbar/navbar.component.ts");
/* harmony import */ var _components_users_login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/users/login/login.component */ "./src/app/components/users/login/login.component.ts");
/* harmony import */ var _components_users_profile_profile_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/users/profile/profile.component */ "./src/app/components/users/profile/profile.component.ts");
/* harmony import */ var _components_page404_page404_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/page404/page404.component */ "./src/app/components/page404/page404.component.ts");
/* harmony import */ var _components_users_register_register_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/users/register/register.component */ "./src/app/components/users/register/register.component.ts");
/* harmony import */ var _components_admin_home_admin_home_admin_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/admin/home-admin/home-admin.component */ "./src/app/components/admin/home-admin/home-admin.component.ts");
/* harmony import */ var _components_admin_list_servicios_list_servicios_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/admin/list-servicios/list-servicios.component */ "./src/app/components/admin/list-servicios/list-servicios.component.ts");
/* harmony import */ var _components_admin_detalle_pedido_detalle_pedido_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/admin/detalle-pedido/detalle-pedido.component */ "./src/app/components/admin/detalle-pedido/detalle-pedido.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _pipes_filter_sucursal_pipe__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./pipes/filter-sucursal.pipe */ "./src/app/pipes/filter-sucursal.pipe.ts");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _components_admin_list_repartidores_list_repartidores_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./components/admin/list-repartidores/list-repartidores.component */ "./src/app/components/admin/list-repartidores/list-repartidores.component.ts");
/* harmony import */ var _components_modal_repartidor_modal_repartidor_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/modal-repartidor/modal-repartidor.component */ "./src/app/components/modal-repartidor/modal-repartidor.component.ts");
/* harmony import */ var _pipes_filter_repartidor_pipe__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./pipes/filter-repartidor.pipe */ "./src/app/pipes/filter-repartidor.pipe.ts");
/* harmony import */ var _components_admin_list_administradores_list_administradores_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./components/admin/list-administradores/list-administradores.component */ "./src/app/components/admin/list-administradores/list-administradores.component.ts");
/* harmony import */ var _components_modal_administrador_modal_administrador_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./components/modal-administrador/modal-administrador.component */ "./src/app/components/modal-administrador/modal-administrador.component.ts");
/* harmony import */ var _pipes_restaurante_filter_restaurante_pipe__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./pipes/restaurante/filter-restaurante.pipe */ "./src/app/pipes/restaurante/filter-restaurante.pipe.ts");
/* harmony import */ var _pipes_administrador_filter_administrador_pipe__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./pipes/administrador/filter-administrador.pipe */ "./src/app/pipes/administrador/filter-administrador.pipe.ts");
/* harmony import */ var _components_admin_list_clientes_list_clientes_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./components/admin/list-clientes/list-clientes.component */ "./src/app/components/admin/list-clientes/list-clientes.component.ts");
/* harmony import */ var _pipes_cliente_cliente_filter_pipe__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./pipes/cliente/cliente-filter.pipe */ "./src/app/pipes/cliente/cliente-filter.pipe.ts");
/* harmony import */ var _ctrl_ngx_csv__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @ctrl/ngx-csv */ "./node_modules/@ctrl/ngx-csv/fesm5/ctrl-ngx-csv.js");
/* harmony import */ var _components_admin_list_transporte_list_transporte_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./components/admin/list-transporte/list-transporte.component */ "./src/app/components/admin/list-transporte/list-transporte.component.ts");
/* harmony import */ var _components_modal_transporte_modal_transporte_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./components/modal-transporte/modal-transporte.component */ "./src/app/components/modal-transporte/modal-transporte.component.ts");
/* harmony import */ var _pipes_transporte_transporte_pipe__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./pipes/transporte/transporte.pipe */ "./src/app/pipes/transporte/transporte.pipe.ts");
/* harmony import */ var _components_restaurante_list_menus_list_menus_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./components/restaurante/list-menus/list-menus.component */ "./src/app/components/restaurante/list-menus/list-menus.component.ts");
/* harmony import */ var _components_restaurante_modal_menu_modal_menu_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./components/restaurante/modal-menu/modal-menu.component */ "./src/app/components/restaurante/modal-menu/modal-menu.component.ts");
/* harmony import */ var _components_admin_list_restaurantes_list_restaurantes_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./components/admin/list-restaurantes/list-restaurantes.component */ "./src/app/components/admin/list-restaurantes/list-restaurantes.component.ts");
/* harmony import */ var _components_modal_restaurante_modal_restaurante_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./components/modal-restaurante/modal-restaurante.component */ "./src/app/components/modal-restaurante/modal-restaurante.component.ts");
/* harmony import */ var _directives_ng_drop_files_directive__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./directives/ng-drop-files.directive */ "./src/app/directives/ng-drop-files.directive.ts");
/* harmony import */ var _components_modal_r_modal_r_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./components/modal-r/modal-r.component */ "./src/app/components/modal-r/modal-r.component.ts");
/* harmony import */ var _pipes_menu_filter_menu_pipe__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./pipes/menu/filter-menu.pipe */ "./src/app/pipes/menu/filter-menu.pipe.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var ngx_print__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ngx-print */ "./node_modules/ngx-print/fesm5/ngx-print.js");
/* harmony import */ var _pipes_servicios_filter_servicios_dia_pipe__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./pipes/servicios/filter-servicios-dia.pipe */ "./src/app/pipes/servicios/filter-servicios-dia.pipe.ts");
/* harmony import */ var _components_admin_list_hostorial_servicios_list_hostorial_servicios_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./components/admin/list-hostorial-servicios/list-hostorial-servicios.component */ "./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.ts");
/* harmony import */ var _pipes_historial_servicios_servicioh_usuario_pipe__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./pipes/historial-servicios/servicioh-usuario.pipe */ "./src/app/pipes/historial-servicios/servicioh-usuario.pipe.ts");
/* harmony import */ var _pipes_historial_servicios_servicioh_repartidor_pipe__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./pipes/historial-servicios/servicioh-repartidor.pipe */ "./src/app/pipes/historial-servicios/servicioh-repartidor.pipe.ts");
/* harmony import */ var _components_admin_list_corte_list_corte_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./components/admin/list-corte/list-corte.component */ "./src/app/components/admin/list-corte/list-corte.component.ts");
/* harmony import */ var _components_admin_list_detail_servicios_list_detail_servicios_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./components/admin/list-detail-servicios/list-detail-servicios.component */ "./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.ts");
/* harmony import */ var _components_restaurante_list_pedidos_list_pedidos_component__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! ./components/restaurante/list-pedidos/list-pedidos.component */ "./src/app/components/restaurante/list-pedidos/list-pedidos.component.ts");
/* harmony import */ var _pipes_restaurante_pedido_pedido_pipe__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! ./pipes/restaurante/pedido/pedido.pipe */ "./src/app/pipes/restaurante/pedido/pedido.pipe.ts");
/* harmony import */ var _components_restaurante_detail_pedidos_convenio_detail_pedidos_convenio_component__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! ./components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component */ "./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.ts");
/* harmony import */ var _components_restaurante_list_historial_pedidos_convenios_list_historial_pedidos_convenios_component__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! ./components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component */ "./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.ts");
/* harmony import */ var _components_admin_config_servicio_config_servicio_component__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! ./components/admin/config-servicio/config-servicio.component */ "./src/app/components/admin/config-servicio/config-servicio.component.ts");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var agm_direction__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! agm-direction */ "./node_modules/agm-direction/fesm5/agm-direction.js");
/* harmony import */ var _components_admin_config_banner_config_banner_component__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! ./components/admin/config-banner/config-banner.component */ "./src/app/components/admin/config-banner/config-banner.component.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _components_admin_list_category_convenio_list_category_convenio_component__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! ./components/admin/list-category-convenio/list-category-convenio.component */ "./src/app/components/admin/list-category-convenio/list-category-convenio.component.ts");
/* harmony import */ var _components_admin_modal_category_convenio_modal_category_convenio_component__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! ./components/admin/modal-category-convenio/modal-category-convenio.component */ "./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.ts");
/* harmony import */ var _components_admin_modal_corte_convenio_modal_corte_convenio_component__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! ./components/admin/modal-corte-convenio/modal-corte-convenio.component */ "./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.ts");
/* harmony import */ var _components_admin_modal_update_restaurant_modal_update_restaurant_component__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! ./components/admin/modal-update-restaurant/modal-update-restaurant.component */ "./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.ts");
/* harmony import */ var _components_admin_modal_update_repartidor_modal_update_repartidor_component__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! ./components/admin/modal-update-repartidor/modal-update-repartidor.component */ "./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.ts");
/* harmony import */ var _material_material_module__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! ./material/material.module */ "./src/app/material/material.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");


















// Otros








































// tslint:disable-next-line:max-line-length


// plugins











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_admin_list_sucursales_list_sucursales_component__WEBPACK_IMPORTED_MODULE_5__["ListSucursalesComponent"],
                _components_admin_deatails_sucursal_deatails_sucursal_component__WEBPACK_IMPORTED_MODULE_6__["DeatailsSucursalComponent"],
                _components_hero_hero_component__WEBPACK_IMPORTED_MODULE_7__["HeroComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
                _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_9__["ModalComponent"],
                _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_10__["NavbarComponent"],
                _components_users_login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"],
                _components_users_profile_profile_component__WEBPACK_IMPORTED_MODULE_12__["ProfileComponent"],
                _components_page404_page404_component__WEBPACK_IMPORTED_MODULE_13__["Page404Component"],
                _components_users_register_register_component__WEBPACK_IMPORTED_MODULE_14__["RegisterComponent"],
                _pipes_filter_sucursal_pipe__WEBPACK_IMPORTED_MODULE_25__["FilterSucursalPipe"],
                _components_admin_home_admin_home_admin_component__WEBPACK_IMPORTED_MODULE_15__["HomeAdminComponent"],
                _components_admin_list_repartidores_list_repartidores_component__WEBPACK_IMPORTED_MODULE_27__["ListRepartidoresComponent"],
                _components_modal_repartidor_modal_repartidor_component__WEBPACK_IMPORTED_MODULE_28__["ModalRepartidorComponent"],
                _pipes_filter_repartidor_pipe__WEBPACK_IMPORTED_MODULE_29__["FilterRepartidorPipe"],
                _components_admin_list_administradores_list_administradores_component__WEBPACK_IMPORTED_MODULE_30__["ListAdministradoresComponent"],
                _components_modal_administrador_modal_administrador_component__WEBPACK_IMPORTED_MODULE_31__["ModalAdministradorComponent"],
                _pipes_restaurante_filter_restaurante_pipe__WEBPACK_IMPORTED_MODULE_32__["FilterRestaurantePipe"],
                _pipes_administrador_filter_administrador_pipe__WEBPACK_IMPORTED_MODULE_33__["FilterAdministradorPipe"],
                _components_admin_list_clientes_list_clientes_component__WEBPACK_IMPORTED_MODULE_34__["ListClientesComponent"],
                _pipes_cliente_cliente_filter_pipe__WEBPACK_IMPORTED_MODULE_35__["ClienteFilterPipe"],
                _components_admin_list_transporte_list_transporte_component__WEBPACK_IMPORTED_MODULE_37__["ListTransporteComponent"],
                _components_modal_transporte_modal_transporte_component__WEBPACK_IMPORTED_MODULE_38__["ModalTransporteComponent"],
                _pipes_transporte_transporte_pipe__WEBPACK_IMPORTED_MODULE_39__["TransportePipe"],
                _components_restaurante_list_menus_list_menus_component__WEBPACK_IMPORTED_MODULE_40__["ListMenusComponent"],
                _components_restaurante_modal_menu_modal_menu_component__WEBPACK_IMPORTED_MODULE_41__["ModalMenuComponent"],
                _components_admin_list_restaurantes_list_restaurantes_component__WEBPACK_IMPORTED_MODULE_42__["ListRestaurantesComponent"],
                _components_modal_restaurante_modal_restaurante_component__WEBPACK_IMPORTED_MODULE_43__["ModalRestauranteComponent"],
                _directives_ng_drop_files_directive__WEBPACK_IMPORTED_MODULE_44__["NgDropFilesDirective"],
                _components_modal_r_modal_r_component__WEBPACK_IMPORTED_MODULE_45__["ModalRComponent"],
                _pipes_menu_filter_menu_pipe__WEBPACK_IMPORTED_MODULE_46__["FilterMenuPipe"],
                _components_admin_list_servicios_list_servicios_component__WEBPACK_IMPORTED_MODULE_16__["ListServiciosComponent"],
                _components_admin_detalle_pedido_detalle_pedido_component__WEBPACK_IMPORTED_MODULE_17__["DetallePedidoComponent"],
                _pipes_servicios_filter_servicios_dia_pipe__WEBPACK_IMPORTED_MODULE_49__["FilterServiciosDiaPipe"],
                _components_admin_list_hostorial_servicios_list_hostorial_servicios_component__WEBPACK_IMPORTED_MODULE_50__["ListHostorialServiciosComponent"],
                _pipes_historial_servicios_servicioh_usuario_pipe__WEBPACK_IMPORTED_MODULE_51__["ServiciohUsuarioPipe"],
                _pipes_historial_servicios_servicioh_repartidor_pipe__WEBPACK_IMPORTED_MODULE_52__["ServiciohRepartidorPipe"],
                _components_admin_list_corte_list_corte_component__WEBPACK_IMPORTED_MODULE_53__["ListCorteComponent"],
                _components_admin_list_detail_servicios_list_detail_servicios_component__WEBPACK_IMPORTED_MODULE_54__["ListDetailServiciosComponent"],
                _components_restaurante_list_pedidos_list_pedidos_component__WEBPACK_IMPORTED_MODULE_55__["ListPedidosComponent"],
                _pipes_restaurante_pedido_pedido_pipe__WEBPACK_IMPORTED_MODULE_56__["PedidoPipe"],
                _components_restaurante_detail_pedidos_convenio_detail_pedidos_convenio_component__WEBPACK_IMPORTED_MODULE_57__["DetailPedidosConvenioComponent"],
                _components_restaurante_list_historial_pedidos_convenios_list_historial_pedidos_convenios_component__WEBPACK_IMPORTED_MODULE_58__["ListHistorialPedidosConveniosComponent"],
                _components_admin_config_servicio_config_servicio_component__WEBPACK_IMPORTED_MODULE_59__["ConfigServicioComponent"],
                _components_admin_config_banner_config_banner_component__WEBPACK_IMPORTED_MODULE_62__["ConfigBannerComponent"],
                _components_admin_list_category_convenio_list_category_convenio_component__WEBPACK_IMPORTED_MODULE_64__["ListCategoryConvenioComponent"],
                _components_admin_modal_category_convenio_modal_category_convenio_component__WEBPACK_IMPORTED_MODULE_65__["ModalCategoryConvenioComponent"],
                _components_admin_modal_corte_convenio_modal_corte_convenio_component__WEBPACK_IMPORTED_MODULE_66__["ModalCorteConvenioComponent"],
                _components_admin_modal_update_restaurant_modal_update_restaurant_component__WEBPACK_IMPORTED_MODULE_67__["ModalUpdateRestaurantComponent"],
                _components_admin_modal_update_repartidor_modal_update_repartidor_component__WEBPACK_IMPORTED_MODULE_68__["ModalUpdateRepartidorComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_18__["FormsModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_20__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_19__["environment"].firebaseConfig),
                _angular_fire_database__WEBPACK_IMPORTED_MODULE_21__["AngularFireDatabaseModule"],
                _angular_fire_storage__WEBPACK_IMPORTED_MODULE_23__["AngularFireStorageModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_26__["NgxPaginationModule"],
                _ctrl_ngx_csv__WEBPACK_IMPORTED_MODULE_36__["CsvModule"],
                ngx_print__WEBPACK_IMPORTED_MODULE_48__["NgxPrintModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_63__["HttpModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_47__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyDhkHiz_LkMhVCfTsGJajw5Ag4u9d6ah2I',
                    language: 'es',
                    libraries: ['geometry', 'places']
                }),
                agm_direction__WEBPACK_IMPORTED_MODULE_61__["AgmDirectionModule"],
                ngx_alerts__WEBPACK_IMPORTED_MODULE_60__["AlertModule"].forRoot({ maxMessages: 5, timeout: 5000, position: 'right' }),
                _angular_http__WEBPACK_IMPORTED_MODULE_63__["HttpModule"],
                _material_material_module__WEBPACK_IMPORTED_MODULE_69__["MaterialModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_70__["BrowserAnimationsModule"],
            ],
            providers: [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_22__["AngularFireAuth"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_24__["AngularFirestore"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/admin/config-banner/config-banner.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/admin/config-banner/config-banner.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nbutton {\n    border-radius: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9jb25maWctYmFubmVyL2NvbmZpZy1iYW5uZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxvQkFBb0I7Q0FDdkIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FkbWluL2NvbmZpZy1iYW5uZXIvY29uZmlnLWJhbm5lci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuYnV0dG9uIHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/admin/config-banner/config-banner.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/admin/config-banner/config-banner.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mt-5 ml-5 mr-5\">\n    <ngx-alerts></ngx-alerts>\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #562B89\"><i class=\"fa fa-picture-o\"></i></span> Configuración de banners </h1>\n        <div class=\"row\">\n            <!-- <div class=\"col\">\n                <div class=\"form-group\">\n                    <input type=\"text\" name=\"filterPost\" class=\"form-control\" placeholder=\"&#xf002; Buscar...\" style=\"font-family:Arial, FontAwesome\" [(ngModel)]=\"filterPost\">\n                </div>\n            </div> -->\n            <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modal\" data-target=\"#modalBanner\"><i class=\"fa fa-plus\"></i> Nuevo banner</button>\n        </div>\n    </div>\n    <div class=\"col\">\n        <mat-form-field>\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n        </mat-form-field>\n\n        <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n            <!-- DESCRIPCION Column -->\n            <ng-container matColumnDef=\"descripcion\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Descripción </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.descripcion}} </td>\n            </ng-container>\n\n            <!-- FECHA Column -->\n            <ng-container matColumnDef=\"fecha\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Fecha </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.fecha}} </td>\n            </ng-container>\n\n            <!-- MODULO Column -->\n            <ng-container matColumnDef=\"modulo\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Módulo </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.modulo}} </td>\n            </ng-container>\n\n            <!-- URL Column -->\n            <ng-container matColumnDef=\"url\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Link URL </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.url}} </td>\n            </ng-container>\n\n            <!-- ESTATADO Column -->\n            <ng-container matColumnDef=\"ba\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.estado == 'true'\">\n                        <mat-icon matBadge=\"+\" matBadgeColor=\"accent\">aspect_ratio</mat-icon>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estado == 'false'\">\n                        <mat-icon matBadge=\"-\" matBadgeColor=\"warn\">aspect_ratio</mat-icon>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <!-- Imagen Column -->\n            <ng-container matColumnDef=\"photourl\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Imagen </th>\n                <td mat-cell *matCellDef=\"let element\"> <img style=\"height: 45px; width: auto;\" class=\"img\" src=\"{{element.photourl}}\"> </td>\n            </ng-container>\n\n            <!-- ELIMINAR Column -->\n            <ng-container matColumnDef=\"delete\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Borrar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"warn\" (click)=\"onDeleteBanner(element)\">\n                        <mat-icon>delete</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <!-- BAJALTA Column -->\n            <ng-container matColumnDef=\"estado\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Cambiar estado </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.estado == 'true'\">\n                        <button mat-mini-fab color=\"blue\" (click)=\"changeEstado(element, 'false')\">\n                            <mat-icon>toggle_off</mat-icon>\n                        </button>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estado == 'false'\">\n                        <button mat-mini-fab color=\"primary\" (click)=\"changeEstado(element, 'true')\">                        \n                            <mat-icon>toggle_on</mat-icon>\n                        </button>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        </table>\n        <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n    </div>\n</div>\n<div class=\"modal\" id=\"modalBanner\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\">Nuevo Banner</h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n            </div>\n            <div class=\"alert alert-dismissible alert-warning\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n                <h4 class=\"alert-heading\">Importante!</h4>\n                <p class=\"mb-0\">Las medidas estandar para las imágenes son 692 x 390 px.</p>\n            </div>\n            <div class=\"modal-body\">\n                <form name=\"formBanner\" #formBanner=\"ngForm\" (ngSubmit)=\"onSaveMenu(formBanner)\">\n                    <div class=\"form-group\">\n                        <label for=\"fecha\">Fecha</label>\n                        <input type=\"date\" name=\"fecha\" class=\"form-control\" [(ngModel)]=\"this.selectedBanner.fecha\">\n                    </div>\n\n                    <div class=\"form-group\">\n                        <label for=\"modulo\">Seleccione el módulo donde se mostrara el banner</label>\n                        <select class=\"form-control\" id=\"estado\" name=\"modulo\" [(ngModel)]=\"this.selectedBanner.modulo\">\n                          <option value=\"indexAppCliente\">Index app Cliente</option>\n                          <option value=\"indexWebConvenio\">Index web Convenio</option>\n                        </select>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <label for=\"descripcion\" class=\"col-form-label\">Descripción:</label>\n                        <textarea class=\"form-control\" name=\"descripcion\" [(ngModel)]=\"this.selectedBanner.descripcion\"></textarea>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <label for=\"url\" class=\"col-form-label\">Link URL:</label>\n                        <input type=\"text\" name=\"url\" class=\"form-control\" [(ngModel)]=\"this.selectedBanner.url\">\n\n                    </div>\n\n                    <div class=\"form-group\">\n                        <label for=\"estado\">Estado</label>\n                        <select class=\"form-control\" id=\"estado\" name=\"estado\" [(ngModel)]=\"this.selectedBanner.estado\">\n                          <option value=\"true\">Activar</option>\n                          <option value=\"false\">Desactivar</option>\n                        </select>\n                    </div>\n                    <div class=\"form-group\">\n                        <h5>Seleccionar imagen:</h5>\n                        <input class=\"hi\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                    </div>\n                    <div class=\"progress\">\n                        <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n                            <!-- <span class=\"progressText\" *ngIf=\"urlImage | async\">\n          Ok!!</span> -->\n                        </div>\n                    </div>\n                    <br>\n                    <input #imageUser type=\"hidden\" [value]=\"urlImage | async\">\n                    <br>\n                    <div>\n                        <!-- <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"archivos.length === 0\">Guardar</button> &nbsp; -->\n                        <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Guardar</button> &nbsp;\n                        <!-- <button type=\"hidden\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button> -->\n\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>"

/***/ }),

/***/ "./src/app/components/admin/config-banner/config-banner.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/admin/config-banner/config-banner.component.ts ***!
  \***************************************************************************/
/*! exports provided: ConfigBannerComponent, BannerInterface */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigBannerComponent", function() { return ConfigBannerComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BannerInterface", function() { return BannerInterface; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_config_banner_config_banner_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/config/banner_config/banner-config.service */ "./src/app/service/config/banner_config/banner-config.service.ts");
/* harmony import */ var src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);










// import { map } from 'rxjs/operators';
var ConfigBannerComponent = /** @class */ (function () {
    function ConfigBannerComponent(_bannerConfg, authService, db, alertService, storage) {
        this._bannerConfg = _bannerConfg;
        this.authService = authService;
        this.db = db;
        this.alertService = alertService;
        this.storage = storage;
        this.displayedColumns = ['descripcion', 'fecha', 'modulo', 'ba', 'photourl', 'url', 'estado', 'delete'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableDataSource"]();
        // tslint:disable-next-line: max-line-length
        this.selectedBanner = { nombreArchivo: null, archivo: null, photourl: null, estaSubiendo: null, progreso: null, url: null
        };
        this.archivos = [];
        this.estaSobreElemento = false;
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = null;
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    // public BannersCollection: AngularFirestoreCollection<BannerInterface>;
    // public Banners: Observable<BannerInterface[]>;
    ConfigBannerComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Sucursal
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    // this.BannersCollection = this.db.collection<any>('banners', ref => ref.where('uidSucursal', '==', this.Uidsucursal));
                    // this.Banners = this.BannersCollection.valueChanges();
                    // this.db.collection('banners', ref => ref.where('uidSucursal', '==', this.Uidsucursal)).valueChanges().subscribe( b => {
                    //   this.banners = b;
                    //   console.log('banners', b);
                    //           });
                    // console.log('usuer', this.Uidsucursal);
                    // Desglose de tablas
                    _this._bannerConfg.getBanner(_this.Uidsucursal)
                        .subscribe(function (res) { return (_this.dataSource.data = res); });
                });
            }
        });
        // this.getListBanner();
    };
    ConfigBannerComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ConfigBannerComponent.prototype.onSaveMenu = function (formMenu) {
        // Enviando datos del formulario al servicio
        if (this.selectedBanner.descripcion === '' || this.selectedBanner.fecha === '' ||
            this.selectedBanner.modulo === '' || !this.urlImage1) {
            this.alertService.warning('Algunos campos no fueron completados.');
        }
        else {
            this._bannerConfg.guardarImagen(this.urlImage1, this.selectedBanner.descripcion, this.selectedBanner.fecha, this.selectedBanner.estado, this.selectedBanner.modulo, this.Uidsucursal, this.selectedBanner.url);
        }
        formMenu.resetForm();
        this.btnClose.nativeElement.click();
    };
    ConfigBannerComponent.prototype.onUpload = function (e) {
        var _this = this;
        console.log('subir', e.target.files[0]);
        var id = Math.random().toString(36).substring(2);
        var file = e.target.files[0];
        var filePath = "banners/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["finalize"])(function () {
            return ref.getDownloadURL().subscribe(function (r) {
                _this.urlImage1 = _this.urlImage = r;
                console.log('foto', r);
            });
        }))
            .subscribe();
    };
    // getListBanner() {
    //   this.getAllBanners().subscribe( banners =>  {
    //     this.banners = banners;
    //     console.log('banners', banners);
    //   });
    // }
    // getAllBanners() {
    //   return this.Banners = this.BannersCollection.snapshotChanges()
    //   .pipe(map( changes => {
    //     return changes.map( action => {
    //       const data = action.payload.doc.data() as BannerInterface;
    //       data.uid = action.payload.doc.id;
    //       return data;
    //     });
    //   }));
    // }
    ConfigBannerComponent.prototype.onDeleteBanner = function (idBanner) {
        var _this = this;
        var idBn = idBanner.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
            title: '¿Estas seguro?',
            text: "Eliminaras el banner!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idBn );
                _this._bannerConfg.deleteMenu(idBn);
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Borrado', 'Banner Borrado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Error!', 'Hubo un error al eliminar el banner', 'error');
        });
        // const confirmacion = confirm('¿Esta seguro de eliminar este banner?');
        // if ( confirmacion ) {
        //   this._bannerConfg.deleteMenu(idBanner);
        // }
    };
    ConfigBannerComponent.prototype.changeEstado = function (idBanner, estado) {
        var _this = this;
        var idBn = idBanner.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
            title: '¿Estas seguro?',
            text: "Cambiaras el estado del banner!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, cambiar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idBn );
                _this._bannerConfg.updateEstadoBanner(idBn, estado);
                sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Estado', 'Banner cambiado de estado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Error!', 'Hubo un error al cabiar de esyatad el banner', 'error');
        });
        // const confirmacion = confirm('¿Esta seguro de cambiar el estado del Banner?');
        // if ( confirmacion ) {
        //   this._bannerConfg.updateEstadoBanner(uidBanner, estado);
        // }
    };
    ConfigBannerComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginator"])
    ], ConfigBannerComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSort"])
    ], ConfigBannerComponent.prototype, "sort", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ConfigBannerComponent.prototype, "btnClose", void 0);
    ConfigBannerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-config-banner',
            template: __webpack_require__(/*! ./config-banner.component.html */ "./src/app/components/admin/config-banner/config-banner.component.html"),
            styles: [__webpack_require__(/*! ./config-banner.component.css */ "./src/app/components/admin/config-banner/config-banner.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_config_banner_config_banner_config_service__WEBPACK_IMPORTED_MODULE_2__["BannerConfigService"],
            src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_5__["AlertService"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_7__["AngularFireStorage"]])
    ], ConfigBannerComponent);
    return ConfigBannerComponent;
}());

var BannerInterface = /** @class */ (function () {
    function BannerInterface(archivo) {
        this.archivo = archivo;
        this.nombreArchivo = archivo.name;
        this.estaSubiendo = false;
        this.progreso = 0;
    }
    return BannerInterface;
}());



/***/ }),

/***/ "./src/app/components/admin/config-servicio/config-servicio.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/admin/config-servicio/config-servicio.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".iconTi2 {\n    height: 35px;\n    width: 35px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9jb25maWctc2VydmljaW8vY29uZmlnLXNlcnZpY2lvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxhQUFhO0lBQ2IsWUFBWTtDQUNmIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9jb25maWctc2VydmljaW8vY29uZmlnLXNlcnZpY2lvLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaWNvblRpMiB7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIHdpZHRoOiAzNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/admin/config-servicio/config-servicio.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/admin/config-servicio/config-servicio.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron\">\n    <ngx-alerts></ngx-alerts>\n    <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-cogs\"></i></span> Configuración de los servicios</h1>\n    <p class=\"lead\"></p>\n    <hr class=\"my-4\">\n    <ul class=\"nav nav-tabs\">\n        <li class=\"nav-item\">\n            <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#Mpago\">Metódo de pago</a>\n        </li>\n        <li class=\"nav-item\">\n            <a class=\"nav-link\" data-toggle=\"tab\" href=\"#vehi\">Vehículos</a>\n        </li>\n        <li class=\"nav-item dropdown\">\n            <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Configuración de servicios</a>\n            <div class=\"dropdown-menu\">\n                <a class=\"dropdown-item\" data-toggle=\"tab\" href=\"#regular\">Servicios Regulares</a>\n                <a class=\"dropdown-item\" data-toggle=\"tab\" href=\"#menu\">Servicios del Menú</a>\n                <!-- <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n                <div class=\"dropdown-divider\"></div>\n                <a class=\"dropdown-item\" href=\"#\">Separated link</a> -->\n            </div>\n        </li>\n    </ul>\n    <div id=\"myTabContent\" class=\"tab-content\">\n        <div class=\"tab-pane fade show active\" id=\"Mpago\">\n            <div class=\"class row pad-10\">\n                <div class=\"col-10 mt-2\">\n                    <div class=\"card border-primary mb-3\" style=\"max-width: 40rem;\">\n                        <div class=\"card-header\">Métodos de pago</div>\n                        <div *ngFor=\"let pago of pagos\" class=\"card-body\">\n                            <form name=\"formPago\" #formPago=\"ngForm\" (ngSubmit)=\"updatePago(pago.uid)\">\n                                <div class=\"form-group\">\n                                    <div class=\"custom-control custom-checkbox\">\n                                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"customCheck1\" [(ngModel)]=\"efectivo\" name=\"Efectivo\" [checked]=\"pago.Efectivo\" data-md-icheck (change)=\"updateEfectivo($event, pago.uid)\">\n                                        <label class=\"custom-control-label\" for=\"customCheck1\"><img class=\"iconTi\" src=\"../../../../assets/imgs/efectivo.png\" title=\"Pago en efectivo\">\n                                                <span *ngIf=\"pago.Efectivo == true\" class=\"badge badge-pill badge-success\">Habilitado</span> \n                                                <span *ngIf=\"pago.Efectivo == false\" class=\"badge badge-pill badge-danger\">Deshabilitado</span>\n                                            </label>\n                                    </div>\n                                    <div class=\"custom-control custom-checkbox\">\n                                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"customCheck2\" [(ngModel)]=\"tarjeta\" name=\"Tarjeta\" [checked]=\"pago.Tarjeta\" data-md-icheck (change)=\"updateTarjeta($event, pago.uid)\">\n                                        <label class=\"custom-control-label\" for=\"customCheck2\"><img class=\"iconTi\" src=\"../../../../assets/imgs/tarjeta-de-credito.png\" title=\"Pago en efectivo\">\n                                                <span *ngIf=\"pago.Tarjeta == true\" class=\"badge badge-pill badge-success\">Habilitado</span> \n                                                <span *ngIf=\"pago.Tarjeta == false\" class=\"badge badge-pill badge-danger\">Deshabilitado</span>\n                                            </label>\n                                    </div>\n                                    <br>\n                                    <fieldset>\n                                        <!-- <button type=\"submit\" class=\"btn btn-primary\">Guardar</button> -->\n                                    </fieldset>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"tab-pane fade\" id=\"vehi\">\n            <div class=\"class row pad-10 mt-2\">\n                <div class=\"col-6\">\n                    <div class=\"card border-primary mb-3\" style=\"max-width: 40rem;\">\n                        <div class=\"card-header\">Vehículos</div>\n                        <div *ngFor=\"let transporte of transportes\" class=\"card-body\">\n                            <form name=\"formTransporte\" #formTransporte=\"ngForm\" (ngSubmit)=\"updateTransporte(transporte.uid)\">\n                                <div class=\"form-group\">\n                                    <div class=\"custom-control custom-checkbox\">\n                                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"customCheck3\" [(ngModel)]=\"transporte.moto\" name=\"moto\" [checked]=\"transporte.moto\" data-md-icheck (change)=\"updateMoto($event, transporte.uid)\">\n                                        <label class=\"custom-control-label\" for=\"customCheck3\"><img class=\"iconTi2\" src=\"../../../../assets/imgs/moto_1.png\" title=\"Motócicleta\">  \n                                                        <span *ngIf=\"transporte.moto == true\" class=\"badge badge-pill badge-success\">Habilitado</span> \n                                                        <span *ngIf=\"transporte.moto == false\" class=\"badge badge-pill badge-danger\">Deshabilitado</span>\n                                                    </label>\n                                    </div>\n                                    <div class=\"custom-control custom-checkbox\">\n                                        <input type=\"checkbox\" class=\"custom-control-input\" id=\"customCheck4\" [(ngModel)]=\"transporte.car\" name=\"car\" [checked]=\"transporte.car\" data-md-icheck (change)=\"updateCarro($event, transporte.uid)\">\n                                        <label class=\"custom-control-label\" for=\"customCheck4\"><img class=\"iconTi2\" src=\"../../../../assets/imgs/car_1.png\" title=\"Carro\">\n                                                        <span *ngIf=\"transporte.car == true\" class=\"badge badge-pill badge-success\">Habilitado</span> \n                                                        <span *ngIf=\"transporte.car == false\" class=\"badge badge-pill badge-danger\">Deshabilitado</span>\n                                                    </label>\n                                    </div>\n                                    <br>\n                                    <fieldset>\n                                        <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\n                                    </fieldset>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <!-- Configuracion de servicios regulares -->\n        <div class=\"tab-pane fade\" id=\"regular\">\n            <div class=\"col-11 mt-3 mb-3\" style=\"text-align:center\">\n                <h3>\n                    Configuración\n                    <small class=\"text-muted\">de tarifas de servicios regulares</small>\n                </h3>\n            </div>\n            <div class=\"row pad-10 mt-2\">\n                <div class=\"col-6\">\n                    <div class=\"card border-primary mb-3\" style=\"max-width: 40rem;\">\n                        <div class=\"card-header\">Costos fijos</div>\n                        <div *ngFor=\"let costo of costos\" class=\"card-body\">\n                            <form name=\"formCosto\" #formCosto=\"ngForm\" (ngSubmit)=\"updateArranque(formCosto, costo.uid)\">\n                                <div class=\"form-group\">\n                                    <label class=\"control-label\">Arranque</label>\n                                    <div class=\"form-group\">\n                                        <div class=\"input-group mb-3\">\n                                            <div class=\"input-group-prepend\">\n                                                <span class=\"input-group-text\">$</span>\n                                            </div>\n                                            <input name=\"arranque\" type=\"number\" class=\"form-control\" [(ngModel)]=\"costo.arranque\" aria-label=\"Cantidad en pesos\">\n                                            <div class=\"input-group-append\">\n                                                <span class=\"input-group-text\"> Cuota inicial</span>\n                                            </div>\n                                        </div>\n                                        <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-6\">\n                    <div *ngFor=\"let tarifa of tarifas\" class=\"card border-primary mb-3\" style=\"max-width: 40rem;\">\n                        <div class=\"card-header\">Precios del servicio</div>\n                        <form name=\"formTarifa\" #formTarifa=\"ngForm\" (ngSubmit)=\"updateTarifas(formTarifa, tarifa.uid)\">\n                            <div class=\"card-body\">\n                                <div class=\"form-group\">\n                                    <label class=\"control-label\">Tarifa x Distancia</label>\n                                    <div class=\"form-group\">\n                                        <div class=\"input-group mb-3\">\n                                            <div class=\"input-group-prepend\">\n                                                <span class=\"input-group-text\">$</span>\n                                            </div>\n                                            <input type=\"number\" name=\"distancia\" [(ngModel)]=\"tarifa.distancia\" class=\"form-control\" aria-label=\"Cantidad en pesos\">\n                                            <div class=\"input-group-append\">\n                                                <span class=\"input-group-text\"> x Km.</span>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <label class=\"control-label\">Tarifa x Tiempo</label>\n                                    <div class=\"form-group\">\n                                        <div class=\"input-group mb-3\">\n                                            <div class=\"input-group-prepend\">\n                                                <span class=\"input-group-text\">$</span>\n                                            </div>\n                                            <input type=\"number\" name=\"tiempo\" [(ngModel)]=\"tarifa.tiempo\" class=\"form-control\" aria-label=\"Cantidad en pesos\">\n                                            <div class=\"input-group-append\">\n                                                <span class=\"input-group-text\"> x minuto</span>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <fieldset>\n                                        <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\n                                    </fieldset>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"tab-pane fade\" id=\"menu\">\n            <div class=\"col-11 mt-3 mb-3\" style=\"text-align:center\">\n                <h3>\n                    Configuración\n                    <small class=\"text-muted\">de tarifas de servicios del Menú de Convenios</small>\n                </h3>\n            </div>\n            <div class=\"row pad-10 mt-2\">\n                <div class=\"col-6\">\n                    <div class=\"card border-primary mb-3\" style=\"max-width: 40rem;\">\n                        <div class=\"card-header\">Costos fijos</div>\n                        <div *ngFor=\"let costo of costos_menu\" class=\"card-body\">\n                            <form name=\"formCostoMenu\" #formCostoMenu=\"ngForm\" (ngSubmit)=\"updateArranqueMenu(formCostoMenu, costo.uid)\">\n                                <div class=\"form-group\">\n                                    <label class=\"control-label\">Arranque</label>\n                                    <div class=\"form-group\">\n                                        <div class=\"input-group mb-3\">\n                                            <div class=\"input-group-prepend\">\n                                                <span class=\"input-group-text\">$</span>\n                                            </div>\n                                            <input name=\"arranque\" type=\"number\" class=\"form-control\" [(ngModel)]=\"costo.arranque\" aria-label=\"Cantidad en pesos\">\n                                            <div class=\"input-group-append\">\n                                                <span class=\"input-group-text\"> Cuota inicial</span>\n                                            </div>\n                                        </div>\n                                        <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-6\">\n                    <div *ngFor=\"let tarifa of tarifas_menu\" class=\"card border-primary mb-3\" style=\"max-width: 40rem;\">\n                        <div class=\"card-header\">Precios del servicio</div>\n                        <form name=\"formTarifaMenu\" #formTarifaMenu=\"ngForm\" (ngSubmit)=\"updateTarifasMenu(formTarifaMenu, tarifa.uid)\">\n                            <div class=\"card-body\">\n                                <div class=\"form-group\">\n                                    <label class=\"control-label\">Tarifa x Distancia</label>\n                                    <div class=\"form-group\">\n                                        <div class=\"input-group mb-3\">\n                                            <div class=\"input-group-prepend\">\n                                                <span class=\"input-group-text\">$</span>\n                                            </div>\n                                            <input type=\"number\" name=\"distancia\" [(ngModel)]=\"tarifa.distancia\" class=\"form-control\" aria-label=\"Cantidad en pesos\">\n                                            <div class=\"input-group-append\">\n                                                <span class=\"input-group-text\"> x Km.</span>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <label class=\"control-label\">Tarifa x Tiempo</label>\n                                    <div class=\"form-group\">\n                                        <div class=\"input-group mb-3\">\n                                            <div class=\"input-group-prepend\">\n                                                <span class=\"input-group-text\">$</span>\n                                            </div>\n                                            <input type=\"number\" name=\"tiempo\" [(ngModel)]=\"tarifa.tiempo\" class=\"form-control\" aria-label=\"Cantidad en pesos\">\n                                            <div class=\"input-group-append\">\n                                                <span class=\"input-group-text\"> x minuto</span>\n                                            </div>\n                                        </div>\n                                    </div>\n                                    <fieldset>\n                                        <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\n                                    </fieldset>\n                                </div>\n                            </div>\n                        </form>\n                    </div>\n                </div>\n                <div class=\"col-6\">\n                    <div class=\"card border-primary mb-3\" style=\"max-width: 40rem;\">\n                        <div class=\"card-header\">Consumo mínimo de los menús</div>\n                        <div *ngFor=\"let costo of costos_menu_min\" class=\"card-body\">\n                            <form name=\"formCostoMenuMin\" #formCostoMenuMin=\"ngForm\" (ngSubmit)=\"updateArranqueMenuMin(formCostoMenuMin, costo.uid)\">\n                                <div class=\"form-group\">\n                                    <label class=\"control-label\">Tarifa</label>\n                                    <div class=\"form-group\">\n                                        <div class=\"input-group mb-3\">\n                                            <div class=\"input-group-prepend\">\n                                                <span class=\"input-group-text\">$</span>\n                                            </div>\n                                            <input name=\"costo\" type=\"number\" class=\"form-control\" [(ngModel)]=\"costo.costo\" aria-label=\"Cantidad en pesos\">\n                                            <div class=\"input-group-append\">\n                                                <span class=\"input-group-text\"> Mínima</span>\n                                            </div>\n                                        </div>\n                                        <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/admin/config-servicio/config-servicio.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admin/config-servicio/config-servicio.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ConfigServicioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigServicioComponent", function() { return ConfigServicioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_config_servicios_config_servicios_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/config/servicios_config/servicios-config.service */ "./src/app/service/config/servicios_config/servicios-config.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/auth.service */ "./src/app/service/auth.service.ts");





var ConfigServicioComponent = /** @class */ (function () {
    function ConfigServicioComponent(_servicio_config, db, authService) {
        this._servicio_config = _servicio_config;
        this.db = db;
        this.authService = authService;
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = null;
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    ConfigServicioComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Sucursal
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    _this.getCosto(_this.Uidsucursal);
                    _this.getCostoMenu(_this.Uidsucursal);
                    _this.getTarifaMenu(_this.Uidsucursal);
                    _this.getTarifaMenuMin(_this.Uidsucursal);
                    _this.getTarifa(_this.Uidsucursal);
                    _this.getPago(_this.Uidsucursal);
                    _this.getTransporte(_this.Uidsucursal);
                    console.log('usuer', _this.Uidsucursal);
                });
            }
        });
    };
    // Servicios regulares
    ConfigServicioComponent.prototype.getCosto = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getCostoFijo(uidSucursal).subscribe(function (costo) {
            _this.costos = costo;
            console.log('costo fijo', costo);
        });
    };
    // Servicios menu
    ConfigServicioComponent.prototype.getCostoMenu = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getCostoFijoMenu(uidSucursal).subscribe(function (costo) {
            _this.costos_menu = costo;
            console.log('costo fijo menu', costo);
        });
    };
    // Servicios regulares
    ConfigServicioComponent.prototype.updateArranque = function (formCosto, uid) {
        var confirma = confirm('Se modificará el costo de arranque, ¿Está seguro de continuar?');
        if (confirma) {
            this._servicio_config.updateArranqueService(formCosto.value, uid);
        }
    };
    // Servicios del menu
    ConfigServicioComponent.prototype.updateArranqueMenu = function (formCostoMenu, uid) {
        var confirma = confirm('Se modificará el costo de arranque, ¿Está seguro de continuar?');
        if (confirma) {
            this._servicio_config.updateArranqueServiceMenu(formCostoMenu.value, uid);
        }
    };
    ConfigServicioComponent.prototype.getTarifa = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getTarifaService(uidSucursal).subscribe(function (tarifa) {
            _this.tarifas = tarifa;
            console.log('tarifa ', tarifa);
        });
    };
    ConfigServicioComponent.prototype.updateTarifas = function (formTarifa, uid) {
        var confirma = confirm('Se modificará las tarifas de distancia y tiempo, ¿Está seguro de continuar?');
        if (confirma) {
            this._servicio_config.updateTarifaService(formTarifa.value, uid);
            console.log('aqui estoy perro', formTarifa.value);
        }
    };
    // Menu
    ConfigServicioComponent.prototype.getTarifaMenu = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getTarifaServiceMenu(uidSucursal).subscribe(function (tarifa) {
            _this.tarifas_menu = tarifa;
            console.log('tarifa ', tarifa);
        });
    };
    ConfigServicioComponent.prototype.updateTarifasMenu = function (formTarifaMenu, uid) {
        var confirma = confirm('Se modificará las tarifas de distancia y tiempo, ¿Está seguro de continuar?');
        if (confirma) {
            this._servicio_config.updateTarifaServiceMenu(formTarifaMenu.value, uid);
        }
    };
    // +++++ Inicio de metodo de pago +++++
    ConfigServicioComponent.prototype.getPago = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getPagoService(uidSucursal).subscribe(function (pago) {
            _this.pagos = pago;
            console.log('pago ', pago);
            _this.pagos.forEach(function (data) {
                _this.efectivo = data.Efectivo;
                console.log(_this.efectivo);
                _this.tarjeta = data.Tarjeta;
                console.log(_this.tarjeta);
            });
        });
    };
    ConfigServicioComponent.prototype.updateEfectivo = function (e, uid) {
        this.efectivo = e.target.checked;
        console.log("check", e.target.checked);
        console.log('valueE', this.efectivo);
        this.updatePago(this.efectivo, this.tarjeta, uid);
    };
    ConfigServicioComponent.prototype.updateTarjeta = function (e, uid) {
        this.tarjeta = e.target.checked;
        console.log('check', e);
        console.log('valueT', this.tarjeta);
        this.updatePago(this.efectivo, this.tarjeta, uid);
    };
    ConfigServicioComponent.prototype.updatePago = function (efectivo, tarjeta, uid) {
        var confirma = confirm('Se modificará el método pago, ¿Está seguro de continuar?');
        if (confirma) {
            this._servicio_config.updatePagoService(efectivo, tarjeta, uid);
        }
    };
    // +++++ Fin de metodo de pago +++++
    ConfigServicioComponent.prototype.getTransporte = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getTransporteService(uidSucursal).subscribe(function (transporte) {
            _this.transportes = transporte;
            console.log('transporte ', transporte);
        });
    };
    ConfigServicioComponent.prototype.updateMoto = function (e, uid) {
        this.moto = e.target.checked;
        console.log('check', e);
        console.log('valueE', this.moto);
        this.updateTransporte(this.moto, this.car, uid);
    };
    ConfigServicioComponent.prototype.updateCarro = function (e, uid) {
        this.car = e.target.checked;
        console.log('check', e);
        console.log('valueT', this.car);
        this.updateTransporte(this.car, this.car, uid);
    };
    ConfigServicioComponent.prototype.updateTransporte = function (moto, car, uid) {
        var confirma = confirm('Se modificará los vehículos para el transporte, ¿Está seguro de continuar?');
        if (confirma) {
            this._servicio_config.updateTransporteService(moto, car, uid);
        }
    };
    // MenuMin
    ConfigServicioComponent.prototype.getTarifaMenuMin = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getTarifaServiceMenuMin(uidSucursal).subscribe(function (tarifa) {
            _this.costos_menu_min = tarifa;
            console.log('tarifa min menu ', tarifa);
        });
    };
    // Servicios del menumin
    ConfigServicioComponent.prototype.updateArranqueMenuMin = function (formCostoMenuMin, uid) {
        var confirma = confirm('Se modificará el costo de arranque, ¿Está seguro de continuar?');
        if (confirma) {
            this._servicio_config.updateArranqueServiceMenuMin(formCostoMenuMin.value, uid);
        }
    };
    ConfigServicioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-config-servicio',
            template: __webpack_require__(/*! ./config-servicio.component.html */ "./src/app/components/admin/config-servicio/config-servicio.component.html"),
            styles: [__webpack_require__(/*! ./config-servicio.component.css */ "./src/app/components/admin/config-servicio/config-servicio.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_service_config_servicios_config_servicios_config_service__WEBPACK_IMPORTED_MODULE_2__["ServiciosConfigService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ConfigServicioComponent);
    return ConfigServicioComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p.card-text {\n    text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9kZWF0YWlscy1zdWN1cnNhbC9kZWF0YWlscy1zdWN1cnNhbC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksb0JBQW9CO0NBQ3ZCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9kZWF0YWlscy1zdWN1cnNhbC9kZWF0YWlscy1zdWN1cnNhbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsicC5jYXJkLXRleHQge1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"sucursal\" class=\"mt-5\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-xs-12 col-sm-6 col-md-4 mx-auto\">\n                <div class=\"card_book\">\n                    <div class=\"card\">\n                        <div class=\"card-body text-center\">\n                            <p><img src=\"{{sucursal.img}}\" alt=\" {{sucursal.estado}} \" class=\"img-fluid\"></p>\n                            <h4 class=\"card-title\"> {{sucursal.estado}} </h4>\n                            <p class=\"card-text\"> {{sucursal.descripcion}} </p>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.ts ***!
  \***********************************************************************************/
/*! exports provided: DeatailsSucursalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeatailsSucursalComponent", function() { return DeatailsSucursalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_data_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/data-api.service */ "./src/app/service/data-api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var DeatailsSucursalComponent = /** @class */ (function () {
    function DeatailsSucursalComponent(dataApi, route) {
        this.dataApi = dataApi;
        this.route = route;
        this.sucursal = {};
    }
    DeatailsSucursalComponent.prototype.ngOnInit = function () {
        var idSucursal = this.route.snapshot.params['id'];
        this.getDetails(idSucursal);
    };
    DeatailsSucursalComponent.prototype.getDetails = function (idSucursal) {
        var _this = this;
        this.dataApi.getOneSucursal(idSucursal).subscribe(function (sucursal) {
            _this.sucursal = sucursal;
        });
    };
    DeatailsSucursalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-deatails-sucursal',
            template: __webpack_require__(/*! ./deatails-sucursal.component.html */ "./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.html"),
            styles: [__webpack_require__(/*! ./deatails-sucursal.component.css */ "./src/app/components/admin/deatails-sucursal/deatails-sucursal.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_data_api_service__WEBPACK_IMPORTED_MODULE_2__["DataApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], DeatailsSucursalComponent);
    return DeatailsSucursalComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/detalle-pedido/detalle-pedido.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/components/admin/detalle-pedido/detalle-pedido.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-grid-tile {\n    background: lightblue;\n}\n\n.example-card {\n    border: solid 4px black;\n    display: inline-block;\n    /* max-width: 680px; */\n    margin-left: -1em;\n    margin-right: -1em;\n    text-align: center;\n}\n\n.esq1 {\n    text-align: right;\n    margin-right: 1.5em;\n    margin-top: -2em;\n}\n\n.log {\n    height: 90px;\n    width: 90px;\n}\n\n.log1 {\n    height: 250px;\n    width: 250px;\n}\n\n.esq {\n    text-align: right;\n    margin-top: -2em;\n}\n\n/* span {\n    height: auto;\n    width: auto;\n    text-align: center;\n} */\n\n.prd {\n    margin-left: -1.9em;\n}\n\n.prd2 {\n    margin-left: -1.9em;\n}\n\n.esq3 {\n    text-align: right;\n    width: 700px;\n    margin-left: 52em;\n    /* margin-top: -3em;  */\n}\n\n.esq4 {\n    text-align: right;\n    /* width: 700px;\n    margin-left: 52em; */\n    /* margin-top: -3em;  */\n}\n\nagm-map {\n    height: 400px;\n}\n\np {\n    font-family: Verdana, Geneva, Tahoma, sans-serif;\n    font-style: unset;\n}\n\nb {\n    font-size: 13px;\n}\n\nh6 {\n    font-family: monospace;\n}\n\n.ticket {\n    height: 250px;\n    width: 150px;\n    border-radius: 1px;\n    border: 5px solid rgb(255, 165, 2);\n}\n\n#map {\n    width: 100%;\n    height: 400px;\n    background-color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9kZXRhbGxlLXBlZGlkby9kZXRhbGxlLXBlZGlkby5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksc0JBQXNCO0NBQ3pCOztBQUVEO0lBQ0ksd0JBQXdCO0lBQ3hCLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLGlCQUFpQjtDQUNwQjs7QUFFRDtJQUNJLGFBQWE7SUFDYixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxjQUFjO0lBQ2QsYUFBYTtDQUNoQjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQixpQkFBaUI7Q0FDcEI7O0FBR0Q7Ozs7SUFJSTs7QUFFSjtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLHdCQUF3QjtDQUMzQjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQjt5QkFDcUI7SUFDckIsd0JBQXdCO0NBQzNCOztBQUVEO0lBQ0ksY0FBYztDQUNqQjs7QUFFRDtJQUNJLGlEQUFpRDtJQUNqRCxrQkFBa0I7Q0FDckI7O0FBRUQ7SUFDSSxnQkFBZ0I7Q0FDbkI7O0FBRUQ7SUFDSSx1QkFBdUI7Q0FDMUI7O0FBRUQ7SUFDSSxjQUFjO0lBQ2QsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixtQ0FBbUM7Q0FDdEM7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osY0FBYztJQUNkLHdCQUF3QjtDQUMzQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vZGV0YWxsZS1wZWRpZG8vZGV0YWxsZS1wZWRpZG8uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1ncmlkLXRpbGUge1xuICAgIGJhY2tncm91bmQ6IGxpZ2h0Ymx1ZTtcbn1cblxuLmV4YW1wbGUtY2FyZCB7XG4gICAgYm9yZGVyOiBzb2xpZCA0cHggYmxhY2s7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIC8qIG1heC13aWR0aDogNjgwcHg7ICovXG4gICAgbWFyZ2luLWxlZnQ6IC0xZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAtMWVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmVzcTEge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIG1hcmdpbi1yaWdodDogMS41ZW07XG4gICAgbWFyZ2luLXRvcDogLTJlbTtcbn1cblxuLmxvZyB7XG4gICAgaGVpZ2h0OiA5MHB4O1xuICAgIHdpZHRoOiA5MHB4O1xufVxuXG4ubG9nMSB7XG4gICAgaGVpZ2h0OiAyNTBweDtcbiAgICB3aWR0aDogMjUwcHg7XG59XG5cbi5lc3Ege1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIG1hcmdpbi10b3A6IC0yZW07XG59XG5cblxuLyogc3BhbiB7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn0gKi9cblxuLnByZCB7XG4gICAgbWFyZ2luLWxlZnQ6IC0xLjllbTtcbn1cblxuLnByZDIge1xuICAgIG1hcmdpbi1sZWZ0OiAtMS45ZW07XG59XG5cbi5lc3EzIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICB3aWR0aDogNzAwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDUyZW07XG4gICAgLyogbWFyZ2luLXRvcDogLTNlbTsgICovXG59XG5cbi5lc3E0IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAvKiB3aWR0aDogNzAwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDUyZW07ICovXG4gICAgLyogbWFyZ2luLXRvcDogLTNlbTsgICovXG59XG5cbmFnbS1tYXAge1xuICAgIGhlaWdodDogNDAwcHg7XG59XG5cbnAge1xuICAgIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBHZW5ldmEsIFRhaG9tYSwgc2Fucy1zZXJpZjtcbiAgICBmb250LXN0eWxlOiB1bnNldDtcbn1cblxuYiB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufVxuXG5oNiB7XG4gICAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZTtcbn1cblxuLnRpY2tldCB7XG4gICAgaGVpZ2h0OiAyNTBweDtcbiAgICB3aWR0aDogMTUwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMXB4O1xuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigyNTUsIDE2NSwgMik7XG59XG5cbiNtYXAge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNDAwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/admin/detalle-pedido/detalle-pedido.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admin/detalle-pedido/detalle-pedido.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"m-3\">\n    <mat-card>\n        <!-- TITULO -->\n        <mat-card-title class=\"text-muted\">Pedido {{ servicio.clave }}</mat-card-title>\n        <mat-divider></mat-divider>\n        <mat-card-content>\n            <table class=\"table table-bordered\">\n                <thead>\n                    <tr>\n\n                        <!-- Estado del servicio -->\n                        <th scope=\"col\">\n                            <div style=\"position:relative;margin: -393px 0 -250px 10px;\">\n                                <mat-card>\n                                    <mat-card-subtitle style=\"text-align: left\">ESTADO</mat-card-subtitle>\n                                    <div class=\"esq mr-1 mb-1\">\n                                        <!-- cambio de estatus -->\n                                        <ul class=\"nav nav-pills\">\n                                            <li class=\"nav-item dropdown\">\n                                                <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Cambiar estatus del servicio</a>\n                                                <div class=\"dropdown-menu\" x-placement=\"bottom-start\" style=\"position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);\">\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Notificando')\">Notificando</a>\n                                                    <!-- <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Aceptado')\">Aceptado</a> -->\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'BuscandoBme')\">Buscar Bme</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Yendo')\">Dirigiendose al establecimiento</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Comprando')\">Comprando</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Comprado')\">Comprado / llevando pedido al cliente</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Gestando')\">Preparando</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Enviando')\">Enviando</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'EnPuerta')\">En Puerta</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Finalizado')\">Finalizando servicio</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Pagado')\">Pagado</a>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Terminado')\">Terminado</a>\n                                                    <div class=\"dropdown-divider\"></div>\n                                                    <a class=\"dropdown-item\" (click)=\"changeStatus(servicio.uid, 'Cancelado')\">Cancelado</a>\n                                                </div>\n                                            </li>\n                                        </ul>\n                                        <!-- termina cambio de estatus -->\n                                    </div>\n                                    <!-- ACEPTADO -->\n                                    <div *ngIf=\"servicio.estatus == 'Notificando'\" class=\"progress-bar badge-pill badge-success\" role=\"progressbar\" style=\"width: 12.5%\" aria-valuenow=\"15\" aria-valuemin=\"0\" aria-valuemax=\"100\">Notificando(12%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Aceptado'\" class=\"progress-bar badge-pill bg-success\" role=\"progressbar\" style=\"width: 25%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Aceptado(25%)</div>\n\n                                    <div *ngIf=\"servicio.estatus == 'BuscandoBme'\" class=\"progress-bar badge-pill bg-success\" role=\"progressbar\" style=\"width: 25%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Buscando Repartidor(25%)</div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Yendo'\" class=\"progress-bar badge-pill bg-info\" role=\"progressbar\" style=\"width: 37.5%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Dirigiendose al establecimiento(37%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Comprando'\" class=\"progress-bar badge-pill bg-primary\" role=\"progressbar\" style=\"width: 50%\" aria-valuenow=\"15\" aria-valuemin=\"0\" aria-valuemax=\"100\">Comprando(50%) </div>\n\n                                    <!-- <div *ngIf=\"servicio.estatus == 'LLevandolo'\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 62.5%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Comprado(62%) </div> -->\n\n                                    <div *ngIf=\"servicio.estatus == 'Gestando'\" class=\"progress-bar badge-pill bg-info\" role=\"progressbar\" style=\"width: 62.5%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Preparando(62%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Comprado'\" class=\"progress-bar badge-pill bg-primary\" role=\"progressbar\" style=\"width: 75%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Llevando el pedido al cliente(75%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Enviando'\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 87.5%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Dirigiendose al domicilio(87%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'EnPuerta'\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 87.5%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Esta en puerta(87%) </div>\n                                    <div *ngIf=\"servicio.estatus == 'Pago'\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 90.5%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Pagando servicio(90%) </div>\n\n\n                                    <div *ngIf=\"servicio.estatus == 'Pagado'\" class=\"progress-bar badge-pill bg-success\" role=\"progressbar\" style=\"width: 100%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Servicio Terminado(100%)\n                                    </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Terminado'\" class=\"progress-bar badge-pill bg-success\" role=\"progressbar\" style=\"width: 100%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Servicio Terminado(100%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Cancelado'\" class=\"progress-bar badge-pill bg-danger\" role=\"progressbar\" style=\"width: 100%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Cancelado</div>\n                                    <!-- Fin de barra de estado -->\n                                </mat-card>\n                            </div>\n                        </th>\n                        <!-- RESUMEN -->\n                        <th scope=\"col\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">RESUMEN</mat-card-subtitle>\n\n                                <table class=\"table table-bordered table-hover \">\n                                    <thead>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Total</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">{{ totalNet_totalEst | currency }} </th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Pago</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">{{servicio.metodo_pago}} </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Nota de pago</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container *ngIf=\"servicio.abierto == null\">\n                                                    Servicio abierto desde aplicación.\n                                                </ng-container>\n                                                <ng-container *ngIf=\"servicio.abierto != null\">\n                                                    Servicio abierto por convenio desde plataforma.\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Fecha</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">{{ servicio.fecha | date: 'dd-MM-yyyy HH:mm:ss' }} </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Tiempo</td>\n                                            <th style=\"text-align:left\">{{ servicio.tiempoServicio }} mins.</th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Distancia</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">{{ servicio.km }} Kms. </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Tipo de pedido</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container>\n                                                    {{ servicio.tipo == 1 ? 'Pide lo que quieras': 'Convenio' }}\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Cliente</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container *ngFor=\"let cliente of clientes\">\n                                                    {{ cliente.username }} {{ cliente.lastname }}\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                        <tr *ngIf=\"servicio.uidRestaurante\">\n                                            <td style=\"text-align:left\" colspan=\"2\">Convenio</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container *ngFor=\"let convenio of convenios\">\n                                                    {{ convenio.username }}\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Repartidor</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container *ngFor=\"let repartidor of repartidores\">\n                                                    {{ repartidor.username }} {{ repartidor.lastname }}\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr>\n                        <!-- TICKETS -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">TICKETS</mat-card-subtitle>\n                                <mat-card>\n                                    <ng-container *ngFor=\"let ped of pedido\">\n                                        <a data-toggle=\"modal\" data-target=\"#modalTicket2\" href=\"\"> <img class=\"ticket\" [src]=\"ped.ticket\" alt=\"\"> </a>\n\n                                        <mat-card-title>{{ ped.total | currency }} </mat-card-title>\n                                        <mat-card-subtitle>\n                                            <a style=\"font-size: 10px;\" href=\"{{'https://maps.google.com/?q='+ ped.pedidoGeo._lat +','+ ped.pedidoGeo._long }}\" target=\"_blank\"> <span class=\"\"><b>{{ ped.direccionLocal }} </b></span></a>\n                                        </mat-card-subtitle>\n                                        <mat-card-footer>{{ ped.nombreLocal }} </mat-card-footer>\n                                    </ng-container>\n                                </mat-card>\n                            </mat-card>\n                        </th>\n                        <!-- MAPA -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <ng-container *ngIf=\"servicio.estatus != 'Terminado' && servicio.estatus != 'Pagado'\">\n                                    <!-- <ng-container *ngIf=\"servicio.estatus != 'Terminado'\"> -->\n                                    <div style=\"text-align: center;\">\n                                        <h4>Aún no se ha generado una ruta.</h4>\n                                        <img class=\"log1\" src=\"../../../../assets/imgs/Espera.gif\" alt=\"BringMe Docs.\">\n                                    </div>\n                                </ng-container>\n                                <ng-container *ngIf=\"servicio.estatus == 'Terminado' || servicio.estatus == 'Pagado'\">\n                                    <div #map id=\"map\"></div>\n                                </ng-container>\n                            </mat-card>\n                        </th>\n                    </tr>\n                    <!-- DETALLE DEL PEDIDO -->\n                    <tr>\n                        <th colspan=\"2\" scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">DETALLE DEL PEDIDO</mat-card-subtitle>\n                                <table class=\"table table-hover \">\n                                    <thead class=\"thead-light\">\n                                        <tr>\n                                            <th scope=\"col\">Cantidad</th>\n                                            <th scope=\"col\">Producto</th>\n                                            <th scope=\"col\">Nota</th>\n                                            <!-- <th scope=\"col\">Comprar en</th>\n                                            <th scope=\"col\">Local</th> -->\n\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr *ngFor=\"let producto of productos\">\n                                            <!-- <ng-container *ngFor=\"let ped of pedido\"> -->\n                                            <th scope=\"row\">\n                                                {{ producto.cantidad }}\n                                            </th>\n                                            <th>\n                                                {{producto.producto }}\n                                                <ng-container *ngIf=\"servicio.uidRestaurante\">\n                                                    {{ producto.descripcion }}\n                                                </ng-container>\n\n                                            </th>\n                                            <th>\n                                                {{ producto.nota }}\n                                                <ng-container *ngIf=\"!servicio.uidRestaurante\">\n                                                    {{ producto.descripcion }}\n                                                </ng-container>\n                                            </th>\n                                            <!-- <td>{{ ped.direccionLocal }} </td>\n                                                <td>{{ ped.nombreLocal }} </td> -->\n                                            <!-- </ng-container> -->\n                                        </tr>\n\n                                    </tbody>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                    <tr>\n                        <!-- COSTO DE SERVICIO -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">COSTO DE SERVICIO</mat-card-subtitle>\n                                <table class=\"table table-hover \">\n                                    <tr>\n                                        <td scope=\"col\">Corte Repartidor</td>\n                                        <td scope=\"col\">{{servicio.corteRep | currency }} </td>\n                                    </tr>\n                                    <tr>\n                                        <th scope=\"row\">Corte BMe</th>\n                                        <td>{{servicio.corteBme | currency }} </td>\n                                    </tr>\n                                    <tr>\n                                        <th scope=\"row\">Total establecimientos adicionales</th>\n                                        <td>{{totalEstablecimientos | currency}} </td>\n                                    </tr>\n                                    <tr>\n                                        <th class=\"table-info\" scope=\"row\">Total servicio</th>\n                                        <td class=\"table-info\">{{servicio.totalServicio | currency }} </td>\n                                    </tr>\n                                    <tr>\n                                        <th class=\"table-success\" colspan=\"2\">\n                                            <mat-card-subtitle>\n                                                Total pagado\n                                            </mat-card-subtitle>\n                                            <mat-card-title>\n                                                {{totalSer_totalEst | currency }}\n                                            </mat-card-title>\n                                        </th>\n                                    </tr>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                        <!-- TOTALES -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">TOTALES</mat-card-subtitle>\n                                <table class=\"table table-hover \">\n                                    <tr>\n                                        <td scope=\"col\">Productos</td>\n                                        <td scope=\"col\">{{servicio.totalProductos | currency }} </td>\n                                    </tr>\n                                    <tr>\n                                        <th scope=\"row\">Servicio</th>\n                                        <td>{{servicio.totalServicio | currency }} </td>\n                                    </tr>\n                                    <tr>\n                                        <th scope=\"row\">Total establecimientos adicionales</th>\n                                        <td>{{servicio.numPedidos * 15 | currency}} </td>\n                                    </tr>\n                                    <tr>\n                                        <th class=\"table-info\" scope=\"row\">Total</th>\n                                        <td class=\"table-info\">{{ totalNet_totalEst | currency }} </td>\n                                    </tr>\n                                    <tr>\n                                        <th class=\"table-success\" colspan=\"2\">\n                                            <mat-card-subtitle>\n                                                Total pagado\n                                            </mat-card-subtitle>\n                                            <mat-card-title>\n                                                {{ totalNet_totalEst | currency }}\n                                            </mat-card-title>\n                                            {{ servicio.metodo_pago }}\n                                        </th>\n                                    </tr>\n                                </table>\n                            </mat-card>\n\n                        </th>\n\n                    </tr>\n                    <tr>\n                        <!-- CLIENTES -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">CLIENTE</mat-card-subtitle>\n                                <table class=\"table table-hover \">\n                                    <ng-container *ngFor=\"let cliente of clientes\">\n                                        <tr>\n                                            <th scope=\"col\">Nombre</th>\n                                            <th scope=\"col\">{{ cliente.username }} {{cliente.lastname}} </th>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Correo</th>\n                                            <td>\n                                                {{cliente.email}}\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Celular</th>\n                                            <td>\n                                                {{cliente.phone}}\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Dirección de pedido</th>\n                                            <td>\n                                                {{ servicio.entregaDir }}\n                                            </td>\n                                        </tr>\n                                    </ng-container>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                        <!-- EVALUACION -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">EVALUACIÓN</mat-card-subtitle>\n\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                    <tr>\n                        <!-- REPARTIDOR -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">REPARTIDOR</mat-card-subtitle>\n                                <table class=\"table table-hover\">\n                                    <ng-container *ngFor=\"let repartidor of repartidores\">\n                                        <tr>\n                                            <th scope=\"col\">Nombre</th>\n                                            <th scope=\"col\">{{ repartidor.username }} {{repartidor.lastname}} </th>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Correo</th>\n                                            <td>\n                                                {{repartidor.email}}\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Celular</th>\n                                            <td>\n                                                {{repartidor.phone}}\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Unidad</th>\n                                            <td>\n                                                {{ repartidor.marca }} - {{repartidor.placa}}\n                                            </td>\n                                        </tr>\n                                    </ng-container>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                        <!-- EVALUACION -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">EVALUACIÓN</mat-card-subtitle>\n                                <table class=\"table table-hover\">\n                                    <tr>\n                                        <th scope=\"col\">Cal. Cliente</th>\n                                        <th scope=\"col\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 1\" src=\"../../../../assets/imgs/1.png\" alt=\"Calificación del servicio\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 2\" src=\"../../../../assets/imgs/2.png\" alt=\"Calificación del servicio\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 3\" src=\"../../../../assets/imgs/3.png\" alt=\"Calificación del servicio\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 4\" src=\"../../../../assets/imgs/4.png\" alt=\"Calificación del servicio\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 5\" src=\"../../../../assets/imgs/5.png\" alt=\"Calificación del servicio\">\n                                        </th>\n                                    </tr>\n                                    <tr>\n                                        <th colspan=\"2\" scope=\"row\">Comentarios del cliente</th>\n                                    </tr>\n                                    <tr>\n                                        <th colspan=\"2\" scope=\"row\">\n                                            {{ servicio.comentarioRepartidor }}\n                                        </th>\n                                    </tr>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                    <!-- NOTAS Y ESTADO -->\n                    <tr>\n                        <th colspan=\"2\" scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">NOTAS Y ESTADO</mat-card-subtitle>\n                                <table class=\"table table-hover\">\n                                    <thead>\n                                        <tr>\n                                            <th scope=\"col\">Nota de plataforma</th>\n                                            <th scope=\"col\">\n                                                {{servicio.abierto == null ? 'Servicio abierto desde aplicación.': 'Servicio abierto por convenio desde plataforma.' }}\n                                            </th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <th scope=\"row\">Uid Servicio</th>\n                                            <td>{{ servicio.uid }}</td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Estatus</th>\n                                            <td>{{ servicio.estatus }} </td>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                </tbody>\n            </table>\n        </mat-card-content>\n    </mat-card>\n</section>\n<!-- Modal de TICKET  -->\n<div class=\"modal\" id=\"modalTicket2\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\">Ticket del servicio con clave: <b>{{ servicio.clave }}</b> </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                        <span aria-hidden=\"true\">&times;</span>\n                      </button>\n            </div>\n            <div class=\"modal-body mt-5\">\n                <ng-container *ngFor=\"let ped of pedido\">\n                    <img style=\"transform: rotate(90deg); height: auto; width: 100%;\" class=\"\" [src]=\"ped.ticket\" alt=\"\">\n                </ng-container>\n            </div>\n            <div class=\"modal-footer mt-5\">\n                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/admin/detalle-pedido/detalle-pedido.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/admin/detalle-pedido/detalle-pedido.component.ts ***!
  \*****************************************************************************/
/*! exports provided: DetallePedidoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallePedidoComponent", function() { return DetallePedidoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/pedidos/pedidos.service */ "./src/app/service/pedidos/pedidos.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);







// declare var google: any;
var DetallePedidoComponent = /** @class */ (function () {
    function DetallePedidoComponent(_pedidoSer, route, afs, http) {
        this._pedidoSer = _pedidoSer;
        this.route = route;
        this.afs = afs;
        this.http = http;
        this.servicio = {};
        this.title = 'Track del servicio';
        this.lat = 24.799448;
        this.lng = 120.979021;
        this.driving = 'DRIVING';
    }
    DetallePedidoComponent.prototype.ngOnInit = function () {
        // this.getDirection(null, null, null);
        var idServicio = this.route.snapshot.params['uid'];
        this.getDetails(idServicio);
        this.getPedido(idServicio);
        this.getProducto(idServicio);
    };
    // // tslint:disable-next-line:use-life-cycle-interface
    // _ngAfterViewInit() {
    //   console.log("afterinit");
    //   setTimeout(() => {
    //     const idServicio = this.route.snapshot.params["uid"];
    //     this._pedidoSer.getOneServicio(idServicio).subscribe(servicio => {
    //       this._pedidoSer.getAllServiciosP(idServicio).subscribe(serviciosP => {
    //         console.log("ServiciosP", serviciosP);
    //         this.servicio = servicio;
    //         console.log("servicio Cordenadas", this.servicio);
    // // Coordendas donde se va entregar el pedido.
    // const _lat = this.servicio.entregaGeo._lat;
    // const _lng = this.servicio.entregaGeo._long;
    // console.log(this.destination);
    // // Coordendas donde se inicia el servicio
    // const lat = this.servicio.partidaGeo._lat;
    // const lng = this.servicio.partidaGeo._long;
    //         console.log(this.origin);
    //         const mapProperties = {
    //           center: new google.maps.LatLng(35.2271, -80.8431),
    //           zoom: 15,
    //           mapTypeId: google.maps.MapTypeId.ROADMAP
    //         };
    //         this.map = new google.maps.Map(
    //           this.mapElement.nativeElement,
    //           mapProperties
    //         );
    //         const directionsService = new google.maps.DirectionsService();
    //         const directionsDisplay = new google.maps.DirectionsRenderer({
    //           draggable: true,
    //           map: this.map
    //         });
    //         directionsDisplay.addListener("directions_changed", function() {
    //           let total = 0;
    //           const myroute = directionsDisplay.getDirections().routes[0];
    //           for (let i = 0; i < myroute.legs.length; i++) {
    //             total += myroute.legs[i].distance.value;
    //           }
    //           total = total / 1000;
    //           this.total = total + " km";
    //           console.log("Total: ", this.total);
    //         });
    //         const waypts = [];
    //         const checkboxArray = serviciosP;
    //         for (let i = 0; i < checkboxArray.length; i++) {
    //           waypts.push({
    //             location: new google.maps.LatLng(
    //               checkboxArray[i].partidaGeo._lat,
    //               checkboxArray[i].partidaGeo._long
    //             )
    //           });
    //         }
    //         console.log("Pruva", waypts);
    //         directionsService.route(
    //           {
    //             origin: new google.maps.LatLng(lat, lng),
    //             destination: new google.maps.LatLng(_lat, _lng),
    //             waypoints: waypts,
    //             travelMode: this.driving,
    //             avoidTolls: true
    //           },
    //           function(response, status) {
    //             if (status === "OK") {
    //               directionsDisplay.setDirections(response);
    //             } else {
    //               alert("Could not display directions due to: " + status);
    //             }
    //           }
    //         );
    //       });
    //     });
    //   }, 1000);
    // }
    // tslint:disable-next-line:use-life-cycle-interface
    // ngAfterViewInit() {
    //   console.log("afterinit");
    //   setTimeout(() => {
    //     const idServicio = this.route.snapshot.params["uid"];
    //     this._pedidoSer.getOneServicio(idServicio).subscribe(servicio => {
    //       this._pedidoSer.getAllServiciosP(idServicio).subscribe(serviciosP => {
    //         console.log("ServiciosP", serviciosP);
    //         this.servicio = servicio;
    //         console.log("servicio Cordenadas", this.servicio);
    //         // Coordendas donde se va entregar el pedido.
    //         const _lat = this.servicio.entregaGeo._lat;
    //         const _lng = this.servicio.entregaGeo._long;
    //         console.log(this.destination);
    //         // Coordendas donde se inicia el servicio
    //         const lat = this.servicio.partidaGeo._lat;
    //         const lng = this.servicio.partidaGeo._long;
    //         console.log(this.origin);
    //         const mapProperties = {
    //           center: new google.maps.LatLng(35.2271, -80.8431),
    //           zoom: 15,
    //           mapTypeId: google.maps.MapTypeId.ROADMAP
    //         };
    //         this.map = new google.maps.Map(
    //           this.mapElement.nativeElement,
    //           mapProperties
    //         );
    //         const directionsService = new google.maps.DirectionsService();
    //         const directionsDisplay = new google.maps.DirectionsRenderer({
    //           draggable: true,
    //           map: this.map
    //         });
    //         directionsDisplay.addListener("directions_changed", function() {
    //           let total = 0;
    //           const myroute = directionsDisplay.getDirections().routes[0];
    //           for (let i = 0; i < myroute.legs.length; i++) {
    //             total += myroute.legs[i].distance.value;
    //           }
    //           total = total / 1000;
    //           this.total = total + " km";
    //           console.log("Total: ", this.total);
    //         });
    //         const waypts = [];
    //         const checkboxArray = serviciosP;
    //         for (let i = 0; i < checkboxArray.length; i++) {
    //           waypts.push({
    //             lat: checkboxArray[i].partidaGeo._lat,
    //             lng: checkboxArray[i].partidaGeo._long
    //           });
    //         }
    //         console.log("Pruva", waypts);
    //         const lineSymbol = {
    //           path: "M 0,-1 0,1",
    //           strokeOpacity: 1,
    //           scale: 4
    //         };
    //         const flightPath = new google.maps.Polyline({
    //           path: waypts,
    //           icons: [
    //             {
    //               icon: lineSymbol,
    //               offset: "0",
    //               repeat: "20px"
    //             }
    //           ],
    //           strokeOpacity: 1.0
    //         });
    //         flightPath.setMap(this.map);
    //       });
    //     });
    //   }, 1000);
    // }
    // tslint:disable-next-line:use-life-cycle-interface ->codigo funcionando
    DetallePedidoComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log('afterinit');
        setTimeout(function () {
            var idServicio = _this.route.snapshot.params['uid'];
            _this._pedidoSer.getOneServicio(idServicio).subscribe(function (servicio) {
                _this._pedidoSer.getAllServiciosP(idServicio).subscribe(function (serviciosP) {
                    console.log('ServiciosP', serviciosP);
                    _this.servicio = servicio;
                    console.log(_this.origin);
                    // Coordendas donde se va entregar el pedido.
                    var _lat = _this.servicio.entregaGeo._lat;
                    var _lng = _this.servicio.entregaGeo._long;
                    console.log(_this.destination);
                    // Coordendas donde se inicia el servicio
                    var lat = _this.servicio.partidaGeo._lat;
                    var lng = _this.servicio.partidaGeo._long;
                    console.log('servicio Cordenadas', _this.servicio);
                    var waypts = [];
                    var checkboxArray = serviciosP;
                    _this.serviciosP = checkboxArray;
                    for (var i = 0; i < checkboxArray.length; i++) {
                        var latlng = checkboxArray[i].partidaGeo._lat +
                            ',' +
                            checkboxArray[i].partidaGeo._long;
                        waypts.push(latlng);
                        // Se asigan estas variables para poder centar el mapa
                        _this.originJSlt = checkboxArray[i].partidaGeo._lat;
                        _this.originJSln = checkboxArray[i].partidaGeo._long;
                    }
                    var ruts = waypts.join('|');
                    var _ruts = serviciosP;
                    console.log('Pruva', ruts);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]({
                        'Content-Type': 'application/json'
                    });
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["RequestOptions"]({
                        headers: headers
                    });
                    var url = 'https://roads.googleapis.com/v1/snapToRoads?path=' +
                        ruts +
                        '&interpolate=true&key=AIzaSyDhkHiz_LkMhVCfTsGJajw5Ag4u9d6ah2I';
                    _this.http.get(url, options).subscribe(function (res) {
                        var mapProperties = {
                            center: new google.maps.LatLng(_this.originJSlt, _this.originJSln),
                            zoom: 17,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapProperties);
                        var directionsService = new google.maps.DirectionsService();
                        var directionsDisplay = new google.maps.DirectionsRenderer({
                            draggable: true,
                            map: _this.map
                        });
                        directionsDisplay.addListener('directions_changed', function () {
                            var total = 0;
                            var myroute = directionsDisplay.getDirections().routes[0];
                            for (var i = 0; i < myroute.legs.length; i++) {
                                total += myroute.legs[i].distance.value;
                            }
                            total = total / 1000;
                            this.total = total + ' km';
                            console.log('Total: ', this.total);
                        });
                        var _waypts = [];
                        var _checkboxArray = _ruts;
                        for (var i = 0; i < _checkboxArray.length; i++) {
                            _waypts.push({
                                location: new google.maps.LatLng(_checkboxArray[i].partidaGeo._lat, _checkboxArray[i].partidaGeo._long)
                            });
                        }
                        console.log('Pruva2', _waypts);
                        // directionsService.route(
                        //   {
                        //     origin: new google.maps.LatLng(
                        //       37.33570507,
                        //       -122.02342902
                        //     ),
                        //     destination: new google.maps.LatLng(
                        //       37.33165083,
                        //       -122.03029752
                        //     ),
                        //      waypoints: _waypts,
                        //     travelMode: this.driving,
                        //     avoidTolls: true
                        //   },
                        //   function(response, status) {
                        //     if (String(status) === 'OK') {
                        //       directionsDisplay.setDirections(response);
                        //     } else {
                        //       alert(
                        //         'Could not display directions due to: ' + status
                        //       );
                        //     }
                        //   }
                        // );
                        _this.respuesta = res;
                        var datos = JSON.parse(_this.respuesta._body);
                        console.log('datos1: ', JSON.parse(_this.respuesta._body));
                        console.log('datos2: ', datos.snappedPoints.length);
                        var snappedCoordinates = [];
                        var placeIdArray = [];
                        var polylines = [];
                        for (var i = 0; i < datos.snappedPoints.length; i++) {
                            var latlng = new google.maps.LatLng(datos.snappedPoints[i].location.latitude, datos.snappedPoints[i].location.longitude);
                            snappedCoordinates.push(latlng);
                            placeIdArray.push(datos.snappedPoints[i].placeId);
                        }
                        var snappedPolyline = new google.maps.Polyline({
                            path: snappedCoordinates,
                            strokeColor: 'red',
                            strokeWeight: 3
                        });
                        var marker = new google.maps.Marker({
                            icon: '../../../../assets/imgs/punterocasa.png',
                            position: new google.maps.LatLng(_this.originJSlt, _this.originJSln),
                            map: _this.map,
                            title: 'Lugar de entrega'
                        });
                        snappedPolyline.setMap(_this.map);
                        polylines.push(snappedPolyline);
                    });
                });
            });
        }, 1000);
    };
    DetallePedidoComponent.prototype.computeTotalDistance = function (result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        total = total / 1000;
        this.total = total + ' km';
        console.log('Total: ', this.total);
    };
    DetallePedidoComponent.prototype.getDetails = function (uid) {
        var _this = this;
        this._pedidoSer.getOneServicio(uid).subscribe(function (servicio) {
            _this.servicio = servicio;
            // tslint:disable-next-line: no-unused-expression
            _this.totalEstablecimientos = _this.servicio.numPedidos * 15;
            var toSer = Number.parseFloat(_this.servicio.totalServicio);
            _this.totalSer_totalEst = _this.totalEstablecimientos + toSer;
            var toNet = Number.parseFloat(_this.servicio.totalNeto);
            _this.totalNet_totalEst = _this.totalEstablecimientos + toNet;
            var idCliente = _this.servicio.uidCliente;
            var idRepartidor = _this.servicio.uidRepartidor;
            var convenio = _this.servicio.uidRestaurante;
            _this.getCliente(idCliente);
            _this.getRepartidor(idRepartidor);
            _this.getConvenio(convenio);
            // console.log('tabla servicio', this.servicio);
            // Coordendas donde se va entregar el pedido.
            _this.destination = {
                lat: _this.servicio.entregaGeo._lat,
                lng: _this.servicio.entregaGeo._long
            };
            console.log(_this.destination);
            // Coordendas donde se inicia el servicio
            _this.origin = {
                lat: _this.servicio.partidaGeo._lat,
                lng: _this.servicio.partidaGeo._long
            };
            console.log(_this.origin);
            // const uidServicio = this.servicio.uid;
            // this.getListPedido();
        });
    };
    DetallePedidoComponent.prototype.getPedido = function (idServicio) {
        var _this = this;
        this._pedidoSer.getPedidos(idServicio).subscribe(function (pedido) {
            _this.pedido = pedido;
            // console.log('pedido', this.pedido);
        });
    };
    DetallePedidoComponent.prototype.getProducto = function (idServicio) {
        var _this = this;
        this._pedidoSer.getProducto(idServicio).subscribe(function (producto) {
            _this.productos = producto;
            // console.log('producto', this.productos);
        });
    };
    DetallePedidoComponent.prototype.getCliente = function (id) {
        var _this = this;
        this._pedidoSer.getCliente(id).subscribe(function (cliente) {
            _this.clientes = cliente;
            // console.log('cliente', this.clientes);
        });
    };
    DetallePedidoComponent.prototype.getRepartidor = function (id) {
        var _this = this;
        this._pedidoSer.getRepartidor(id).subscribe(function (repa) {
            _this.repartidores = repa;
            // console.log('reapas', this.repartidores);
        });
    };
    DetallePedidoComponent.prototype.getConvenio = function (id) {
        var _this = this;
        this._pedidoSer.getConvenio(id).subscribe(function (conv) {
            _this.convenios = conv;
            // console.log('reapas', this.repartidores);
        });
    };
    // getDirection(latConvenio, lngConvenio, pedidos) {
    //   // si es un servicio de convenio
    //   if (latConvenio) {
    //     this.waypoints = [
    //       {
    //         location: { lat: latConvenio, lng: lngConvenio },
    //         stopover: false
    //       }
    //     ];
    //     console.log("estoy en el si");
    //     // Cuando es un servicio regular
    //   } else {
    //     console.log("estoy en el no");
    //     console.log("mia de nadie mas", pedidos);
    //     if (pedidos && latConvenio == null) {
    //       for (const pedido of pedidos) {
    //         const lat = pedido.pedidoGeo._lat;
    //         console.log("pedido lat", lat);
    //         const long = pedido.pedidoGeo._long;
    //         console.log("pedido long", long);
    //         this.waypoints = [
    //           {
    //             location: { lat: lat, lng: long },
    //             stopover: false
    //           }
    //         ];
    //       }
    //     } else {
    //       console.log(" Pues me cambio");
    //     }
    //   }
    //   this.renderOptions = {
    //     suppressMarkers: true
    //   };
    // }
    DetallePedidoComponent.prototype.changeStatus = function (serviciosUId, status) {
        var _this = this;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: '¿Estas seguro?',
            text: "Cambiaras el estado del servicio",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, cambiar'
        }).then(function (result) {
            if (result.value) {
                _this.afs
                    .collection('servicios')
                    .doc(serviciosUId)
                    .update({
                    estatus: status
                });
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Estado', 'Servicio cambiado de estado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Error!', 'Hubo un error al cabiar de esyatad el banner', 'error');
        });
        //   const confirma = confirm(
        //     '¿Seguro de cambiar el estado de este servicio a ' + status + '?'
        //   );
        //   if (confirma) {
        //     this.afs
        //       .collection('servicios')
        //       .doc(serviciosUId)
        //       .update({
        //         estatus: status
        //       });
        //   }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DetallePedidoComponent.prototype, "mapElement", void 0);
    DetallePedidoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detalle-pedido',
            template: __webpack_require__(/*! ./detalle-pedido.component.html */ "./src/app/components/admin/detalle-pedido/detalle-pedido.component.html"),
            styles: [__webpack_require__(/*! ./detalle-pedido.component.css */ "./src/app/components/admin/detalle-pedido/detalle-pedido.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__["PedidosService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"]])
    ], DetallePedidoComponent);
    return DetallePedidoComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/home-admin/home-admin.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/components/admin/home-admin/home-admin.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-grid-tile {\n    /* background: #ff864e; */\n    border-radius: 25px;\n    /* border-start-start-radius: 25px; */\n}\n\n.example-card {\n    max-width: 150px;\n}\n\n.im {\n    height: 120px;\n    width: 100px;\n}\n\n/* .esq {\n    text-align: left;\n    margin-top: -3.9em;\n    background-image: url('../../../../assets/imgs/carretera2.jpeg')\n} */\n\n.esq {\n    height: auto;\n    width: 100%;\n    border-radius: 2px;\n}\n\n.esq1 {\n    margin-top: -3.5em;\n    margin-bottom: 1em;\n    margin-left: -1.5em;\n}\n\n.bringme {\n    color: #ff793f;\n    text-align: center;\n}\n\npad-10 {\n    padding: 10px;\n}\n\nagm-map {\n    height: 600px;\n}\n\n/* .side-bar {\n    display: inline-block;\n    margin-top: -50%;\n    margin-left: 20%;\n    width: 80%;\n    padding-left: 0px;\n    overflow-x: hidden;\n} */\n\n.badge-light {\n    zoom: 2;\n}\n\n.badge-info {\n    zoom: 2;\n}\n\n.ml-1,\n.mx-1 {\n    margin-left: 14rem !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9ob21lLWFkbWluL2hvbWUtYWRtaW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDBCQUEwQjtJQUMxQixvQkFBb0I7SUFDcEIsc0NBQXNDO0NBQ3pDOztBQUVEO0lBQ0ksaUJBQWlCO0NBQ3BCOztBQUVEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7O0FBR0Q7Ozs7SUFJSTs7QUFFSjtJQUNJLGFBQWE7SUFDYixZQUFZO0lBQ1osbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixvQkFBb0I7Q0FDdkI7O0FBRUQ7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksY0FBYztDQUNqQjs7QUFFRDtJQUNJLGNBQWM7Q0FDakI7O0FBR0Q7Ozs7Ozs7SUFPSTs7QUFFSjtJQUNJLFFBQVE7Q0FDWDs7QUFFRDtJQUNJLFFBQVE7Q0FDWDs7QUFFRDs7SUFFSSw4QkFBOEI7Q0FDakMiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FkbWluL2hvbWUtYWRtaW4vaG9tZS1hZG1pbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LWdyaWQtdGlsZSB7XG4gICAgLyogYmFja2dyb3VuZDogI2ZmODY0ZTsgKi9cbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIC8qIGJvcmRlci1zdGFydC1zdGFydC1yYWRpdXM6IDI1cHg7ICovXG59XG5cbi5leGFtcGxlLWNhcmQge1xuICAgIG1heC13aWR0aDogMTUwcHg7XG59XG5cbi5pbSB7XG4gICAgaGVpZ2h0OiAxMjBweDtcbiAgICB3aWR0aDogMTAwcHg7XG59XG5cblxuLyogLmVzcSB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBtYXJnaW4tdG9wOiAtMy45ZW07XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvaW1ncy9jYXJyZXRlcmEyLmpwZWcnKVxufSAqL1xuXG4uZXNxIHtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xufVxuXG4uZXNxMSB7XG4gICAgbWFyZ2luLXRvcDogLTMuNWVtO1xuICAgIG1hcmdpbi1ib3R0b206IDFlbTtcbiAgICBtYXJnaW4tbGVmdDogLTEuNWVtO1xufVxuXG4uYnJpbmdtZSB7XG4gICAgY29sb3I6ICNmZjc5M2Y7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5wYWQtMTAge1xuICAgIHBhZGRpbmc6IDEwcHg7XG59XG5cbmFnbS1tYXAge1xuICAgIGhlaWdodDogNjAwcHg7XG59XG5cblxuLyogLnNpZGUtYmFyIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXRvcDogLTUwJTtcbiAgICBtYXJnaW4tbGVmdDogMjAlO1xuICAgIHdpZHRoOiA4MCU7XG4gICAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xufSAqL1xuXG4uYmFkZ2UtbGlnaHQge1xuICAgIHpvb206IDI7XG59XG5cbi5iYWRnZS1pbmZvIHtcbiAgICB6b29tOiAyO1xufVxuXG4ubWwtMSxcbi5teC0xIHtcbiAgICBtYXJnaW4tbGVmdDogMTRyZW0gIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/admin/home-admin/home-admin.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/admin/home-admin/home-admin.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"side-bar\">\n    <ng-container *ngIf=\"usuario.tipo == 1\">\n        <ol class=\"breadcrumb\">\n            <li class=\"breadcrumb-item active text-right\">\n                <a title=\"Gestion de banners\" routerLink=\"../config-banner\"><img style=\"height: 25px; width:auto;\" src=\"../../../../assets/imgs/navegador.png\" alt=\"Gestion de banners\"></a>\n            </li>\n            <li class=\"breadcrumb-item active text-right\">\n                <a title=\"Gestion de servicio\" routerLink=\"../config-servicio\"><img style=\"height: 28px; width:auto;\" src=\"../../../../assets/imgs/ajustes.png\" alt=\"Gestion de servicio\"></a>\n            </li>\n            <li class=\"breadcrumb-item active text-right\">\n                <a title=\"Gestión categorías convenio\" routerLink=\"../convenio-categoria\"><img style=\"height: 28px; width:auto;\" src=\"../../../../assets/imgs/categoria.png\" alt=\"Gestión categorías convenio\"></a>\n            </li>\n        </ol>\n    </ng-container>\n    <div class=\"alert alert-dismissible alert-success\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n        <strong>¡Bienvenido! </strong>\n        <h3>{{ usuario.username }} </h3> <strong>Buen día!</strong>\n    </div>\n    <!-- Admin -->\n    <ng-container *ngIf=\"usuario.tipo == 1\">\n        <div class=\"alert alert-success alert-dismissible\">\n            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n            <strong>Los servicios!</strong> Así estamos en la configuración de los servicios.\n        </div>\n        <div style=\"\">\n            <div class=\"row pad-10 ml-1 mb-2\">\n                <div style=\"border-radius: 15px; margin: 5px 5px;\" class=\"text-muted col-sm-3 col-md-3 bg-light\">\n                    <div class=\"thumbnail text-center\">\n                        <img style=\"height: 75px; width:auto;\" src=\"../../../../assets/imgs/comprar.png\" alt=\"...\">\n                        <div class=\"caption\" *ngFor=\"let tarifa of tarifas\">\n                            <h3>Precios del servicio</h3>\n                            Distancia:\n                            <span class=\"badge badge-info mb-2\">{{  tarifa.distancia | currency }} </span><br> Tiempo:\n                            <span class=\"badge badge-info mb-2\">{{ tarifa.tiempo | currency }} </span>\n                            <p><a href=\"#\" routerLink=\"../config-servicio\" class=\"btn btn-primary\" role=\"button\">Administrar</a></p>\n                        </div>\n                    </div>\n                </div>\n                <div style=\"border-radius: 15px; margin: 5px 5px;\" class=\"text-muted col-sm-3 col-md-3 bg-light\">\n                    <div class=\"thumbnail text-center\">\n                        <img style=\"height: 75px; width:auto;\" src=\"../../../../assets/imgs/arranque.png\" alt=\"...\">\n                        <div class=\"caption\" *ngFor=\"let costo of costos\">\n                            <h3>Costo de arranque</h3>\n                            <p>Costo: </p>\n                            <span class=\"badge badge-info mb-2\">{{ costo.arranque | currency }} </span>\n                            <p><a href=\"#\" routerLink=\"../config-servicio\" class=\"btn btn-primary\" role=\"button\">Cambiar</a></p>\n                        </div>\n                    </div>\n                </div>\n                <div style=\"border-radius: 15px; margin: 5px 5px;\" class=\"text-muted col-sm-3 col-md-3 bg-light\">\n                    <div class=\"thumbnail text-center\">\n                        <img style=\"height: 75px; width:auto;\" src=\"../../../../assets/imgs/comprador.png\" alt=\"...\">\n                        <div class=\"caption\" *ngFor=\"let pago of pagos\">\n                            <h3>Métodos de pago</h3>\n                            <p><img class=\"iconTi\" src=\"../../../../assets/imgs/efectivo.png\" title=\"Pago en efectivo\">\n                                <span class=\"badge badge-success\" *ngIf=\"pago.Efectivo == true\"> Habilitado</span>\n                                <span class=\"badge badge-danger\" *ngIf=\"pago.Efectivo == false\"> No dispoible</span>\n                            </p>\n                            <p><img class=\"iconTi\" src=\"../../../../assets/imgs/tarjeta-de-credito.png\" title=\"Pago en tarjeta de crédito.\">\n                                <span class=\"badge badge-success\" *ngIf=\"pago.Tarjeta == true\"> Habilitado</span>\n                                <span class=\"badge badge-danger\" *ngIf=\"pago.Tarjeta == false\"> No dispoible</span>\n                            </p>\n                            <p><a href=\"#\" routerLink=\"../config-servicio\" class=\"btn btn-primary\" role=\"button\">Ver</a></p>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </ng-container>\n    <!-- Terminan alerts de admin  -->\n\n    <div>\n        <div class=\"esq1\">\n            <img class=\"esq\" src=\"../../../../assets/imgs/lolo-banner.svg\" alt=\"\">\n        </div>\n        <!-- Convenio -->\n        <div class=\"container\">\n            <ng-container *ngIf=\"usuario.tipo == 3\">\n                <div class=\"alert alert-success alert-dismissible\">\n                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n                    <strong>Te mostramos algo de lo que ha pasado!</strong> Así estamos al día.\n                </div>\n                <div>\n                    <mat-grid-list cols=\"3\" rowHeight=\"2:1\">\n                        <mat-grid-tile>\n                            <div style=\"border-radius: 15px; margin: 5px 5px;\" class=\"text-white col-sm-10 col-md-10 bg-info\">\n                                <div class=\"mt-3 thumbnail text-center\">\n                                    <mat-icon class=\"mr-5\" matBadge=\"{{  menus.length }}\" matBadgeColor=\"warn\">\n                                        <img style=\"height: 75px; width:auto;\" src=\"../../../../assets/imgs/dispositivos.png\" alt=\"...\">\n                                    </mat-icon>\n                                    <div class=\"mt-5 caption\">\n                                        <h3>Productos en el menú</h3>\n                                        <button class=\"md-1\" routerLink=\"../../restaurante/restaurante-list-menu\" mat-flat-button color=\"accent\">\n                                        <mat-icon aria-label=\"Example icon-button with a heart icon\">arrow_forward</mat-icon>\n                                    </button>\n                                    </div>\n                                </div>\n                            </div>\n                        </mat-grid-tile>\n                        <mat-grid-tile>\n                            <div style=\"border-radius: 15px; margin: 5px 5px;\" class=\"text-white col-sm-10 col-md-10 bg-info\">\n                                <div class=\"mt-3 thumbnail text-center\">\n                                    <mat-icon class=\"mr-5\" matBadge=\"{{ servicios.length }}\" matBadgeColor=\"warn\">\n                                        <img style=\"height: 75px; width:auto;\" src=\"../../../../assets/imgs/mapa.png\" alt=\"...\">\n                                    </mat-icon>\n                                    <div class=\"mt-5 caption\">\n                                        <h3>Total de servicios</h3>\n                                        <button class=\"md-1\" routerLink=\"../../convenio/list-historial-servicios\" mat-flat-button color=\"accent\">\n                                        <mat-icon aria-label=\"Example icon-button with a heart icon\">arrow_forward </mat-icon>\n                                    </button>\n                                    </div>\n                                </div>\n                            </div>\n                        </mat-grid-tile>\n                        <mat-grid-tile>\n                            <div style=\"border-radius: 15px; margin: 5px 5px;\" class=\"text-white col-sm-10 col-md-10 bg-info\">\n                                <div class=\"mt-3 thumbnail text-center\">\n                                    <mat-icon class=\"mr-5\" matBadge=\"{{ serviciosapp.length }}\" matBadgeColor=\"warn\">\n                                        <img style=\"height: 75px; width:auto;\" src=\"../../../../assets/imgs/dispositivos.png\" alt=\"...\">\n                                    </mat-icon>\n                                    <div class=\"mt-5 caption\">\n                                        <h3>Servicios por app</h3>\n                                        <button class=\"md-1\" routerLink=\"../../convenio/list-historial-servicios\" mat-flat-button color=\"accent\">\n                                        <mat-icon aria-label=\"Example icon-button with a heart icon\">arrow_forward</mat-icon>\n                                    </button>\n                                    </div>\n                                </div>\n                            </div>\n                        </mat-grid-tile>\n                    </mat-grid-list>\n\n                </div>\n            </ng-container>\n        </div>\n        <hr class=\"my-4\">\n        <!-- Terminan alerts de convenio  -->\n        <ng-container *ngIf=\"usuario.tipo == 1\">\n            <div class=\"row pad-10\">\n                <div class=\"col-8\">\n                    <h4 class=\"list-group-item list-group-item-action active\">\n                        <i class=\"fa fa-globe\"></i> Mapa <small *ngIf=\"siguiendoNombre\">siguiendo a: {{ siguiendoNombre }} ID: {{ siguiendoA }} </small> </h4>\n                    <agm-map [latitude]=\"lat\" [longitude]=\"lng\" [zoom]=\"17\">\n                        <agm-marker *ngFor=\"let repartidor of repartidores\" [latitude]=\"repartidor.geolocalizacion._lat\" [longitude]=\"repartidor.geolocalizacion._long\"></agm-marker>\n                    </agm-map>\n                </div>\n                <div class=\"col-4 \">\n                    <h4 class=\"list-group-item list-group-item-action active\"> <i class=\"fa fa-motorcycle\"></i> Repartidores</h4>\n                    <div class=\"list-group\">\n                        <a (click)=\"seguir(repartidor)\" *ngFor=\"let repartidor of repartidores\" class=\"list-group-item list-group-item-action\">{{ repartidor.username }} {{ repartidor.lastname }} </a>\n                        <br>\n                        <button *ngIf=\"siguiendoNombre\" (click)=\"dejarDeSeguir()\" class=\"btn btn-primary btn-block\">Dejar de seguir</button>\n                    </div>\n                </div>\n            </div>\n        </ng-container>\n    </div>\n    <div class=\"modal fade\" data-backdrop=\"static\" data-keyboard=\"false\" id=\"mostrarmodal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"basicModal\" aria-hidden=\"true\">\n        <div class=\"modal-dialog\">\n            <div class=\"modal-content\">\n                <div class=\"modal-header\">\n                    <h3>Aviso!</h3>\n                </div>\n                <div class=\"modal-body\">\n                    <h4>Pago Vencido :(</h4>\n                    No se ha realizado tu pago mensual.\n                </div>\n                <!-- <div class=\"modal-footer\">\n                                    <button class=\"btn btn-lg btn-danger btn-block\" (click)=\"onLogout()\">Cerrar sesión</button>\n                                </div> -->\n            </div>\n        </div>\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/components/admin/home-admin/home-admin.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/admin/home-admin/home-admin.component.ts ***!
  \*********************************************************************/
/*! exports provided: HomeAdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAdminComponent", function() { return HomeAdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var src_app_service_config_servicios_config_servicios_config_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/config/servicios_config/servicios-config.service */ "./src/app/service/config/servicios_config/servicios-config.service.ts");






var HomeAdminComponent = /** @class */ (function () {
    function HomeAdminComponent(authService, db, afs, _servicio_config) {
        this.authService = authService;
        this.db = db;
        this.afs = afs;
        this._servicio_config = _servicio_config;
        this.repartidores = [];
        this.init = false;
        this.siguiendoNombre = null;
        this.siguiendoA = null;
        this.uidUser = null;
        this.usuario = {};
        this.uidSucursal = null;
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            // photoUrl: '',
            roles: {}
        };
        this.providerId = 'null';
    }
    HomeAdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                // this.user.photoUrl = user.photoURL;
                _this.providerId = user.providerData[0].providerId;
                // this.user.username = user.displayName;
                // this.user.email = user.email;
                // this.user.photoUrl = user.photoURL;
                // this.providerId = user.providerData[0].providerId;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    _this.uidSucursal = _this.usuario.uidSucursal;
                    console.log('BD', _this.usuario);
                    console.log('BD2', _this.uidSucursal);
                    _this.getCosto(_this.uidSucursal);
                    _this.getTarifa(_this.uidSucursal);
                    _this.getPago(_this.uidSucursal);
                    _this.getListMenu(_this.uidUser);
                    _this.getListServicios(_this.uidUser);
                    _this.getListServiciosApp(_this.uidUser);
                    _this.db.collection('users', function (ref) { return ref.where('tipo', '==', 2)
                        .where('uidSucursal', '==', _this.uidSucursal); }).valueChanges()
                        // tslint:disable-next-line:no-shadowed-variable
                        .subscribe(function (data) {
                        console.log('repartidores', data);
                        _this.repartidores = data;
                        if (!_this.init) {
                            _this.lat = data[0].geolocalizacion._lat;
                            _this.lng = data[0].geolocalizacion._long;
                            _this.init = true;
                        }
                        if (_this.siguiendoA) {
                            data.forEach(function (repartidor) {
                                if (repartidor.uid === _this.siguiendoA) {
                                    _this.lat = repartidor.geolocalizacion._lat;
                                    _this.lng = repartidor.geolocalizacion._long;
                                }
                            });
                        }
                    });
                    if (_this.usuario.pago === 'Vencido') {
                        $(document).ready(function () {
                            $('#mostrarmodal').modal('show');
                        });
                    }
                });
            }
        });
        // const time = moment.duration(1557154847000).asMinutes();
        // const time2 = moment.duration(1557165647000).asMinutes();
        // const res = (time2 - time);
        // const hora = moment(res).format('HH:mm');
        //  const time = moment(1557139235000).hours();
        //  const horaxminut = time * 60;
        //  const time2 = moment(1557139235000).minutes();
        //  const suma = (horaxminut + time2) / time;
        // console.log('hora', time);
        // console.log('res', res);
        // console.log('format', hora);
        // console.log('hora', horaxminut);
        // console.log('minuto', time2);
        // console.log('minutos', suma);
        // const time2 = moment()
    };
    HomeAdminComponent.prototype.seguir = function (repartidor) {
        this.siguiendoA = repartidor.uid;
        this.siguiendoNombre = repartidor.username;
        this.lat = repartidor.geolocalizacion._lat;
        this.lng = repartidor.geolocalizacion._long;
    };
    HomeAdminComponent.prototype.dejarDeSeguir = function () {
        this.siguiendoA = null;
        this.siguiendoNombre = null;
    };
    HomeAdminComponent.prototype.onLogout = function () {
        this.afs.auth.signOut();
    };
    HomeAdminComponent.prototype.getCosto = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getCostoFijo(uidSucursal).subscribe(function (costo) {
            _this.costos = costo;
            console.log('costo', costo);
        });
    };
    HomeAdminComponent.prototype.getTarifa = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getTarifaService(uidSucursal).subscribe(function (tarifa) {
            _this.tarifas = tarifa;
            console.log('tarifa ', tarifa);
        });
    };
    HomeAdminComponent.prototype.getPago = function (uidSucursal) {
        var _this = this;
        this._servicio_config.getPagoService(uidSucursal).subscribe(function (pago) {
            _this.pagos = pago;
            console.log('pago ', pago);
        });
    };
    HomeAdminComponent.prototype.getListMenu = function (uidUser) {
        var _this = this;
        this.db.collection('menus', function (ref) { return ref.where('userUid', '==', uidUser).where('estado', '==', '1'); })
            .valueChanges().subscribe(function (m) {
            _this.menus = m;
        });
    };
    HomeAdminComponent.prototype.getListServicios = function (uidUser) {
        var _this = this;
        this.db.collection('servicios', function (ref) { return ref.where('uidRestaurante', '==', uidUser).where('estatus', '==', 'Terminado'); })
            .valueChanges().subscribe(function (m) {
            _this.servicios = m;
        });
    };
    HomeAdminComponent.prototype.getListServiciosApp = function (uidUser) {
        var _this = this;
        this.db.collection('servicios', function (ref) { return ref.where('uidRestaurante', '==', uidUser).where('abierto', '==', null)
            .where('estatus', '==', 'Terminado'); })
            .valueChanges().subscribe(function (m) {
            _this.serviciosapp = m;
            console.log('app', m.length);
        });
    };
    HomeAdminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home-admin',
            template: __webpack_require__(/*! ./home-admin.component.html */ "./src/app/components/admin/home-admin/home-admin.component.html"),
            styles: [__webpack_require__(/*! ./home-admin.component.css */ "./src/app/components/admin/home-admin/home-admin.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            src_app_service_config_servicios_config_servicios_config_service__WEBPACK_IMPORTED_MODULE_5__["ServiciosConfigService"]])
    ], HomeAdminComponent);
    return HomeAdminComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-administradores/list-administradores.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/components/admin/list-administradores/list-administradores.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nsection {\n    margin-right: 110px;\n    margin-left: 110px;\n}\n\n.csv {\n    text-align: right;\n    height: auto;\n    width: auto;\n    margin-bottom: 1em;\n}\n\np {\n    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n    font-size: 10px;\n    margin-bottom: -1px;\n    color: #562B89;\n}\n\n.eta {\n    margin-right: 2px;\n    margin-bottom: 2px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\n/*\n   server-side-angular-way.component.css\n*/\n\n.no-data-available {\n    text-align: center;\n}\n\n/*\n     src/styles.css (i.e. your global style)\n  */\n\n.dataTables_empty {\n    display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LWFkbWluaXN0cmFkb3Jlcy9saXN0LWFkbWluaXN0cmFkb3Jlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtDQUNmOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLFlBQVk7SUFDWixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSx1SEFBdUg7SUFDdkgsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixlQUFlO0NBQ2xCOztBQUVEO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFHRDs7RUFFRTs7QUFFRjtJQUNJLG1CQUFtQjtDQUN0Qjs7QUFHRDs7SUFFSTs7QUFFSjtJQUNJLGNBQWM7Q0FDakIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FkbWluL2xpc3QtYWRtaW5pc3RyYWRvcmVzL2xpc3QtYWRtaW5pc3RyYWRvcmVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5zZWN0aW9uIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDExMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMTBweDtcbn1cblxuLmNzdiB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIG1hcmdpbi1ib3R0b206IDFlbTtcbn1cblxucCB7XG4gICAgZm9udC1mYW1pbHk6ICdMdWNpZGEgU2FucycsICdMdWNpZGEgU2FucyBSZWd1bGFyJywgJ0x1Y2lkYSBHcmFuZGUnLCAnTHVjaWRhIFNhbnMgVW5pY29kZScsIEdlbmV2YSwgVmVyZGFuYSwgc2Fucy1zZXJpZjtcbiAgICBmb250LXNpemU6IDEwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTFweDtcbiAgICBjb2xvcjogIzU2MkI4OTtcbn1cblxuLmV0YSB7XG4gICAgbWFyZ2luLXJpZ2h0OiAycHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xufVxuXG5idXR0b24ge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbmlucHV0IHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5cbi8qXG4gICBzZXJ2ZXItc2lkZS1hbmd1bGFyLXdheS5jb21wb25lbnQuY3NzXG4qL1xuXG4ubm8tZGF0YS1hdmFpbGFibGUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuXG4vKlxuICAgICBzcmMvc3R5bGVzLmNzcyAoaS5lLiB5b3VyIGdsb2JhbCBzdHlsZSlcbiAgKi9cblxuLmRhdGFUYWJsZXNfZW1wdHkge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/admin/list-administradores/list-administradores.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/admin/list-administradores/list-administradores.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <ngx-alerts></ngx-alerts>\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-user-circle-o\"></i></span> Listado de Administradores</h1>\n        <div class=\"row\">\n            <!-- <div class=\"col\"> -->\n            <!-- <div class=\"form-group\"> -->\n            <!--Buscador-->\n            <!-- <input type=\"text\" class=\"form-control\" placeholder=\"&#xf002; Buscar nombre...\" style=\"font-family:Arial, FontAwesome\" name=\"filterPost\" [(ngModel)]=\"filterPost\"> -->\n            <!-- </div> -->\n            <!-- </div> -->\n            <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modal\" data-target=\"#modalAdmin\"> <i class=\"fa fa-plus\"></i> Nuevo administrador </button>\n        </div>\n    </div>\n\n    <div class=\"col\">\n        <div class=\"csv table table-light \">\n            <p>Exportar CSV</p>\n            <a csvLink [data]=\"administradores\" class=\"eta btn btn-success\"><i class=\"fa fa-file-excel-o\" aria-hidden=\"true\"></i></a>\n        </div>\n\n        <mat-form-field>\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n        </mat-form-field>\n\n        <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n            <!-- NOMBRE Column -->\n            <ng-container matColumnDef=\"username\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Nombre </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.username}} {{element.lastname}} </td>\n            </ng-container>\n\n            <!-- TELEFONO Column -->\n            <ng-container matColumnDef=\"phone\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Teléfono </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.phone}} </td>\n            </ng-container>\n\n            <!-- CORREO Column -->\n            <ng-container matColumnDef=\"email\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Correo </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.email}} </td>\n            </ng-container>\n\n            <!-- ESTATUS Column -->\n            <ng-container matColumnDef=\"activo\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.activo == true\">\n                        <mat-icon matBadge=\"+\" matBadgeColor=\"accent\">person_outline</mat-icon>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.activo == false\">\n                        <mat-icon matBadge=\"-\" matBadgeColor=\"warn\">person_outline</mat-icon>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <!-- ELIMINAR Column -->\n            <ng-container matColumnDef=\"delete\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Borrar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"warn\" (click)=\"onDeleteAdmin(element)\">\n                        <mat-icon>delete</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <!-- STATUS Column -->\n            <ng-container matColumnDef=\"status\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.activo == true\">\n                        <button mat-mini-fab color=\"blue\" (click)=\"onDesactivarAdmin(element)\">\n                            <mat-icon>toggle_off</mat-icon>\n                        </button>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.activo == false\">\n                        <button mat-mini-fab color=\"primary\" (click)=\"onActivarAdmin(element)\">                        \n                            <mat-icon>toggle_on</mat-icon>\n                        </button>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        </table>\n        <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n    </div>\n</section>\n<app-modal-administrador></app-modal-administrador>"

/***/ }),

/***/ "./src/app/components/admin/list-administradores/list-administradores.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/admin/list-administradores/list-administradores.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ListAdministradoresComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListAdministradoresComponent", function() { return ListAdministradoresComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_administrador_administrador_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/administrador/administrador-data.service */ "./src/app/service/administrador/administrador-data.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);







var ListAdministradoresComponent = /** @class */ (function () {
    function ListAdministradoresComponent(dataAdm, db, authService) {
        this.dataAdm = dataAdm;
        this.db = db;
        this.authService = authService;
        this.displayedColumns = ['username', 'email', 'phone', 'activo', 'delete', 'status'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"]();
        this.filterPost = '';
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
        this.pageActual = 1;
    }
    ListAdministradoresComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Sucursal
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    console.log('usuario', data);
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    _this.getListAdministrador(_this.Uidsucursal);
                    console.log('usuer', _this.Uidsucursal);
                    // Desglose de tabla
                    _this.dataAdm.getAdmins(_this.Uidsucursal)
                        .subscribe(function (res) { return (_this.dataSource.data = res); });
                });
            }
        });
    };
    ListAdministradoresComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListAdministradoresComponent.prototype.getListAdministrador = function (uidSucursal) {
        var _this = this;
        console.log('uidsucu', uidSucursal);
        this.db.collection('users', function (ref) { return ref.where('tipo', '==', 1)
            .where('uidSucursal', '==', uidSucursal); }).valueChanges().subscribe(function (data) {
            _this.administradores = data;
            console.log('admins', data);
        });
    };
    ListAdministradoresComponent.prototype.onDeleteAdmin = function (idAdmin) {
        var _this = this;
        var idAd = idAdmin.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: '¿Estas seguro?',
            text: "Eliminaras al administrador!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.dataAdm.deleteAdmin(idAd);
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Borrado', 'Administrador Borrado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Error!', 'Hubo un error al eliminar el administrador', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de eliminar este usuario?');
        // if (confirmacion) {
        //   this.dataAdm.deleteAdmin(idAdmin);
        // }
    };
    ListAdministradoresComponent.prototype.onDesactivarAdmin = function (idAdmin) {
        var _this = this;
        var idAd = idAdmin.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: '¿Estas seguro?',
            text: "Desactivaras al administrador!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, desactivar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.dataAdm.desactivarAdmin(idAd);
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Desactivado', 'Administrador desactivado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Error!', 'Hubo un error al desactivar el administrador', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de desactivar este usuario?');
        // if (confirmacion) {
        //   this.dataAdm.desactivarAdmin(uid);
        // }
    };
    ListAdministradoresComponent.prototype.onActivarAdmin = function (idAdmin) {
        var _this = this;
        var idAd = idAdmin.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: '¿Estas seguro?',
            text: "Activaras al administrador!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, activar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.dataAdm.activarAdmin(idAd);
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Activado', 'Administrador activado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Error!', 'Hubo un error al activar el administrador', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de activar este usuario?');
        // if (confirmacion) {
        //   this.dataAdm.activarAdmin(uid);
        // }
    };
    ListAdministradoresComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], ListAdministradoresComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], ListAdministradoresComponent.prototype, "sort", void 0);
    ListAdministradoresComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-administradores',
            template: __webpack_require__(/*! ./list-administradores.component.html */ "./src/app/components/admin/list-administradores/list-administradores.component.html"),
            styles: [__webpack_require__(/*! ./list-administradores.component.css */ "./src/app/components/admin/list-administradores/list-administradores.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_administrador_administrador_data_service__WEBPACK_IMPORTED_MODULE_2__["AdministradorDataService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ListAdministradoresComponent);
    return ListAdministradoresComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-category-convenio/list-category-convenio.component.css":
/*!**********************************************************************************************!*\
  !*** ./src/app/components/admin/list-category-convenio/list-category-convenio.component.css ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nsection {\n    margin-right: 110px;\n    margin-left: 110px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\n/*\n   server-side-angular-way.component.css\n*/\n\n.no-data-available {\n    text-align: center;\n}\n\n/*\n     src/styles.css (i.e. your global style)\n  */\n\n.dataTables_empty {\n    display: none;\n}\n\n.csv {\n    text-align: right;\n    height: auto;\n    width: auto;\n    margin-bottom: 1em;\n}\n\np {\n    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n    font-size: 10px;\n    margin-bottom: -1px;\n    color: #562B89;\n}\n\n.eta {\n    margin-right: 2px;\n    margin-bottom: 2px;\n}\n\n.img {\n    height: 75px;\n    width: 75px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LWNhdGVnb3J5LWNvbnZlbmlvL2xpc3QtY2F0ZWdvcnktY29udmVuaW8uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxvQkFBb0I7SUFDcEIsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUdEOztFQUVFOztBQUVGO0lBQ0ksbUJBQW1CO0NBQ3RCOztBQUdEOztJQUVJOztBQUVKO0lBQ0ksY0FBYztDQUNqQjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsWUFBWTtJQUNaLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLHVIQUF1SDtJQUN2SCxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLGVBQWU7Q0FDbEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksYUFBYTtJQUNiLFlBQVk7Q0FDZiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vbGlzdC1jYXRlZ29yeS1jb252ZW5pby9saXN0LWNhdGVnb3J5LWNvbnZlbmlvLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5zZWN0aW9uIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDExMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMTBweDtcbn1cblxuYnV0dG9uIHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5pbnB1dCB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuXG4vKlxuICAgc2VydmVyLXNpZGUtYW5ndWxhci13YXkuY29tcG9uZW50LmNzc1xuKi9cblxuLm5vLWRhdGEtYXZhaWxhYmxlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cblxuLypcbiAgICAgc3JjL3N0eWxlcy5jc3MgKGkuZS4geW91ciBnbG9iYWwgc3R5bGUpXG4gICovXG5cbi5kYXRhVGFibGVzX2VtcHR5IHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4uY3N2IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogMWVtO1xufVxuXG5wIHtcbiAgICBmb250LWZhbWlseTogJ0x1Y2lkYSBTYW5zJywgJ0x1Y2lkYSBTYW5zIFJlZ3VsYXInLCAnTHVjaWRhIEdyYW5kZScsICdMdWNpZGEgU2FucyBVbmljb2RlJywgR2VuZXZhLCBWZXJkYW5hLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAtMXB4O1xuICAgIGNvbG9yOiAjNTYyQjg5O1xufVxuXG4uZXRhIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG5cbi5pbWcge1xuICAgIGhlaWdodDogNzVweDtcbiAgICB3aWR0aDogNzVweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/admin/list-category-convenio/list-category-convenio.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/admin/list-category-convenio/list-category-convenio.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <ngx-alerts></ngx-alerts>\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color:#562B89;\"><i class=\"fa fa-sitemap\"></i></span> Listado de categorías </h1>\n        <div class=\"row\">\n            <!-- <div class=\"col\">\n                <div class=\"form-group\">\n                    <input type=\"text\" name=\"filterPost\" class=\"form-control\" placeholder=\"&#xf002; Buscar...\" style=\"font-family:Arial, FontAwesome\" [(ngModel)]=\"filterPost\">\n                </div>\n            </div> -->\n            <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modal\" data-target=\"#modalCategoria\"><i class=\"fa fa-plus\"></i> Nueva categoría</button>\n        </div>\n    </div>\n    <div class=\"col\">\n\n        <mat-form-field>\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n        </mat-form-field>\n\n        <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n            <!-- POSICION Column -->\n            <ng-container matColumnDef=\"posicion\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Posición </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.posicion}} </td>\n            </ng-container>\n\n            <!-- NOMBRE Column -->\n            <ng-container matColumnDef=\"nombre\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Nombre </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.nombre}} </td>\n            </ng-container>\n\n            <!-- DESCRIPCION Column -->\n            <ng-container matColumnDef=\"descripcion\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Descripción </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.descripcion}} </td>\n            </ng-container>\n\n            <!-- IMAGEN Column -->\n            <ng-container matColumnDef=\"imagen\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Imagen </th>\n                <td mat-cell *matCellDef=\"let element\"> <img style=\"height: 45px; width: auto;\" class=\"img\" src=\"{{element.imagen}}\"> </td>\n            </ng-container>\n\n            <!-- EDITAR Column -->\n            <ng-container matColumnDef=\"edit\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Editar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"accent\" data-toggle=\"modal\" data-target=\"#modalCategoria\" (click)=\"onPreUpdateCategoria(element)\">\n                        <mat-icon>edit</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <!-- ELIMINAR Column -->\n            <ng-container matColumnDef=\"delete\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Borrar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"warn\" (click)=\"borrar(element)\">\n                        <mat-icon>delete</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        </table>\n        <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n\n    </div>\n</section>\n<app-modal-category-convenio></app-modal-category-convenio>"

/***/ }),

/***/ "./src/app/components/admin/list-category-convenio/list-category-convenio.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/admin/list-category-convenio/list-category-convenio.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: ListCategoryConvenioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListCategoryConvenioComponent", function() { return ListCategoryConvenioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_categoria_categoria_convenio_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/categoria/categoria-convenio.service */ "./src/app/service/categoria/categoria-convenio.service.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);







var ListCategoryConvenioComponent = /** @class */ (function () {
    function ListCategoryConvenioComponent(_service_cat, authService, afs) {
        this._service_cat = _service_cat;
        this.authService = authService;
        this.afs = afs;
        this.displayedColumns = ['posicion', 'nombre', 'descripcion', 'imagen', 'edit', 'delete'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"]();
        this.usuario = {};
        this.userUid = null;
        this.filterPost = '';
        this.pageActual = 1;
        this.uidUser = null;
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    ListCategoryConvenioComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.afs.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    // this.getListRepartidores(this.Uidsucursal);
                    _this._service_cat.getCategorias(_this.Uidsucursal)
                        .subscribe(function (res) { return (_this.dataSource.data = res); });
                    // console.log('usuer', this.Uidsucursal);
                });
            }
        });
    };
    ListCategoryConvenioComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    // getAllCat(uid) {
    //   this.getListCategorias(uid).subscribe( cate =>  {
    //     this.categorias = cate;
    //     console.log('categorias', cate);
    //   });
    // }
    // getListCategorias(uid) {
    //   console.log('uidsu', uid);
    //    this.CategoriasCollection = this.afs.collection<Categorias>('categoria_critico', ref =>
    //     ref.where('uidSucursal', '==', uid).orderBy('posicion', 'asc'));
    //      this.Categorias = this.CategoriasCollection.valueChanges();
    //      return this.Categorias = this.CategoriasCollection.snapshotChanges()
    //     .pipe(map( changes => {
    //      return changes.map( action => {
    //     const data = action.payload.doc.data() as Categorias;
    //     data.id = action.payload.doc.id;
    //     return data;
    //   });
    // }));
    // this.afs.collection('categoria_critico', ref =>
    //  ref.where('uidSucursal', '==', uid).orderBy('posicion', 'asc')).valueChanges().subscribe( categoria => {
    //   this.categorias = categoria;
    //   console.log('categoria', categoria);
    // });
    // }
    ListCategoryConvenioComponent.prototype.onPreUpdateCategoria = function (categoria) {
        console.log('CATE', categoria);
        this._service_cat.selectedCategoria = Object.assign({}, categoria);
    };
    ListCategoryConvenioComponent.prototype.borrar = function (id) {
        var _this = this;
        var idCa = id.id;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: '¿Estas seguro?',
            text: "Eliminaras la categoria!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this._service_cat.eliminar(idCa);
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Borrado', 'Categoria Borrado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Error!', 'Hubo un error al eliminar la categoria', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de eliminar esta categoría?');
        // if ( confirmacion ) {
        //   this._service_cat.eliminar(id);
        // }
    };
    ListCategoryConvenioComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], ListCategoryConvenioComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], ListCategoryConvenioComponent.prototype, "sort", void 0);
    ListCategoryConvenioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-category-convenio',
            template: __webpack_require__(/*! ./list-category-convenio.component.html */ "./src/app/components/admin/list-category-convenio/list-category-convenio.component.html"),
            styles: [__webpack_require__(/*! ./list-category-convenio.component.css */ "./src/app/components/admin/list-category-convenio/list-category-convenio.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_categoria_categoria_convenio_service__WEBPACK_IMPORTED_MODULE_2__["CategoriaConvenioService"],
            _service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"]])
    ], ListCategoryConvenioComponent);
    return ListCategoryConvenioComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-clientes/list-clientes.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/admin/list-clientes/list-clientes.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nsection {\n    margin-right: 110px;\n    margin-left: 110px;\n}\n\n.csv {\n    text-align: right;\n    height: auto;\n    width: auto;\n    margin-bottom: 1em;\n}\n\np {\n    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n    font-size: 10px;\n    margin-bottom: -1px;\n    color: #562B89;\n}\n\n.eta {\n    margin-right: 2px;\n    margin-bottom: 2px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\n/*\n   server-side-angular-way.component.css\n*/\n\n.no-data-available {\n    text-align: center;\n}\n\n/*\n     src/styles.css (i.e. your global style)\n  */\n\n.dataTables_empty {\n    display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LWNsaWVudGVzL2xpc3QtY2xpZW50ZXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxvQkFBb0I7SUFDcEIsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixZQUFZO0lBQ1osbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksdUhBQXVIO0lBQ3ZILGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsZUFBZTtDQUNsQjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxvQkFBb0I7Q0FDdkI7O0FBRUQ7SUFDSSxvQkFBb0I7Q0FDdkI7O0FBR0Q7O0VBRUU7O0FBRUY7SUFDSSxtQkFBbUI7Q0FDdEI7O0FBR0Q7O0lBRUk7O0FBRUo7SUFDSSxjQUFjO0NBQ2pCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LWNsaWVudGVzL2xpc3QtY2xpZW50ZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbnNlY3Rpb24ge1xuICAgIG1hcmdpbi1yaWdodDogMTEwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDExMHB4O1xufVxuXG4uY3N2IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogMWVtO1xufVxuXG5wIHtcbiAgICBmb250LWZhbWlseTogJ0x1Y2lkYSBTYW5zJywgJ0x1Y2lkYSBTYW5zIFJlZ3VsYXInLCAnTHVjaWRhIEdyYW5kZScsICdMdWNpZGEgU2FucyBVbmljb2RlJywgR2VuZXZhLCBWZXJkYW5hLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAtMXB4O1xuICAgIGNvbG9yOiAjNTYyQjg5O1xufVxuXG4uZXRhIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG5cbmJ1dHRvbiB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuaW5wdXQge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cblxuLypcbiAgIHNlcnZlci1zaWRlLWFuZ3VsYXItd2F5LmNvbXBvbmVudC5jc3NcbiovXG5cbi5uby1kYXRhLWF2YWlsYWJsZSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5cbi8qXG4gICAgIHNyYy9zdHlsZXMuY3NzIChpLmUuIHlvdXIgZ2xvYmFsIHN0eWxlKVxuICAqL1xuXG4uZGF0YVRhYmxlc19lbXB0eSB7XG4gICAgZGlzcGxheTogbm9uZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/admin/list-clientes/list-clientes.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/admin/list-clientes/list-clientes.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <!-- <ngx-alerts></ngx-alerts> -->\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-male\"></i></span> Listado de clientes</h1>\n        <div class=\"row\">\n            <div class=\"col\">\n                <div class=\"form-group\">\n                    <!--Buscador-->\n                    <!-- <input type=\"text\" class=\"form-control\" placeholder=\"&#xf002; Buscar nombre...\" style=\"font-family:Arial, FontAwesome\" name=\"filterPost\" [(ngModel)]=\"filterPost\"> -->\n                </div>\n            </div>\n            <!-- <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modnaval\" data-target=\"#modalAdmin\"> <i class=\"fa fa-plus\"></i> Nuevo administrador </button> -->\n        </div>\n    </div>\n\n\n    <div class=\"col\">\n        <div class=\"csv table table-light \">\n            <p>Exportar CSV</p>\n            <a csvLink [data]=\"clientes\" class=\"eta btn btn-success\"><i class=\"fa fa-file-excel-o\" aria-hidden=\"true\"></i></a>\n        </div>\n        <mat-form-field>\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n        </mat-form-field>\n\n        <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n            <!-- NOMBRE Column -->\n            <ng-container matColumnDef=\"username\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Nombre </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.username}} {{element.lastname}} </td>\n            </ng-container>\n\n            <!-- TELEFONO Column -->\n            <ng-container matColumnDef=\"phone\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Teléfono </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.phone}} </td>\n            </ng-container>\n\n            <!-- CORREO Column -->\n            <ng-container matColumnDef=\"email\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Correo </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.email}} </td>\n            </ng-container>\n\n            <!-- ESTATUS Column -->\n            <ng-container matColumnDef=\"activo\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.activo == true\">\n                        <mat-icon matBadge=\"+\" matBadgeColor=\"accent\">person_outline</mat-icon>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.activo == false\">\n                        <mat-icon matBadge=\"-\" matBadgeColor=\"warn\">person_outline</mat-icon>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <!-- ELIMINAR Column -->\n            <ng-container matColumnDef=\"delete\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Borrar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"warn\" (click)=\"onDeleteCliente(element)\">\n                        <mat-icon>delete</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <!-- STATUS Column -->\n            <ng-container matColumnDef=\"status\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.activo == true\">\n                        <button mat-mini-fab color=\"blue\" (click)=\"onDesactivarCliente(element)\">\n                            <mat-icon>toggle_off</mat-icon>\n                        </button>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.activo == false\">\n                        <button mat-mini-fab color=\"primary\" (click)=\"onActivarCliente(element)\">                        \n                            <mat-icon>toggle_on</mat-icon>\n                        </button>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        </table>\n        <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n\n    </div>\n</section>\n<app-modal-administrador></app-modal-administrador>"

/***/ }),

/***/ "./src/app/components/admin/list-clientes/list-clientes.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/admin/list-clientes/list-clientes.component.ts ***!
  \***************************************************************************/
/*! exports provided: ListClientesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListClientesComponent", function() { return ListClientesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_cliente_cliente_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/cliente/cliente.service */ "./src/app/service/cliente/cliente.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var src_app_service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/restaurante/restaurante-data.service */ "./src/app/service/restaurante/restaurante-data.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_7__);








var ListClientesComponent = /** @class */ (function () {
    function ListClientesComponent(dataCle, db, authService, http) {
        this.dataCle = dataCle;
        this.db = db;
        this.authService = authService;
        this.http = http;
        this.displayedColumns = ['username', 'email', 'phone', 'activo', 'delete', 'status'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"]();
        this.filterPost = '';
        this.pageActual = 1;
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            uidSucursal: ''
        };
    }
    ListClientesComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Sucursal
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db
                    .doc("users/" + _this.uidUser)
                    .valueChanges()
                    .subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    _this.getListClientes(_this.Uidsucursal);
                    console.log('usuer', _this.Uidsucursal);
                    // Desglose de tabla
                    _this.dataCle.getClientes(_this.Uidsucursal)
                        .subscribe(function (res) { return (_this.dataSource.data = res); });
                });
            }
        });
    };
    ListClientesComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListClientesComponent.prototype.getListClientes = function (uidSucursal) {
        var _this = this;
        console.log('uidsucu', uidSucursal);
        this.db
            .collection('users', function (ref) {
            return ref.where('tipo', '==', 4).where('uidSucursal', '==', uidSucursal);
        })
            .valueChanges()
            .subscribe(function (data) {
            _this.clientes = data;
            // console.log('covenios', data);
            _this.data = JSON.stringify(_this.clientes);
        });
    };
    ListClientesComponent.prototype.onDeleteCliente = function (cliente) {
        var _this = this;
        var idCl = cliente.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire({
            title: '¿Estas seguro?',
            text: "Eliminaras al cliente!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.dataCle.deleteClientes(idCl);
                sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('Borrado', 'Cliente Borrado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('Error!', 'Hubo un error al eliminar el cliente', 'error');
        });
        // const confirmacion = confirm(
        //   '¿Estas seguro de eliminar a este usuario de los registros?'
        // );
        // if (confirmacion) {
        //   this.dataCle.deleteClientes(idCliente);
        // }
    };
    ListClientesComponent.prototype.onDesactivarCliente = function (cliente) {
        var _this = this;
        var idCl = cliente.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire({
            title: '¿Estas seguro?',
            text: "Desactivaras al cliente!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, desactivar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.authService.desactivarConvenio(idCl);
                sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('Desactivado', 'Cliente desactivado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('Error!', 'Hubo un error al desactivar el cliente', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de desactivar este usuario?');
        // if (confirmacion) {
        //   this.authService.desactivarConvenio(uid);
        // }
    };
    ListClientesComponent.prototype.onActivarCliente = function (cliente) {
        var _this = this;
        var idCl = cliente.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire({
            title: '¿Estas seguro?',
            text: "Activaras al cliente!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, activar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.authService.activarConvenio(idCl);
                sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('Activado', 'Cliente activado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_7___default.a.fire('Error!', 'Hubo un error al activar el cliente', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de activar este usuario?');
        // if (confirmacion) {
        //   this.authService.activarConvenio(uid);
        // }
    };
    ListClientesComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginator"])
    ], ListClientesComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSort"])
    ], ListClientesComponent.prototype, "sort", void 0);
    ListClientesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-clientes',
            template: __webpack_require__(/*! ./list-clientes.component.html */ "./src/app/components/admin/list-clientes/list-clientes.component.html"),
            styles: [__webpack_require__(/*! ./list-clientes.component.css */ "./src/app/components/admin/list-clientes/list-clientes.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_service_cliente_cliente_service__WEBPACK_IMPORTED_MODULE_2__["ClienteService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            src_app_service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_4__["RestauranteDataService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"]])
    ], ListClientesComponent);
    return ListClientesComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-corte/list-corte.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/components/admin/list-corte/list-corte.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "button {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\n/*\n   server-side-angular-way.component.css\n*/\n\n.no-data-available {\n    text-align: center;\n}\n\n/*\n     src/styles.css (i.e. your global style)\n  */\n\n.dataTables_empty {\n    display: none;\n}\n\n/* .cen {\n    margin-left: 15em;\n    width: 900px;\n} */\n\n.modal-content {\n    width: 270%;\n    margin-left: -85%;\n    zoom: 80%;\n}\n\n.iconTi {\n    height: 35px;\n    width: 35px;\n}\n\n.iconTi1 {\n    text-align: right;\n    height: 100px;\n    width: 100px;\n}\n\n.sp {\n    color: #562B89;\n    font: bold;\n}\n\n.m1 {\n    margin-left: 10em;\n}\n\n.m2 {\n    margin-top: 2.5em;\n}\n\n.dp {\n    font-size: 1em;\n}\n\n.esq {\n    text-align: right;\n    margin-top: 2em;\n}\n\nspan {\n    font-display: 2em;\n}\n\n/* .mra {\n    margin-left: 5em;\n} */\n\ntable {\n    margin-right: auto;\n    margin-left: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LWNvcnRlL2xpc3QtY29ydGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFHRDs7RUFFRTs7QUFFRjtJQUNJLG1CQUFtQjtDQUN0Qjs7QUFHRDs7SUFFSTs7QUFFSjtJQUNJLGNBQWM7Q0FDakI7O0FBR0Q7OztJQUdJOztBQUVKO0lBQ0ksWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixVQUFVO0NBQ2I7O0FBRUQ7SUFDSSxhQUFhO0lBQ2IsWUFBWTtDQUNmOztBQUVEO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxhQUFhO0NBQ2hCOztBQUVEO0lBQ0ksZUFBZTtJQUNmLFdBQVc7Q0FDZDs7QUFFRDtJQUNJLGtCQUFrQjtDQUNyQjs7QUFFRDtJQUNJLGtCQUFrQjtDQUNyQjs7QUFFRDtJQUNJLGVBQWU7Q0FDbEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0NBQ25COztBQUVEO0lBQ0ksa0JBQWtCO0NBQ3JCOztBQUdEOztJQUVJOztBQUVKO0lBQ0ksbUJBQW1CO0lBQ25CLGtCQUFrQjtDQUNyQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vbGlzdC1jb3J0ZS9saXN0LWNvcnRlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJidXR0b24ge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbmlucHV0IHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5cbi8qXG4gICBzZXJ2ZXItc2lkZS1hbmd1bGFyLXdheS5jb21wb25lbnQuY3NzXG4qL1xuXG4ubm8tZGF0YS1hdmFpbGFibGUge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuXG4vKlxuICAgICBzcmMvc3R5bGVzLmNzcyAoaS5lLiB5b3VyIGdsb2JhbCBzdHlsZSlcbiAgKi9cblxuLmRhdGFUYWJsZXNfZW1wdHkge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cblxuLyogLmNlbiB7XG4gICAgbWFyZ2luLWxlZnQ6IDE1ZW07XG4gICAgd2lkdGg6IDkwMHB4O1xufSAqL1xuXG4ubW9kYWwtY29udGVudCB7XG4gICAgd2lkdGg6IDI3MCU7XG4gICAgbWFyZ2luLWxlZnQ6IC04NSU7XG4gICAgem9vbTogODAlO1xufVxuXG4uaWNvblRpIHtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gICAgd2lkdGg6IDM1cHg7XG59XG5cbi5pY29uVGkxIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIHdpZHRoOiAxMDBweDtcbn1cblxuLnNwIHtcbiAgICBjb2xvcjogIzU2MkI4OTtcbiAgICBmb250OiBib2xkO1xufVxuXG4ubTEge1xuICAgIG1hcmdpbi1sZWZ0OiAxMGVtO1xufVxuXG4ubTIge1xuICAgIG1hcmdpbi10b3A6IDIuNWVtO1xufVxuXG4uZHAge1xuICAgIGZvbnQtc2l6ZTogMWVtO1xufVxuXG4uZXNxIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBtYXJnaW4tdG9wOiAyZW07XG59XG5cbnNwYW4ge1xuICAgIGZvbnQtZGlzcGxheTogMmVtO1xufVxuXG5cbi8qIC5tcmEge1xuICAgIG1hcmdpbi1sZWZ0OiA1ZW07XG59ICovXG5cbnRhYmxlIHtcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG59Il19 */"

/***/ }),

/***/ "./src/app/components/admin/list-corte/list-corte.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/admin/list-corte/list-corte.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\n    <div class=\"jumbotron\">\n        <ngx-alerts></ngx-alerts>\n        <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-money\"></i></span> Cortes</h1>\n        <hr class=\"my-4\">\n        <ul class=\"nav nav-tabs\">\n            <li class=\"nav-item\">\n                <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#home\"> Repartidor</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" data-toggle=\"tab\" href=\"#profile\">Restaurante</a>\n                <!-- </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" data-toggle=\"tab\" href=\"#periodo\">Periodo</a>\n            </li> -->\n        </ul>\n        <div id=\"myTabContent\" class=\"tab-content\">\n            <div class=\"tab-pane fade show active\" id=\"home\">\n                <br>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        <input type=\"text\" name=\"filterPost\" class=\"form-control\" placeholder=\"&#xf002; Buscar nombre...\" style=\"font-family:Arial, FontAwesome\" [(ngModel)]=\"filterPost\">\n                    </div>\n                </div> <br>\n                <div class=\"col\">\n                    <table class=\"table table-warning table-sm table-hover\">\n                        <thead>\n                            <tr>\n                                <th scope=\"col\">#</th>\n                                <th scope=\"col\">Nombre</th>\n                                <th scope=\"col\">Apellido</th>\n                                <th scope=\"col\">Correo</th>\n                                <th scope=\"col\">&nbsp;</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let repartidor of repartidores | filterRepartidor: filterPost | paginate: { itemsPerPage: 10, currentPage: p }; index as i\" class=\"table-light\">\n                                <th scope=\"row\">{{ i + 1 }} </th>\n                                <td scope=\"row\">{{ repartidor.username }} </td>\n                                <td scope=\"row\">{{ repartidor.lastname }} </td>\n                                <td scope=\"row\">{{ repartidor.email }} </td>\n                                <td scope=\"row\"> <button title=\"Realizar corte\" (click)=\"getListServicios(repartidor.uid)\" data-toggle=\"modal\" data-target=\"#modalCorte\" class=\"btn btn-info\"><i class=\"fa fa-scissors\"></i></button></td>\n\n                            </tr>\n                        </tbody>\n                    </table>\n                    <pagination-controls (pageChange)=\"p = $event\"></pagination-controls>\n                </div>\n            </div>\n            <div class=\"tab-pane fade\" id=\"profile\">\n                <br>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        <input type=\"text\" name=\"filterPost2\" class=\"form-control\" placeholder=\"&#xf002; Buscar nombre del convenio...\" style=\"font-family:Arial, FontAwesome\" [(ngModel)]=\"filterPost2\">\n                    </div>\n                </div> <br>\n                <div class=\"col\">\n                    <table class=\"table table-warning table-sm table-hover\">\n                        <thead>\n                            <tr>\n                                <th scope=\"col\">#</th>\n                                <th scope=\"col\">Nombre</th>\n                                <th scope=\"col\">Contacto</th>\n                                <th scope=\"col\">Correo</th>\n                                <th scope=\"col\">Estatus pago</th>\n                                <th scope=\"col\">&nbsp;</th>\n                                <th scope=\"col\">&nbsp;</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let convenio of convenios | filterRestaurante: filterPost2 | paginate: { itemsPerPage: 10, currentPage: p1 } ; index as i\" class=\"table-light\">\n                                <th scope=\"row\">{{ i + 1 }} </th>\n                                <td scope=\"row\">{{ convenio.username }} </td>\n                                <td scope=\"row\">{{ convenio.contacto }} </td>\n                                <td scope=\"row\">{{ convenio.email }} </td>\n                                <td *ngIf=\"convenio.pago == 'Vencido'\" scope=\"row\"> <span class=\"badge badge-pill badge-danger\">Vencido</span> </td>\n                                <td *ngIf=\"convenio.pago == 'Habil'\" scope=\"row\"><span class=\"badge badge-pill badge-success\">Hábil</span> </td>\n                                <td *ngIf=\"convenio.pago == 'Vencido'\" scope=\"row\"><button title=\"Dar alta\" (click)=\"darAlta(convenio.uid)\" class=\"btn btn-danger\"><i class=\"fa fa-caret-square-o-up\"></i></button></td>\n                                <td *ngIf=\"convenio.pago == 'Habil'\" scope=\"row\"><button title=\"Dar baja\" (click)=\"darBaja(convenio.uid)\" class=\"btn btn-info\"><i class=\"fa fa-archive\"></i></button></td>\n                                <td scope=\"row\"> <button title=\"Realizar corte\" (click)=\"getUidConvenio(convenio.uid)\" data-toggle=\"modal\" data-target=\"#modalCorteConvenio\" class=\"btn btn-info\"><i class=\"fa fa-scissors\"></i></button></td>\n                                <td scope=\"row\"></td>\n                                <app-modal-corte-convenio [convenioUid]='convenio.uid'></app-modal-corte-convenio>\n                            </tr>\n                        </tbody>\n                    </table>\n                    <pagination-controls (pageChange)=\"p1 = $event\"></pagination-controls>\n                </div>\n            </div>\n            <!-- <div class=\"tab-pane fade\" id=\"periodo\">\n                <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy\n                    salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork.</p>\n            </div> -->\n        </div>\n    </div>\n</section>\n<!-- Lista de servicios para corte de repartidor -->\n<div class=\"modal\" id=\"modalCorte\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <ng-container *ngFor=\"let repartidor of repartidores\">\n                    <h5 *ngIf=\"this.uidBme == repartidor.uid\" class=\"modal-title\"> Lista de servicios de {{ repartidor.username}} {{ repartidor.lastname}}</h5>\n                </ng-container>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n              </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"row pad-10 m1\">\n                    <!--Buscadores-->\n                    <div class=\"col-4\">\n                        <span class=\"sp\"><p>Fecha de inicio</p></span>\n                        <input type=\"date\" class=\"form-control\" style=\"font-family: Arial\" [(ngModel)]=\"fechaInicio\">\n                    </div>\n                    <div class=\"col-4\">\n                        <span class=\"sp\"><p>Fecha de termino</p></span>\n                        <input type=\"date\" class=\"form-control\" style=\"font-family: Arial\" [(ngModel)]=\"fechaFin\">\n                    </div>\n                    <div class=\"col-4 m2\">\n                        <button class=\"btn btn-primary\" (click)=\"corteFiltro(fechaInicio, fechaFin)\" data-toggle=\"modal\" data-target=\"#modalListCorte\"><i class=\"fa fa-search\"></i> Filtrar</button>\n                        <!-- <button class=\"btn btn-primary\" (click)=\"resetFiltro()\" data-toggle=\"modal\" data-target=\"#modalListCorte\"><i class=\"fa fa-search\"></i> Reset </button> -->\n                    </div>\n                </div>\n                <br>\n                <div class=\"row pad-10\">\n                    <div style=\"margin-left: 10em;\" class=\"table-responsive text-nowrap\">\n                        <table class=\"table table-active table-sm table-hover ml-5 mr-5 w-75\">\n                            <thead>\n                                <tr>\n                                    <th scope=\"col\">Clave</th>\n                                    <th scope=\"col\">Fecha/hora</th>\n                                    <th scope=\"col\">Cliente</th>\n                                    <th scope=\"col\">Tipo</th>\n                                    <th scope=\"col\"> Método de pago</th>\n                                    <th scope=\"col\">Estado</th>\n                                    <th scope=\"col\">Opciones</th>\n                                </tr>\n                            </thead>\n                            <tbody>\n                                <tr *ngFor=\"let servicio of servicios |  paginate: { itemsPerPage: 10, currentPage: p2 }\" class=\"table-light\">\n                                    <th scope=\"row\"> {{ servicio.clave }} </th>\n                                    <th scope=\"row\">{{servicio.fecha | date:'medium'}} </th>\n                                    <ng-container *ngIf=\"servicio.cliente\">\n                                        <td>{{ servicio.cliente }} </td>\n                                    </ng-container>\n                                    <ng-container *ngFor=\"let usuario of usuarios\">\n                                        <ng-container *ngIf=\"usuario.uid == servicio.uidCliente\">\n                                            <td *ngIf=\"usuario.uid == servicio.uidCliente\"> {{ usuario.username }} {{ usuario.lastname }} </td>\n                                        </ng-container>\n                                    </ng-container>\n                                    <td *ngIf=\"servicio.tipo && servicio.abierto == null\"><img class=\"iconTi\" src=\"../../../../assets/imgs/tipo1.png\" title=\"Servicio pide lo que quieras.\"></td>\n                                    <td *ngIf=\"servicio.abierto == 'Abierto plataforma'\"><img class=\"iconTi\" src=\"../../../../assets/imgs/tipo2.png\" title=\"Servicio de convenio.\"></td>\n                                    <td *ngIf=\"servicio.metodo_pago == 'Efectivo'\"><img class=\"iconTi\" src=\"../../../../assets/imgs/efectivo.png\" title=\"Pago en efectivo\"></td>\n                                    <td *ngIf=\"servicio.metodo_pago == 'Tarjeta'\"><img class=\"iconTi\" src=\"../../../../assets/imgs/tarjeta-de-credito.png\" title=\"Pago con tarjeta\"></td>\n                                    <!-- Estados de pide lo que quiera -->\n                                    <td *ngIf=\"servicio.estatus == 'Cancelado'\"><span class=\"badge badge-danger\">Cancelado</span></td>\n                                    <td *ngIf=\"servicio.estatus == 'Terminado'\"><span class=\"badge badge-info\">Terminado</span></td>\n                                    <td><a class=\"bz btn btn-outline-primary\" target=\"blank\" title=\"Detalle del servicio.\" routerLink=\"../../servicio/{{servicio.uid}}\"><i class=\"fa fa-caret-square-o-down\" aria-hidden=\"true\"></i> </a> </td>\n                                </tr>\n                            </tbody>\n                            <tbody *ngIf=\"servicios?.length == 0\">\n                                <tr>\n                                    <td colspan=\"8\" class=\"no-data-available\">No data!</td>\n                                </tr>\n                                <tbody>\n                        </table>\n                        <pagination-controls (pageChange)=\"p2 = $event\"></pagination-controls>\n\n                    </div>\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cerrar</button>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- Corte de repartidor -->\n<div class=\"modal\" id=\"modalListCorte\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\">Corte</h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n              </button>\n            </div>\n            <div class=\"esq mr-3\">\n                <button type=\"button\" class=\"btn btn-outline-primary\" title=\"Imprimir\" printSectionId=\"print-section\" ngxPrint><i class=\"fa fa-print\" aria-hidden=\"true\"></i></button>\n            </div>\n            <div *ngIf=\"listasServicios != null\" class=\"modal-body\" id=\"print-section\">\n                <div class=\"row\">\n                    <div class=\"card border-primary mb-3 ml-5\" style=\"max-width: auto;\">\n                        <div class=\"card-header\">Periodo de corte del {{ fechaInicio | date: 'dd/MM/yyyy' }} al {{ fechaFin | date: 'dd/MM/yyyy' }} </div>\n                        <div class=\"card-body\">\n                            <h5>Corte repartidor</h5>\n                            <div class=\"row pad-14\">\n                                <div class=\"col-3\">\n                                    <ng-container *ngFor=\"let repartidor of repartidores\">\n                                        <ng-container *ngIf=\"this.uidBme == repartidor.uid\">\n                                            <h4 class=\"card-title\">{{repartidor.username}} {{ repartidor.lastaname}} </h4>\n                                            <h6 class=\"card-subtitle text-muted\">{{ repartidor.uid }}</h6>\n                                            <p class=\"card-text\">{{ repartidor.address }} </p>\n                                            <p class=\"card-text\">{{ repartidor.phone }} </p>\n                                            <p class=\"card-text\">{{ repartidor.email }} </p>\n                                        </ng-container>\n                                    </ng-container>\n                                </div>\n                                <div class=\"col-5 mra\" style=\"margin-top: -2em;\">\n                                    <h6><i class=\"fa fa-bookmark-o\"></i> Resumen de corte de servicios regulares </h6>\n                                    <table class=\"table-active table-sm\">\n                                        <thead>\n                                            <tr>\n                                                <th scope=\"col\">Método</th>\n                                                <th scope=\"col\">Total servicio</th>\n                                                <th scope=\"col\">Total Corte Repartidor</th>\n                                                <th scope=\"col\">Total Corte BringMe</th>\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <tr class=\"table-success\">\n                                                <td scope=\"col\">Efectivo</td>\n                                                <td scope=\"col\">{{ this.totalServicioEfec | currency }} </td>\n                                                <td scope=\"col\">{{ this.totalCorteEfecRep | currency }} </td>\n                                                <td scope=\"col\">{{ this.totalCorteEfecBme | currency }} </td>\n                                            </tr>\n                                            <tr class=\"table-info\">\n                                                <td scope=\"col\">Tarjeta</td>\n                                                <td scope=\"col\">{{ this.totalServicioTar | currency }} </td>\n                                                <td scope=\"col\">{{ this.totalCorteTarRep | currency }} </td>\n                                                <td scope=\"col\">{{ this.totalCorteTarBme | currency }} </td>\n                                            </tr>\n                                        </tbody>\n                                    </table>\n                                    <!-- Resumen de cortes de convenio  -->\n                                    <h6><i class=\"fa fa-bookmark-o\"></i> Resumen de corte de servicios de convenio </h6>\n                                    <table class=\"table-active table-sm\">\n                                        <thead>\n                                            <tr>\n                                                <th scope=\"col\">Método</th>\n                                                <th scope=\"col\">Total servicio</th>\n                                                <th scope=\"col\">Total Corte Repartidor</th>\n                                                <th scope=\"col\">Total Corte BringMe</th>\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <ng-container>\n                                                <tr class=\"table-success\">\n                                                    <td scope=\"col\">Efectivo</td>\n                                                    <td scope=\"col\">{{ this.totalServicioEfec2 | currency }} </td>\n                                                    <td scope=\"col\">{{ this.totalCorteEfecRep2 | currency }} </td>\n                                                    <td scope=\"col\">{{ this.totalCorteEfecBme2 | currency }} </td>\n                                                </tr>\n                                            </ng-container>\n\n                                            <!-- <tr class=\"table-info\">\n                                                <td scope=\"col\">Tarjeta</td>\n                                                <td scope=\"col\">{{ this.totalServicioTar2 | currency }} </td>\n                                                <td scope=\"col\">{{ this.totalCorteTarRep2 | currency }} </td>\n                                                <td scope=\"col\">{{ this.totalCorteTarBme2 | currency }} </td>\n                                            </tr> -->\n                                        </tbody>\n                                    </table>\n                                </div>\n                                <div class=\"col-3\">\n                                    <table class=\"table-active table-sm\">\n                                        <thead>\n                                            <tr>\n                                                <th scope=\"col\">Concepto</th>\n                                                <th scope=\"col\">Cantidad</th>\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <ng-container>\n                                                <tr class=\"table-success\">\n                                                    <td scope=\"col\">BringMe te paga por servicios de tarjeta:</td>\n                                                    <td scope=\"col\">\n                                                        <ng-container *ngIf=\"totalCorteTarRep; else elseTemplate\">\n                                                            <!-- <span class=\"badge badge-pill badge-info dp\">{{ totalCorteTarRep + totalCorteTarRep2  | currency }}</span> -->\n                                                            <span class=\"badge badge-pill badge-primary dp\">{{ this.totalCorteTarRep | currency }}</span>\n                                                        </ng-container>\n                                                        <ng-template #elseTemplate>\n                                                            <span class=\"badge badge-pill badge-primary dp\">{{ totalCorteTarRep | currency }}</span>\n                                                        </ng-template>\n                                                        <ng-template #elseTemplate>\n                                                            <span class=\"badge badge-pill badge-primary dp\">{{ totalCorteTarRep2 | currency }}</span>\n                                                        </ng-template>\n                                                    </td>\n                                                </tr>\n                                            </ng-container>\n                                            <ng-container>\n                                                <tr class=\"table-success\">\n                                                    <td scope=\"col\">Tu debes a BringMe:</td>\n                                                    <td scope=\"col\">\n                                                        <ng-container *ngIf=\"totalCorteEfecBme && totalCorteEfecBme2; else elseTemplate2\">\n                                                            <span class=\"badge badge-pill badge-warning dp\">{{ totalCorteEfecBme + totalCorteEfecBme2 | currency }}</span>\n                                                        </ng-container>\n                                                        <ng-template #elseTemplate2>\n                                                            <span class=\"badge badge-pill badge-warning dp\">{{ totalCorteEfecBme | currency }}</span>\n                                                            <span class=\"badge badge-pill badge-warning dp\">{{ totalCorteEfecBme2 | currency }}</span>\n                                                        </ng-template>\n                                                    </td>\n                                                </tr>\n                                            </ng-container>\n                                            <ng-container>\n                                                <tr class=\"table-success\">\n                                                    <td scope=\"col\">Tu Corte semanal:</td>\n                                                    <td scope=\"col\">\n                                                        <ng-container *ngIf=\"sumaCorteRep && sumaCorteRep2; else elseTemplate3\">\n                                                            <span class=\"badge badge-pill badge-info dp\">{{ sumaCorteRep + sumaCorteRep2 | currency }}</span>\n                                                        </ng-container>\n                                                        <ng-template #elseTemplate3>\n                                                            <span class=\"badge badge-pill badge-info dp\">{{ this.sumaCorteRep | currency }}</span>\n                                                            <span class=\"badge badge-pill badge-info dp\">{{ sumaCorteRep2 | currency }}</span>\n                                                        </ng-template>\n                                                    </td>\n                                                </tr>\n                                            </ng-container>\n                                            <ng-container>\n                                                <tr class=\"table-success\">\n                                                    <td scope=\"col\"><b>Saldo a favor</b></td>\n                                                    <td scope=\"col\">\n                                                        <ng-container *ngIf=\"totalCorteTarRep && totalCorteEfecBme && totalCorteEfecBme2; else elseTemplate4\">\n                                                            <span class=\"badge badge-pill badge-success dp\">{{ (totalCorteEfecBme + totalCorteEfecBme2) -totalCorteTarRep | currency }}</span>\n                                                        </ng-container>\n                                                        <ng-template #elseTemplate4>\n                                                            <span class=\"badge badge-pill badge-success dp\">{{ totalCorteTarRep - totalCorteEfecBme | currency }}</span>\n                                                            <span class=\"badge badge-pill badge-success dp\">{{ totalCorteTarRep - totalCorteEfecBme2 | currency }}</span>\n                                                            <span class=\"badge badge-pill badge-success dp\">{{  (sumaCorteRep + sumaCorteRep2) - (totalCorteEfecBme + totalCorteEfecBme2)   | currency }}</span>\n                                                        </ng-template>\n                                                    </td>\n                                                </tr>\n                                            </ng-container>\n                                            <!-- <tr class=\"table-info\">\n                                                        <td scope=\"col\">Tarjeta</td>\n                                                        <td scope=\"col\">{{ this.totalServicioTar2 | currency }} </td>\n                                                        <td scope=\"col\">{{ this.totalCorteTarRep2 | currency }} </td>\n                                                        <td scope=\"col\">{{ this.totalCorteTarBme2 | currency }} </td>\n                                                    </tr> -->\n                                        </tbody>\n                                    </table>\n                                    <img class=\"iconTi1\" style=\"text-align: right;\" src=\"../../../../assets/imgs/log3.png\" title=\"Logo\">\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <!-- Tabla de servicios regulares para el corte del repartidor -->\n                    <div class=\"row\">\n                        <div class=\"row ml-5\" style=\"text-align: center;\">\n                            <h4><i class=\"fa fa-bookmark-o\"></i> Resumen de servicios regulares </h4>\n                        </div>\n                        <div class=\"table-responsive text-nowrap\">\n                            <table class=\"table table-active table-sm table-hover ml-5 mr-5 w-50\">\n                                <thead>\n                                    <tr>\n                                        <th>#</th>\n                                        <th scope=\"col\">Clave</th>\n                                        <th>UID</th>\n                                        <th scope=\"col\">Fecha</th>\n                                        <th scope=\"col\">Cliente</th>\n                                        <th scope=\"col\">Tipo</th>\n                                        <th scope=\"col\">Método de pago</th>\n                                        <!-- <th scope=\"col\">Estado</th> -->\n                                        <th scope=\"col\">Total producto</th>\n                                        <th scope=\"col\">Total servicio</th>\n                                        <th scope=\"col\">Comisión</th>\n                                        <th scope=\"col\">Total neto sin comisión</th>\n                                        <th scope=\"col\">Corte repartidor</th>\n                                        <th scope=\"col\">Corte para BringMe</th>\n                                        <th scope=\"col\">Opciones</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr *ngFor=\"let servicio of listasServicios |  paginate: { itemsPerPage: 10, currentPage: p3 }; index as i\" class=\"table-light\">\n                                        <th scope=\"row\">{{ i + 1 }} </th>\n                                        <th scope=\"row\"> {{ servicio.clave }} </th>\n                                        <th>{{ servicio.uid }} </th>\n                                        <th scope=\"row\">{{servicio.fecha | date:'medium'}} </th>\n                                        <ng-container *ngFor=\"let usuario of usuarios\">\n                                            <ng-container *ngIf=\"usuario.uid == servicio.uidCliente\">\n                                                <td *ngIf=\"usuario.uid == servicio.uidCliente\"> {{ usuario.username }} {{ usuario.lastname }} </td>\n                                            </ng-container>\n                                        </ng-container>\n                                        <td *ngIf=\"servicio.tipo\"><img class=\"iconTi\" src=\"../../../../assets/imgs/tipo1.png\" title=\"Servicio pide lo que quieras.\"></td>\n                                        <td *ngIf=\"servicio.metodo_pago == 'Efectivo'\"><img class=\"iconTi\" src=\"../../../../assets/imgs/money.png\" title=\"Pago en efectivo\"></td>\n                                        <td *ngIf=\"servicio.metodo_pago == 'Tarjeta'\"><img class=\"iconTi\" src=\"../../../../assets/imgs/tarjeta-de-credito.png\" title=\"Pago con tarjeta\"></td>\n                                        <!-- Estados de pide lo que quiera -->\n                                        <!-- <td *ngIf=\"servicio.estatus == 'Cancelado'\"><span class=\"badge badge-danger\">Cancelado</span></td>\n                                        <td *ngIf=\"servicio.estatus == 'Terminado'\"><span class=\"badge badge-info\">Terminado</span></td> -->\n                                        <td>{{ servicio.totalProductos | currency }} </td>\n                                        <td>{{ servicio.totalServicio | currency }} </td>\n                                        <td>{{ servicio.comision | currency }} </td>\n                                        <td>{{ servicio.totalProductos + servicio.totalServicio | currency }} </td>\n                                        <td>{{ servicio.corteRep | currency }} </td>\n                                        <td>{{ servicio.corteBme | currency }} </td>\n                                        <td><a class=\"bz btn btn-outline-primary\" target=\"blank\" title=\"Detalle del servicio.\" routerLink=\"../../servicio/{{servicio.uid}}\"><i class=\"fa fa-caret-square-o-down\" aria-hidden=\"true\"></i> </a> </td>\n                                    </tr>\n                                    <tr>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <td></td>\n                                        <!-- <td></td> -->\n                                        <td><b>Total:</b></td>\n                                        <td></td>\n                                        <td><span class=\"badge badge-pill badge-success dp\">{{ this.sumaCorteRep | currency }} </span></td>\n                                        <td><span class=\"badge badge-pill badge-success dp\">{{ this.sumaCorteBme | currency }} </span></td>\n                                        <td><span class=\"badge badge-pill badge-primary dp\"></span></td>\n\n                                    </tr>\n                                </tbody>\n                                <tbody *ngIf=\"servicios?.length == 0\">\n                                    <tr>\n                                        <td colspan=\"8\" class=\"no-data-available\">No data!</td>\n                                    </tr>\n                                    <tbody>\n                            </table>\n                        </div>\n                        <pagination-controls (pageChange)=\"p3 = $event\"></pagination-controls>\n\n                        <!--Lista de  Servicios de convenio hechos por los repartidores -->\n                        <div class=\"row\">\n                            <div class=\"row ml-5\" style=\"text-align: center;\">\n                                <h4><i class=\"fa fa-bookmark-o\"></i> Resumen de servicios a convenios </h4>\n                            </div>\n                            <div class=\"table-responsive text-nowrap\">\n                                <table class=\"table table-active table-sm table-hover ml-5 mr-5 w-75\">\n                                    <thead>\n                                        <tr>\n                                            <th>#</th>\n                                            <th scope=\"col\">Clave</th>\n                                            <th>UID</th>\n                                            <th scope=\"col\">Fecha</th>\n                                            <th scope=\"col\">Convenio</th>\n                                            <th scope=\"col\">Tipo</th>\n                                            <th scope=\"col\">Método de pago</th>\n                                            <!-- <th scope=\"col\">Estado</th> -->\n                                            <th scope=\"col\">Total producto</th>\n                                            <th scope=\"col\">Total servicio</th>\n                                            <th scope=\"col\">Comisión</th>\n                                            <th scope=\"col\">Total neto sin comisión</th>\n                                            <th scope=\"col\">Corte repartidor</th>\n                                            <th scope=\"col\">Corte para BringMe</th>\n                                            <th scope=\"col\">Opciones</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr *ngFor=\"let servicio of listasServiciosConvenio1 |  paginate: { itemsPerPage: 10, currentPage: p3 }; index as i\" class=\"table-light\">\n                                            <th scope=\"row\">{{ i + 1 }} </th>\n                                            <th scope=\"row\"> {{ servicio.clave }} </th>\n                                            <th>{{ servicio.uid }} </th>\n                                            <th scope=\"row\">{{servicio.fecha | date:'medium'}} </th>\n                                            <ng-container *ngFor=\"let usuario of usuarios\">\n                                                <ng-container *ngIf=\"usuario.uid == servicio.uidRestaurante\">\n                                                    <td *ngIf=\"usuario.uid == servicio.uidRestaurante\"> {{ usuario.username }} </td>\n                                                </ng-container>\n                                            </ng-container>\n                                            <td *ngIf=\"servicio.tipo\"><img class=\"iconTi\" src=\"../../../../assets/imgs/tipo2.png\" title=\"Servicio de convenio.\"></td>\n                                            <td *ngIf=\"servicio.metodo_pago == 'Efectivo'\"><img class=\"iconTi\" src=\"../../../../assets/imgs/money.png\" title=\"Pago en efectivo\"></td>\n                                            <td *ngIf=\"servicio.metodo_pago == 'Tarjeta'\"><img class=\"iconTi\" src=\"../../../../assets/imgs/tarjeta-de-credito.png\" title=\"Pago con tarjeta\"></td>\n                                            <!-- Estados de pide lo que quiera -->\n                                            <!-- <td *ngIf=\"servicio.estatus == 'Cancelado'\"><span class=\"badge badge-danger\">Cancelado</span></td>\n                                            <td *ngIf=\"servicio.estatus == 'Terminado'\"><span class=\"badge badge-info\">Terminado</span></td> -->\n                                            <td>{{ servicio.totalProductos | currency }} </td>\n                                            <td>{{ servicio.totalServicio | currency }} </td>\n                                            <td>{{ servicio.comision | currency }} </td>\n                                            <td>{{ servicio.totalProductos + servicio.totalServicio | currency}} </td>\n                                            <td>{{ servicio.corteRep | currency }} </td>\n                                            <td>{{ servicio.corteBme | currency }} </td>\n                                            <td><a class=\"bz btn btn-outline-primary\" target=\"blank\" title=\"Detalle del servicio.\" routerLink=\"../../servicio/{{servicio.uid}}\"><i class=\"fa fa-caret-square-o-down\" aria-hidden=\"true\"></i> </a> </td>\n                                        </tr>\n                                        <tr>\n                                            <!-- <td></td> -->\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td></td>\n                                            <td><b>Total:</b></td>\n                                            <td><span class=\"badge badge-pill badge-success dp\">{{ this.sumaCorteRep2 | currency }} </span></td>\n                                            <td><span class=\"badge badge-pill badge-success dp\">{{ this.sumaCorteBme2 | currency }} </span></td>\n                                        </tr>\n                                    </tbody>\n                                    <tbody *ngIf=\"servicios?.length == 0\">\n                                        <tr>\n                                            <td colspan=\"8\" class=\"no-data-available\">No data!</td>\n                                        </tr>\n                                        <tbody>\n                                </table>\n                            </div>\n                            <pagination-controls (pageChange)=\"p3 = $event\"></pagination-controls>\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div style=\"margin-right: 20em;\" class=\"modal-footer\">\n                <!-- <button type=\"button\" class=\"btn btn-primary\">Save changes</button> -->\n                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Cerrar</button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/admin/list-corte/list-corte.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/admin/list-corte/list-corte.component.ts ***!
  \*********************************************************************/
/*! exports provided: ListCorteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListCorteComponent", function() { return ListCorteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_corte_cortes_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/corte/cortes.service */ "./src/app/service/corte/cortes.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var src_app_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/pedidos/pedidos.service */ "./src/app/service/pedidos/pedidos.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");







var ListCorteComponent = /** @class */ (function () {
    function ListCorteComponent(corte, afs, _pedidoPro, auth) {
        this.corte = corte;
        this.afs = afs;
        this._pedidoPro = _pedidoPro;
        this.auth = auth;
        // Para extraer el uid del repartidor y enviarlo al modal
        this.convenioUid = null;
        this.filterPost = '';
        this.filterPost2 = '';
        this.pageActual = 1;
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    ListCorteComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.getUidConvenio(this.uidConvenio);
        this.getAllUsuarios();
        this.getAllCortes();
        // Sucursal
        console.log('user', this.user);
        this.auth.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.afs.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    _this.getListRepartidores(_this.Uidsucursal);
                    _this.getListConvenios(_this.Uidsucursal);
                    console.log('usuer', _this.Uidsucursal);
                });
            }
        });
    };
    ListCorteComponent.prototype.getListRepartidores = function (uidSucursal) {
        var _this = this;
        this.afs.collection('users', function (ref) { return ref.where('tipo', '==', 2)
            .where('uidSucursal', '==', uidSucursal); })
            .valueChanges().subscribe(function (data) {
            _this.repartidores = data;
        });
    };
    ListCorteComponent.prototype.getListConvenios = function (uidSucursal) {
        var _this = this;
        this.afs.collection('users', function (ref) { return ref.where('tipo', '==', 3)
            .where('uidSucursal', '==', uidSucursal); })
            .valueChanges().subscribe(function (data) {
            _this.convenios = data;
            console.log('convenios', data);
        });
    };
    ListCorteComponent.prototype.getListServicios = function (uidRepartidor) {
        var _this = this;
        this.afs.collection('servicios', function (ref) {
            return ref.where('uidRepartidor', '==', uidRepartidor).where('estatus', '==', 'Terminado');
        })
            .valueChanges().subscribe(function (data) {
            _this.servicios = data;
            _this.uidBme = uidRepartidor;
            console.log(data);
        });
    };
    ListCorteComponent.prototype.getAllUsuarios = function () {
        var _this = this;
        this._pedidoPro.getAllUsuarios().subscribe(function (usuarios) {
            _this.usuarios = usuarios;
        });
    };
    // Corte de repartidor
    ListCorteComponent.prototype.corteFiltro = function (fechaInicio, fechaFin) {
        var _this = this;
        // console.log(fechaInicio, fechaFin, this.uidBme);
        //   this.ver = moment().isBetween(fechaInicio, fechaFin);
        //     console.log('verificacion', this.ver);
        //   if (  this.ver === true && this.uidBme ) {
        console.log('Primer filtro', this.date1 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaInicio).format('x'), 'fin', this.date2 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaFin).format('x'));
        this.afs.collection('servicios', function (ref) {
            return ref.orderBy('fecha', 'asc').startAt(_this.date1).endAt(_this.date2)
                .where('abierto', '==', null).where('estatus', '==', 'Terminado')
                .where('uidRepartidor', '==', _this.uidBme);
        }).valueChanges().subscribe(function (data) {
            _this.listasServicios = data;
            console.log('Lista servicios tipo 1', _this.listasServicios);
            // Suma de los cortes del repartidor
            _this.sumaCorteRep = _this.listasServicios.reduce(function (acc, obj) { return acc + (obj.corteRep); }, 0);
            console.log('sumaCorteRep', _this.sumaCorteRep);
            // Suma de los cortes del repartidor
            _this.sumaCorteBme = _this.listasServicios.reduce(function (acc, obj) { return acc + (obj.corteBme); }, 0);
            console.log('sumaCorteBme', _this.sumaCorteBme);
            // Servicios de convenio hechos por los repartidores
            console.log('inicio', _this.date1 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaInicio).format('x'), 'fin', _this.date2 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaFin).format('x'));
            _this.afs.collection('servicios', function (ref) {
                return ref.orderBy('fecha', 'asc').startAt(_this.date1).endAt(_this.date2)
                    .where('abierto', '==', 'Abierto plataforma').where('estatus', '==', 'Terminado')
                    .where('uidRepartidor', '==', _this.uidBme);
            }).valueChanges().subscribe(function (data2) {
                _this.listasServiciosConvenio1 = data2;
                console.log('querySerConv', _this.listasServiciosConvenio1);
                // Suma de los cortes del repartidor
                _this.sumaCorteRep2 = _this.listasServiciosConvenio1.reduce(function (acc, obj) { return acc + (obj.corteRep); }, 0);
                // Suma de los cortes del repartidor
                _this.sumaCorteBme2 = _this.listasServiciosConvenio1.reduce(function (acc, obj) { return acc + (obj.corteBme); }, 0);
            });
            // Servicios de convenio hechos por los repartidores en efectivo
            console.log('inicio', _this.date1 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaInicio).format('x'), 'fin', _this.date2 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaFin).format('x'));
            _this.afs.collection('servicios', function (ref) {
                return ref.orderBy('fecha', 'asc').startAt(_this.date1).endAt(_this.date2)
                    .where('abierto', '==', 'Abierto plataforma').where('estatus', '==', 'Terminado')
                    .where('uidRepartidor', '==', _this.uidBme).where('metodo_pago', '==', 'Efectivo');
            }).valueChanges().subscribe(function (data2) {
                _this.listasServiciosConvenio1Efectivo = data2;
                // console.log('querySerConv', this.listasServiciosConvenio1Efectivo);
                // Suma total de los servicios del repartidor
                _this.totalServicioEfec2 = _this.listasServiciosConvenio1Efectivo.reduce(function (acc, obj) { return acc + (obj.totalServicio); }, 0);
                // Suma total de los corte del repartidor en efectivo
                _this.totalCorteEfecRep2 = _this.listasServiciosConvenio1Efectivo.reduce(function (acc, obj) { return acc + (obj.corteRep); }, 0);
                // Suma total de los corte del repartidor en efectivo
                _this.totalCorteEfecBme2 = _this.listasServiciosConvenio1Efectivo.reduce(function (acc, obj) { return acc + (obj.corteBme); }, 0);
            });
            // Registros de solo tarjeta de convenio
            console.log('inicio', _this.date1 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaInicio).format('x'), 'fin', _this.date2 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaFin).format('x'));
            _this.afs.collection('servicios', function (ref) {
                return ref.orderBy('fecha').startAt(_this.date1).endAt(_this.date2).where('tipo', '==', 'Abierto plataforma').where('estatus', '==', 'Terminado')
                    .where('metodo_pago', '==', 'Tarjeta')
                    .where('uidRepartidor', '==', _this.uidBme);
            }).valueChanges().subscribe(function (data1) {
                _this.listasServiciosConvenio1Tarjeta = data1;
                // console.log('query Tarjeta', this.listasServiciosConvenio1Tarjeta);
                // Suma total de los servicios del repartidor
                _this.totalServicioTar2 = _this.listasServiciosConvenio1Tarjeta.reduce(function (acc, obj) { return acc + (obj.totalServicio); }, 0);
                // Suma total de los corte del repartidor en efectivo
                _this.totalCorteTarRep2 = _this.listasServiciosConvenio1Tarjeta.reduce(function (acc, obj) { return acc + (obj.corteRep); }, 0);
                // Suma total de los corte del repartidor en efectivo
                _this.totalCorteTarBme2 = _this.listasServiciosConvenio1Tarjeta.reduce(function (acc, obj) { return acc + (obj.corteBme); }, 0);
            });
            // Registros de solo tarjeta
            console.log('inicio', _this.date1 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaInicio).format('x'), 'fin', _this.date2 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaFin).format('x'));
            _this.afs.collection('servicios', function (ref) {
                return ref.orderBy('fecha').startAt(_this.date1).endAt(_this.date2).where('abierto', '==', null)
                    .where('estatus', '==', 'Terminado')
                    .where('metodo_pago', '==', 'Tarjeta')
                    .where('uidRepartidor', '==', _this.uidBme);
            }).valueChanges().subscribe(function (data1) {
                _this.listasTarjeta = data1;
                // console.log('query Tarjeta', this.listasTarjeta);
                // Suma total de los servicios del repartidor
                _this.totalServicioTar = _this.listasTarjeta.reduce(function (acc, obj) { return acc + (obj.totalServicio); }, 0);
                // Suma total de los corte del repartidor en efectivo
                _this.totalCorteTarRep = _this.listasTarjeta.reduce(function (acc, obj) { return acc + (obj.corteRep); }, 0);
                // Suma total de los corte del repartidor en efectivo
                _this.totalCorteTarBme = _this.listasTarjeta.reduce(function (acc, obj) { return acc + (obj.corteBme); }, 0);
            });
            // Registros de solo efectivo
            console.log('inicio', _this.date1 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaInicio).format('x'), 'fin', _this.date2 = moment__WEBPACK_IMPORTED_MODULE_5__(fechaFin).format('x'));
            _this.afs.collection('servicios', function (ref) {
                return ref.orderBy('fecha').startAt(_this.date1).endAt(_this.date2).where('abierto', '==', null)
                    .where('estatus', '==', 'Terminado')
                    .where('metodo_pago', '==', 'Efectivo')
                    .where('uidRepartidor', '==', _this.uidBme);
            }).valueChanges().subscribe(function (data2) {
                _this.listasEfectivo = data2;
                // console.log('query Efectivo', this.listasEfectivo);
                // Suma total de los servicios del repartidor
                _this.totalServicioEfec = _this.listasEfectivo.reduce(function (acc, obj) { return acc + (obj.totalServicio); }, 0);
                // Suma total de los corte del repartidor en efectivo
                _this.totalCorteEfecRep = _this.listasEfectivo.reduce(function (acc, obj) { return acc + (obj.corteRep); }, 0);
                // Suma total de los corte del repartidor en efectivo
                _this.totalCorteEfecBme = _this.listasEfectivo.reduce(function (acc, obj) { return acc + (obj.corteBme); }, 0);
            });
        });
        // } else {
        //   alert('Parece que no se encontraron registros.');
        // }
    };
    ListCorteComponent.prototype.getAllCortes = function () {
        var _this = this;
        this.corte.getAllCortes().subscribe(function (cortes) {
            _this.cortes = cortes;
        });
    };
    ListCorteComponent.prototype.darBaja = function (uid) {
        var confirmacion = confirm('¿Estas seguro de dar de baja a este convenio?');
        if (confirmacion) {
            this.auth.vencido(uid);
        }
    };
    ListCorteComponent.prototype.darAlta = function (uid) {
        var confirmacion = confirm('¿Estas seguro de activar este convenio?');
        if (confirmacion) {
            this.auth.habil(uid);
        }
    };
    ListCorteComponent.prototype.getUidConvenio = function (uid) {
        console.log('uidConevnio', uid);
        this.uidConvenio = uid;
        this.corte.getCorteUidConvenio(uid);
    };
    ListCorteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-corte',
            template: __webpack_require__(/*! ./list-corte.component.html */ "./src/app/components/admin/list-corte/list-corte.component.html"),
            styles: [__webpack_require__(/*! ./list-corte.component.css */ "./src/app/components/admin/list-corte/list-corte.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_service_corte_cortes_service__WEBPACK_IMPORTED_MODULE_2__["CortesService"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            src_app_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_4__["PedidosService"], _service_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"]])
    ], ListCorteComponent);
    return ListCorteComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.css":
/*!********************************************************************************************!*\
  !*** ./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.css ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vbGlzdC1kZXRhaWwtc2VydmljaW9zL2xpc3QtZGV0YWlsLXNlcnZpY2lvcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  list-detail-servicios works!\n</p>\n"

/***/ }),

/***/ "./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: ListDetailServiciosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListDetailServiciosComponent", function() { return ListDetailServiciosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ListDetailServiciosComponent = /** @class */ (function () {
    function ListDetailServiciosComponent() {
    }
    ListDetailServiciosComponent.prototype.ngOnInit = function () {
    };
    ListDetailServiciosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-detail-servicios',
            template: __webpack_require__(/*! ./list-detail-servicios.component.html */ "./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.html"),
            styles: [__webpack_require__(/*! ./list-detail-servicios.component.css */ "./src/app/components/admin/list-detail-servicios/list-detail-servicios.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ListDetailServiciosComponent);
    return ListDetailServiciosComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.css":
/*!**************************************************************************************************!*\
  !*** ./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.css ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\n.iconTi {\n    height: 35px;\n    width: 35px;\n}\n\ntable {\n    margin-right: 110px;\n    margin-left: 110px;\n    text-align: center;\n}\n\n.bz {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\np {\n    color: #562B89;\n}\n\n.modal-content {\n    width: 263%;\n    margin-left: -79%;\n}\n\n.bt {\n    margin-top: 2.5em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LWhvc3RvcmlhbC1zZXJ2aWNpb3MvbGlzdC1ob3N0b3JpYWwtc2VydmljaW9zLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxnQkFBZ0I7SUFDaEIsWUFBWTtDQUNmOztBQUVEO0lBQ0ksYUFBYTtJQUNiLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksZUFBZTtDQUNsQjs7QUFFRDtJQUNJLFlBQVk7SUFDWixrQkFBa0I7Q0FDckI7O0FBRUQ7SUFDSSxrQkFBa0I7Q0FDckIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FkbWluL2xpc3QtaG9zdG9yaWFsLXNlcnZpY2lvcy9saXN0LWhvc3RvcmlhbC1zZXJ2aWNpb3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5pY29uVGkge1xuICAgIGhlaWdodDogMzVweDtcbiAgICB3aWR0aDogMzVweDtcbn1cblxudGFibGUge1xuICAgIG1hcmdpbi1yaWdodDogMTEwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDExMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJ6IHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5pbnB1dCB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxucCB7XG4gICAgY29sb3I6ICM1NjJCODk7XG59XG5cbi5tb2RhbC1jb250ZW50IHtcbiAgICB3aWR0aDogMjYzJTtcbiAgICBtYXJnaW4tbGVmdDogLTc5JTtcbn1cblxuLmJ0IHtcbiAgICBtYXJnaW4tdG9wOiAyLjVlbTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <!-- <ngx-alerts></ngx-alerts> -->\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-history\"></i></span> Pedidos</h1>\n        <strong class=\"mr-auto\">Sucursal</strong>\n        <div class=\"row\">\n            <div class=\"col\">\n                <div class=\"form-group\">\n                    <!--Buscador-->\n                    <!-- <input type=\"text\" class=\"form-control\" placeholder=\"&#xf002; Buscar nombre...\" style=\"font-family:Arial, FontAwesome\" name=\"filterPost\" [(ngModel)]=\"filterPost\"> -->\n                </div>\n            </div>\n            <!-- <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modnaval\" data-target=\"#modalAdmin\"> <i class=\"fa fa-plus\"></i> Nuevo administrador </button> -->\n        </div>\n    </div>\n\n\n    <div class=\"col m-5\">\n\n        <mat-form-field>\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n        </mat-form-field>\n\n        <div style=\"margin-right: 10em;\">\n            <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n                <!-- CLAVE Column -->\n                <ng-container matColumnDef=\"clave\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Clave </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.clave}} </td>\n                </ng-container>\n\n                <!-- TIPO Column -->\n                <ng-container matColumnDef=\"tipo\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Tipo </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <ng-container *ngIf=\"element.tipo && element.abierto == null\">\n                            <img class=\"iconTi\" src=\"../../../../assets/imgs/tipo1.png\" title=\"Servicio pide lo que quieras.\">\n                        </ng-container>\n                        <ng-container *ngIf=\"element.abierto == 'Abierto plataforma'\">\n                            <img class=\"iconTi\" src=\"../../../../assets/imgs/tipo2.png\" title=\"Servicio de convenio.\">\n                        </ng-container>\n                    </td>\n                </ng-container>\n\n                <!-- ESTATUS Column -->\n                <ng-container matColumnDef=\"estatus\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estatus </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <ng-container *ngIf=\"element.estatus == 'Terminado' \">\n                            <button type=\"button\" style=\"text-align: center;\" class=\"btn btn-success\">                     \n                                 <mat-icon>done</mat-icon>\n                            </button>\n                        </ng-container>\n                        <ng-container *ngIf=\"element.estatus == 'Pagado' \">\n                            <button type=\"button\" style=\"text-align: center;\" class=\"btn btn-success\">                     \n                                 <mat-icon>done</mat-icon>\n                            </button>\n                        </ng-container>\n                        <ng-container *ngIf=\"element.estatus == 'Cancelado' \">\n                            <button type=\"button\" style=\"text-align: center;\" class=\"btn btn-danger\">                     \n                                <mat-icon>clear</mat-icon>\n                           </button>\n                        </ng-container>\n                    </td>\n                </ng-container>\n\n                <!-- FECHA Column -->\n                <ng-container matColumnDef=\"fecha\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Fecha </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.fecha | date: 'dd/MM/yy h:mm:ss' }} </td>\n                </ng-container>\n\n                <!-- CLIENTE Column -->\n                <ng-container matColumnDef=\"nombreUsuario\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Cliente </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.nombreUsuario}} </td>\n                </ng-container>\n\n                <!-- TOTALNETO Column -->\n                <ng-container matColumnDef=\"totalNeto\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Total </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <ng-container *ngIf=\"!element.totalNeto\">\n                            $0.00\n                        </ng-container>\n                        <ng-container *ngIf=\"element.totalNeto\">\n                            {{ element.totalNeto | currency }}\n                        </ng-container>\n                    </td>\n                </ng-container>\n\n                <!-- DETALLE Column -->\n                <ng-container matColumnDef=\"detalle\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Detalle </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <button mat-mini-fab color=\"warn\" title=\"Detalle del pedido.\" routerLink=\"../../servicio/{{element.uid}}\">\n                            <mat-icon>assignment</mat-icon>\n                        </button>\n                    </td>\n                </ng-container>\n\n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n            </table>\n            <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        </div>\n\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: ListHostorialServiciosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHostorialServiciosComponent", function() { return ListHostorialServiciosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/pedidos/pedidos.service */ "./src/app/service/pedidos/pedidos.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");






var ListHostorialServiciosComponent = /** @class */ (function () {
    function ListHostorialServiciosComponent(_pedido, afs, authService) {
        this._pedido = _pedido;
        this.afs = afs;
        this.authService = authService;
        this.displayedColumns = ['clave', 'tipo', 'estatus', 'fecha', 'nombreUsuario', 'totalNeto', 'detalle'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"]();
        this.filterPost = '';
        this.filterPost1 = '';
        this.filterPost2 = '';
        this.fechaInicio = '';
        this.fechaFin = '';
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    ListHostorialServiciosComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.getAllUsuarios();
        // this.getAllRepartidores();
        // Sucursal
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.afs
                    .doc("users/" + _this.uidUser)
                    .valueChanges()
                    .subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    // this.getAllPedidos(this.Uidsucursal);
                    // this.getAllPedidosCancel(this.Uidsucursal);
                    console.log('usuer', _this.Uidsucursal);
                    _this.getAllPedidos(_this.Uidsucursal);
                });
            }
        });
    };
    ListHostorialServiciosComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListHostorialServiciosComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    ListHostorialServiciosComponent.prototype.getAllPedidos = function (uidSucursal) {
        var _this = this;
        // console.log('uidsSer', uidSucursal);
        this.afs.collection('servicios', function (ref) {
            return ref
                .where('uidSucursal', '==', uidSucursal).orderBy('fecha', 'desc');
        })
            .valueChanges().subscribe(function (data) {
            var serv = data;
            var servicios = [];
            serv.forEach(function (p) {
                if (p.estatus === 'Cancelado' ||
                    p.estatus === 'Terminado' ||
                    p.estatus === 'Pagado') {
                    servicios.push(p);
                }
            });
            console.log("servicios_", servicios);
            _this.pedidos = servicios;
            _this.dataSource.data = servicios;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], ListHostorialServiciosComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], ListHostorialServiciosComponent.prototype, "sort", void 0);
    ListHostorialServiciosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-hostorial-servicios',
            template: __webpack_require__(/*! ./list-hostorial-servicios.component.html */ "./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.html"),
            styles: [__webpack_require__(/*! ./list-hostorial-servicios.component.css */ "./src/app/components/admin/list-hostorial-servicios/list-hostorial-servicios.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__["PedidosService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _service_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], ListHostorialServiciosComponent);
    return ListHostorialServiciosComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-repartidores/list-repartidores.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/admin/list-repartidores/list-repartidores.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nsection {\n    margin-right: 100px;\n    margin-left: 110px;\n}\n\n.csv {\n    text-align: right;\n    height: auto;\n    width: auto;\n    margin-bottom: 1em;\n}\n\np {\n    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n    font-size: 10px;\n    margin-bottom: -1px;\n    color: #562B89;\n}\n\n.eta {\n    margin-right: 2px;\n    margin-bottom: 2px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LXJlcGFydGlkb3Jlcy9saXN0LXJlcGFydGlkb3Jlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtDQUNmOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLFlBQVk7SUFDWixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSx1SEFBdUg7SUFDdkgsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixlQUFlO0NBQ2xCOztBQUVEO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vbGlzdC1yZXBhcnRpZG9yZXMvbGlzdC1yZXBhcnRpZG9yZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbnNlY3Rpb24ge1xuICAgIG1hcmdpbi1yaWdodDogMTAwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDExMHB4O1xufVxuXG4uY3N2IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogMWVtO1xufVxuXG5wIHtcbiAgICBmb250LWZhbWlseTogJ0x1Y2lkYSBTYW5zJywgJ0x1Y2lkYSBTYW5zIFJlZ3VsYXInLCAnTHVjaWRhIEdyYW5kZScsICdMdWNpZGEgU2FucyBVbmljb2RlJywgR2VuZXZhLCBWZXJkYW5hLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAtMXB4O1xuICAgIGNvbG9yOiAjNTYyQjg5O1xufVxuXG4uZXRhIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG5cbmJ1dHRvbiB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuaW5wdXQge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/admin/list-repartidores/list-repartidores.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/admin/list-repartidores/list-repartidores.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <!-- <ngx-alerts></ngx-alerts> -->\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-motorcycle\"></i></span> Listado de repartidores</h1>\n        <div class=\"row\">\n            <!-- <div class=\"col\">\n                <div class=\"form-group\">\n                    <input type=\"text\" class=\"form-control\" placeholder=\"&#xf002; Buscar nombre...\" style=\"font-family:Arial, FontAwesome\" name=\"filterPost\" [(ngModel)]=\"filterPost\">\n                </div>\n            </div> -->\n            <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modal\" data-target=\"#modalRepartidor\"><i class=\"fa fa-plus\"></i> Nuevo repartidor</button>\n        </div>\n    </div>\n    <div class=\"col\">\n        <div class=\"csv table table-light \">\n            <p>Exportar CSV</p>\n            <a csvLink [data]=\"repartidores\" class=\"eta btn btn-success\"><i class=\"fa fa-file-excel-o\" aria-hidden=\"true\"></i></a>\n        </div>\n        <mat-form-field>\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n        </mat-form-field>\n\n        <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n            <!-- NOMBRE Column -->\n            <ng-container matColumnDef=\"username\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Nombre </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.username}} {{element.lastname}} </td>\n            </ng-container>\n\n            <!-- TELEFONO Column -->\n            <ng-container matColumnDef=\"phone\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Teléfono </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.phone}} </td>\n            </ng-container>\n\n            <!-- CORREO Column -->\n            <ng-container matColumnDef=\"email\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Correo </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.email}} </td>\n            </ng-container>\n\n            <!-- MARCA Column -->\n            <ng-container matColumnDef=\"marca\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Marca transporte </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.marca}} </td>\n            </ng-container>\n\n            <!-- PLACA Column -->\n            <ng-container matColumnDef=\"placa\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Placa </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.placa}} </td>\n            </ng-container>\n\n            <!-- ESTATUS Column -->\n            <ng-container matColumnDef=\"activo\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.activo == true\">\n                        <mat-icon matBadge=\"+\" matBadgeColor=\"accent\">person_outline</mat-icon>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.activo == false\">\n                        <mat-icon matBadge=\"-\" matBadgeColor=\"warn\">person_outline</mat-icon>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <!-- EDITAR Column -->\n            <ng-container matColumnDef=\"edit\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Editar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"accent\" routerLink=\"../../updateRepa/{{element.uid}}\">\n                        <mat-icon>edit</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <!-- ELIMINAR Column -->\n            <ng-container matColumnDef=\"delete\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Borrar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"warn\" (click)=\"onDeleteRepartidor(element)\">\n                        <mat-icon>delete</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <!-- STATUS Column -->\n            <ng-container matColumnDef=\"status\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.activo == true\">\n                        <button mat-mini-fab color=\"blue\" (click)=\"onDesactivarRepartidor(element)\">\n                            <mat-icon>toggle_off</mat-icon>\n                        </button>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.activo == false\">\n                        <button mat-mini-fab color=\"primary\" (click)=\"onActivarRepartidor(element)\">                        \n                            <mat-icon>toggle_on</mat-icon>\n                        </button>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        </table>\n        <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n    </div>\n</section>\n<app-modal-repartidor></app-modal-repartidor>"

/***/ }),

/***/ "./src/app/components/admin/list-repartidores/list-repartidores.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/admin/list-repartidores/list-repartidores.component.ts ***!
  \***********************************************************************************/
/*! exports provided: ListRepartidoresComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListRepartidoresComponent", function() { return ListRepartidoresComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_repartidor_repartidor_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/repartidor/repartidor-data.service */ "./src/app/service/repartidor/repartidor-data.service.ts");
/* harmony import */ var src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);







var ListRepartidoresComponent = /** @class */ (function () {
    function ListRepartidoresComponent(reDa, authService, db) {
        this.reDa = reDa;
        this.authService = authService;
        this.db = db;
        this.displayedColumns = ['username', 'email', 'phone', 'marca', 'placa', 'activo', 'edit', 'delete', 'status'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"]();
        this.filterPost = '';
        this.pageActual = 1;
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    ListRepartidoresComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    _this.getListRepartidores(_this.Uidsucursal);
                    _this.reDa.getRepartidores(_this.Uidsucursal)
                        .subscribe(function (res) { return (_this.dataSource.data = res); });
                    // console.log('usuer', this.Uidsucursal);
                });
            }
        });
    };
    ListRepartidoresComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListRepartidoresComponent.prototype.getListRepartidores = function (uidSucursal) {
        var _this = this;
        this.db.collection('users', function (ref) { return ref.where('tipo', '==', 2)
            .where('uidSucursal', '==', uidSucursal); }).valueChanges().subscribe(function (data) {
            _this.repartidores = data;
            // console.log('repas', data);
        });
        this.data = JSON.stringify(this.repartidores);
    };
    ListRepartidoresComponent.prototype.onDeleteRepartidor = function (repa) {
        var _this = this;
        var idRepa = repa.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: '¿Estas seguro?',
            text: "Eliminaras al repartidor!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.reDa.deleteRepartidor(idRepa);
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Borrado', 'Repartidor Borrado', 'success');
            }
            // else {
            //   Swal.fire('Error!', 'Hubo un error al eliminar el repartidor');
            // }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Error!', 'Hubo un error al eliminar el repartidor', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de eliminar este repartidor?');
        // if (confirmacion) {
        //   this.reDa.deleteRepartidor(idRepartidor);
        // }
    };
    ListRepartidoresComponent.prototype.onDesactivarRepartidor = function (repa) {
        var _this = this;
        var idRepa = repa.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: '¿Estás seguro?',
            text: "Desactivaras al repartidor!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, desactivar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.reDa.desactivarRepartidor(idRepa);
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Desactivado', 'Repartidor desactivado de la plataforma', 'success');
            }
            //  else {
            //   Swal.fire('Error!', 'Hubo un error al desactivar el repartidor');
            // }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Error!', 'Hubo un error al desactivar el repartidor', 'error');
        });
    };
    ListRepartidoresComponent.prototype.onActivarRepartidor = function (repa) {
        var _this = this;
        var idRepa = repa.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            title: '¿Estás seguro?',
            text: "Activaras al repartidor!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, activar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.reDa.activarRepartidor(idRepa);
                sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Activado', 'Repartidor activado a la plataforma', 'success');
            }
            // else {
            //   Swal.fire('Error!', 'Hubo un error al activar el repartidor');
            // }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire('Error!', 'Hubo un error al activar el repartidor', 'error');
        });
    };
    ListRepartidoresComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], ListRepartidoresComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], ListRepartidoresComponent.prototype, "sort", void 0);
    ListRepartidoresComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-repartidores',
            template: __webpack_require__(/*! ./list-repartidores.component.html */ "./src/app/components/admin/list-repartidores/list-repartidores.component.html"),
            styles: [__webpack_require__(/*! ./list-repartidores.component.css */ "./src/app/components/admin/list-repartidores/list-repartidores.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_repartidor_repartidor_data_service__WEBPACK_IMPORTED_MODULE_2__["RepartidorDataService"],
            src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"]])
    ], ListRepartidoresComponent);
    return ListRepartidoresComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-restaurantes/list-restaurantes.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/admin/list-restaurantes/list-restaurantes.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nsection {\n    margin-right: 110px;\n    margin-left: 110px;\n}\n\n.csv {\n    text-align: right;\n    height: auto;\n    width: auto;\n    margin-bottom: 1em;\n}\n\np {\n    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n    font-size: 10px;\n    margin-bottom: -1px;\n    color: #562B89;\n}\n\n.eta {\n    margin-right: 2px;\n    margin-bottom: 2px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\n/*\n   server-side-angular-way.component.css\n*/\n\n.no-data-available {\n    text-align: center;\n}\n\n/*\n     src/styles.css (i.e. your global style)\n  */\n\n.dataTables_empty {\n    display: none;\n}\n\nagm-map {\n    height: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LXJlc3RhdXJhbnRlcy9saXN0LXJlc3RhdXJhbnRlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtDQUNmOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLFlBQVk7SUFDWixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSx1SEFBdUg7SUFDdkgsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixlQUFlO0NBQ2xCOztBQUVEO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFHRDs7RUFFRTs7QUFFRjtJQUNJLG1CQUFtQjtDQUN0Qjs7QUFHRDs7SUFFSTs7QUFFSjtJQUNJLGNBQWM7Q0FDakI7O0FBRUQ7SUFDSSxjQUFjO0NBQ2pCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LXJlc3RhdXJhbnRlcy9saXN0LXJlc3RhdXJhbnRlcy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuc2VjdGlvbiB7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMTBweDtcbiAgICBtYXJnaW4tbGVmdDogMTEwcHg7XG59XG5cbi5jc3Yge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGhlaWdodDogYXV0bztcbiAgICB3aWR0aDogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiAxZW07XG59XG5cbnAge1xuICAgIGZvbnQtZmFtaWx5OiAnTHVjaWRhIFNhbnMnLCAnTHVjaWRhIFNhbnMgUmVndWxhcicsICdMdWNpZGEgR3JhbmRlJywgJ0x1Y2lkYSBTYW5zIFVuaWNvZGUnLCBHZW5ldmEsIFZlcmRhbmEsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IC0xcHg7XG4gICAgY29sb3I6ICM1NjJCODk7XG59XG5cbi5ldGEge1xuICAgIG1hcmdpbi1yaWdodDogMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbn1cblxuYnV0dG9uIHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5pbnB1dCB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuXG4vKlxuICAgc2VydmVyLXNpZGUtYW5ndWxhci13YXkuY29tcG9uZW50LmNzc1xuKi9cblxuLm5vLWRhdGEtYXZhaWxhYmxlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cblxuLypcbiAgICAgc3JjL3N0eWxlcy5jc3MgKGkuZS4geW91ciBnbG9iYWwgc3R5bGUpXG4gICovXG5cbi5kYXRhVGFibGVzX2VtcHR5IHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG5hZ20tbWFwIHtcbiAgICBoZWlnaHQ6IDMwMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/admin/list-restaurantes/list-restaurantes.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/admin/list-restaurantes/list-restaurantes.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <div class=\"container\">\n        <ul class=\"nav nav-tabs\">\n            <li class=\"nav-item\">\n                <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#home\">Lista de Convenios</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" data-toggle=\"tab\" href=\"#profile\">Agregar nuevo Convenio</a>\n            </li>\n        </ul>\n        <ngx-alerts></ngx-alerts>\n        <div id=\"myTabContent\" class=\"tab-content\">\n            <div class=\"tab-pane fade active show\" id=\"home\">\n                <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-cutlery\"></i></span> Listado de Convenios\n                </h1>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        <div class=\"form-group\">\n                            <!--Buscador-->\n                            <!-- <input type=\"text\" class=\"form-control\" placeholder=\"&#xf002; Buscar nombre...\" style=\"font-family:Arial, FontAwesome\" name=\"filterPost\" [(ngModel)]=\"filterPost\"> -->\n                        </div>\n                    </div>\n                    <!-- <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modal\" data-target=\"#modalAdmin\"> <i class=\"fa fa-plus\"></i> Nuevo Convenio </button> -->\n                </div>\n                <div class=\"col\">\n                    <div class=\"csv table table-light \">\n                        <p>Exportar CSV</p>\n                        <a csvLink [data]=\"restaurantes\" class=\"eta btn btn-success\"><i class=\"fa fa-file-excel-o\"\n                aria-hidden=\"true\"></i></a>\n                    </div>\n                    <mat-form-field>\n                        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n                    </mat-form-field>\n\n                    <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n                        <!-- UID Column -->\n                        <ng-container matColumnDef=\"uid\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> UID </th>\n                            <td mat-cell *matCellDef=\"let element\"> {{element.uid}} </td>\n                        </ng-container>\n\n                        <!-- NOMBRE Column -->\n                        <ng-container matColumnDef=\"username\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Nombre </th>\n                            <td mat-cell *matCellDef=\"let element\"> {{element.username}} </td>\n                        </ng-container>\n\n                        <!-- CONTACTO Column -->\n                        <ng-container matColumnDef=\"contacto\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Contacto </th>\n                            <td mat-cell *matCellDef=\"let element\"> {{element.contacto}} </td>\n                        </ng-container>\n\n                        <!-- TELEFONO Column -->\n                        <ng-container matColumnDef=\"phone\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Teléfono </th>\n                            <td mat-cell *matCellDef=\"let element\"> {{element.phone}} </td>\n                        </ng-container>\n\n                        <!-- PLACA Column -->\n                        <ng-container matColumnDef=\"email\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Correo </th>\n                            <td mat-cell *matCellDef=\"let element\"> {{element.email}} </td>\n                        </ng-container>\n\n                        <!-- ESTATUS Column -->\n                        <ng-container matColumnDef=\"activo\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estatus </th>\n                            <td mat-cell *matCellDef=\"let element\">\n                                <ng-container *ngIf=\"element.activo == true\">\n                                    <mat-icon matBadge=\"+\" matBadgeColor=\"accent\">person_outline</mat-icon>\n                                </ng-container>\n                                <ng-container *ngIf=\"element.activo == false\">\n                                    <mat-icon matBadge=\"-\" matBadgeColor=\"warn\">person_outline</mat-icon>\n                                </ng-container>\n                            </td>\n                        </ng-container>\n\n                        <!-- EDITAR Column -->\n                        <ng-container matColumnDef=\"edit\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Editar </th>\n                            <td mat-cell *matCellDef=\"let element\">\n                                <button mat-mini-fab color=\"accent\" routerLink=\"../../updateRest/{{element.uid}}\">\n                                    <mat-icon>edit</mat-icon>\n                                </button>\n                            </td>\n                        </ng-container>\n\n                        <!-- ELIMINAR Column -->\n                        <ng-container matColumnDef=\"delete\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Borrar </th>\n                            <td mat-cell *matCellDef=\"let element\">\n                                <button mat-mini-fab color=\"warn\" (click)=\"onDeleteRes(element)\">\n                                    <mat-icon>delete</mat-icon>\n                                </button>\n                            </td>\n                        </ng-container>\n\n                        <!-- STATUS Column -->\n                        <ng-container matColumnDef=\"status\">\n                            <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Estatus </th>\n                            <td mat-cell *matCellDef=\"let element\">\n                                <ng-container *ngIf=\"element.activo == true\">\n                                    <button mat-mini-fab color=\"blue\" (click)=\"onDesactivarConvenio(element)\">\n                                        <mat-icon>toggle_off</mat-icon>\n                                    </button>\n                                </ng-container>\n                                <ng-container *ngIf=\"element.activo == false\">\n                                    <button mat-mini-fab color=\"primary\" (click)=\"onActivarConvenio(element)\">                        \n                                        <mat-icon>toggle_on</mat-icon>\n                                    </button>\n                                </ng-container>\n                            </td>\n                        </ng-container>\n\n                        <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                        <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n                    </table>\n                    <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n                    <div style=\"padding-top: 3em;\">\n\n                    </div>\n                </div>\n            </div>\n            <div class=\"tab-pane fade\" id=\"profile\">\n                <div style=\"padding-top: 2em;\" class=\"body\">\n                    <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-paper-plane-o\"></i></span> Agregar nuevo convenio\n                    </h1>\n                    <form name=\"formAdmin\" #formAdmin=\"ngForm\" (ngSubmit)=\"cargarImagenes(formAdmin)\">\n                        <div class=\"form-group\">\n                            <label for=\"nombre\"><b>Nombre: </b> </label>\n                            <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"La Fondita\" [(ngModel)]=\"this.username\">\n                        </div>\n                        <div class=\"form-group\">\n                            <label for=\"contacto\"><b>Contacto:</b> </label>\n                            <input type=\"text\" name=\"contacto\" class=\"form-control\" placeholder=\"Maria Gonzalez\" [(ngModel)]=\"this.contacto\">\n                        </div>\n                        <div class=\"form-group\">\n                            <div class=\"form-group\">\n                                <label><b>Ingrese dirección de entrega: </b> </label>\n                                <input type=\"text\" class=\"form-control\" (keydown.enter)=\"$event.preventDefault()\" placeholder=\"Buscar Dirección\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\" type=\"text\" required=\"true\" #search>\n                            </div>\n                            <agm-map [latitude]=\"latitude\" [longitude]=\"longitude\" [zoom]=\"zoom\">\n                                <agm-marker [latitude]=\"latitude\" [longitude]=\"longitude\" [markerDraggable]=\"true\" (dragEnd)=\"markerDragEnd($event)\"></agm-marker>\n                            </agm-map>\n                            <h5>Address: {{address}}</h5>\n                            <div>Latitude: {{latitude}}</div>\n                            <div>Longitude: {{longitude}}</div>\n                        </div>\n                        <div class=\"form-group\">\n                            <label for=\"telefono\"><b>Teléfono:</b></label>\n                            <input type=\"number\" name=\"phone\" class=\"form-control\" placeholder=\"4775642879\" [(ngModel)]=\"this.phone\">\n                        </div>\n                        <div class=\"form-group\">\n                            <label for=\"categoria\"><b>Selecciona una categoría:</b> </label>\n                            <select class=\"form-control\" id=\"categoria\" name=\"categoria\" [(ngModel)]=\"this.categoria\">\n                <option *ngFor=\"let categoria of categorias\" value=\"{{categoria.id}}\">{{ categoria.nombre }} </option>\n              </select>\n                        </div>\n                        <div class=\"form-group\">\n                            <label for=\"hora_abierto\"><b>Hora de abrir:</b> </label>\n                            <input type=\"time\" class=\"form-control\" name=\"hora_abierto\" [(ngModel)]=\"this.hora_abierto\">\n                        </div>\n                        <div class=\"form-group\">\n                            <label for=\"hora_cerrar\"><b>Hora de cerrar:</b> </label>\n                            <input type=\"time\" class=\"form-control\" name=\"hora_cerrar\" [(ngModel)]=\"this.hora_cerrar\">\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"email\"><b>Correo electrónico:</b></label>\n                            <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"ejemplo@ejemplo.com.mx\" [(ngModel)]=\"this.email\" required minlength=\"8\">\n                        </div>\n                        <div class=\"form-group\">\n                            <label for=\"password\"><b>Contraseña</b></label>\n                            <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"this.password\" required minlength=\"5\" maxlength=\"10\">\n                        </div>\n                        <hr>\n                        <div class=\"form-group\">\n                            <h5><b>Seleccionar imagen:</b></h5>\n                            <input class=\"form-control\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                        </div>\n                        <div class=\"progress\">\n                            <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n                                <!-- <span class=\"progressText\" *ngIf=\"urlImage | async\">\n              Ok!!</span> -->\n                            </div>\n                        </div>\n                        <div style=\" padding-top: 2em; padding-bottom: 2em;\">\n                            <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Actualizar</button> &nbsp;\n                            <!-- <button (click)=\"limpiarArchivos()\" class=\"btn btn-danger\">Limpiar</button> -->\n                        </div>\n                    </form>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</section>"

/***/ }),

/***/ "./src/app/components/admin/list-restaurantes/list-restaurantes.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/admin/list-restaurantes/list-restaurantes.component.ts ***!
  \***********************************************************************************/
/*! exports provided: ListRestaurantesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListRestaurantesComponent", function() { return ListRestaurantesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/restaurante/restaurante-data.service */ "./src/app/service/restaurante/restaurante-data.service.ts");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_11__);












var ListRestaurantesComponent = /** @class */ (function () {
    function ListRestaurantesComponent(dataRes, mapsAPILoader, ngZone, authService, db, alertService, storage) {
        this.dataRes = dataRes;
        this.mapsAPILoader = mapsAPILoader;
        this.ngZone = ngZone;
        this.authService = authService;
        this.db = db;
        this.alertService = alertService;
        this.storage = storage;
        this.displayedColumns = ['uid', 'username', 'email', 'phone', 'contacto', 'activo', 'edit', 'delete', 'status'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatTableDataSource"]();
        this.filterPost = '';
        this.pageActual = 1;
        // maps
        this.title = 'AGM project';
        this.estaSobreElemento = false;
        this.archivos = [];
        this.tipo = 3;
        this.activo = true;
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    ListRestaurantesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.categoriaList();
        this.mapsAPILoader.load().then(function () {
            _this.setCurrentLocation();
            _this.geoCoder = new google.maps.Geocoder();
            var autocomplete = new google.maps.places.Autocomplete(_this.searchElementRef.nativeElement, {
                types: ['address']
            });
            autocomplete.addListener('place_changed', function () {
                _this.ngZone.run(function () {
                    // get the place result
                    var place = autocomplete.getPlace();
                    // verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }
                    // set latitude, longitude and zoom
                    _this.latitude = place.geometry.location.lat();
                    _this.longitude = place.geometry.location.lng();
                    _this.zoom = 12;
                });
            });
        });
        // Sucursal
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db
                    .doc("users/" + _this.uidUser)
                    .valueChanges()
                    .subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    _this.getListRestaurante(_this.Uidsucursal);
                    console.log('usuer', _this.Uidsucursal);
                    _this.db
                        .collection('categoria_critico', function (ref) {
                        return ref
                            .where('uidSucursal', '==', _this.Uidsucursal)
                            .orderBy('posicion', 'asc');
                    })
                        .valueChanges()
                        .subscribe(function (categoria) {
                        _this.categorias = categoria;
                        console.log('categoria', categoria);
                    });
                    // Desglose de tabla
                    _this.authService.getConvenios(_this.Uidsucursal)
                        .subscribe(function (res) { return (_this.dataSource.data = res); });
                });
            }
        });
    };
    ListRestaurantesComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListRestaurantesComponent.prototype.getListRestaurante = function (uidSucursal) {
        var _this = this;
        console.log('uidsucu', uidSucursal);
        this.db
            .collection('users', function (ref) {
            return ref.where('tipo', '==', 3).where('uidSucursal', '==', uidSucursal);
        })
            .valueChanges()
            .subscribe(function (data) {
            _this.restaurantes = data;
            console.log('covenios', data);
        });
        JSON.stringify(this.restaurantes);
    };
    ListRestaurantesComponent.prototype.onDeleteRes = function (uid) {
        var _this = this;
        var idCo = uid.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire({
            title: '¿Estás seguro?',
            text: "Eliminaras al convenio!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.dataRes.deleteRestaurante(idCo);
                sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire('Eliminado', 'Convenio eliminado de la plataforma', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire('Error!', 'Hubo un error al eliminar el convenio', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de eliminar este usuario?');
        // if (confirmacion) {
        //   this.dataRes.deleteRestaurante(idRes);
        // }
    };
    // Get Current Location Coordinates
    ListRestaurantesComponent.prototype.setCurrentLocation = function () {
        var _this = this;
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                _this.latitude = position.coords.latitude;
                _this.longitude = position.coords.longitude;
                _this.zoom = 8;
                _this.getAddress(_this.latitude, _this.longitude);
            });
        }
    };
    ListRestaurantesComponent.prototype.getAddress = function (latitude, longitude) {
        var _this = this;
        this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, function (results, status) {
            console.log(results);
            console.log(status);
            if (status === 'OK') {
                if (results[0]) {
                    _this.zoom = 12;
                    _this.address = results[0].formatted_address;
                }
                else {
                    window.alert('No results found');
                }
            }
            else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    };
    ListRestaurantesComponent.prototype.markerDragEnd = function ($event) {
        console.log('markdrag', $event);
        this.latitude = $event.coords.lat;
        this.longitude = $event.coords.lng;
        this.getAddress(this.latitude, this.longitude);
    };
    // subir convenio
    ListRestaurantesComponent.prototype.cargarImagenes = function (formAdmin) {
        // console.log('hora', this.hora_abierto);
        var abierto = moment__WEBPACK_IMPORTED_MODULE_11__(this.hora_abierto, 'HHmmss');
        console.log('este', abierto.format());
        console.log(abierto.format('HH:mm:ss'));
        var abiertoFormat = abierto.format('HH:mm:ss');
        var cerrar = moment__WEBPACK_IMPORTED_MODULE_11__(this.hora_cerrar, 'HHmmss');
        console.log(cerrar.format());
        console.log(cerrar.format('HH:mm:ss'));
        var cerrarFormat = cerrar.format('HH:mm:ss');
        if (this.email === '' ||
            this.password === '' ||
            this.username === '' ||
            this.contacto === '' ||
            this.address === '' ||
            this.phone === '' ||
            this.hora_cerrar === '' ||
            this.categoria === '' ||
            this.direccion === '' ||
            !this.urlImage) {
            this.alertService.warning('Algunos campos no estan completos.');
        }
        else if (this.password.length < 7) {
            this.alertService.warning('La contraseña debe tener más de 7 caracteres');
        }
        else {
            this.direccion = new firebase__WEBPACK_IMPORTED_MODULE_4__["firestore"].GeoPoint(this.latitude, this.longitude);
            this.authService.newRegister(abiertoFormat, cerrarFormat, this.email, this.password, this.username, this.contacto, this.address, this.phone, this.categoria, this.direccion, this.Uidsucursal, this.urlImage1);
            formAdmin.resetForm();
            // console.log('abierto', cerrar);
        }
    };
    // imagen
    ListRestaurantesComponent.prototype.onUpload = function (e) {
        var _this = this;
        console.log('subir', e.target.files[0]);
        var id = Math.random()
            .toString(36)
            .substring(2);
        var file = e.target.files[0];
        var filePath = "users/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(function () {
            return ref.getDownloadURL().subscribe(function (r) {
                _this.urlImage1 = _this.urlImage = r;
                console.log('foto', r);
            });
        }))
            .subscribe();
    };
    ListRestaurantesComponent.prototype.onDesactivarConvenio = function (uid) {
        var _this = this;
        var idCo = uid.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire({
            title: '¿Estás seguro?',
            text: "Desactivaras al convenio!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, desactivar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.authService.desactivarConvenio(idCo);
                sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire('Desactivado', 'Convenio desactivado de la plataforma', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire('Error!', 'Hubo un error al desactivar el convenio', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de desactivar este usuario?');
        // if (confirmacion) {
        //   this.authService.desactivarConvenio(uid);
        // }
    };
    ListRestaurantesComponent.prototype.onActivarConvenio = function (uid) {
        var _this = this;
        var idCo = uid.uid;
        sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire({
            title: '¿Estás seguro?',
            text: "Activaras al convenio!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, activar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.authService.activarConvenio(idCo);
                sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire('Activado', 'Convenio activado de la plataforma', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_10___default.a.fire('Error!', 'Hubo un error al activar el convenio', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de activar este usuario?');
        // if (confirmacion) {
        //   this.authService.activarConvenio(uid);
        // }
    };
    // trae todos las categorías
    ListRestaurantesComponent.prototype.categoriaList = function () {
        var _this = this;
        this.db
            .collection('categoria_critico')
            .valueChanges()
            .subscribe(function (cat) {
            _this.list_Categorias = cat;
            console.log('cate', cat);
        });
    };
    ListRestaurantesComponent.prototype.onPreUpdateRestaurant = function (rest) {
        console.log('REST', rest);
        this.dataRes.isUserRestaurante = Object.assign({}, rest);
    };
    ListRestaurantesComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_9__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatPaginator"])
    ], ListRestaurantesComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_9__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatSort"])
    ], ListRestaurantesComponent.prototype, "sort", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('search'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ListRestaurantesComponent.prototype, "searchElementRef", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ListRestaurantesComponent.prototype, "btnClose", void 0);
    ListRestaurantesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-restaurantes',
            template: __webpack_require__(/*! ./list-restaurantes.component.html */ "./src/app/components/admin/list-restaurantes/list-restaurantes.component.html"),
            styles: [__webpack_require__(/*! ./list-restaurantes.component.css */ "./src/app/components/admin/list-restaurantes/list-restaurantes.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_2__["RestauranteDataService"],
            _agm_core__WEBPACK_IMPORTED_MODULE_3__["MapsAPILoader"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            src_app_service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_2__["RestauranteDataService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_6__["AlertService"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_8__["AngularFireStorage"]])
    ], ListRestaurantesComponent);
    return ListRestaurantesComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-servicios/list-servicios.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/components/admin/list-servicios/list-servicios.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\n.iconTi {\n    height: 35px;\n    width: 35px;\n}\n\n/* table {\n    margin-right: auto;\n    margin-left: auto;\n} */\n\n.bz {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\nspan {\n    font-size: 12px;\n}\n\n/* .noServ {\n    text-align: center;\n    font-size: 1rem;\n    color: #fff;\n    border-radius: 3rem;\n    background-color: #e84118;\n} */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LXNlcnZpY2lvcy9saXN0LXNlcnZpY2lvcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtDQUNmOztBQUVEO0lBQ0ksZ0JBQWdCO0lBQ2hCLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLGFBQWE7SUFDYixZQUFZO0NBQ2Y7O0FBR0Q7OztJQUdJOztBQUVKO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksZ0JBQWdCO0NBQ25COztBQUdEOzs7Ozs7SUFNSSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vbGlzdC1zZXJ2aWNpb3MvbGlzdC1zZXJ2aWNpb3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbmJ1dHRvbiB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuLmljb25UaSB7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIHdpZHRoOiAzNXB4O1xufVxuXG5cbi8qIHRhYmxlIHtcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG59ICovXG5cbi5ieiB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuaW5wdXQge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbnNwYW4ge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuXG4vKiAubm9TZXJ2IHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJvcmRlci1yYWRpdXM6IDNyZW07XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U4NDExODtcbn0gKi8iXX0= */"

/***/ }),

/***/ "./src/app/components/admin/list-servicios/list-servicios.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admin/list-servicios/list-servicios.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <div class=\"container\">\n        <!-- <h3 class=\"display-6\"> <i class=\"fa fa-globe\"></i> Servicios de: {{ jsToday }} </h3> -->\n        <!-- <mat-label>Servicios</mat-label> -->\n        <h2><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-globe\"></i></span> Servicios del día</h2>\n        <div class=\"row\">\n            <div class=\"col\">\n                <div class=\"form-group\">\n                    <!--Buscador-->\n                    <!-- <input type=\"text\" class=\"form-control\" placeholder=\"&#xf002; Buscar servicio por clave...\" style=\"font-family:Arial, FontAwesome\" name=\"filterPost\" [(ngModel)]=\"filterPost\"> -->\n                </div>\n            </div>\n        </div>\n\n        <!-- <div class=\"row pad-10\">\n            <div class=\"col-2\">\n                <div class=\"card text-white bg-primary mb-3\" style=\"max-width: 30rem;\">\n                    <div class=\"card-header\">Servicios del Día</div>\n                    <div class=\"card-body\">\n                        <h4 class=\"card-title\">Primary card title</h4>\n                        <p class=\"card-text\">Some quick</p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-2\">\n                <div class=\"card text-white bg-warning mb-3\" style=\"max-width: 30rem;\">\n                    <div class=\"card-header\">Header</div>\n                    <div class=\"card-body\">\n                        <h4 class=\"card-title\">Primary card title</h4>\n                        <p class=\"card-text\">Some quick</p>\n                    </div>\n                </div>\n            </div>\n        </div> -->\n        <hr class=\"my-4\">\n        <!-- <div class=\"row pad-10\"> -->\n        <mat-form-field>\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n        </mat-form-field>\n\n        <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n            <!-- CLAVE Column -->\n            <ng-container matColumnDef=\"clave\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Clave </th>\n                <td mat-cell *matCellDef=\"let element\"> <b>{{element.clave}}</b> </td>\n            </ng-container>\n\n            <!-- HORA Column -->\n            <ng-container matColumnDef=\"hora\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Hora </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.fecha | date:'shortTime'}}</td>\n            </ng-container>\n\n            <!-- USUARIO Column -->\n            <ng-container matColumnDef=\"usuario\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Usuario </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.nombreUsuario}} </td>\n            </ng-container>\n\n            <!-- TIPO Column -->\n            <ng-container matColumnDef=\"tipo\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Tipo </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.tipo && element.abierto == null\">\n                        <img class=\"iconTi\" src=\"../../../../assets/imgs/tipo1.png\" title=\"Servicio pide lo que quieras.\">\n                    </ng-container>\n                    <ng-container *ngIf=\"element.abierto == 'Abierto plataforma'\">\n                        <img class=\"iconTi\" src=\"../../../../assets/imgs/tipo2.png\" title=\"Servicio de convenio.\">\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <!-- ESTADO Column -->\n            <ng-container matColumnDef=\"estatus\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estado </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.estatus == 'Notificando'\">\n                        <span class=\"badge badge-primary\">Buscando Repartidor</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'Aceptado'\">\n                        <span class=\"badge badge-success\">Aceptado</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'Yendo'\">\n                        <span class=\"badge badge-primary\">Dirigiendose al establecimiento</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'Comprando'\">\n                        <span class=\"badge badge-primary\">Comprando</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'Comprado'\">\n                        <span class=\"badge badge-primary\">LLevando el pedido al cliente</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'EnPuerta'\">\n                        <span class=\"badge badge-primary\">Esta en puerta</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'Pago'\">\n                        <span class=\"badge badge-success\">Pagando servicio</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'Pagado'\">\n                        <span class=\"badge badge-success\">Servicio Finalizando</span>\n                    </ng-container>\n                    <!-- Restaurante estados -->\n                    <ng-container *ngIf=\"element.estatus == 'Gestando'\">\n                        <span class=\"badge badge-primary\">Se esta preparando el\n                        pedido</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'AceptaBme'\">\n                        <span class=\"badge badge-primary\">Servicio aceptado por\n                        BMe</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'BuscandoBme'\">\n                        <span class=\"badge badge-primary\">Notificando al repartidor</span>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estatus == 'Enviando'\">\n                        <span class=\"badge badge-primary\">Dirigiendose al domicilio</span>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <!-- DETALLE Column -->\n            <ng-container matColumnDef=\"detalle\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Detalle </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"accent\" title=\"Detalle del pedido.\" routerLink=\"../../servicio/{{element.uid}}\">\n                        <mat-icon>list</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        </table>\n        <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n    </div>\n    <!-- </div> -->\n</section>"

/***/ }),

/***/ "./src/app/components/admin/list-servicios/list-servicios.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/admin/list-servicios/list-servicios.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ListServiciosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListServiciosComponent", function() { return ListServiciosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/pedidos/pedidos.service */ "./src/app/service/pedidos/pedidos.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");







var ListServiciosComponent = /** @class */ (function () {
    function ListServiciosComponent(_pedidoPro, db, authService) {
        this._pedidoPro = _pedidoPro;
        this.db = db;
        this.authService = authService;
        this.displayedColumns = ['clave', 'hora', 'usuario', 'tipo', 'estatus', 'detalle'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"]();
        this.filterPost = '';
        this.today = new Date();
        this.jsToday = '';
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
        this.jsToday = Object(_angular_common__WEBPACK_IMPORTED_MODULE_3__["formatDate"])(this.today, 'dd-MM-yyyy', 'en-US');
    }
    ListServiciosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getAllUsuarios();
        // Sucursal
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    _this.getAllPedidos(_this.Uidsucursal);
                    console.log('usuer', _this.Uidsucursal);
                });
            }
        });
    };
    ListServiciosComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListServiciosComponent.prototype.getAllPedidos = function (uidSucursal) {
        var _this = this;
        console.log('uidsSer', uidSucursal);
        this.db.collection('servicios', function (ref) {
            return ref
                .where('uidSucursal', '==', uidSucursal).orderBy('fecha', 'desc');
        })
            .valueChanges().subscribe(function (data) {
            var serv = data;
            var servicios = [];
            serv.forEach(function (p) {
                if (p.estatus !== 'Cancelado' &&
                    p.estatus !== 'Terminado' &&
                    p.estatus !== 'creando' &&
                    p.estatus !== 'Pagado') {
                    servicios.push(p);
                }
            });
            // console.log("servicios_", servicios);
            _this.pedidos = servicios;
            _this.dataSource.data = servicios;
        });
    };
    ListServiciosComponent.prototype.getAllUsuarios = function () {
        var _this = this;
        this._pedidoPro.getAllUsuarios().subscribe(function (usuarios) {
            _this.usuarios = usuarios;
        });
    };
    ListServiciosComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatPaginator"])
    ], ListServiciosComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSort"])
    ], ListServiciosComponent.prototype, "sort", void 0);
    ListServiciosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-servicios',
            template: __webpack_require__(/*! ./list-servicios.component.html */ "./src/app/components/admin/list-servicios/list-servicios.component.html"),
            styles: [__webpack_require__(/*! ./list-servicios.component.css */ "./src/app/components/admin/list-servicios/list-servicios.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__["PedidosService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            _service_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]])
    ], ListServiciosComponent);
    return ListServiciosComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-sucursales/list-sucursales.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/admin/list-sucursales/list-sucursales.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img-fluid {\n    height: 120px;\n    width: 120px;\n    border-radius: 20px;\n}\n\nsection {\n    margin-right: 110px;\n    margin-left: 110px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LXN1Y3Vyc2FsZXMvbGlzdC1zdWN1cnNhbGVzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxjQUFjO0lBQ2QsYUFBYTtJQUNiLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxvQkFBb0I7Q0FDdkI7O0FBRUQ7SUFDSSxvQkFBb0I7Q0FDdkIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FkbWluL2xpc3Qtc3VjdXJzYWxlcy9saXN0LXN1Y3Vyc2FsZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWctZmx1aWQge1xuICAgIGhlaWdodDogMTIwcHg7XG4gICAgd2lkdGg6IDEyMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG59XG5cbnNlY3Rpb24ge1xuICAgIG1hcmdpbi1yaWdodDogMTEwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDExMHB4O1xufVxuXG5idXR0b24ge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbmlucHV0IHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/admin/list-sucursales/list-sucursales.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/admin/list-sucursales/list-sucursales.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-building\"></i></span> Listado de sucursales</h1>\n        <div class=\"row\">\n            <div class=\"col\">\n                <div class=\"form-group\">\n                    <input type=\"text\" name=\"filterPost\" class=\"form-control\" placeholder=\"&#xf002; Buscar...\" style=\"font-family:Arial, FontAwesome\" [(ngModel)]=\"filterPost\">\n                </div>\n            </div>\n            <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modal\" data-target=\"#modalSucursal\"><i class=\"fa fa-plus\"></i> Nueva Sucursal</button>\n\n        </div>\n    </div>\n    <div class=\"col\">\n        <table class=\"table table-warning table-sm table-hover\">\n            <thead>\n                <tr>\n                    <th scope=\"col\">#</th>\n                    <th scope=\"col\">Estado</th>\n                    <th scope=\"col\">Descripción</th>\n                    <th scope=\"col\">&nbsp;</th>\n                    <th scope=\"col\">&nbsp;</th>\n                </tr>\n            </thead>\n            <tbody>\n                <tr *ngFor=\"let sucursal of sucursales | filterSucursal:filterPost | paginate: { itemsPerPage: 5, currentPage: p }; index as i\" class=\"table-light\">\n                    <th scope=\"row\"> {{i + 1 }} </th>\n                    <td> {{ sucursal.estado }} </td>\n                    <td> {{ sucursal.descripcion }} </td>\n\n                    <!-- <td> {{sucursal.opciones == 1 ? 'Si': 'No'}} </td> -->\n                    <!-- <td *ngIf=\"sucursal.userId == userUid || isAdmin == true; else noOwner\"> -->\n                    <td>\n                        <button class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#modalSucursal\" (click)=\"onPreUpdateSucursal(sucursal)\"><i class=\"fa fa-pencil\"></i></button>\n                    </td>\n                    <!-- <ng-template #noOwner>\n                        <td>\n                            <button class=\"btn btn-primary\" disabled=\"true\"><i class=\"fa fa-pencil\"></i></button>\n                        </td>\n                    </ng-template> -->\n                    <td>\n                        <!-- <td *ngIf=\"isAdmin == true; else noAdmin\"> -->\n                        <button class=\"btn btn-danger\" (click)=\"onDeleteSucursal(sucursal.id)\"><i class=\"fa fa-trash\"></i></button>\n                    </td>\n                    <!-- <ng-template #noAdmin>\n                        <td>\n                            <button class=\"btn btn-danger\" disabled=\"true\">Borrar</button>\n                        </td>\n                    </ng-template> -->\n                </tr>\n            </tbody>\n        </table>\n        <pagination-controls (pageChange)=\"p = $event\"></pagination-controls>\n    </div>\n</section>\n<app-modal [userUid]=\"userUid\"></app-modal>"

/***/ }),

/***/ "./src/app/components/admin/list-sucursales/list-sucursales.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admin/list-sucursales/list-sucursales.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ListSucursalesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListSucursalesComponent", function() { return ListSucursalesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_data_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/data-api.service */ "./src/app/service/data-api.service.ts");
/* harmony import */ var src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/auth.service */ "./src/app/service/auth.service.ts");




var ListSucursalesComponent = /** @class */ (function () {
    function ListSucursalesComponent(dataApi, authService) {
        this.dataApi = dataApi;
        this.authService = authService;
        this.filterPost = '';
        this.pageActual = 1;
        this.isAdmin = null;
        this.userUid = null;
    }
    ListSucursalesComponent.prototype.ngOnInit = function () {
        this.getListSucursales();
        this.getCurrent();
    };
    ListSucursalesComponent.prototype.getCurrent = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.authService.isUserAdmin(_this.userUid).subscribe(function (userRoles) {
                    _this.isAdmin = Object.assign({}, userRoles.roles).hasOwnProperty('admin');
                });
            }
        });
    };
    ListSucursalesComponent.prototype.getListSucursales = function () {
        var _this = this;
        this.dataApi.getAllSucursales().subscribe(function (sucursales) {
            _this.sucursales = sucursales;
        });
    };
    ListSucursalesComponent.prototype.onDeleteSucursal = function (idSucursal) {
        var confirmacion = confirm('¿Estas seguro de eliminar este registro?');
        if (confirmacion) {
            this.dataApi.deleteSucursal(idSucursal);
        }
    };
    ListSucursalesComponent.prototype.onPreUpdateSucursal = function (sucursal) {
        console.log('UPDATE', sucursal);
        this.dataApi.selectedSucursal = Object.assign({}, sucursal);
    };
    ListSucursalesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-sucursales',
            template: __webpack_require__(/*! ./list-sucursales.component.html */ "./src/app/components/admin/list-sucursales/list-sucursales.component.html"),
            styles: [__webpack_require__(/*! ./list-sucursales.component.css */ "./src/app/components/admin/list-sucursales/list-sucursales.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_data_api_service__WEBPACK_IMPORTED_MODULE_2__["DataApiService"], src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], ListSucursalesComponent);
    return ListSucursalesComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/list-transporte/list-transporte.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/admin/list-transporte/list-transporte.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "section {\n    margin-right: 110px;\n    margin-left: 110px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\n/*\n   server-side-angular-way.component.css\n*/\n\n.no-data-available {\n    text-align: center;\n}\n\n/*\n     src/styles.css (i.e. your global style)\n  */\n\n.dataTables_empty {\n    display: none;\n}\n\n.csv {\n    text-align: right;\n    height: auto;\n    width: auto;\n    margin-bottom: 1em;\n}\n\np {\n    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n    font-size: 10px;\n    margin-bottom: -1px;\n    color: #562B89;\n}\n\n.eta {\n    margin-right: 2px;\n    margin-bottom: 2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LXRyYW5zcG9ydGUvbGlzdC10cmFuc3BvcnRlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxvQkFBb0I7SUFDcEIsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUdEOztFQUVFOztBQUVGO0lBQ0ksbUJBQW1CO0NBQ3RCOztBQUdEOztJQUVJOztBQUVKO0lBQ0ksY0FBYztDQUNqQjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsWUFBWTtJQUNaLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLHVIQUF1SDtJQUN2SCxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLGVBQWU7Q0FDbEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsbUJBQW1CO0NBQ3RCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9saXN0LXRyYW5zcG9ydGUvbGlzdC10cmFuc3BvcnRlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJzZWN0aW9uIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDExMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMTBweDtcbn1cblxuYnV0dG9uIHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5pbnB1dCB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuXG4vKlxuICAgc2VydmVyLXNpZGUtYW5ndWxhci13YXkuY29tcG9uZW50LmNzc1xuKi9cblxuLm5vLWRhdGEtYXZhaWxhYmxlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cblxuLypcbiAgICAgc3JjL3N0eWxlcy5jc3MgKGkuZS4geW91ciBnbG9iYWwgc3R5bGUpXG4gICovXG5cbi5kYXRhVGFibGVzX2VtcHR5IHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4uY3N2IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogMWVtO1xufVxuXG5wIHtcbiAgICBmb250LWZhbWlseTogJ0x1Y2lkYSBTYW5zJywgJ0x1Y2lkYSBTYW5zIFJlZ3VsYXInLCAnTHVjaWRhIEdyYW5kZScsICdMdWNpZGEgU2FucyBVbmljb2RlJywgR2VuZXZhLCBWZXJkYW5hLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAtMXB4O1xuICAgIGNvbG9yOiAjNTYyQjg5O1xufVxuXG4uZXRhIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/admin/list-transporte/list-transporte.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/admin/list-transporte/list-transporte.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-car\"></i></span> Listado de transportes</h1>\n        <div class=\"row\">\n            <div class=\"col\">\n                <div class=\"form-group\">\n                    <input type=\"text\" name=\"filterPost\" class=\"form-control\" placeholder=\"&#xf002; Buscar placa...\" style=\"font-family:Arial, FontAwesome\" [(ngModel)]=\"filterPost\">\n                </div>\n            </div>\n            <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modal\" data-target=\"#modalTrnsporte\"><i class=\"fa fa-plus\"></i> Nuevo Vehículo</button>\n\n        </div>\n    </div>\n    <div class=\"col\">\n        <div class=\"csv table table-light \">\n            <p>Exportar CSV</p>\n            <a csvLink [data]=\"transportes\" class=\"eta btn btn-success\"><i class=\"fa fa-file-excel-o\" aria-hidden=\"true\"></i></a>\n        </div>\n        <table class=\"table table-warning table-sm table-hover\">\n            <thead>\n                <tr>\n                    <th scope=\"col\">#</th>\n                    <th scope=\"col\">Tipo</th>\n                    <th scope=\"col\">Marca</th>\n                    <th scope=\"col\">Modelo</th>\n                    <th scope=\"col\">Placa</th>\n                    <th scope=\"col\">Descripción</th>\n                    <th scope=\"col\">&nbsp;</th>\n                    <th scope=\"col\">&nbsp;</th>\n                </tr>\n            </thead>\n            <tbody *ngIf=\"transportes?.length != 0\">\n                <tr *ngFor=\"let transporte of transportes | transporte: filterPost | paginate: { itemsPerPage: 5, currentPage: p }; index as i\" class=\"table-light\">\n                    <th scope=\"row\"> {{i + 1 }} </th>\n                    <td> {{ transporte.tipo }} </td>\n                    <td> {{ transporte.marca }} </td>\n                    <td> {{ transporte.modelo }} </td>\n                    <td> {{ transporte.placa }} </td>\n                    <td> {{ transporte.descripcion }} </td>\n                    <td>\n                        <button class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#modalTrnsporte\" (click)=\"onPreUpdateTransporte(transporte)\"><i class=\"fa fa-pencil\"></i></button>\n                    </td>\n                    <td>\n                        <button class=\"btn btn-danger\" (click)=\"onDeleteTransporte(transporte.id)\"><i class=\"fa fa-trash\"></i></button>\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n        <tbody *ngIf=\"transportes?.length == 0\">\n            <tr>\n                <td colspan=\"8\" class=\"no-data-available\">No data!</td>\n            </tr>\n            <tbody>\n                <pagination-controls (pageChange)=\"p = $event\"></pagination-controls>\n    </div>\n</section>\n<app-modal-transporte></app-modal-transporte>"

/***/ }),

/***/ "./src/app/components/admin/list-transporte/list-transporte.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/admin/list-transporte/list-transporte.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ListTransporteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListTransporteComponent", function() { return ListTransporteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_transporte_transporte_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/transporte/transporte.service */ "./src/app/service/transporte/transporte.service.ts");



var ListTransporteComponent = /** @class */ (function () {
    function ListTransporteComponent(dataTra) {
        this.dataTra = dataTra;
        this.filterPost = '';
        this.pageActual = 1;
    }
    ListTransporteComponent.prototype.ngOnInit = function () {
        this.getListTransportes();
    };
    ListTransporteComponent.prototype.getListTransportes = function () {
        var _this = this;
        this.dataTra.getAllTransportes().subscribe(function (transportes) {
            _this.transportes = transportes;
            _this.data = JSON.stringify(_this.transportes);
        });
    };
    ListTransporteComponent.prototype.onDeleteTransporte = function (idTransporte) {
        var confirmacion = confirm('¿Estas seguro de eliminar este registro?');
        if (confirmacion) {
            this.dataTra.deleteTransporte(idTransporte);
        }
    };
    ListTransporteComponent.prototype.onPreUpdateTransporte = function (transporte) {
        this.dataTra.selectedTransporte = Object.assign({}, transporte);
    };
    ListTransporteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-transporte',
            template: __webpack_require__(/*! ./list-transporte.component.html */ "./src/app/components/admin/list-transporte/list-transporte.component.html"),
            styles: [__webpack_require__(/*! ./list-transporte.component.css */ "./src/app/components/admin/list-transporte/list-transporte.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_transporte_transporte_service__WEBPACK_IMPORTED_MODULE_2__["TransporteService"]])
    ], ListTransporteComponent);
    return ListTransporteComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img {\n    height: 40px;\n    width: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9tb2RhbC1jYXRlZ29yeS1jb252ZW5pby9tb2RhbC1jYXRlZ29yeS1jb252ZW5pby5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksYUFBYTtJQUNiLFlBQVk7Q0FDZiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vbW9kYWwtY2F0ZWdvcnktY29udmVuaW8vbW9kYWwtY2F0ZWdvcnktY29udmVuaW8uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWcge1xuICAgIGhlaWdodDogNDBweDtcbiAgICB3aWR0aDogYXV0bztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modalCategoria\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">\n                    Categoría\n                </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n            </div>\n            <ngx-alerts></ngx-alerts>\n            <div class=\"alert alert-dismissible alert-warning\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n                <h4 class=\"alert-heading\">Importante!</h4>\n                <p class=\"mb-0\">Las medidas estandar para las imágenes son 600 x 198 px.</p>\n            </div>\n            <div class=\"modal-body\">\n                <form name=\"formCategoria\" #formCategoria=\"ngForm\" (ngSubmit)=\"onSaveCategoria(formCategoria)\">\n\n                    <input type=\"hidden\" name=\"id\" [(ngModel)]=\"this.serv_cat.selectedCategoria.id\">\n\n                    <div class=\"form-group\">\n                        <label for=\"nombre\">Nombre</label>\n                        <input type=\"text\" name=\"nombre\" class=\"form-control\" placeholder=\"Farmacia\" [(ngModel)]=\"this.serv_cat.selectedCategoria.nombre\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"posicion\">Posición</label>\n                        <input type=\"number\" name=\"posicion\" class=\"form-control\" placeholder=\"1\" [(ngModel)]=\"this.serv_cat.selectedCategoria.posicion\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"descripcion\" class=\"col-form-label\">Descripción:</label>\n                        <textarea class=\"form-control\" name=\"descripcion\" [(ngModel)]=\"this.serv_cat.selectedCategoria.descripcion\"></textarea>\n                    </div>\n                    <img *ngIf=\"this.serv_cat.selectedCategoria.imagen\" class=\"img\" src=\"{{ this.serv_cat.selectedCategoria.imagen }} \">\n                    <input type=\"hidden\" name=\"photourl\" [(ngModel)]=\"this.serv_cat.selectedCategoria.imagen\">\n\n                    <div class=\"form-group\">\n                        <h5>Seleccionar imagen:</h5>\n                        <input class=\"hi\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                    </div>\n                    <div class=\"progress\">\n                        <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n                        </div>\n                    </div>\n                    <br>\n                    <!-- <input #imageUser type=\"hidden\" [value]=\"urlImage | async\"> -->\n                    <br>\n                    <div>\n                        <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Guardar</button> &nbsp;\n                    </div>\n\n                    <div class=\"oril\">\n                        <!-- <button type=\"hidden\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button> -->\n                    </div>\n                </form>\n            </div>\n\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ModalCategoryConvenioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalCategoryConvenioComponent", function() { return ModalCategoryConvenioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _service_categoria_categoria_convenio_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../service/categoria/categoria-convenio.service */ "./src/app/service/categoria/categoria-convenio.service.ts");
/* harmony import */ var src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");







var ModalCategoryConvenioComponent = /** @class */ (function () {
    function ModalCategoryConvenioComponent(storage, serv_cat, auth, afs) {
        this.storage = storage;
        this.serv_cat = serv_cat;
        this.auth = auth;
        this.afs = afs;
        this.user = {
            uid: '',
            username: '',
            roles: {},
            geolocalizacion: {}
        };
        this.uidSucursal = null;
        this.usuario = {};
    }
    ModalCategoryConvenioComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth.isAuth().subscribe(function (user) {
            if (user) {
                _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                // Consultamos los datos del usuario
                _this.afs.doc("users/" + _this.user.uid).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    // Extraemos el uid de la sucursal al que pertence
                    _this.uidSucursal = _this.usuario.uidSucursal;
                    console.log('uidSucursal', _this.uidSucursal);
                    // this.onSaveCategoria(null, this.uidSucursal);
                });
            }
        });
    };
    ModalCategoryConvenioComponent.prototype.onSaveCategoria = function (formCategoria) {
        console.log('idAdd', formCategoria.value.id);
        if (formCategoria.value.id === null || undefined) {
            // Es un nuevo producto
            this.serv_cat.guardarCategoria(this.urlImage1, this.serv_cat.selectedCategoria.nombre, this.serv_cat.selectedCategoria.posicion, this.serv_cat.selectedCategoria.descripcion, this.uidSucursal);
            console.log('save', this.serv_cat.selectedCategoria);
        }
        else {
            // Cuando se edita un producto.
            console.log('idUpd', this.serv_cat.selectedCategoria.id);
            this.serv_cat.editarCategoria(this.urlImage1, formCategoria.value);
        }
        formCategoria.resetForm();
        this.btnClose.nativeElement.click();
    };
    ModalCategoryConvenioComponent.prototype.onUpload = function (e) {
        var _this = this;
        console.log('subir', e.target.files[0]);
        var id = Math.random().toString(36).substring(2);
        var file = e.target.files[0];
        var filePath = "categorias_convenio/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            return ref.getDownloadURL().subscribe(function (r) {
                _this.urlImage1 = _this.urlImage = r;
                console.log('foto', r);
            });
        }))
            .subscribe();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalCategoryConvenioComponent.prototype, "btnClose", void 0);
    ModalCategoryConvenioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-category-convenio',
            template: __webpack_require__(/*! ./modal-category-convenio.component.html */ "./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.html"),
            styles: [__webpack_require__(/*! ./modal-category-convenio.component.css */ "./src/app/components/admin/modal-category-convenio/modal-category-convenio.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__["AngularFireStorage"],
            _service_categoria_categoria_convenio_service__WEBPACK_IMPORTED_MODULE_4__["CategoriaConvenioService"],
            src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"]])
    ], ModalCategoryConvenioComponent);
    return ModalCategoryConvenioComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.css":
/*!******************************************************************************************!*\
  !*** ./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.css ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".modal-content {\n    width: 170%;\n    margin-left: -35%;\n    /* zoom: 80%; */\n}\n\nspan {\n    font-display: 2em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9tb2RhbC1jb3J0ZS1jb252ZW5pby9tb2RhbC1jb3J0ZS1jb252ZW5pby5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixnQkFBZ0I7Q0FDbkI7O0FBRUQ7SUFDSSxrQkFBa0I7Q0FDckIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2FkbWluL21vZGFsLWNvcnRlLWNvbnZlbmlvL21vZGFsLWNvcnRlLWNvbnZlbmlvLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubW9kYWwtY29udGVudCB7XG4gICAgd2lkdGg6IDE3MCU7XG4gICAgbWFyZ2luLWxlZnQ6IC0zNSU7XG4gICAgLyogem9vbTogODAlOyAqL1xufVxuXG5zcGFuIHtcbiAgICBmb250LWRpc3BsYXk6IDJlbTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.html":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal\" id=\"modalCorteConvenio\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\">Corte al convenio </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n            </div>\n            <div class=\"modal-body\">\n                <p>Filtra por fecha</p>\n                <div class=\"row pad-10 m1\">\n                    <!--Buscadores-->\n                    <div class=\"col-4\">\n                        <span class=\"sp\"><p>Fecha de inicio</p></span>\n                        <input type=\"date\" class=\"form-control\" style=\"font-family: Arial\" [(ngModel)]=\"fechaInicio\">\n                    </div>\n                    <div class=\"col-4\">\n                        <span class=\"sp\"><p>Fecha de termino</p></span>\n                        <input type=\"date\" class=\"form-control\" style=\"font-family: Arial\" [(ngModel)]=\"fechaFin\">\n                    </div>\n                    <div class=\"col-4 m2\">\n                        <button class=\"btn btn-primary\" (click)=\"corteFiltroConvenio(fechaInicio, fechaFin)\"><i class=\"fa fa-search\"></i> Filtrar</button>\n                        <!-- <button class=\"btn btn-primary\" (click)=\"resetFiltro()\" data-toggle=\"modal\" data-target=\"#modalListCorte\"><i class=\"fa fa-search\"></i> Reset </button> -->\n                    </div>\n                </div>\n                <hr class=\"my-4\">\n                <div class=\"col\" id=\"print-section\">\n                    <!-- Datos del convenio  -->\n                    <ng-container *ngFor=\"let convenio of convenio\">\n                        <div class=\"row pad-10\">\n                            <div class=\"col-6\">\n                                <h3>\n                                    Corte de\n                                    <small class=\"text-muted\">{{ convenio.username }}</small>\n                                </h3>\n                                <blockquote class=\"blockquote\">\n                                    <p><strong>Nombre contacto: </strong>{{ convenio.contacto }}.</p>\n                                    <p><strong>Dirección: </strong>{{ convenio.address }}.</p>\n                                    <p><strong>Email: </strong>{{ convenio.email }}.</p>\n                                    <p><strong>Teléfono: </strong>{{ convenio.phone }}.</p>\n                                </blockquote>\n                            </div>\n                            <div class=\"col-3\">\n                                <div style=\"\">\n                                    <img style=\"height: 95px; width: auto;\" [src]=\"convenio.photourl\" alt=\"{{convenio.username}} \">\n                                </div>\n                            </div>\n                            <div class=\"col-3\">\n                                <div>\n                                    <button type=\"button\" class=\"btn btn-outline-primary\" title=\"Imprimir\" printSectionId=\"print-section\" ngxPrint><i class=\"fa fa-print\" aria-hidden=\"true\"></i></button>\n                                </div>\n                                <br>\n                                <table class=\"table table-warning table-sm table-hover\">\n                                    <thead>\n                                        <tr>\n                                            <th scope=\"col\">Concepto</th>\n                                            <th scope=\"col\">Cantidad</th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr class=\"table-success\">\n                                            <td>No. servicios</td>\n                                            <td>{{ noServicios }} </td>\n                                        </tr>\n                                        <tr class=\"table-success\">\n                                            <td>Costo x Servicio</td>\n                                            <td>{{ costoxservicio | currency }} </td>\n                                        </tr>\n                                        <tr class=\"table-success\">\n                                            <td>Total x Servicios</td>\n                                            <td>{{ productoServicios | currency}}</td>\n                                        </tr>\n                                        <tr class=\"table-success\">\n                                            <td>Total x Productos</td>\n                                            <td>{{ totalServicios | currency}}</td>\n                                        </tr>\n                                        <tr class=\"table-success\">\n                                            <td>Utilidad</td>\n                                            <td><span class=\"badge badge-pill badge-success\">{{ diferenciaConvenio | currency}} </span></td>\n                                        </tr>\n                                        <tr class=\"table-success\">\n                                            <td>Costo por suscripción</td>\n                                            <td> {{ suscripcion | currency }} </td>\n                                        </tr>\n                                        <tr class=\"table-success\">\n                                            <td><b>Total a pagar</b></td>\n                                            <td><span class=\"badge badge-pill badge-primary\">{{ suscripcion + productoServicios | currency }} </span></td>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </div>\n                        </div>\n                    </ng-container>\n\n                    <table class=\"table table-warning table-sm table-hover\">\n                        <thead>\n                            <tr>\n                                <th scope=\"col\">#</th>\n                                <th scope=\"col\">Clave</th>\n                                <th scope=\"col\">UID</th>\n                                <th scope=\"col\">Fecha</th>\n                                <th scope=\"col\">Método de pago</th>\n                                <th scope=\"col\">Estatus</th>\n                                <th scope=\"col\">Total productos</th>\n                                <th scope=\"col\">&nbsp;</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let corte of cortes | paginate: { itemsPerPage: 25, currentPage: p } ; index as i\" class=\"table-light\">\n                                <th scope=\"row\">{{ i + 1 }} </th>\n                                <td scope=\"row\">{{ corte.username }} </td>\n                                <td scope=\"row\">{{ corte.uid }} </td>\n                                <td scope=\"row\">{{ corte.fecha | date: 'short' }} </td>\n                                <td scope=\"row\">{{ corte.metodo_pago }} </td>\n                                <td scope=\"row\"><span class=\"badge badge-pill badge-success\"> {{ corte.estatus }}</span></td>\n                                <td scope=\"row\">{{ corte.totalProductos | currency }} </td>\n                                <td scope=\"row\"><a class=\"bz btn btn-outline-primary\" target=\"blank\" title=\"Detalle del servicio.\" routerLink=\"../../servicio/{{corte.uid}}\"><i class=\"fa fa-caret-square-o-down\" aria-hidden=\"true\"></i> </a></td>\n                            </tr>\n                            <tr>\n                                <td></td>\n                                <td></td>\n                                <td></td>\n                                <td></td>\n                                <td></td>\n                                <td><b>Total: </b></td>\n                                <td><span class=\"badge badge-pill badge-primary\">{{totalServicios | currency}} </span> </td>\n                            </tr>\n                        </tbody>\n                        <tbody *ngIf=\"cortes?.length == 0\">\n                            <tr>\n                                <td colspan=\"8\" class=\"no-data-available\">Esperando recopilación de servicios...</td>\n                            </tr>\n                            <tbody>\n                    </table>\n                    <pagination-controls (pageChange)=\"p = $event\"></pagination-controls>\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <!-- <button type=\"button\" (click)=\"get()\" class=\"btn btn-primary\">Save changes</button> -->\n                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: ModalCorteConvenioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalCorteConvenioComponent", function() { return ModalCorteConvenioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var src_app_service_corte_cortes_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/corte/cortes.service */ "./src/app/service/corte/cortes.service.ts");





var ModalCorteConvenioComponent = /** @class */ (function () {
    function ModalCorteConvenioComponent(afs, serviceCorte) {
        this.afs = afs;
        this.serviceCorte = serviceCorte;
        this.convenioUid = null;
    }
    ModalCorteConvenioComponent.prototype.ngOnInit = function () {
        // console.log('convenio uid', this.convenioUid);
        // this.getConvenioInfo();
    };
    // get() {
    //   console.log('id', this.convenioUid);
    //   this.corteFiltroConvenio(null, null, this.convenioUid);
    // }
    ModalCorteConvenioComponent.prototype.corteFiltroConvenio = function (fechaI, fechaF) {
        var _this = this;
        console.log('fechas', fechaI, fechaF);
        // seteando variables a milisegundos
        console.log('Primer filtro', this.date1 = moment__WEBPACK_IMPORTED_MODULE_2__(fechaI).format('x'), 'fin', this.date2 = moment__WEBPACK_IMPORTED_MODULE_2__(fechaF).format('x'));
        this.serviceCorte.getAllCortesConvenio(this.date1, this.date2).subscribe(function (cortes) {
            _this.cortes = cortes;
            _this.totalServicios = _this.cortes.reduce(function (acc, obj) { return acc + (obj.totalProductos); }, 0);
            // Asiganamos el costo x servicio a 25
            _this.suscripcion = 600;
            _this.costoxservicio = 25;
            // Extraemos el numero de servicios
            _this.noServicios = _this.cortes.length;
            // producto de costoxservicio * noservicios
            _this.productoServicios = (_this.costoxservicio * _this.noServicios);
            // diferencia de productoServicios - total servicios
            _this.diferenciaConvenio = (_this.totalServicios - _this.productoServicios);
            console.log('cortes', cortes);
        });
        this.serviceCorte.getListConvenio().subscribe(function (convenio) {
            _this.convenio = convenio;
            console.log('convenio', convenio);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ModalCorteConvenioComponent.prototype, "convenioUid", void 0);
    ModalCorteConvenioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-corte-convenio',
            template: __webpack_require__(/*! ./modal-corte-convenio.component.html */ "./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.html"),
            styles: [__webpack_require__(/*! ./modal-corte-convenio.component.css */ "./src/app/components/admin/modal-corte-convenio/modal-corte-convenio.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            src_app_service_corte_cortes_service__WEBPACK_IMPORTED_MODULE_4__["CortesService"]])
    ], ModalCorteConvenioComponent);
    return ModalCorteConvenioComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".rounded {\n    width: 300px;\n    height: 300px;\n    border-radius: 160px;\n    border: 5px solid rgb(255, 165, 2);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9tb2RhbC11cGRhdGUtcmVwYXJ0aWRvci9tb2RhbC11cGRhdGUtcmVwYXJ0aWRvci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksYUFBYTtJQUNiLGNBQWM7SUFDZCxxQkFBcUI7SUFDckIsbUNBQW1DO0NBQ3RDIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9tb2RhbC11cGRhdGUtcmVwYXJ0aWRvci9tb2RhbC11cGRhdGUtcmVwYXJ0aWRvci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJvdW5kZWQge1xuICAgIHdpZHRoOiAzMDBweDtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDE2MHB4O1xuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigyNTUsIDE2NSwgMik7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ngx-alerts></ngx-alerts>\n<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col\">\n            <div class=\"text-center\" style=\"padding-top: 2em;\">\n                <h3><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-motorcycle\"></i> Editando repartidor: </span> {{repartidor.username}}</h3>\n            </div>\n            <form name=\"formRepartidor\" #formRepartidor=\"ngForm\" (ngSubmit)=\"onSaveRepartidor(formRepartidor)\">\n                <!-- <input type=\"hidden\" name=\"id\" [(ngModel)]=\"this.dataApi.selectedSucursal.id\"> -->\n                <div class=\"form-group\">\n                    <label for=\"nombre\"> <b>Nombre:</b></label>\n                    <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"Juan Eduardo\" [(ngModel)]=\"repartidor.username\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"apellido\"> <b>Apellido:</b></label>\n                    <input type=\"text\" name=\"lastname\" class=\"form-control\" placeholder=\"Martinez Gutierrez\" [(ngModel)]=\"repartidor.lastname\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"direccion\"> <b>Dirección:</b></label>\n                    <input type=\"text\" name=\"address\" class=\"form-control\" placeholder=\"Paseos del Moral # 123\" [(ngModel)]=\"repartidor.address\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"telefono\"> <b>Teléfono:</b></label>\n                    <input type=\"number\" name=\"phone\" class=\"form-control\" placeholder=\"4775642879\" [(ngModel)]=\"repartidor.phone\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"transporte\"> <b>Marca de tranporte:</b></label>\n                    <input type=\"text\" name=\"marca\" class=\"form-control\" placeholder=\"Italika\" [(ngModel)]=\"repartidor.marca\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"placa\"><b>Placa:</b></label>\n                    <input type=\"text\" name=\"placa\" class=\"form-control\" placeholder=\"ASD-987-98\" [(ngModel)]=\"repartidor.placa\">\n                </div>\n                <!-- <div class=\"form-group\">\n                    <label for=\"link_amazon\">Correo electrónico</label>\n                    <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"ejemplo@ejemplo.com.mx\" [(ngModel)]=\"email\" required minlength=\"8\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"link_amazon\">Contraseña</label>\n                    <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"password\" required minlength=\"5\" maxlength=\"10\">\n                </div> -->\n                <hr>\n                <label for=\"imagen\"><b> <em> Imagen actual del repartidor </em></b> </label><br>\n                <div class=\"text-center\">\n                    <img [src]=\"repartidor.photourl\" class=\"rounded\" />\n                </div>\n                <div class=\"form-group\">\n                    <h5> <b>Seleccionar imagen:</b> </h5>\n                    <input class=\"hi\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                </div>\n                <div class=\"progress\">\n                    <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n                        <!-- <span class=\"progressText\" *ngIf=\"urlImage | async\">\n          Ok!!</span> -->\n                    </div>\n                </div>\n                <br>\n                <input #imageUser type=\"hidden\" [value]=\"urlImage | async\">\n                <!-- <button *ngIf=\"urlImage | async; else btnDisabled\" type=\"submit\" class=\"btn btn-lg btn-primary btn-block\">Registrar</button> -->\n                <div style=\"padding-bottom: 2em;\">\n                    <button type=\"submit\" class=\"btn btn-lg btn-primary btn-block\">Actualizar</button>\n                </div>\n\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ModalUpdateRepartidorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalUpdateRepartidorComponent", function() { return ModalUpdateRepartidorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var _service_repartidor_repartidor_data_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../service/repartidor/repartidor-data.service */ "./src/app/service/repartidor/repartidor-data.service.ts");








var ModalUpdateRepartidorComponent = /** @class */ (function () {
    function ModalUpdateRepartidorComponent(route, db, storage, alertService, authService) {
        this.route = route;
        this.db = db;
        this.storage = storage;
        this.alertService = alertService;
        this.authService = authService;
        this.repartidor = {};
    }
    ModalUpdateRepartidorComponent.prototype.ngOnInit = function () {
        this.idRepa = this.route.snapshot.params['uid'];
        // console.log('uidRepa: ', this.idRepa);
        this.getRepa();
    };
    ModalUpdateRepartidorComponent.prototype.getRepa = function () {
        var _this = this;
        this.db
            .collection('users')
            .doc(this.idRepa)
            .valueChanges()
            .subscribe(function (repa) {
            _this.repartidor = repa;
            console.log('repartidor', _this.repartidor);
            _this.urlImage1 = _this.repartidor.photourl;
            _this.Uidsucursal = _this.repartidor.uidSucursal;
        });
    };
    ModalUpdateRepartidorComponent.prototype.onSaveRepartidor = function (formRepartidor) {
        if (this.repartidor.username === '' ||
            this.repartidor.lastname === '' ||
            this.repartidor.marca === '' ||
            this.repartidor.placa === '' ||
            !this.urlImage1) {
            this.alertService.warning('Algunos campos no estan completos.');
        }
        else {
            this.authService.updateRepartidor(this.idRepa, this.repartidor.username, this.repartidor.lastname, this.repartidor.address, this.repartidor.phone, this.repartidor.marca, this.repartidor.placa, this.Uidsucursal, this.urlImage1);
            formRepartidor.resetForm();
        }
    };
    ModalUpdateRepartidorComponent.prototype.onUpload = function (e) {
        var _this = this;
        console.log('subir', e.target.files[0]);
        var id = Math.random()
            .toString(36)
            .substring(2);
        var file = e.target.files[0];
        var filePath = "repartidor/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () {
            return ref.getDownloadURL().subscribe(function (r) {
                _this.urlImage1 = _this.urlImage = r;
                console.log('foto', r);
            });
        }))
            .subscribe();
    };
    ModalUpdateRepartidorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-update-repartidor',
            template: __webpack_require__(/*! ./modal-update-repartidor.component.html */ "./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.html"),
            styles: [__webpack_require__(/*! ./modal-update-repartidor.component.css */ "./src/app/components/admin/modal-update-repartidor/modal-update-repartidor.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__["AngularFireStorage"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_6__["AlertService"],
            _service_repartidor_repartidor_data_service__WEBPACK_IMPORTED_MODULE_7__["RepartidorDataService"]])
    ], ModalUpdateRepartidorComponent);
    return ModalUpdateRepartidorComponent;
}());



/***/ }),

/***/ "./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "agm-map {\n    height: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9tb2RhbC11cGRhdGUtcmVzdGF1cmFudC9tb2RhbC11cGRhdGUtcmVzdGF1cmFudC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztDQUNqQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vbW9kYWwtdXBkYXRlLXJlc3RhdXJhbnQvbW9kYWwtdXBkYXRlLXJlc3RhdXJhbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImFnbS1tYXAge1xuICAgIGhlaWdodDogMzAwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ngx-alerts></ngx-alerts>\n<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col\">\n            <div style=\"padding-top: 2em;\" class=\"text-center\">\n                <h1>\n                    <span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-cutlery\"></i></span> {{restaurante.username}}\n                </h1>\n            </div>\n            <form name=\"formAdmin\" #formAdmin=\"ngForm\" (ngSubmit)=\"cargarImagenes(formAdmin)\">\n                <div class=\"form-group\">\n                    <label for=\"nombre\"><b>Nombre: </b></label>\n                    <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"La Fondita\" [(ngModel)]=\"restaurante.username\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"contacto\"><b>Contacto: </b> </label>\n                    <input type=\"text\" name=\"contacto\" class=\"form-control\" placeholder=\"Maria Gonzalez\" [(ngModel)]=\"restaurante.contacto\">\n                </div>\n                <div class=\"form-group\">\n                    <div class=\"form-group\">\n                        <label><b>Ingrese dirección de entrega:</b> </label>\n                        <input type=\"text\" class=\"form-control\" (keydown.enter)=\"$event.preventDefault()\" placeholder=\"Buscar Dirección\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\" type=\"text\" required=\"true\" #search>\n                    </div>\n                    <agm-map [latitude]=\"restaurante.direccion.latitude\" [longitude]=\"restaurante.direccion.longitude\" [zoom]=\"zoom\">\n                        <agm-marker [latitude]=\"restaurante.direccion.latitude\" [longitude]=\"restaurante.direccion.longitude\" [markerDraggable]=\"true\" (dragEnd)=\"markerDragEnd($event)\"></agm-marker>\n                    </agm-map>\n                    <h5>Address: {{restaurante.address}}</h5>\n                    <div>Latitude: {{restaurante.direccion.latitude}}</div>\n                    <div>Longitude: {{restaurante.direccion.longitude}}</div>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"telefono\"><b>Teléfono: </b></label>\n                    <input type=\"number\" name=\"phone\" class=\"form-control\" placeholder=\"4775642879\" [(ngModel)]=\"restaurante.phone\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"categoria\"><b>Selecciona una categoría:</b> </label>\n                    <select class=\"form-control\" id=\"categoria\" name=\"categoria\" [(ngModel)]=\"restaurante.uidCategoria\">\n                  <option *ngFor=\"let categoria of categorias\" value=\"{{categoria.id}}\">{{ categoria.nombre }} </option>\n                </select>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"hora_abierto\"><b>Hora de abrir: </b> </label>\n                    <input type=\"time\" class=\"form-control\" name=\"hora_abierto\" [(ngModel)]=\"restaurante.hora_abierto\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"hora_cerrar\"><b>Hora de cerrar:</b> </label>\n                    <input type=\"time\" class=\"form-control\" name=\"hora_cerrar\" [(ngModel)]=\"restaurante.hora_cerrar\">\n                </div>\n                <!-- <div class=\"form-group\">\n                    <label for=\"email\">Correo electrónico</label>\n                    <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"ejemplo@ejemplo.com.mx\" [(ngModel)]=\"restaurante.email\" required minlength=\"8\">\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"password\">Contraseña</label>\n                    <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"restaurante.password\" required minlength=\"5\" maxlength=\"10\">\n                </div> -->\n                <hr>\n                <div class=\"text-center\">\n                    <img [src]=\"restaurante.photourl\" class=\"rounded\" width=\"20%\" height=\"20%\">\n                </div>\n                <div class=\"form-group\">\n                    <h5>Seleccionar imagen:</h5>\n                    <input class=\"form-control\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                </div>\n                <div class=\"progress\">\n                    <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n                        <!-- <span class=\"progressText\" *ngIf=\"urlImage | async\">\n              Ok!!</span> -->\n                    </div>\n                </div>\n                <div style=\" padding-top: 2em; padding-bottom: 2em;\">\n                    <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Actualizar</button> &nbsp;\n                    <!-- <button (click)=\"limpiarArchivos()\" class=\"btn btn-danger\">Limpiar</button> -->\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ModalUpdateRestaurantComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalUpdateRestaurantComponent", function() { return ModalUpdateRestaurantComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/restaurante/restaurante-data.service */ "./src/app/service/restaurante/restaurante-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");











var ModalUpdateRestaurantComponent = /** @class */ (function () {
    function ModalUpdateRestaurantComponent(mapsAPILoader, ngZone, db, dataRes, route, alertService, authService, storage) {
        this.mapsAPILoader = mapsAPILoader;
        this.ngZone = ngZone;
        this.db = db;
        this.dataRes = dataRes;
        this.route = route;
        this.alertService = alertService;
        this.authService = authService;
        this.storage = storage;
        // maps
        this.title = 'AGM project';
        this.restaurante = {};
    }
    ModalUpdateRestaurantComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.idRest = this.route.snapshot.params['uid'];
        // console.log("idRest", idRest);
        this.getRest();
        this.categoriaList();
        this.mapsAPILoader.load().then(function () {
            _this.setCurrentLocation();
            _this.geoCoder = new google.maps.Geocoder();
            var autocomplete = new google.maps.places.Autocomplete(_this.searchElementRef.nativeElement, {
                types: ['address']
            });
            autocomplete.addListener('place_changed', function () {
                _this.ngZone.run(function () {
                    // get the place result
                    var place = autocomplete.getPlace();
                    // verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }
                    // set latitude, longitude and zoom
                    _this.latitude = place.geometry.location.lat();
                    _this.longitude = place.geometry.location.lng();
                    _this.zoom = 8;
                });
            });
        });
    };
    // trae todos las categorías
    ModalUpdateRestaurantComponent.prototype.categoriaList = function () {
        var _this = this;
        this.db
            .collection('categoria_critico')
            .valueChanges()
            .subscribe(function (cat) {
            _this.categorias = cat;
            console.log('cate', cat);
        });
    };
    // traer restaurante
    ModalUpdateRestaurantComponent.prototype.getRest = function () {
        var _this = this;
        this.db
            .collection('users')
            .doc(this.idRest)
            .valueChanges()
            .subscribe(function (rest) {
            _this.restaurante = rest;
            console.log('restaurante', _this.restaurante.photourl);
            _this.urlImage1 = _this.restaurante.photourl;
            _this.Uidsucursal = _this.restaurante.uidSucursal;
        });
    };
    // Get Current Location Coordinates
    ModalUpdateRestaurantComponent.prototype.setCurrentLocation = function () {
        var _this = this;
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                _this.latitude = position.coords.latitude;
                _this.longitude = position.coords.longitude;
                _this.zoom = 8;
                _this.getAddress(_this.latitude, _this.longitude);
            });
        }
    };
    ModalUpdateRestaurantComponent.prototype.getAddress = function (latitude, longitude) {
        var _this = this;
        this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, function (results, status) {
            console.log(results);
            console.log(status);
            if (status === 'OK') {
                if (results[0]) {
                    _this.zoom = 12;
                    _this.address = results[0].formatted_address;
                }
                else {
                    window.alert('No results found');
                }
            }
            else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    };
    ModalUpdateRestaurantComponent.prototype.markerDragEnd = function ($event) {
        console.log('markdrag', $event);
        this.latitude = $event.coords.lat;
        this.longitude = $event.coords.lng;
        this.getAddress(this.latitude, this.longitude);
    };
    // subir convenio
    ModalUpdateRestaurantComponent.prototype.cargarImagenes = function (formAdmin) {
        // console.log('hora', this.hora_abierto);
        var abierto = moment__WEBPACK_IMPORTED_MODULE_7__(this.restaurante.hora_abierto, 'HHmmss');
        console.log('este', abierto.format());
        console.log(abierto.format('HH:mm:ss'));
        var abiertoFormat = abierto.format('HH:mm:ss');
        var cerrar = moment__WEBPACK_IMPORTED_MODULE_7__(this.restaurante.hora_cerrar, 'HHmmss');
        console.log(cerrar.format());
        console.log(cerrar.format('HH:mm:ss'));
        var cerrarFormat = cerrar.format('HH:mm:ss');
        if (this.restaurante.username === '' ||
            this.restaurante.contacto === '' ||
            this.restaurante.address === '' ||
            this.restaurante.phone === '' ||
            this.restaurante.hora_cerrar === '' ||
            this.restaurante.uidCategoria === '' ||
            this.restaurante.direccion === '' ||
            !this.urlImage1) {
            this.alertService.warning('Algunos campos no estan completos.');
        }
        else {
            this.direccion = new firebase__WEBPACK_IMPORTED_MODULE_4__["firestore"].GeoPoint(this.restaurante.direccion._lat, this.restaurante.direccion._long);
            this.authService.updateConvenio(abiertoFormat, cerrarFormat, this.idRest, this.restaurante.username, this.restaurante.contacto, this.restaurante.address, this.restaurante.phone, this.restaurante.uidCategoria, this.restaurante.direccion, this.Uidsucursal, this.urlImage1);
            // formAdmin.resetForm();
            // console.log('abierto', cerrar);
        }
    };
    // imagen
    ModalUpdateRestaurantComponent.prototype.onUpload = function (e) {
        var _this = this;
        console.log('subir', e.target.files[0]);
        var id = Math.random()
            .toString(36)
            .substring(2);
        var file = e.target.files[0];
        var filePath = "users/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["finalize"])(function () {
            return ref.getDownloadURL().subscribe(function (r) {
                _this.urlImage1 = _this.urlImage = r;
                console.log('foto', r);
            });
        }))
            .subscribe();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('search'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalUpdateRestaurantComponent.prototype, "searchElementRef", void 0);
    ModalUpdateRestaurantComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-update-restaurant',
            template: __webpack_require__(/*! ./modal-update-restaurant.component.html */ "./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.html"),
            styles: [__webpack_require__(/*! ./modal-update-restaurant.component.css */ "./src/app/components/admin/modal-update-restaurant/modal-update-restaurant.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_agm_core__WEBPACK_IMPORTED_MODULE_2__["MapsAPILoader"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"],
            _service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_5__["RestauranteDataService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_8__["AlertService"],
            _service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_5__["RestauranteDataService"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"]])
    ], ModalUpdateRestaurantComponent);
    return ModalUpdateRestaurantComponent;
}());



/***/ }),

/***/ "./src/app/components/hero/hero.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/hero/hero.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaGVyby9oZXJvLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/hero/hero.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/hero/hero.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Main jumbotron for a primary marketing message or call to action -->\n<div class=\"jumbotron\">\n    <div class=\"container\">\n        <h1 class=\"display-3\">Hello, world!</h1>\n        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>\n        <p>\n            <a class=\"btn btn-primary btn-lg\" href=\"#\" role=\"button\">Learn more &raquo;</a>\n        </p>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/hero/hero.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/hero/hero.component.ts ***!
  \***************************************************/
/*! exports provided: HeroComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeroComponent", function() { return HeroComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeroComponent = /** @class */ (function () {
    function HeroComponent() {
    }
    HeroComponent.prototype.ngOnInit = function () {
    };
    HeroComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-hero',
            template: __webpack_require__(/*! ./hero.component.html */ "./src/app/components/hero/hero.component.html"),
            styles: [__webpack_require__(/*! ./hero.component.css */ "./src/app/components/hero/hero.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeroComponent);
    return HeroComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".im {\n    height: auto;\n    width: 100%;\n}\n\n.dw {\n    text-align: center;\n    margin-top: 8em;\n}\n\n.lolo {\n    position: relative;\n    margin-top: 6rem !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7SUFDYixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0NBQ25COztBQUVEO0lBQ0ksbUJBQW1CO0lBQ25CLDRCQUE0QjtDQUMvQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW0ge1xuICAgIGhlaWdodDogYXV0bztcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmR3IHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogOGVtO1xufVxuXG4ubG9sbyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIG1hcmdpbi10b3A6IDZyZW0gIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n    <div class=\"lolo\">\n\n        <img src=\"../assets/imgs/lolo.svg\" class=\"img-fluid img-responsive\">\n\n    </div>\n\n    <!-- <div class=\"dw\"> -->\n    <!-- <h1 class=\"display-3 text-center\">!Bienvenido!</h1> -->\n\n    <!-- <div class=\"card text-white bg-primary mb-3\" style=\"max-width: 20rem;\">\n            <div class=\"card-header\">Login</div>\n            <div class=\"card-body\">\n                <h4 class=\"card-title\">Soy BringMe Admin</h4>\n                <p class=\"lead\">\n                    <a class=\"btn btn-secondary\" href=\"\" routerLink=\"/user/login\" role=\"button\">Ir al Login</a>\n                </p>\n            </div>\n        </div> -->\n    <!-- </div> -->\n</div>"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_data_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/data-api.service */ "./src/app/service/data-api.service.ts");



var HomeComponent = /** @class */ (function () {
    function HomeComponent(dataApi) {
        this.dataApi = dataApi;
        this.sucursales = [];
        this.sucursal = '';
    }
    HomeComponent.prototype.ngOnInit = function () {
        // this.dataApi.getAllSucursales().subscribe(sucursales => {
        //   console.log('SUCURSALES', sucursales);
        //   this.sucursales = sucursales;
        // });
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_data_api_service__WEBPACK_IMPORTED_MODULE_2__["DataApiService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/modal-administrador/modal-administrador.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/components/modal-administrador/modal-administrador.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWwtYWRtaW5pc3RyYWRvci9tb2RhbC1hZG1pbmlzdHJhZG9yLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/modal-administrador/modal-administrador.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/modal-administrador/modal-administrador.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modalAdmin\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">\n                    Agregar Nuevo administrador\n                    <!-- {{!this.dataApi.selectedSucursal.id ? 'Nueva Sucursal' : 'Actualizar Sucursal'}} -->\n                </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n            </div>\n            <div class=\"modal-body\">\n                <form name=\"formAdmin\" #formAdmin=\"ngForm\" (ngSubmit)=\"onSaveAdministrador(formAdmin)\">\n                    <!-- <input type=\"hidden\" name=\"id\" [(ngModel)]=\"this.dataApi.selectedSucursal.id\"> -->\n                    <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"isError\">\n                        {{msgError}}\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Nombre</label>\n                        <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"Juan Eduardo\" [(ngModel)]=\"username\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Apellido</label>\n                        <input type=\"text\" name=\"lastname\" class=\"form-control\" placeholder=\"Martinez Gutierrez\" [(ngModel)]=\"lastname\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Dirección</label>\n                        <input type=\"text\" name=\"address\" class=\"form-control\" placeholder=\"Paseos del Moral # 123\" [(ngModel)]=\"address\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Teléfono</label>\n                        <input type=\"number\" name=\"phone\" class=\"form-control\" placeholder=\"4775642879\" [(ngModel)]=\"phone\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Correo electrónico</label>\n                        <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"ejemplo@ejemplo.com.mx\" [(ngModel)]=\"email\" required minlength=\"8\">\n                    </div>\n                    <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"email.touched && !email.valid\">\n                        <div *ngIf=\"email.errors.required\">Correo requerido</div>\n                        <div *ngIf=\"email.errors.minlength\">EL correo debe tener más de 8 caracteres.</div>\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Contraseña</label>\n                        <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"password\" required minlength=\"5\" maxlength=\"15\">\n                    </div>\n                    <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"password.touched && !password.valid\">\n                        <div *ngIf=\"password.errors.required\">Contraseña requerida</div>\n                        <div *ngIf=\"password.errors.minlength\">La contraseña debe tener al menos 5 caracteres.</div>\n                    </div>\n                    <hr>\n                    <div class=\"form-group\">\n                        <h5>Seleccionar imagen:</h5>\n                        <input class=\"hi\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                    </div>\n                    <div class=\"progress\">\n                        <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n\n                            <!-- <span class=\"progressText\" *ngIf=\"urlImage | async\">\n        Ok!!</span> -->\n                        </div>\n                    </div>\n                    <br>\n                    <input #imageUser type=\"hidden\" [value]=\"urlImage | async\">\n                    <button *ngIf=\"urlImage | async; else btnDisabled\" type=\"submit\" class=\"btn btn-lg btn-primary btn-block\">Registrar</button>\n                    <ng-template #btnDisabled>\n                        <button type=\"submit\" disabled=true class=\"btn btn-lg btn-secondary btn-block\">Registrar</button>\n                    </ng-template>\n                    <!-- <button type=\"hidden\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button> -->\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/modal-administrador/modal-administrador.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/modal-administrador/modal-administrador.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ModalAdministradorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalAdministradorComponent", function() { return ModalAdministradorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _service_administrador_administrador_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/administrador/administrador-data.service */ "./src/app/service/administrador/administrador-data.service.ts");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");








var ModalAdministradorComponent = /** @class */ (function () {
    function ModalAdministradorComponent(authService, router, storage, db, alertService) {
        this.authService = authService;
        this.router = router;
        this.storage = storage;
        this.db = db;
        this.alertService = alertService;
        this.email = '';
        this.password = '';
        this.username = '';
        this.lastname = '';
        this.address = '';
        this.phone = '';
        // Sucursales
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    ModalAdministradorComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Sucursal
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    console.log('usuario', data);
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    console.log('BD', _this.Uidsucursal);
                    // this.getListRestaurante(this.Uidsucursal);
                    console.log('usuer', _this.Uidsucursal);
                });
            }
        });
    };
    ModalAdministradorComponent.prototype.onSaveAdministrador = function (formAdmin) {
        var _this = this;
        if (this.username === '' || this.lastname === '' || this.address === '' ||
            this.phone === '' || this.email === '' || this.password === '' || !this.urlImage) {
            this.alertService.warning('Algunos campos no estan completos.');
        }
        else if (this.password.length < 7) {
            this.alertService.warning('La contraseña debe tener más de 7 caracteres.');
        }
        else {
            this.authService.registerUser(this.username, this.lastname, this.address, this.phone, this.email, this.password, this.Uidsucursal)
                .then(function (res) {
                _this.authService.isAuth().subscribe(function (user) {
                    if (user) {
                        user.updateProfile({
                            displayName: '',
                            photoURL: _this.inputImageUser.nativeElement.value
                        }).then(function () {
                            formAdmin.resetForm();
                            _this.btnClose.nativeElement.click();
                        }).catch(function (error) { return _this.alertService.danger(error); });
                    }
                });
            }).catch(function (err) { return _this.alertService.danger(err); });
        }
    };
    ModalAdministradorComponent.prototype.onUpload = function (e) {
        var _this = this;
        // console.log('subir', e.target.files[0]);
        var id = Math.random().toString(36).substring(2);
        var file = e.target.files[0];
        var filePath = "administrador/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () { return _this.urlImage = ref.getDownloadURL(); })).subscribe();
        console.log('id', id);
        console.log('file', file);
        console.log('filepath', filePath);
        console.log('ref', ref);
        console.log('id', id);
        console.log('urlimage', JSON.stringify(this.urlImage));
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalAdministradorComponent.prototype, "btnClose", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('imageUser'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalAdministradorComponent.prototype, "inputImageUser", void 0);
    ModalAdministradorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-administrador',
            template: __webpack_require__(/*! ./modal-administrador.component.html */ "./src/app/components/modal-administrador/modal-administrador.component.html"),
            styles: [__webpack_require__(/*! ./modal-administrador.component.css */ "./src/app/components/modal-administrador/modal-administrador.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_administrador_administrador_data_service__WEBPACK_IMPORTED_MODULE_6__["AdministradorDataService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__["AngularFireStorage"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_7__["AlertService"]])
    ], ModalAdministradorComponent);
    return ModalAdministradorComponent;
}());



/***/ }),

/***/ "./src/app/components/modal-r/modal-r.component.css":
/*!**********************************************************!*\
  !*** ./src/app/components/modal-r/modal-r.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWwtci9tb2RhbC1yLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/modal-r/modal-r.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/modal-r/modal-r.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modalAdmin\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">\n                    Agregar Nuevo Restaurante\n                    <!-- {{!this.dataApi.selectedSucursal.id ? 'Nueva Sucursal' : 'Actualizar Sucursal'}} -->\n                </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n            </div>\n            <div class=\"modal-body\">\n\n                <form name=\"formAdmin\" #formAdmin=\"ngForm\" (click)=\"cargarImagenes(formAdmin)\">\n                    <div class=\"form-group\">\n                        <label for=\"email\">Correo electrónico</label>\n                        <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"ejemplo@ejemplo.com.mx\" [(ngModel)]=\"email\" required minlength=\"8\">\n                    </div>\n                    <div *ngIf=\"imagenPreview\">\n                        <img [src]=\"\" alt=\"\">\n                    </div>\n                    <div>\n                        <button type=\"file\" color=\"primary\">Seleccionar</button>\n                    </div>\n                    <div>\n                        <button type=\"submit\">Registrar</button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/modal-r/modal-r.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/modal-r/modal-r.component.ts ***!
  \*********************************************************/
/*! exports provided: ModalRComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalRComponent", function() { return ModalRComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ModalRComponent = /** @class */ (function () {
    function ModalRComponent() {
    }
    ModalRComponent.prototype.ngOnInit = function () {
    };
    ModalRComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-r',
            template: __webpack_require__(/*! ./modal-r.component.html */ "./src/app/components/modal-r/modal-r.component.html"),
            styles: [__webpack_require__(/*! ./modal-r.component.css */ "./src/app/components/modal-r/modal-r.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ModalRComponent);
    return ModalRComponent;
}());



/***/ }),

/***/ "./src/app/components/modal-repartidor/modal-repartidor.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/modal-repartidor/modal-repartidor.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWwtcmVwYXJ0aWRvci9tb2RhbC1yZXBhcnRpZG9yLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/modal-repartidor/modal-repartidor.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/modal-repartidor/modal-repartidor.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modalRepartidor\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n    <ngx-alerts></ngx-alerts>\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">\n                    Nuevo Repartidor\n                    <!-- {{!this.dataApi.selectedSucursal.id ? 'Nueva Sucursal' : 'Actualizar Sucursal'}} -->\n                </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n            </div>\n            <div class=\"modal-body\">\n                <form name=\"formRepartidor\" #formRepartidor=\"ngForm\" (ngSubmit)=\"onSaveRepartidor(formRepartidor)\">\n                    <!-- <input type=\"hidden\" name=\"id\" [(ngModel)]=\"this.dataApi.selectedSucursal.id\"> -->\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Nombre</label>\n                        <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"Juan Eduardo\" [(ngModel)]=\"username\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Apellido</label>\n                        <input type=\"text\" name=\"lastname\" class=\"form-control\" placeholder=\"Martinez Gutierrez\" [(ngModel)]=\"lastname\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Dirección</label>\n                        <input type=\"text\" name=\"address\" class=\"form-control\" placeholder=\"Paseos del Moral # 123\" [(ngModel)]=\"address\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Teléfono</label>\n                        <input type=\"number\" name=\"phone\" class=\"form-control\" placeholder=\"4775642879\" [(ngModel)]=\"phone\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"placa\">Marca de tranporte</label>\n                        <input type=\"text\" name=\"marca\" class=\"form-control\" placeholder=\"Italika\" [(ngModel)]=\"marca\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"placa\">Placa</label>\n                        <input type=\"text\" name=\"placa\" class=\"form-control\" placeholder=\"ASD-987-98\" [(ngModel)]=\"placa\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Correo electrónico</label>\n                        <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"ejemplo@ejemplo.com.mx\" [(ngModel)]=\"email\" required minlength=\"8\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Contraseña</label>\n                        <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"password\" required minlength=\"5\" maxlength=\"10\">\n                    </div>\n                    <hr>\n                    <div class=\"form-group\">\n                        <h5>Seleccionar imagen:</h5>\n                        <input class=\"hi\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                    </div>\n                    <div class=\"progress\">\n                        <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n                            <!-- <span class=\"progressText\" *ngIf=\"urlImage | async\">\n          Ok!!</span> -->\n                        </div>\n                    </div>\n                    <br>\n                    <input #imageUser type=\"hidden\" [value]=\"urlImage | async\">\n                    <!-- <button *ngIf=\"urlImage | async; else btnDisabled\" type=\"submit\" class=\"btn btn-lg btn-primary btn-block\">Registrar</button> -->\n                    <button type=\"submit\" class=\"btn btn-lg btn-primary btn-block\">Registrar</button>\n                    <ng-template #btnDisabled>\n                        <button type=\"submit\" disabled=true class=\"btn btn-lg btn-secondary btn-block\">Registrar</button>\n                    </ng-template>\n                    <!-- <button type=\"hidden\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button> -->\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/modal-repartidor/modal-repartidor.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/modal-repartidor/modal-repartidor.component.ts ***!
  \***************************************************************************/
/*! exports provided: ModalRepartidorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalRepartidorComponent", function() { return ModalRepartidorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");








var ModalRepartidorComponent = /** @class */ (function () {
    function ModalRepartidorComponent(authService, router, storage, db, alertService) {
        this.authService = authService;
        this.router = router;
        this.storage = storage;
        this.db = db;
        this.alertService = alertService;
        this.email = null;
        this.password = null;
        this.username = null;
        this.lastname = null;
        this.address = null;
        this.phone = null;
        this.placa = null;
        this.marca = null;
        this.uidUser = null;
        this.usuario = {};
        this.Uidsucursal = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            uidSucursal: '',
            roles: {}
        };
    }
    ModalRepartidorComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('user', this.user);
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    // tslint:disable-next-line:no-unused-expression
                    _this.Uidsucursal = _this.usuario.uidSucursal;
                    console.log('usuer', _this.Uidsucursal);
                });
            }
        });
    };
    ModalRepartidorComponent.prototype.onUpload = function (e) {
        var _this = this;
        console.log('subir', e.target.files[0]);
        var id = Math.random().toString(36).substring(2);
        var file = e.target.files[0];
        var filePath = "repartidor/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () {
            return ref.getDownloadURL().subscribe(function (r) {
                _this.urlImage1 = _this.urlImage = r;
                console.log('foto', r);
            });
        }))
            .subscribe();
    };
    // onImg (img) {
    //   this.onSaveRepartidor(null, img);
    // }
    ModalRepartidorComponent.prototype.onSaveRepartidor = function (formRepartidor) {
        var _this = this;
        if (this.username === '' || this.lastname === '' || this.marca === '' || this.placa === '' ||
            this.email === '' || this.password === '' || !this.urlImage) {
            this.alertService.warning('Algunos campos no estan completos.');
        }
        else if (this.password.length < 7) {
            this.alertService.warning('La contraseña debe tener más de 7 caracteres.');
        }
        else {
            this.authService.newRegister(this.username, this.lastname, this.address, this.phone, this.marca, this.placa, this.email, this.password, this.Uidsucursal, this.urlImage1)
                .then(function (res) {
                _this.authService.isAuth().subscribe(function (user) {
                    if (user) {
                        user.updateProfile({
                            displayName: '',
                            photoURL: _this.inputImageUser.nativeElement.value
                        }).then(function () {
                        }).catch(function (error) { return _this.alertService.danger(error); });
                    }
                });
            }).catch(function (err) { return _this.alertService.danger(err); });
            formRepartidor.resetForm();
            this.btnClose.nativeElement.click();
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalRepartidorComponent.prototype, "btnClose", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('imageUser'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalRepartidorComponent.prototype, "inputImageUser", void 0);
    ModalRepartidorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-repartidor',
            template: __webpack_require__(/*! ./modal-repartidor.component.html */ "./src/app/components/modal-repartidor/modal-repartidor.component.html"),
            styles: [__webpack_require__(/*! ./modal-repartidor.component.css */ "./src/app/components/modal-repartidor/modal-repartidor.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__["AngularFireStorage"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_7__["AlertService"]])
    ], ModalRepartidorComponent);
    return ModalRepartidorComponent;
}());



/***/ }),

/***/ "./src/app/components/modal-restaurante/modal-restaurante.component.css":
/*!******************************************************************************!*\
  !*** ./src/app/components/modal-restaurante/modal-restaurante.component.css ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".oril {\n    text-align: right;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC1yZXN0YXVyYW50ZS9tb2RhbC1yZXN0YXVyYW50ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0NBQ3JCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC1yZXN0YXVyYW50ZS9tb2RhbC1yZXN0YXVyYW50ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm9yaWwge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/modal-restaurante/modal-restaurante.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/components/modal-restaurante/modal-restaurante.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modalAdmin\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">\n                    Agregar Nuevo Convenio\n                    <!-- {{!this.dataApi.selectedSucursal.id ? 'Nueva Sucursal' : 'Actualizar Sucursal'}} -->\n                </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n            </div>\n            <div class=\"modal-body\">\n                <form name=\"formAdmin\" #formAdmin=\"ngForm\" (click)=\"cargarImagenes(formAdmin)\">\n                    <!-- <input type=\"hidden\" name=\"id\" [(ngModel)]=\"this.dataApi.selectedSucursal.id\"> -->\n                    <!-- <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"isError\">\n                        {{msgError}}\n                    </div> -->\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Nombre</label>\n                        <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"La Fondita\" [(ngModel)]=\"username\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"contacto\">Contacto</label>\n                        <input type=\"text\" name=\"contacto\" class=\"form-control\" placeholder=\"Maria Gonzalez\" [(ngModel)]=\"contacto\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Dirección</label>\n                        <input type=\"text\" name=\"address\" class=\"form-control\" placeholder=\"Paseos del Moral # 123\" [(ngModel)]=\"address\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Teléfono</label>\n                        <input type=\"number\" name=\"phone\" class=\"form-control\" placeholder=\"4775642879\" [(ngModel)]=\"phone\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"email\">Correo electrónico</label>\n                        <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"ejemplo@ejemplo.com.mx\" [(ngModel)]=\"email\" required minlength=\"8\">\n                    </div>\n                    <!-- <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"email.touched && !email.valid\">\n                        <div *ngIf=\"email.errors.required\">Correo requerido</div>\n                        <div *ngIf=\"email.errors.minlength\">EL correo debe tener más de 8 caracteres.</div>\n                    </div> -->\n                    <div class=\"form-group\">\n                        <label for=\"password\">Contraseña</label>\n                        <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"password\" required minlength=\"5\" maxlength=\"10\">\n                    </div>\n                    <!-- <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"password.touched && !password.valid\">\n                        <div *ngIf=\"password.errors.required\">Contraseña requerida</div>\n                        <div *ngIf=\"password.errors.minlength\">La contraseña debe tener al menos 5 caracteres.</div>\n                    </div> -->\n                    <hr>\n                    <div class=\"form-group\">\n                        <div class=\"col-md-12\">\n                            <p> Seleccione el logo del Convenio </p>\n                            <div appNgDropFiles [archivos]=\"archivos\" (mouseSobre)=\"pruebaSobreElemento = $event\" [ngClass]=\"{'file-over': estaSobreElemento}\" class=\"well drop-zone\">\n                                <p>Deje caer el archivo aquí</p>\n                                <img src=\"assets/drop-images.png\" alt=\"\">\n                            </div>\n                            <table class=\"table\">\n                                <thead class=\"table table-warning\">\n                                    <tr>\n                                        <th>Nombre</th>\n                                        <th>Tamaño</th>\n                                        <th>Progreso</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr *ngFor=\"let archivo of archivos\">\n                                        <td> {{ archivo.nombreArchivo }} </td>\n                                        <td> {{ archivo.archivo.size / 1024 / 1024 | number: '.2-2' }} MB</td>\n                                        <td>\n                                            <div class=\"progress\">\n                                                <div class=\"progress-bar\" role=\"progressbar\" [ngStyle]=\"{'width': archivo.progreso + '%'}\"></div>\n                                            </div>\n                                        </td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n                    <br>\n                    <div>\n                        <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"archivos.length === 0\">Guardar</button> &nbsp;\n                        <button (click)=\"limpiarArchivos()\" class=\"btn btn-danger\">Limpiar</button>\n                    </div>\n                    <div class=\"oril\">\n                        <button type=\"hidden\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/modal-restaurante/modal-restaurante.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/modal-restaurante/modal-restaurante.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ModalRestauranteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalRestauranteComponent", function() { return ModalRestauranteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../service/restaurante/restaurante-data.service */ "./src/app/service/restaurante/restaurante-data.service.ts");






var ModalRestauranteComponent = /** @class */ (function () {
    // public photoURL: string = '';
    // data: any[];
    // public selectedUser: UserInterface = { roles: {} };
    function ModalRestauranteComponent(authService, router, storage, db) {
        this.authService = authService;
        this.router = router;
        this.storage = storage;
        this.db = db;
        this.estaSobreElemento = false;
        this.archivos = [];
        this.email = '';
        this.password = '';
        this.username = '';
        this.contacto = '';
        this.address = '';
        this.phone = '';
        this.tipo = 3;
        this.activo = true;
    }
    ModalRestauranteComponent.prototype.ngOnInit = function () {
    };
    // cargarImagenes(formAdmin: NgForm) {
    //   this.authService.cargarImagenesFirebase(this.archivos, this.email, this.password, this.username,
    //      this.contacto, this.address, this.phone);
    //   formAdmin.resetForm();
    //   this.limpiarArchivos();
    //   // this.btnClose.nativeElement.click();
    // }
    ModalRestauranteComponent.prototype.limpiarArchivos = function () {
        this.archivos = [];
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalRestauranteComponent.prototype, "btnClose", void 0);
    ModalRestauranteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-restaurante',
            template: __webpack_require__(/*! ./modal-restaurante.component.html */ "./src/app/components/modal-restaurante/modal-restaurante.component.html"),
            styles: [__webpack_require__(/*! ./modal-restaurante.component.css */ "./src/app/components/modal-restaurante/modal-restaurante.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_5__["RestauranteDataService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_3__["AngularFireStorage"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"]])
    ], ModalRestauranteComponent);
    return ModalRestauranteComponent;
}());



/***/ }),

/***/ "./src/app/components/modal-transporte/modal-transporte.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/modal-transporte/modal-transporte.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWwtdHJhbnNwb3J0ZS9tb2RhbC10cmFuc3BvcnRlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/modal-transporte/modal-transporte.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/modal-transporte/modal-transporte.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modalTrnsporte\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">\n                    {{!this.dataTra.selectedTransporte.id ? 'Nueva Sucursal' : 'Actualizar Sucursal'}}\n                </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n            </div>\n            <div class=\"modal-body\">\n                <form name=\"formTransporte\" #formTransporte=\"ngForm\" (ngSubmit)=\"onSaveTransporte(formTransporte)\">\n                    <input type=\"hidden\" name=\"id\" [(ngModel)]=\"this.dataTra.selectedTransporte.id\">\n\n                    <div class=\"form-group\">\n                        <label for=\"tipo\">Tipo</label>\n                        <input type=\"text\" name=\"tipo\" class=\"form-control\" placeholder=\"Moto, Camioneta, etc\" [(ngModel)]=\"this.dataTra.selectedTransporte.tipo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"marca\">Marca</label>\n                        <input type=\"text\" name=\"marca\" class=\"form-control\" placeholder=\"Ford, Italika\" [(ngModel)]=\"this.dataTra.selectedTransporte.marca\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"modelo\">Modelo</label>\n                        <input type=\"text\" name=\"modelo\" class=\"form-control\" placeholder=\"S10, Lux, etc\" [(ngModel)]=\"this.dataTra.selectedTransporte.modelo\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"placa\">Placa</label>\n                        <input type=\"text\" name=\"placa\" class=\"form-control\" placeholder=\"S40-12-52\" [(ngModel)]=\"this.dataTra.selectedTransporte.placa\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"descripcion\" class=\"col-form-label\">Descripcion:</label>\n                        <textarea class=\"form-control\" name=\"descripcion\" [(ngModel)]=\"this.dataTra.selectedTransporte.descripcion\"></textarea>\n                    </div>\n                    <div class=\"modal-footer\">\n                        <button type=\"button\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button>\n                        <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\n                    </div>\n                </form>\n            </div>\n\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/modal-transporte/modal-transporte.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/modal-transporte/modal-transporte.component.ts ***!
  \***************************************************************************/
/*! exports provided: ModalTransporteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalTransporteComponent", function() { return ModalTransporteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_transporte_transporte_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/transporte/transporte.service */ "./src/app/service/transporte/transporte.service.ts");


// import { TransporteInterface } from '../../models/transporte';

var ModalTransporteComponent = /** @class */ (function () {
    function ModalTransporteComponent(dataTra) {
        this.dataTra = dataTra;
    }
    ModalTransporteComponent.prototype.ngOnInit = function () {
    };
    ModalTransporteComponent.prototype.onSaveTransporte = function (formTransporte) {
        if (formTransporte.value.id === null) {
            // formTransporte.value.useUid = this.userUid;
            this.dataTra.addTransporte(formTransporte.value);
        }
        else {
            this.dataTra.updateTransporte(formTransporte.value);
        }
        formTransporte.resetForm();
        this.btnClose.nativeElement.click();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalTransporteComponent.prototype, "btnClose", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ModalTransporteComponent.prototype, "userUid", void 0);
    ModalTransporteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-transporte',
            template: __webpack_require__(/*! ./modal-transporte.component.html */ "./src/app/components/modal-transporte/modal-transporte.component.html"),
            styles: [__webpack_require__(/*! ./modal-transporte.component.css */ "./src/app/components/modal-transporte/modal-transporte.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_transporte_transporte_service__WEBPACK_IMPORTED_MODULE_2__["TransporteService"]])
    ], ModalTransporteComponent);
    return ModalTransporteComponent;
}());



/***/ }),

/***/ "./src/app/components/modal/modal.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/modal/modal.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWwvbW9kYWwuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/modal/modal.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/modal/modal.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modalSucursal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">\n                    {{!this.dataApi.selectedSucursal.id ? 'Nueva Sucursal' : 'Actualizar Sucursal'}}\n                </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n            </div>\n            <div class=\"modal-body\">\n                <form name=\"formSucursal\" #formSucursal=\"ngForm\" (ngSubmit)=\"onSaveSucursal(formSucursal)\">\n                    <input type=\"hidden\" name=\"id\" [(ngModel)]=\"this.dataApi.selectedSucursal.id\">\n\n                    <div class=\"form-group\">\n                        <label for=\"link_amazon\">Estado</label>\n                        <input type=\"text\" name=\"estado\" class=\"form-control\" placeholder=\"Estado\" [(ngModel)]=\"this.dataApi.selectedSucursal.estado\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"descripcion\" class=\"col-form-label\">Descripcion:</label>\n                        <textarea class=\"form-control\" name=\"descripcion\" [(ngModel)]=\"this.dataApi.selectedSucursal.descripcion\"></textarea>\n                    </div>\n                    <div class=\"modal-footer\">\n                        <button type=\"button\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button>\n                        <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\n                    </div>\n                </form>\n            </div>\n\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/modal/modal.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/modal/modal.component.ts ***!
  \*****************************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_data_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/data-api.service */ "./src/app/service/data-api.service.ts");



var ModalComponent = /** @class */ (function () {
    function ModalComponent(dataApi) {
        this.dataApi = dataApi;
    }
    ModalComponent.prototype.ngOnInit = function () {
        console.log('UserId', this.userUid);
    };
    ModalComponent.prototype.onSaveSucursal = function (formSucursal) {
        if (formSucursal.value.id === null) {
            // New
            formSucursal.value.userUid = this.userUid;
            this.dataApi.addSucursal(formSucursal.value);
        }
        else {
            // Update
            this.dataApi.updateSucursal(formSucursal.value);
        }
        formSucursal.resetForm();
        this.btnClose.nativeElement.click();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalComponent.prototype, "btnClose", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ModalComponent.prototype, "userUid", void 0);
    ModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__(/*! ./modal.component.html */ "./src/app/components/modal/modal.component.html"),
            styles: [__webpack_require__(/*! ./modal.component.css */ "./src/app/components/modal/modal.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_data_api_service__WEBPACK_IMPORTED_MODULE_2__["DataApiService"]])
    ], ModalComponent);
    return ModalComponent;
}());



/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " ul li {\n     float: left;\n     display: block;\n     line-height: 30px;\n }\n \n ul li a {\n     position: relative;\n     margin: 0px 1.5px;\n }\n \n ul li a:after {\n     position: absolute;\n     bottom: 7px;\n     left: 0px;\n     width: 100%;\n     height: 4px;\n     background-color: #ffffff;\n     content: \"\";\n     opacity: 0;\n     transition: opacity 0.3s ease 0s, -webkit-transform 0.3s ease 0s;\n     transition: opacity 0.3s ease 0s, transform 0.3s ease 0s;\n     transition: opacity 0.3s ease 0s, transform 0.3s ease 0s, -webkit-transform 0.3s ease 0s;\n     -webkit-transform: translateY(20px);\n             transform: translateY(20px);\n }\n \n ul li a:hover:after {\n     opacity: 1;\n     -webkit-transform: translateY(15px);\n             transform: translateY(15px)\n }\n \n .fa {\n     display: 2;\n }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkNBQUM7S0FDSSxZQUFZO0tBQ1osZUFBZTtLQUNmLGtCQUFrQjtFQUNyQjs7Q0FFRDtLQUNJLG1CQUFtQjtLQUNuQixrQkFBa0I7RUFDckI7O0NBRUQ7S0FDSSxtQkFBbUI7S0FDbkIsWUFBWTtLQUNaLFVBQVU7S0FDVixZQUFZO0tBQ1osWUFBWTtLQUNaLDBCQUEwQjtLQUMxQixZQUFZO0tBQ1osV0FBVztLQUNYLGlFQUF5RDtLQUF6RCx5REFBeUQ7S0FBekQseUZBQXlEO0tBQ3pELG9DQUE0QjthQUE1Qiw0QkFBNEI7RUFDL0I7O0NBRUQ7S0FDSSxXQUFXO0tBQ1gsb0NBQTJCO2FBQTNCLDJCQUEyQjtFQUM5Qjs7Q0FFRDtLQUNJLFdBQVc7RUFDZCIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIHVsIGxpIHtcbiAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICBsaW5lLWhlaWdodDogMzBweDtcbiB9XG4gXG4gdWwgbGkgYSB7XG4gICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgbWFyZ2luOiAwcHggMS41cHg7XG4gfVxuIFxuIHVsIGxpIGE6YWZ0ZXIge1xuICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgIGJvdHRvbTogN3B4O1xuICAgICBsZWZ0OiAwcHg7XG4gICAgIHdpZHRoOiAxMDAlO1xuICAgICBoZWlnaHQ6IDRweDtcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAgY29udGVudDogXCJcIjtcbiAgICAgb3BhY2l0eTogMDtcbiAgICAgdHJhbnNpdGlvbjogb3BhY2l0eSAwLjNzIGVhc2UgMHMsIHRyYW5zZm9ybSAwLjNzIGVhc2UgMHM7XG4gICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgyMHB4KTtcbiB9XG4gXG4gdWwgbGkgYTpob3ZlcjphZnRlciB7XG4gICAgIG9wYWNpdHk6IDE7XG4gICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgxNXB4KVxuIH1cbiBcbiAuZmEge1xuICAgICBkaXNwbGF5OiAyO1xuIH0iXX0= */"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">\n    <a class=\"navbar-brand\" href=\"#\" routerLink=\"admin/home-admin\"><img src=\"../assets/imgs/lolo-logo.svg\" class=\"img-fluid img-responsive\"></a>\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor01\" aria-controls=\"navbarColor01\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <span class=\"navbar-toggler-icon\"></span>\n    </button>\n\n\n    <div class=\"collapse navbar-collapse\" id=\"navbarColor01\">\n        <ul class=\"navbar-nav ml-auto\">\n            <li class=\"nav-item\" *ngIf=\"!isLogged\">\n                <a class=\"nav-link\" routerLink=\"/\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i> Home</a>\n            </li>\n            <li class=\"nav-item\" *ngIf=\"isLogged\">\n                <a class=\"nav-link\" routerLink=\"admin/home-admin\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i> Home</a>\n            </li>\n            <li class=\"nav-item\" *ngIf=\"!isLogged\">\n                <a class=\"nav-link\" routerLink=\"/user/login\">Login </a>\n            </li>\n            <ng-template #NoAdmin>\n                <li class=\"nav-item\" *ngIf=\"isLogged\">\n                    <a class=\"nav-link\" routerLink=\"restaurante/restaurante-list-menu\"><i class=\"fa fa-book\"\n                            aria-hidden=\"true\"></i> Menú</a>\n                </li>\n                <li class=\"nav-item dropdown\" *ngIf=\"isLogged\">\n                    <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Servicios <i class=\" dw fa fa-chevron-down\"\n                            aria-hidden=\"true\"></i></a>\n                    <div class=\"dropdown-menu\">\n                        <a class=\"dropdown-item\" routerLink=\"admin/list-pedidos-res\" href=\"#\"><i\n                                class=\"fa fa-motorcycle\" aria-hidden=\"true\"></i> Pedidos</a>\n                        <a class=\"dropdown-item\" href=\"#\" routerLink=\"convenio/list-historial-servicios\"><i\n                                class=\"fa fa-history\" aria-hidden=\"true\"></i> Historial de pedidos</a>\n                    </div>\n                </li>\n                <!-- <li class=\"nav-item\" *ngIf=\"isLogged\">\n                    <a class=\"nav-link\" routerLink=\"/user/profile\"><i class=\"fa fa-bar-chart\" aria-hidden=\"true\"></i> Reportes</a>\n                </li> -->\n            </ng-template>\n\n\n            <li *ngIf=\"isLogged && isAdmin == true; else NoAdmin\" class=\"nav-item dropdown\">\n                <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-address-card-o\" aria-hidden=\"true\"></i> Administración de\n                    Usuarios <i class=\" dw fa fa-chevron-down\" aria-hidden=\"true\"></i></a>\n                <div class=\"dropdown-menu\" x-placement=\"bottom-start\" style=\"position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);\">\n                    <a class=\"dropdown-item\" routerLink=\"admin/list-repartidores\" href=\"#\"><i class=\"fa fa-motorcycle\"\n                            aria-hidden=\"true\"></i> Repartidores</a>\n                    <a class=\"dropdown-item\" routerLink=\"admin/list-restaurantes\"><i class=\"fa fa-cutlery\"\n                            aria-hidden=\"true\"></i> Convenios</a>\n                    <a class=\"dropdown-item\" routerLink=\"admin/list-administradores\" href=\"#\"><i\n                            class=\"fa fa-user-circle-o\" aria-hidden=\"true\"></i> Administradores</a>\n                    <a class=\"dropdown-item\" routerLink=\"admin/list-clientes\" href=\"#\"><i class=\"fa fa-male\"\n                            aria-hidden=\"true\"></i> Clientes</a>\n                    <div class=\"dropdown-divider\"></div>\n                    <!-- <a class=\"dropdown-item\" routerLink=\"admin/list-transportes\" href=\"#\"><i class=\"fa fa-car\" aria-hidden=\"true\"></i>Transportes</a> -->\n                </div>\n            </li>\n            <!-- <li class=\"nav-item\" *ngIf=\"isLogged && isAdmin == true; \">\n                <a class=\"nav-link\" routerLink=\"admin/list-sucursal\"><i class=\"fa fa-building\" aria-hidden=\"true\"></i> Sucursales</a>\n            </li> -->\n            <li *ngIf=\"isLogged && isAdmin == true\" class=\"nav-item dropdown\">\n                <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-paper-plane-o\" aria-hidden=\"true\"></i> Admin Servicios <i\n                        class=\" dw fa fa-chevron-down\" aria-hidden=\"true\"></i></a>\n                <div class=\"dropdown-menu\" x-placement=\"bottom-start\" style=\"position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);\">\n                    <a class=\"dropdown-item\" routerLink=\"admin/list-servicios\" href=\"#\"><i class=\"fa fa-globe\"\n                            aria-hidden=\"true\"></i> Servicios</a>\n                    <div class=\"dropdown-divider\"></div>\n                    <a class=\"dropdown-item\" routerLink=\"admin/list-historial-servicios\" href=\"#\"><i\n                            class=\"fa fa-history\" aria-hidden=\"true\"></i> Historial</a>\n                </div>\n            </li>\n\n            <li class=\"nav-item\" *ngIf=\"isLogged && isAdmin == true;\">\n                <a class=\"nav-link\" routerLink=\"admin/list-cortes\"><i class=\"fa fa-money\" aria-hidden=\"true\"></i>\n                    Cortes</a>\n            </li>\n\n            <li *ngIf=\"isLogged\" class=\"nav-item dropdown mr-3\">\n                <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"><span style=\"font-size: 1em;\"><i\n                            class=\"fa fa-user\"></i></span> + Usuario... <i class=\" dw fa fa-chevron-down\"\n                        aria-hidden=\"true\"></i> </a>\n\n                <div class=\"dropdown-menu\" x-placement=\"bottom-start\" style=\"position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);\">\n                    <div style=\"text-align: center;\" class=\"col-12\">\n                        <img style=\"height: 75px; width: 75px;\" src=\"{{usuario?.photourl}}\" class=\"rounded-circle img-thumbnail\" alt=\"profile\">\n                    </div>\n                    <a class=\"dropdown-item\" routerLink=\"/user/profile\" href=\"#\"><i class=\"fa fa-address-card \"\n                            aria-hidden=\"true\"></i> Perfil</a>\n\n                    <div class=\"dropdown-divider \"></div>\n                    <a class=\"dropdown-item \" href=\"# \" (click)=\"onLogout()\"><i class=\"fa fa-sign-out \"\n                            aria-hidden=\"true \"></i> Cerrar sesión</a>\n\n                </div>\n            </li>\n        </ul>\n    </div>\n</nav>"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.ts ***!
  \*******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/restaurante/restaurante-data.service */ "./src/app/service/restaurante/restaurante-data.service.ts");
/* harmony import */ var _service_administrador_administrador_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../service/administrador/administrador-data.service */ "./src/app/service/administrador/administrador-data.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");







var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(authService, afsAuth, dataRes, dataAdmin, db) {
        var _this = this;
        this.authService = authService;
        this.afsAuth = afsAuth;
        this.dataRes = dataRes;
        this.dataAdmin = dataAdmin;
        this.db = db;
        this.isLogged = false;
        this.isRestaurante = null;
        this.userUid = null;
        this.isAdmin = null;
        this.uidUser = null;
        this.user = {
            username: '',
            email: '',
            photoUrl: '',
            roles: {}
        };
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.userUid = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.user.photoUrl = user.photoURL;
                _this.db.doc("users/" + _this.userUid).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    console.log('ids', data);
                });
            }
        });
    }
    NavbarComponent.prototype.ngOnInit = function () {
        this.getCurrentUser();
        // this.getUserRestaurante();
    };
    NavbarComponent.prototype.getCurrentUser = function () {
        var _this = this;
        // tslint:disable-next-line:no-shadowed-variable
        this.authService.isAuth().subscribe(function (auth) {
            if (auth) {
                console.log('user logged');
                _this.isLogged = true;
                _this.userUid = auth.uid;
                _this.dataAdmin.isUserAdministrador(_this.userUid).subscribe(function (userRole) {
                    _this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('administrador');
                });
            }
            else {
                console.log('Not user logged');
                _this.isLogged = false;
            }
        });
    };
    // getUserRestaurante() {
    //   this.dataRes.isAuth().subscribe( auth => {
    //     if (auth) {
    //       this.userUid = auth.uid;
    //       this.dataRes.isUserRestaurante(this.userUid).subscribe(userRoles => {
    //         this.isRestaurante = Object.assign({}, userRoles.roles).hasOwnProperty('restaurante');
    //       })
    //     }
    //   })
    // }
    NavbarComponent.prototype.onLogout = function () {
        this.afsAuth.auth.signOut();
    };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__(/*! ./navbar.component.css */ "./src/app/components/navbar/navbar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
            _service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_4__["RestauranteDataService"],
            _service_administrador_administrador_data_service__WEBPACK_IMPORTED_MODULE_5__["AdministradorDataService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/components/page404/page404.component.css":
/*!**********************************************************!*\
  !*** ./src/app/components/page404/page404.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGFnZTQwNC9wYWdlNDA0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/page404/page404.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/page404/page404.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row mt-4\">\n    <div class=\"col-md-6 mx-auto text-center\">\n        <div class=\"card\">\n            <h1>Oops!</h1>\n            <h2>404 Página no encontrada.</h2>\n            <div class=\"card-body\">\n                <a href=\"/\" class=\"btn btn-primary\"><span class=\"fa fa-home\"></span> Home</a>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/page404/page404.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/page404/page404.component.ts ***!
  \*********************************************************/
/*! exports provided: Page404Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Page404Component", function() { return Page404Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var Page404Component = /** @class */ (function () {
    function Page404Component() {
    }
    Page404Component.prototype.ngOnInit = function () {
    };
    Page404Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page404',
            template: __webpack_require__(/*! ./page404.component.html */ "./src/app/components/page404/page404.component.html"),
            styles: [__webpack_require__(/*! ./page404.component.css */ "./src/app/components/page404/page404.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Page404Component);
    return Page404Component;
}());



/***/ }),

/***/ "./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.css":
/*!******************************************************************************************************!*\
  !*** ./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.css ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-grid-tile {\n    background: lightblue;\n}\n\n.example-card {\n    border: solid 4px black;\n    display: inline-block;\n    /* max-width: 680px; */\n    margin-left: -1em;\n    margin-right: -1em;\n    text-align: center;\n}\n\n.esq1 {\n    text-align: right;\n    margin-right: 1.5em;\n    margin-top: -2em;\n}\n\n.log {\n    height: 90px;\n    width: 90px;\n}\n\n.log1 {\n    height: 250px;\n    width: 250px;\n}\n\n.esq {\n    text-align: right;\n    margin-top: -2em;\n}\n\n/* span {\n    height: auto;\n    width: auto;\n    text-align: center;\n} */\n\n.prd {\n    margin-left: -1.9em;\n}\n\n.prd2 {\n    margin-left: -1.9em;\n}\n\n.esq3 {\n    text-align: right;\n    width: 700px;\n    margin-left: 52em;\n    /* margin-top: -3em;  */\n}\n\n.esq4 {\n    text-align: right;\n    /* width: 700px;\n    margin-left: 52em; */\n    /* margin-top: -3em;  */\n}\n\nagm-map {\n    height: 400px;\n}\n\np {\n    font-family: Verdana, Geneva, Tahoma, sans-serif;\n    font-style: unset;\n}\n\nb {\n    font-size: 13px;\n}\n\nh6 {\n    font-family: monospace;\n}\n\n.ticket {\n    height: 250px;\n    width: 150px;\n    border-radius: 1px;\n    border: 5px solid rgb(255, 165, 2);\n}\n\n#map {\n    width: 100%;\n    height: 400px;\n    background-color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZXN0YXVyYW50ZS9kZXRhaWwtcGVkaWRvcy1jb252ZW5pby9kZXRhaWwtcGVkaWRvcy1jb252ZW5pby5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksc0JBQXNCO0NBQ3pCOztBQUVEO0lBQ0ksd0JBQXdCO0lBQ3hCLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsb0JBQW9CO0lBQ3BCLGlCQUFpQjtDQUNwQjs7QUFFRDtJQUNJLGFBQWE7SUFDYixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxjQUFjO0lBQ2QsYUFBYTtDQUNoQjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQixpQkFBaUI7Q0FDcEI7O0FBR0Q7Ozs7SUFJSTs7QUFFSjtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLG9CQUFvQjtDQUN2Qjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLHdCQUF3QjtDQUMzQjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQjt5QkFDcUI7SUFDckIsd0JBQXdCO0NBQzNCOztBQUVEO0lBQ0ksY0FBYztDQUNqQjs7QUFFRDtJQUNJLGlEQUFpRDtJQUNqRCxrQkFBa0I7Q0FDckI7O0FBRUQ7SUFDSSxnQkFBZ0I7Q0FDbkI7O0FBRUQ7SUFDSSx1QkFBdUI7Q0FDMUI7O0FBRUQ7SUFDSSxjQUFjO0lBQ2QsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixtQ0FBbUM7Q0FDdEM7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osY0FBYztJQUNkLHdCQUF3QjtDQUMzQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcmVzdGF1cmFudGUvZGV0YWlsLXBlZGlkb3MtY29udmVuaW8vZGV0YWlsLXBlZGlkb3MtY29udmVuaW8uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1ncmlkLXRpbGUge1xuICAgIGJhY2tncm91bmQ6IGxpZ2h0Ymx1ZTtcbn1cblxuLmV4YW1wbGUtY2FyZCB7XG4gICAgYm9yZGVyOiBzb2xpZCA0cHggYmxhY2s7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIC8qIG1heC13aWR0aDogNjgwcHg7ICovXG4gICAgbWFyZ2luLWxlZnQ6IC0xZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAtMWVtO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmVzcTEge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIG1hcmdpbi1yaWdodDogMS41ZW07XG4gICAgbWFyZ2luLXRvcDogLTJlbTtcbn1cblxuLmxvZyB7XG4gICAgaGVpZ2h0OiA5MHB4O1xuICAgIHdpZHRoOiA5MHB4O1xufVxuXG4ubG9nMSB7XG4gICAgaGVpZ2h0OiAyNTBweDtcbiAgICB3aWR0aDogMjUwcHg7XG59XG5cbi5lc3Ege1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIG1hcmdpbi10b3A6IC0yZW07XG59XG5cblxuLyogc3BhbiB7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn0gKi9cblxuLnByZCB7XG4gICAgbWFyZ2luLWxlZnQ6IC0xLjllbTtcbn1cblxuLnByZDIge1xuICAgIG1hcmdpbi1sZWZ0OiAtMS45ZW07XG59XG5cbi5lc3EzIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICB3aWR0aDogNzAwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDUyZW07XG4gICAgLyogbWFyZ2luLXRvcDogLTNlbTsgICovXG59XG5cbi5lc3E0IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAvKiB3aWR0aDogNzAwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDUyZW07ICovXG4gICAgLyogbWFyZ2luLXRvcDogLTNlbTsgICovXG59XG5cbmFnbS1tYXAge1xuICAgIGhlaWdodDogNDAwcHg7XG59XG5cbnAge1xuICAgIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBHZW5ldmEsIFRhaG9tYSwgc2Fucy1zZXJpZjtcbiAgICBmb250LXN0eWxlOiB1bnNldDtcbn1cblxuYiB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufVxuXG5oNiB7XG4gICAgZm9udC1mYW1pbHk6IG1vbm9zcGFjZTtcbn1cblxuLnRpY2tldCB7XG4gICAgaGVpZ2h0OiAyNTBweDtcbiAgICB3aWR0aDogMTUwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMXB4O1xuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigyNTUsIDE2NSwgMik7XG59XG5cbiNtYXAge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNDAwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"m-3\">\n    <mat-card>\n        <!-- TITULO -->\n        <mat-card-title class=\"text-muted\">Pedido {{ servicio.clave }}</mat-card-title>\n        <mat-divider></mat-divider>\n        <mat-card-content>\n            <table class=\"table table-bordered\">\n                <thead>\n                    <tr>\n\n                        <!-- Estado del servicio -->\n                        <th scope=\"col\">\n                            <div style=\"position:relative;margin: -393px 0 -250px 10px;\">\n                                <mat-card>\n                                    <mat-card-subtitle style=\"text-align: left\">ESTADO</mat-card-subtitle>\n\n                                    <!-- ACEPTADO -->\n                                    <div *ngIf=\"servicio.estatus == 'Notificando'\" class=\"progress-bar badge-pill badge-success\" role=\"progressbar\" style=\"width: 12.5%\" aria-valuenow=\"15\" aria-valuemin=\"0\" aria-valuemax=\"100\">Notificando(12%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Aceptado'\" class=\"progress-bar badge-pill bg-success\" role=\"progressbar\" style=\"width: 25%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Aceptado(25%)</div>\n\n                                    <div *ngIf=\"servicio.estatus == 'BuscandoBme'\" class=\"progress-bar badge-pill bg-success\" role=\"progressbar\" style=\"width: 25%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Buscando Repartidor(25%)</div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Yendo'\" class=\"progress-bar badge-pill bg-info\" role=\"progressbar\" style=\"width: 37.5%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Dirigiendose al establecimiento(37%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Comprando'\" class=\"progress-bar badge-pill bg-primary\" role=\"progressbar\" style=\"width: 50%\" aria-valuenow=\"15\" aria-valuemin=\"0\" aria-valuemax=\"100\">Comprando(50%) </div>\n\n                                    <!-- <div *ngIf=\"servicio.estatus == 'LLevandolo'\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 62.5%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Comprado(62%) </div> -->\n\n                                    <div *ngIf=\"servicio.estatus == 'Gestando'\" class=\"progress-bar badge-pill bg-info\" role=\"progressbar\" style=\"width: 62.5%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Preparando(62%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Comprado'\" class=\"progress-bar badge-pill bg-primary\" role=\"progressbar\" style=\"width: 75%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Llevando el pedido al cliente(75%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Enviando'\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 87.5%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Dirigiendose al domicilio(87%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'EnPuerta'\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 87.5%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Esta en puerta(87%) </div>\n                                    <div *ngIf=\"servicio.estatus == 'Pago'\" class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 90.5%\" aria-valuenow=\"30\" aria-valuemin=\"0\" aria-valuemax=\"100\">Pagando servicio(90%) </div>\n\n\n                                    <div *ngIf=\"servicio.estatus == 'Pagado'\" class=\"progress-bar badge-pill bg-success\" role=\"progressbar\" style=\"width: 95%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\"> Casi Finalizando servicio(95%)\n                                    </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Terminado'\" class=\"progress-bar badge-pill bg-success\" role=\"progressbar\" style=\"width: 100%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Servicio Terminado(100%) </div>\n\n                                    <div *ngIf=\"servicio.estatus == 'Cancelado'\" class=\"progress-bar badge-pill bg-danger\" role=\"progressbar\" style=\"width: 100%\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\">Cancelado</div>\n                                    <!-- Fin de barra de estado -->\n                                </mat-card>\n                            </div>\n                        </th>\n                        <!-- RESUMEN -->\n                        <th scope=\"col\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">RESUMEN</mat-card-subtitle>\n\n                                <table class=\"table table-bordered table-hover \">\n                                    <thead>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Total</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">{{ servicio.totalProductos | currency }} </th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Pago</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">{{servicio.metodo_pago}} </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Nota de pago</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container *ngIf=\"servicio.abierto == null\">\n                                                    Servicio abierto desde aplicación.\n                                                </ng-container>\n                                                <ng-container *ngIf=\"servicio.abierto != null\">\n                                                    Servicio abierto por convenio desde plataforma.\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Fecha</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">{{ servicio.fecha | date: 'dd-MM-yyyy HH:mm:ss' }} </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Tiempo</td>\n                                            <th style=\"text-align:left\">{{ servicio.tiempoServicio }} mins.</th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Distancia</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">{{ servicio.km }} Kms. </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Tipo de pedido</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container>\n                                                    {{ servicio.tipo == 1 ? 'Pide lo que quieras': 'Convenio' }}\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Cliente</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container *ngFor=\"let cliente of clientes\">\n                                                    {{ cliente.username }} {{ cliente.lastname }}\n                                                </ng-container>\n                                                <ng-container *ngIf=\"servicio.cliente\">\n                                                    {{ servicio.cliente }}\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                        <tr>\n                                            <td style=\"text-align:left\" colspan=\"2\">Repartidor</td>\n                                            <th style=\"text-align:left\" colspan=\"2\">\n                                                <ng-container *ngFor=\"let repartidor of repartidores\">\n                                                    {{ repartidor.username }} {{ repartidor.lastname }}\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr>\n                        <!-- TICKETS -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">TICKETS</mat-card-subtitle>\n                                <mat-card>\n                                    <ng-container *ngFor=\"let ped of pedido\">\n                                        <a data-toggle=\"modal\" data-target=\"#modalTicket2\" href=\"\"> <img class=\"ticket\" [src]=\"ped.ticket\" alt=\"\"> </a>\n\n                                        <mat-card-title>{{ ped.total | currency }} </mat-card-title>\n                                        <mat-card-subtitle>\n                                            <a style=\"font-size: 10px;\" href=\"{{'https://maps.google.com/?q='+ ped.pedidoGeo._lat +','+ ped.pedidoGeo._long }}\" target=\"_blank\"> <span class=\"\"><b>{{ ped.direccionLocal }} </b></span></a>\n                                        </mat-card-subtitle>\n                                        <mat-card-footer>{{ ped.nombreLocal }} </mat-card-footer>\n                                    </ng-container>\n                                </mat-card>\n                            </mat-card>\n                        </th>\n                        <!-- MAPA -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <ng-container *ngIf=\"servicio.estatus != 'Terminado' && servicio.estatus != 'Pagado'\">\n                                    <!-- <ng-container *ngIf=\"servicio.estatus != 'Terminado'\"> -->\n                                    <div style=\"text-align: center;\">\n                                        <h4>Aún no se ha generado una ruta.</h4>\n                                        <img class=\"log1\" src=\"../../../../assets/imgs/Espera.gif\" alt=\"BringMe Docs.\">\n                                    </div>\n                                </ng-container>\n                                <ng-container *ngIf=\"servicio.estatus == 'Terminado' || servicio.estatus == 'Pagado'\">\n                                    <div #map id=\"map\"></div>\n                                </ng-container>\n                            </mat-card>\n                        </th>\n                    </tr>\n                    <!-- DETALLE DEL PEDIDO -->\n                    <tr>\n                        <th colspan=\"2\" scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">DETALLE DEL PEDIDO</mat-card-subtitle>\n                                <table class=\"table table-hover \">\n                                    <thead class=\"thead-light\">\n                                        <tr>\n                                            <th scope=\"col\">Cantidad</th>\n                                            <th scope=\"col\">Producto</th>\n                                            <th scope=\"col\">Nota</th>\n                                            <!-- <th scope=\"col\">Comprar en</th>\n                                            <th scope=\"col\">Local</th> -->\n\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr *ngFor=\"let producto of productos\">\n                                            <!-- <ng-container *ngFor=\"let ped of pedido\"> -->\n                                            <th scope=\"row\">\n                                                {{ producto.cantidad }}\n                                            </th>\n                                            <th>\n                                                {{producto.producto }}\n                                                <ng-container *ngIf=\"servicio.uidRestaurante\">\n                                                    {{ producto.descripcion }}\n                                                </ng-container>\n\n                                            </th>\n                                            <th>\n                                                {{ producto.nota }}\n                                                <ng-container *ngIf=\"!servicio.uidRestaurante\">\n                                                    {{ producto.descripcion }}\n                                                </ng-container>\n                                            </th>\n                                            <!-- <td>{{ ped.direccionLocal }} </td>\n                                                <td>{{ ped.nombreLocal }} </td> -->\n                                            <!-- </ng-container> -->\n                                        </tr>\n\n                                    </tbody>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                    <tr>\n                        <!-- COSTO DE SERVICIO -->\n                        <!-- <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">COSTO DE SERVICIO</mat-card-subtitle>\n                                <table class=\"table table-hover \">\n\n                                    <tr>\n                                        <th class=\"table-info\" scope=\"row\">Total servicio</th>\n                                        <td class=\"table-info\">{{ servicio.totalProductos | currency }} </td>\n                                    </tr>\n                                    <tr>\n                                        <th class=\"table-success\" colspan=\"2\">\n                                            <mat-card-subtitle>\n                                                Total pagado\n                                            </mat-card-subtitle>\n                                            <mat-card-title>\n                                                {{totalSer_totalEst | currency }}\n                                            </mat-card-title>\n                                        </th>\n                                    </tr>\n                                </table>\n                            </mat-card>\n\n                        </th> -->\n                        <!-- TOTALES -->\n                        <!-- <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">TOTALES</mat-card-subtitle>\n                                <table class=\"table table-hover \">\n                                    <tr>\n                                        <td scope=\"col\">Productos</td>\n                                        <td scope=\"col\">{{servicio.totalProductos | currency }} </td>\n                                    </tr>\n\n                                    <tr>\n                                        <th class=\"table-info\" scope=\"row\">Total</th>\n                                        <td class=\"table-info\">{{ servicio.totalProductos | currency }} </td>\n                                    </tr>\n                                    <tr>\n                                        <th class=\"table-success\" colspan=\"2\">\n                                            <mat-card-subtitle>\n                                                Total pagado\n                                            </mat-card-subtitle>\n                                            <mat-card-title>\n                                                {{ servicio.totalProductos | currency }}\n                                            </mat-card-title>\n                                            {{ servicio.metodo_pago }}\n                                        </th>\n                                    </tr>\n                                </table>\n                            </mat-card>\n\n                        </th> -->\n\n                    </tr>\n                    <tr>\n                        <!-- CLIENTES -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">CLIENTE</mat-card-subtitle>\n                                <table class=\"table table-hover \">\n                                    <ng-container *ngFor=\"let cliente of clientes\">\n                                        <tr>\n                                            <th scope=\"col\">Nombre</th>\n                                            <th scope=\"col\">{{ cliente.username }} {{cliente.lastname}}\n                                                <ng-container *ngIf=\"servicio.cliente\">\n                                                    {{ servicio.cliente }}\n                                                </ng-container>\n                                            </th>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Correo</th>\n                                            <td>\n                                                {{cliente.email}}\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Celular</th>\n                                            <td>\n                                                {{cliente.phone}}\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Dirección de pedido</th>\n                                            <td>\n                                                {{ servicio.entregaDir }}\n                                            </td>\n                                        </tr>\n                                    </ng-container>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                        <!-- EVALUACION -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">EVALUACIÓN</mat-card-subtitle>\n\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                    <tr>\n                        <!-- REPARTIDOR -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">REPARTIDOR</mat-card-subtitle>\n                                <table class=\"table table-hover\">\n                                    <ng-container *ngFor=\"let repartidor of repartidores\">\n                                        <tr>\n                                            <th scope=\"col\">Nombre</th>\n                                            <th scope=\"col\">{{ repartidor.username }} {{repartidor.lastname}} </th>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Correo</th>\n                                            <td>\n                                                {{repartidor.email}}\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Celular</th>\n                                            <td>\n                                                {{repartidor.phone}}\n                                            </td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Unidad</th>\n                                            <td>\n                                                {{ repartidor.marca }} - {{repartidor.placa}}\n                                            </td>\n                                        </tr>\n                                    </ng-container>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                        <!-- EVALUACION -->\n                        <th scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">EVALUACIÓN</mat-card-subtitle>\n                                <table class=\"table table-hover\">\n                                    <tr>\n                                        <th scope=\"col\">Cal. Cliente</th>\n                                        <th scope=\"col\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 1\" src=\"../../../../assets/imgs/1.png\" alt=\"Calificación del servicio\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 2\" src=\"../../../../assets/imgs/2.png\" alt=\"Calificación del servicio\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 3\" src=\"../../../../assets/imgs/3.png\" alt=\"Calificación del servicio\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 4\" src=\"../../../../assets/imgs/4.png\" alt=\"Calificación del servicio\">\n                                            <img *ngIf=\"servicio.calificacionRepartidor == 5\" src=\"../../../../assets/imgs/5.png\" alt=\"Calificación del servicio\">\n                                        </th>\n                                    </tr>\n                                    <tr>\n                                        <th colspan=\"2\" scope=\"row\">Comentarios del cliente</th>\n                                    </tr>\n                                    <tr>\n                                        <th colspan=\"2\" scope=\"row\">\n                                            {{ servicio.comentarioRepartidor }}\n                                        </th>\n                                    </tr>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                    <!-- NOTAS Y ESTADO -->\n                    <tr>\n                        <th colspan=\"2\" scope=\"row\">\n                            <mat-card>\n                                <mat-card-subtitle style=\"text-align: left\">NOTAS Y ESTADO</mat-card-subtitle>\n                                <table class=\"table table-hover\">\n                                    <thead>\n                                        <tr>\n                                            <th scope=\"col\">Nota de plataforma</th>\n                                            <th scope=\"col\">\n                                                {{servicio.abierto == null ? 'Servicio abierto desde aplicación.': 'Servicio abierto por convenio desde plataforma.' }}\n                                            </th>\n                                        </tr>\n                                    </thead>\n                                    <tbody>\n                                        <tr>\n                                            <th scope=\"row\">Uid Servicio</th>\n                                            <td>{{ servicio.uid }}</td>\n                                        </tr>\n                                        <tr>\n                                            <th scope=\"row\">Estatus</th>\n                                            <td>{{ servicio.estatus }} </td>\n                                        </tr>\n                                    </tbody>\n                                </table>\n                            </mat-card>\n\n                        </th>\n                    </tr>\n                </tbody>\n            </table>\n        </mat-card-content>\n    </mat-card>\n</section>\n<!-- Modal de TICKET  -->\n<div class=\"modal\" id=\"modalTicket2\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\">Ticket del servicio con clave: <b>{{ servicio.clave }}</b> </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                        <span aria-hidden=\"true\">&times;</span>\n                      </button>\n            </div>\n            <div class=\"modal-body mt-5\">\n                <ng-container *ngFor=\"let ped of pedido\">\n                    <img style=\"transform: rotate(90deg); height: auto; width: 100%;\" class=\"\" [src]=\"ped.ticket\" alt=\"\">\n                </ng-container>\n            </div>\n            <div class=\"modal-footer mt-5\">\n                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: DetailPedidosConvenioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailPedidosConvenioComponent", function() { return DetailPedidosConvenioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/pedidos/pedidos.service */ "./src/app/service/pedidos/pedidos.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");






var DetailPedidosConvenioComponent = /** @class */ (function () {
    function DetailPedidosConvenioComponent(_pedidoSer, route, afs, http) {
        this._pedidoSer = _pedidoSer;
        this.route = route;
        this.afs = afs;
        this.http = http;
        this.servicio = {};
        this.title = 'Track del servicio';
        this.lat = 24.799448;
        this.lng = 120.979021;
        this.driving = 'DRIVING';
    }
    DetailPedidosConvenioComponent.prototype.ngOnInit = function () {
        // this.getDirection(null, null, null);
        var idServicio = this.route.snapshot.params['uid'];
        this.getDetails(idServicio);
        this.getPedido(idServicio);
        this.getProducto(idServicio);
    };
    // // tslint:disable-next-line:use-life-cycle-interface
    // _ngAfterViewInit() {
    //   console.log("afterinit");
    //   setTimeout(() => {
    //     const idServicio = this.route.snapshot.params["uid"];
    //     this._pedidoSer.getOneServicio(idServicio).subscribe(servicio => {
    //       this._pedidoSer.getAllServiciosP(idServicio).subscribe(serviciosP => {
    //         console.log("ServiciosP", serviciosP);
    //         this.servicio = servicio;
    //         console.log("servicio Cordenadas", this.servicio);
    // // Coordendas donde se va entregar el pedido.
    // const _lat = this.servicio.entregaGeo._lat;
    // const _lng = this.servicio.entregaGeo._long;
    // console.log(this.destination);
    // // Coordendas donde se inicia el servicio
    // const lat = this.servicio.partidaGeo._lat;
    // const lng = this.servicio.partidaGeo._long;
    //         console.log(this.origin);
    //         const mapProperties = {
    //           center: new google.maps.LatLng(35.2271, -80.8431),
    //           zoom: 15,
    //           mapTypeId: google.maps.MapTypeId.ROADMAP
    //         };
    //         this.map = new google.maps.Map(
    //           this.mapElement.nativeElement,
    //           mapProperties
    //         );
    //         const directionsService = new google.maps.DirectionsService();
    //         const directionsDisplay = new google.maps.DirectionsRenderer({
    //           draggable: true,
    //           map: this.map
    //         });
    //         directionsDisplay.addListener("directions_changed", function() {
    //           let total = 0;
    //           const myroute = directionsDisplay.getDirections().routes[0];
    //           for (let i = 0; i < myroute.legs.length; i++) {
    //             total += myroute.legs[i].distance.value;
    //           }
    //           total = total / 1000;
    //           this.total = total + " km";
    //           console.log("Total: ", this.total);
    //         });
    //         const waypts = [];
    //         const checkboxArray = serviciosP;
    //         for (let i = 0; i < checkboxArray.length; i++) {
    //           waypts.push({
    //             location: new google.maps.LatLng(
    //               checkboxArray[i].partidaGeo._lat,
    //               checkboxArray[i].partidaGeo._long
    //             )
    //           });
    //         }
    //         console.log("Pruva", waypts);
    //         directionsService.route(
    //           {
    //             origin: new google.maps.LatLng(lat, lng),
    //             destination: new google.maps.LatLng(_lat, _lng),
    //             waypoints: waypts,
    //             travelMode: this.driving,
    //             avoidTolls: true
    //           },
    //           function(response, status) {
    //             if (status === "OK") {
    //               directionsDisplay.setDirections(response);
    //             } else {
    //               alert("Could not display directions due to: " + status);
    //             }
    //           }
    //         );
    //       });
    //     });
    //   }, 1000);
    // }
    // tslint:disable-next-line:use-life-cycle-interface
    // ngAfterViewInit() {
    //   console.log("afterinit");
    //   setTimeout(() => {
    //     const idServicio = this.route.snapshot.params["uid"];
    //     this._pedidoSer.getOneServicio(idServicio).subscribe(servicio => {
    //       this._pedidoSer.getAllServiciosP(idServicio).subscribe(serviciosP => {
    //         console.log("ServiciosP", serviciosP);
    //         this.servicio = servicio;
    //         console.log("servicio Cordenadas", this.servicio);
    //         // Coordendas donde se va entregar el pedido.
    //         const _lat = this.servicio.entregaGeo._lat;
    //         const _lng = this.servicio.entregaGeo._long;
    //         console.log(this.destination);
    //         // Coordendas donde se inicia el servicio
    //         const lat = this.servicio.partidaGeo._lat;
    //         const lng = this.servicio.partidaGeo._long;
    //         console.log(this.origin);
    //         const mapProperties = {
    //           center: new google.maps.LatLng(35.2271, -80.8431),
    //           zoom: 15,
    //           mapTypeId: google.maps.MapTypeId.ROADMAP
    //         };
    //         this.map = new google.maps.Map(
    //           this.mapElement.nativeElement,
    //           mapProperties
    //         );
    //         const directionsService = new google.maps.DirectionsService();
    //         const directionsDisplay = new google.maps.DirectionsRenderer({
    //           draggable: true,
    //           map: this.map
    //         });
    //         directionsDisplay.addListener("directions_changed", function() {
    //           let total = 0;
    //           const myroute = directionsDisplay.getDirections().routes[0];
    //           for (let i = 0; i < myroute.legs.length; i++) {
    //             total += myroute.legs[i].distance.value;
    //           }
    //           total = total / 1000;
    //           this.total = total + " km";
    //           console.log("Total: ", this.total);
    //         });
    //         const waypts = [];
    //         const checkboxArray = serviciosP;
    //         for (let i = 0; i < checkboxArray.length; i++) {
    //           waypts.push({
    //             lat: checkboxArray[i].partidaGeo._lat,
    //             lng: checkboxArray[i].partidaGeo._long
    //           });
    //         }
    //         console.log("Pruva", waypts);
    //         const lineSymbol = {
    //           path: "M 0,-1 0,1",
    //           strokeOpacity: 1,
    //           scale: 4
    //         };
    //         const flightPath = new google.maps.Polyline({
    //           path: waypts,
    //           icons: [
    //             {
    //               icon: lineSymbol,
    //               offset: "0",
    //               repeat: "20px"
    //             }
    //           ],
    //           strokeOpacity: 1.0
    //         });
    //         flightPath.setMap(this.map);
    //       });
    //     });
    //   }, 1000);
    // }
    // tslint:disable-next-line:use-life-cycle-interface ->codigo funcionando
    DetailPedidosConvenioComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log('afterinit');
        setTimeout(function () {
            var idServicio = _this.route.snapshot.params['uid'];
            _this._pedidoSer.getOneServicio(idServicio).subscribe(function (servicio) {
                _this._pedidoSer.getAllServiciosP(idServicio).subscribe(function (serviciosP) {
                    console.log('ServiciosP', serviciosP);
                    _this.servicio = servicio;
                    console.log(_this.origin);
                    // Coordendas donde se va entregar el pedido.
                    var _lat = _this.servicio.entregaGeo._lat;
                    var _lng = _this.servicio.entregaGeo._long;
                    console.log(_this.destination);
                    // Coordendas donde se inicia el servicio
                    var lat = _this.servicio.partidaGeo._lat;
                    var lng = _this.servicio.partidaGeo._long;
                    console.log('servicio Cordenadas', _this.servicio);
                    var waypts = [];
                    var checkboxArray = serviciosP;
                    _this.serviciosP = checkboxArray;
                    for (var i = 0; i < checkboxArray.length; i++) {
                        var latlng = checkboxArray[i].partidaGeo._lat +
                            ',' +
                            checkboxArray[i].partidaGeo._long;
                        waypts.push(latlng);
                        // Se asigan estas variables para poder centar el mapa
                        _this.originJSlt = checkboxArray[i].partidaGeo._lat;
                        _this.originJSln = checkboxArray[i].partidaGeo._long;
                    }
                    var ruts = waypts.join('|');
                    var _ruts = serviciosP;
                    console.log('Pruva', ruts);
                    var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]({
                        'Content-Type': 'application/json'
                    });
                    var options = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["RequestOptions"]({
                        headers: headers
                    });
                    var url = 'https://roads.googleapis.com/v1/snapToRoads?path=' +
                        ruts +
                        '&interpolate=true&key=AIzaSyDhkHiz_LkMhVCfTsGJajw5Ag4u9d6ah2I';
                    _this.http.get(url, options).subscribe(function (res) {
                        var mapProperties = {
                            center: new google.maps.LatLng(_this.originJSlt, _this.originJSln),
                            zoom: 17,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapProperties);
                        var directionsService = new google.maps.DirectionsService();
                        var directionsDisplay = new google.maps.DirectionsRenderer({
                            draggable: true,
                            map: _this.map
                        });
                        directionsDisplay.addListener('directions_changed', function () {
                            var total = 0;
                            var myroute = directionsDisplay.getDirections().routes[0];
                            for (var i = 0; i < myroute.legs.length; i++) {
                                total += myroute.legs[i].distance.value;
                            }
                            total = total / 1000;
                            this.total = total + ' km';
                            console.log('Total: ', this.total);
                        });
                        var _waypts = [];
                        var _checkboxArray = _ruts;
                        for (var i = 0; i < _checkboxArray.length; i++) {
                            _waypts.push({
                                location: new google.maps.LatLng(_checkboxArray[i].partidaGeo._lat, _checkboxArray[i].partidaGeo._long)
                            });
                        }
                        console.log('Pruva2', _waypts);
                        // directionsService.route(
                        //   {
                        //     origin: new google.maps.LatLng(
                        //       37.33570507,
                        //       -122.02342902
                        //     ),
                        //     destination: new google.maps.LatLng(
                        //       37.33165083,
                        //       -122.03029752
                        //     ),
                        //      waypoints: _waypts,
                        //     travelMode: this.driving,
                        //     avoidTolls: true
                        //   },
                        //   function(response, status) {
                        //     if (String(status) === 'OK') {
                        //       directionsDisplay.setDirections(response);
                        //     } else {
                        //       alert(
                        //         'Could not display directions due to: ' + status
                        //       );
                        //     }
                        //   }
                        // );
                        _this.respuesta = res;
                        var datos = JSON.parse(_this.respuesta._body);
                        console.log('datos1: ', JSON.parse(_this.respuesta._body));
                        console.log('datos2: ', datos.snappedPoints.length);
                        var snappedCoordinates = [];
                        var placeIdArray = [];
                        var polylines = [];
                        for (var i = 0; i < datos.snappedPoints.length; i++) {
                            var latlng = new google.maps.LatLng(datos.snappedPoints[i].location.latitude, datos.snappedPoints[i].location.longitude);
                            snappedCoordinates.push(latlng);
                            placeIdArray.push(datos.snappedPoints[i].placeId);
                        }
                        var snappedPolyline = new google.maps.Polyline({
                            path: snappedCoordinates,
                            strokeColor: 'red',
                            strokeWeight: 3
                        });
                        var marker = new google.maps.Marker({
                            icon: '../../../../assets/imgs/punterocasa.png',
                            position: new google.maps.LatLng(_this.originJSlt, _this.originJSln),
                            map: _this.map,
                            title: 'Lugar de entrega'
                        });
                        snappedPolyline.setMap(_this.map);
                        polylines.push(snappedPolyline);
                    });
                });
            });
        }, 1000);
    };
    DetailPedidosConvenioComponent.prototype.computeTotalDistance = function (result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        total = total / 1000;
        this.total = total + ' km';
        console.log('Total: ', this.total);
    };
    DetailPedidosConvenioComponent.prototype.getDetails = function (uid) {
        var _this = this;
        this._pedidoSer.getOneServicio(uid).subscribe(function (servicio) {
            _this.servicio = servicio;
            // tslint:disable-next-line: no-unused-expression
            _this.totalEstablecimientos = _this.servicio.numPedidos * 15;
            var toSer = Number.parseFloat(_this.servicio.totalServicio);
            _this.totalSer_totalEst = _this.totalEstablecimientos + toSer;
            var toNet = Number.parseFloat(_this.servicio.totalNeto);
            _this.totalNet_totalEst = _this.totalEstablecimientos + toNet;
            var idCliente = _this.servicio.uidCliente;
            var idRepartidor = _this.servicio.uidRepartidor;
            var convenio = _this.servicio.uidRestaurante;
            _this.getCliente(idCliente);
            _this.getRepartidor(idRepartidor);
            _this.getConvenio(convenio);
            // console.log('tabla servicio', this.servicio);
            // Coordendas donde se va entregar el pedido.
            _this.destination = {
                lat: _this.servicio.entregaGeo._lat,
                lng: _this.servicio.entregaGeo._long
            };
            console.log(_this.destination);
            // Coordendas donde se inicia el servicio
            _this.origin = {
                lat: _this.servicio.partidaGeo._lat,
                lng: _this.servicio.partidaGeo._long
            };
            console.log(_this.origin);
            // const uidServicio = this.servicio.uid;
            // this.getListPedido();
        });
    };
    DetailPedidosConvenioComponent.prototype.getPedido = function (idServicio) {
        var _this = this;
        this._pedidoSer.getPedidos(idServicio).subscribe(function (pedido) {
            _this.pedido = pedido;
            // console.log('pedido', this.pedido);
        });
    };
    DetailPedidosConvenioComponent.prototype.getProducto = function (idServicio) {
        var _this = this;
        this._pedidoSer.getProducto(idServicio).subscribe(function (producto) {
            _this.productos = producto;
            // console.log('producto', this.productos);
        });
    };
    DetailPedidosConvenioComponent.prototype.getCliente = function (id) {
        var _this = this;
        this._pedidoSer.getCliente(id).subscribe(function (cliente) {
            _this.clientes = cliente;
            // console.log('cliente', this.clientes);
        });
    };
    DetailPedidosConvenioComponent.prototype.getRepartidor = function (id) {
        var _this = this;
        this._pedidoSer.getRepartidor(id).subscribe(function (repa) {
            _this.repartidores = repa;
            // console.log('reapas', this.repartidores);
        });
    };
    DetailPedidosConvenioComponent.prototype.getConvenio = function (id) {
        var _this = this;
        this._pedidoSer.getConvenio(id).subscribe(function (conv) {
            _this.convenios = conv;
            // console.log('reapas', this.repartidores);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DetailPedidosConvenioComponent.prototype, "mapElement", void 0);
    DetailPedidosConvenioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detail-pedidos-convenio',
            template: __webpack_require__(/*! ./detail-pedidos-convenio.component.html */ "./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.html"),
            styles: [__webpack_require__(/*! ./detail-pedidos-convenio.component.css */ "./src/app/components/restaurante/detail-pedidos-convenio/detail-pedidos-convenio.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__["PedidosService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"]])
    ], DetailPedidosConvenioComponent);
    return DetailPedidosConvenioComponent;
}());



/***/ }),

/***/ "./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.css":
/*!************************************************************************************************************************!*\
  !*** ./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.iconTi {\n    height: 35px;\n    width: 35px;\n}\n\ntable {\n    margin-right: auto;\n    margin-left: auto;\n}\n\n.bz {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\n/* button {\n    border-radius: 25px;\n} */\n\np {\n    color: #562B89;\n}\n\n.modal-content {\n    width: 263%;\n    margin-left: -79%;\n}\n\n.bt {\n    margin-top: 2.5em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZXN0YXVyYW50ZS9saXN0LWhpc3RvcmlhbC1wZWRpZG9zLWNvbnZlbmlvcy9saXN0LWhpc3RvcmlhbC1wZWRpZG9zLWNvbnZlbmlvcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtDQUNmOztBQUVEO0lBQ0ksYUFBYTtJQUNiLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLG1CQUFtQjtJQUNuQixrQkFBa0I7Q0FDckI7O0FBRUQ7SUFDSSxvQkFBb0I7Q0FDdkI7O0FBRUQ7SUFDSSxvQkFBb0I7Q0FDdkI7O0FBR0Q7O0lBRUk7O0FBRUo7SUFDSSxlQUFlO0NBQ2xCOztBQUVEO0lBQ0ksWUFBWTtJQUNaLGtCQUFrQjtDQUNyQjs7QUFFRDtJQUNJLGtCQUFrQjtDQUNyQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcmVzdGF1cmFudGUvbGlzdC1oaXN0b3JpYWwtcGVkaWRvcy1jb252ZW5pb3MvbGlzdC1oaXN0b3JpYWwtcGVkaWRvcy1jb252ZW5pb3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmljb25UaSB7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIHdpZHRoOiAzNXB4O1xufVxuXG50YWJsZSB7XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xufVxuXG4uYnoge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbmlucHV0IHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5cbi8qIGJ1dHRvbiB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn0gKi9cblxucCB7XG4gICAgY29sb3I6ICM1NjJCODk7XG59XG5cbi5tb2RhbC1jb250ZW50IHtcbiAgICB3aWR0aDogMjYzJTtcbiAgICBtYXJnaW4tbGVmdDogLTc5JTtcbn1cblxuLmJ0IHtcbiAgICBtYXJnaW4tdG9wOiAyLjVlbTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.html ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <!-- <ngx-alerts></ngx-alerts> -->\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #562B89;\"><i class=\"fa fa-history\"></i></span> Pedidos</h1>\n        <strong class=\"mr-auto\">Sucursal</strong>\n        <div class=\"row\">\n            <div class=\"col\">\n                <div class=\"form-group\">\n                    <!--Buscador-->\n                    <!-- <input type=\"text\" class=\"form-control\" placeholder=\"&#xf002; Buscar nombre...\" style=\"font-family:Arial, FontAwesome\" name=\"filterPost\" [(ngModel)]=\"filterPost\"> -->\n                </div>\n            </div>\n            <!-- <button class=\"btn btn-primary float-right mb-3\" data-toggle=\"modnaval\" data-target=\"#modalAdmin\"> <i class=\"fa fa-plus\"></i> Nuevo administrador </button> -->\n        </div>\n    </div>\n\n\n    <div class=\"col m-5\">\n        <div style=\"\">\n            <mat-form-field>\n                <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n            </mat-form-field>\n\n            <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n                <!-- CLAVE Column -->\n                <ng-container matColumnDef=\"clave\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Clave </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.clave}} </td>\n                </ng-container>\n\n                <!-- TIPO Column -->\n                <ng-container matColumnDef=\"tipo\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Tipo </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <ng-container *ngIf=\"element.tipo && element.abierto == null\">\n                            <img class=\"iconTi\" src=\"../../../../assets/imgs/tipo1.png\" title=\"Servicio pide lo que quieras.\">\n                        </ng-container>\n                        <ng-container *ngIf=\"element.abierto == 'Abierto plataforma'\">\n                            <img class=\"iconTi\" src=\"../../../../assets/imgs/tipo2.png\" title=\"Servicio de convenio.\">\n                        </ng-container>\n                    </td>\n                </ng-container>\n\n                <!-- ESTATUS Column -->\n                <ng-container matColumnDef=\"estatus\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estatus </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <ng-container *ngIf=\"element.estatus == 'Terminado' \">\n                            <button type=\"button\" style=\"text-align: center;\" class=\"btn btn-success\">                     \n                                 <mat-icon>done</mat-icon>\n                            </button>\n                        </ng-container>\n                        <ng-container *ngIf=\"element.estatus == 'Cancelado' \">\n                            <button type=\"button\" style=\"text-align: center;\" class=\"btn btn-danger\">                     \n                                <mat-icon>clear</mat-icon>\n                           </button>\n                        </ng-container>\n                    </td>\n                </ng-container>\n\n                <!-- FECHA Column -->\n                <ng-container matColumnDef=\"fecha\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Fecha </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.fecha | date: 'dd/MM/yy h:mm:ss' }} </td>\n                </ng-container>\n\n                <!-- CLIENTE Column -->\n                <ng-container matColumnDef=\"nombreUsuario\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Cliente </th>\n                    <td mat-cell *matCellDef=\"let element\"> {{element.nombreUsuario}} </td>\n                </ng-container>\n\n                <!-- TOTALNETO Column -->\n                <ng-container matColumnDef=\"totalNeto\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Total </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <ng-container *ngIf=\"!element.totalNeto\">\n                            $0.00\n                        </ng-container>\n                        <ng-container *ngIf=\"element.totalNeto\">\n                            {{ element.totalNeto | currency }}\n                        </ng-container>\n                    </td>\n                </ng-container>\n\n                <!-- DETALLE Column -->\n                <ng-container matColumnDef=\"detalle\">\n                    <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Detalle </th>\n                    <td mat-cell *matCellDef=\"let element\">\n                        <button mat-mini-fab color=\"warn\" title=\"Detalle del pedido.\" title=\"Detalle del pedido.\" routerLink=\"../../servicio-convenio/{{element.uid}}\">\n                            <mat-icon>assignment</mat-icon>\n                        </button>\n                    </td>\n                </ng-container>\n\n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n            </table>\n            <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        </div>\n\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.ts":
/*!***********************************************************************************************************************!*\
  !*** ./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.ts ***!
  \***********************************************************************************************************************/
/*! exports provided: ListHistorialPedidosConveniosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListHistorialPedidosConveniosComponent", function() { return ListHistorialPedidosConveniosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/service/pedidos/pedidos.service */ "./src/app/service/pedidos/pedidos.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");






var ListHistorialPedidosConveniosComponent = /** @class */ (function () {
    function ListHistorialPedidosConveniosComponent(authService, _pedido, afs) {
        this.authService = authService;
        this._pedido = _pedido;
        this.afs = afs;
        this.displayedColumns = ['clave', 'tipo', 'estatus', 'fecha', 'nombreUsuario', 'totalNeto', 'detalle'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"]();
        this.filterPost = '';
        this.filterPost1 = '';
        this.filterPost2 = '';
        this.fechaInicio = '';
        this.fechaFin = '';
        this.uidConvenio = '';
        this.user = {
            uid: '',
            username: '',
            email: '',
            // photoUrl: '',
            direccion: '',
            geolocalizacion: {},
            roles: {}
        };
    }
    ListHistorialPedidosConveniosComponent.prototype.ngOnInit = function () {
        // this.getAllPedidos();
        this.getAllHistorial();
    };
    ListHistorialPedidosConveniosComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListHistorialPedidosConveniosComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    // Pedidos Terminados
    ListHistorialPedidosConveniosComponent.prototype.getAllHistorial = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidConvenio = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.afs.collection('servicios', function (ref) {
                    return ref
                        .where('uidRestaurante', '==', _this.uidConvenio).orderBy('fecha', 'desc');
                })
                    .valueChanges().subscribe(function (data) {
                    var serv = data;
                    var servicios = [];
                    serv.forEach(function (p) {
                        if (p.estatus === 'Cancelado' ||
                            p.estatus === 'Terminado') {
                            servicios.push(p);
                        }
                    });
                    console.log("servicios_", servicios);
                    _this.pedidos = servicios;
                    _this.dataSource.data = servicios;
                });
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatPaginator"])
    ], ListHistorialPedidosConveniosComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSort"])
    ], ListHistorialPedidosConveniosComponent.prototype, "sort", void 0);
    ListHistorialPedidosConveniosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-historial-pedidos-convenios',
            template: __webpack_require__(/*! ./list-historial-pedidos-convenios.component.html */ "./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.html"),
            styles: [__webpack_require__(/*! ./list-historial-pedidos-convenios.component.css */ "./src/app/components/restaurante/list-historial-pedidos-convenios/list-historial-pedidos-convenios.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], src_app_service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_2__["PedidosService"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]])
    ], ListHistorialPedidosConveniosComponent);
    return ListHistorialPedidosConveniosComponent;
}());



/***/ }),

/***/ "./src/app/components/restaurante/list-menus/list-menus.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/restaurante/list-menus/list-menus.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\nsection {\n    margin-right: 110px;\n    margin-left: 110px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\ninput {\n    border-radius: 25px;\n}\n\n/*\n   server-side-angular-way.component.css\n*/\n\n.no-data-available {\n    text-align: center;\n}\n\n/*\n     src/styles.css (i.e. your global style)\n  */\n\n.dataTables_empty {\n    display: none;\n}\n\n.csv {\n    text-align: right;\n    height: auto;\n    width: auto;\n    margin-bottom: 1em;\n}\n\np {\n    font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;\n    font-size: 10px;\n    margin-bottom: -1px;\n    color: #562B89;\n}\n\n.eta {\n    margin-right: 2px;\n    margin-bottom: 2px;\n}\n\n.img {\n    height: 75px;\n    width: 75px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZXN0YXVyYW50ZS9saXN0LW1lbnVzL2xpc3QtbWVudXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLGdCQUFnQjtJQUNoQixZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxvQkFBb0I7SUFDcEIsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUdEOztFQUVFOztBQUVGO0lBQ0ksbUJBQW1CO0NBQ3RCOztBQUdEOztJQUVJOztBQUVKO0lBQ0ksY0FBYztDQUNqQjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2IsWUFBWTtJQUNaLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLHVIQUF1SDtJQUN2SCxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLGVBQWU7Q0FDbEI7O0FBRUQ7SUFDSSxrQkFBa0I7SUFDbEIsbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksYUFBYTtJQUNiLFlBQVk7Q0FDZiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcmVzdGF1cmFudGUvbGlzdC1tZW51cy9saXN0LW1lbnVzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5zZWN0aW9uIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDExMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMTBweDtcbn1cblxuYnV0dG9uIHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5pbnB1dCB7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuXG4vKlxuICAgc2VydmVyLXNpZGUtYW5ndWxhci13YXkuY29tcG9uZW50LmNzc1xuKi9cblxuLm5vLWRhdGEtYXZhaWxhYmxlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cblxuLypcbiAgICAgc3JjL3N0eWxlcy5jc3MgKGkuZS4geW91ciBnbG9iYWwgc3R5bGUpXG4gICovXG5cbi5kYXRhVGFibGVzX2VtcHR5IHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4uY3N2IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogMWVtO1xufVxuXG5wIHtcbiAgICBmb250LWZhbWlseTogJ0x1Y2lkYSBTYW5zJywgJ0x1Y2lkYSBTYW5zIFJlZ3VsYXInLCAnTHVjaWRhIEdyYW5kZScsICdMdWNpZGEgU2FucyBVbmljb2RlJywgR2VuZXZhLCBWZXJkYW5hLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAtMXB4O1xuICAgIGNvbG9yOiAjNTYyQjg5O1xufVxuXG4uZXRhIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG5cbi5pbWcge1xuICAgIGhlaWdodDogNzVweDtcbiAgICB3aWR0aDogNzVweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/restaurante/list-menus/list-menus.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/restaurante/list-menus/list-menus.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section class=\"row mt-5\">\n    <ngx-alerts></ngx-alerts>\n    <div class=\"container\">\n        <h1><span style=\"font-size: 1em; color: #007bff\"><i class=\"fa fa-book\"></i></span> Listado del menú </h1>\n        <div style=\"text-align: center;\">\n            <!-- <div class=\"col\">\n                <div class=\"form-group\">\n                    <input type=\"text\" name=\"filterPost\" class=\"form-control\" placeholder=\"&#xf002; Buscar...\" style=\"font-family:Arial, FontAwesome\" [(ngModel)]=\"filterPost\">\n                </div>\n            </div> -->\n            <button class=\"btn btn-info float-right mb-3\" data-toggle=\"modal\" data-target=\"#modalMenu\"><i class=\"fa fa-plus\"></i> Nuevo producto</button>\n        </div>\n    </div>\n    <div class=\"col\">\n        <mat-form-field>\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n        </mat-form-field>\n\n        <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n            <!-- PRODUCTO Column -->\n            <ng-container matColumnDef=\"nombre\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Producto </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.nombre}} </td>\n            </ng-container>\n\n            <!-- PRECIO Column -->\n            <ng-container matColumnDef=\"precio\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Precio </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.precio}} </td>\n            </ng-container>\n\n            <!-- DESCRIPCION Column -->\n            <ng-container matColumnDef=\"descripcion\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Descripción </th>\n                <td mat-cell *matCellDef=\"let element\"> {{element.descripcion}} </td>\n            </ng-container>\n\n            <!-- IMAGEN Column -->\n            <ng-container matColumnDef=\"photourl\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Imagen </th>\n                <td mat-cell *matCellDef=\"let element\"> <img style=\"height: 45px; width: auto;\" class=\"img\" src=\"{{element.photourl}}\"> </td>\n            </ng-container>\n\n            <!-- ESTATUS Column -->\n            <ng-container matColumnDef=\"estado\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.estado == 1\">\n                        <mat-icon matBadge=\"+\" matBadgeColor=\"accent\">aspect_ratio</mat-icon>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estado == 2\">\n                        <mat-icon matBadge=\"-\" matBadgeColor=\"warn\">aspect_ratio</mat-icon>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <!-- EDITAR Column -->\n            <ng-container matColumnDef=\"edit\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Editar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button data-toggle=\"modal\" data-target=\"#modalMenu\" mat-mini-fab color=\"accent\" (click)=\"onPreUpdateMenu(element)\">\n                        <mat-icon>edit</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <!-- ELIMINAR Column -->\n            <ng-container matColumnDef=\"delete\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Borrar </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <button mat-mini-fab color=\"warn\" (click)=\"onDeleteMenu(element)\">\n                        <mat-icon>delete</mat-icon>\n                    </button>\n                </td>\n            </ng-container>\n\n            <!-- STATUS Column -->\n            <ng-container matColumnDef=\"status\">\n                <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Estatus </th>\n                <td mat-cell *matCellDef=\"let element\">\n                    <ng-container *ngIf=\"element.estado == 1\">\n                        <button mat-mini-fab color=\"blue\" (click)=\"desactivarProducto(element)\">\n                            <mat-icon>toggle_off</mat-icon>\n                        </button>\n                    </ng-container>\n                    <ng-container *ngIf=\"element.estado == 2\">\n                        <button mat-mini-fab color=\"primary\" (click)=\"activarProducto(element)\">                        \n                            <mat-icon>toggle_on</mat-icon>\n                        </button>\n                    </ng-container>\n                </td>\n            </ng-container>\n\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n        </table>\n        <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n        <div style=\"padding-top: 3em;\">\n\n        </div>\n\n    </div>\n</section>\n<app-modal-menu [userUid]=\"userUid\"></app-modal-menu>"

/***/ }),

/***/ "./src/app/components/restaurante/list-menus/list-menus.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/restaurante/list-menus/list-menus.component.ts ***!
  \***************************************************************************/
/*! exports provided: ListMenusComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListMenusComponent", function() { return ListMenusComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_restaurante_menu_menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/restaurante-menu/menu.service */ "./src/app/service/restaurante-menu/menu.service.ts");
/* harmony import */ var src_app_service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/service/restaurante/restaurante-data.service */ "./src/app/service/restaurante/restaurante-data.service.ts");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_8__);









var ListMenusComponent = /** @class */ (function () {
    function ListMenusComponent(dataMenu, dataser, auth, afs) {
        var _this = this;
        this.dataMenu = dataMenu;
        this.dataser = dataser;
        this.auth = auth;
        this.afs = afs;
        this.displayedColumns = ['nombre', 'precio', 'descripcion', 'photourl', 'estado', 'edit', 'delete', 'status'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTableDataSource"]();
        this.isRestaurante = null;
        this.userUid = null;
        this.filterPost = '';
        this.pageActual = 1;
        this.user = {
            uid: '',
            username: '',
            roles: {},
            geolocalizacion: {}
        };
        this.auth.isAuth().subscribe(function (user) {
            if (user) {
                _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.MenusCollection = _this.afs.collection('menus', function (ref) { return ref.where('userUid', '==', _this.user.uid); });
                _this.Menus = _this.MenusCollection.valueChanges();
            }
        });
    }
    ListMenusComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getCurrentUser();
        this.auth.isAuth().subscribe(function (user) {
            if (user) {
                _this.user.uid = user.uid;
                _this.user.username = user.displayName;
            }
            _this.getAllMenus()
                .subscribe(function (res) { return (_this.dataSource.data = res); });
        });
        // this.getListMenu();
    };
    // getListMenu() {
    //   this.getAllMenus().subscribe( menus =>  {
    //     this.menus = menus;
    //   });
    // }
    ListMenusComponent.prototype.getCurrentUser = function () {
        var _this = this;
        this.dataser.isAuth().subscribe(function (auth) {
            if (auth) {
                _this.userUid = auth.uid;
                _this.dataser.isUserRestaurante(_this.userUid).subscribe(function (userRole) {
                    _this.isRestaurante = Object.assign({}, userRole.roles).hasOwnProperty('restaurante');
                });
            }
        });
    };
    ListMenusComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListMenusComponent.prototype.onDeleteMenu = function (menu) {
        var _this = this;
        var idMn = menu.id;
        console.log('iid menu', menu);
        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: '¿Estas seguro?',
            text: "Eliminaras este producto de la lista del men\u00FA!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.dataMenu.deleteMenu(idMn);
                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Eliminado', 'Producto eliminado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Error!', 'Hubo un error al eliminar el producto', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de eliminar este producto?');
        // if ( confirmacion ) {
        //   this.dataMenu.deleteMenu(idMenu);
        // }
    };
    ListMenusComponent.prototype.onPreUpdateMenu = function (menu) {
        console.log('MENU', menu);
        this.dataMenu.selectedMenu = Object.assign({}, menu);
    };
    ListMenusComponent.prototype.getAllMenus = function () {
        return this.Menus = this.MenusCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        }));
    };
    ListMenusComponent.prototype.desactivarProducto = function (menu) {
        var _this = this;
        var idMn = menu.id;
        console.log('iid menu', idMn);
        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: '¿Estas seguro?',
            text: "Desactivaras este producto de la lista del men\u00FA!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, desactivar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.dataMenu.updateMenuDesactivar(idMn);
                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Desactivado', 'Producto desactivado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Error!', 'Hubo un error al desactivar el producto', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de desactivar este producto?');
        // if ( confirmacion ) {
        //   this.dataMenu.updateMenuDesactivar(uidMenu);
        // }
    };
    ListMenusComponent.prototype.activarProducto = function (menu) {
        var _this = this;
        var idMn = menu.id;
        console.log('iid menu', idMn);
        sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire({
            title: '¿Estas seguro?',
            text: "Activaras este producto de la lista al men\u00FA!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, activar'
        }).then(function (result) {
            if (result.value) {
                // console.log('Datos', idRepa );
                _this.dataMenu.updateMenuActivar(idMn);
                sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Activado', 'Producto activado', 'success');
            }
        }).catch(function (error) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_8___default.a.fire('Error!', 'Hubo un error al activar el producto', 'error');
        });
        // const confirmacion = confirm('¿Estas seguro de activar este producto?');
        // if ( confirmacion ) {
        //   this.dataMenu.updateMenuActivar(uidMenu);
        // }
    };
    ListMenusComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_7__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatPaginator"])
    ], ListMenusComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSort"])
    ], ListMenusComponent.prototype, "sort", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ListMenusComponent.prototype, "userUidd", void 0);
    ListMenusComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-menus',
            template: __webpack_require__(/*! ./list-menus.component.html */ "./src/app/components/restaurante/list-menus/list-menus.component.html"),
            styles: [__webpack_require__(/*! ./list-menus.component.css */ "./src/app/components/restaurante/list-menus/list-menus.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_restaurante_menu_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"],
            src_app_service_restaurante_restaurante_data_service__WEBPACK_IMPORTED_MODULE_3__["RestauranteDataService"],
            _service_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestore"]])
    ], ListMenusComponent);
    return ListMenusComponent;
}());



/***/ }),

/***/ "./src/app/components/restaurante/list-pedidos/list-pedidos.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/components/restaurante/list-pedidos/list-pedidos.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n    width: 100%;\n}\n\n.mat-form-field {\n    font-size: 14px;\n    width: 100%;\n}\n\ninput {\n    border-radius: 25px;\n}\n\nbutton {\n    border-radius: 25px;\n}\n\nagm-map {\n    height: 300px;\n}\n\n.modal-content {\n    width: 263%;\n    margin-left: -79%;\n}\n\n.secc {\n    margin-top: 3em;\n    margin-right: 110px;\n    margin-left: 110px;\n}\n\n.secc1 {\n    margin-right: 110px;\n    margin-left: 110px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZXN0YXVyYW50ZS9saXN0LXBlZGlkb3MvbGlzdC1wZWRpZG9zLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxnQkFBZ0I7SUFDaEIsWUFBWTtDQUNmOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksY0FBYztDQUNqQjs7QUFFRDtJQUNJLFlBQVk7SUFDWixrQkFBa0I7Q0FDckI7O0FBRUQ7SUFDSSxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixtQkFBbUI7Q0FDdEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3Jlc3RhdXJhbnRlL2xpc3QtcGVkaWRvcy9saXN0LXBlZGlkb3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbmlucHV0IHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xufVxuXG5idXR0b24ge1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbmFnbS1tYXAge1xuICAgIGhlaWdodDogMzAwcHg7XG59XG5cbi5tb2RhbC1jb250ZW50IHtcbiAgICB3aWR0aDogMjYzJTtcbiAgICBtYXJnaW4tbGVmdDogLTc5JTtcbn1cblxuLnNlY2Mge1xuICAgIG1hcmdpbi10b3A6IDNlbTtcbiAgICBtYXJnaW4tcmlnaHQ6IDExMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMTBweDtcbn1cblxuLnNlY2MxIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDExMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/restaurante/list-pedidos/list-pedidos.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/components/restaurante/list-pedidos/list-pedidos.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section>\n    <ngx-alerts></ngx-alerts>\n    <div class=\"secc\">\n        <ul class=\"nav nav-tabs\">\n            <li class=\"nav-item\">\n                <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#home\">Servicios del Día</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"nav-link\" data-toggle=\"tab\" href=\"#profile\">Nuevo pedido</a>\n            </li>\n        </ul>\n        <div id=\"myTabContent\" class=\"tab-content\">\n            <div class=\"mt-3 tab-pane fade active show\" id=\"home\">\n                <h1><span style=\"font-size: 1em; color: #007bff\"><i class=\"fa fa-motorcycle\"></i></span> Lista de servicios </h1>\n                <div class=\"form-group\">\n                    <!--Buscador-->\n                    <!-- <input type=\"text\" class=\"form-control\" placeholder=\"&#xf002; Buscar servicio por clave...\" style=\"font-family:Arial, FontAwesome\" name=\"filterPedido\" [(ngModel)]=\"filterPedido\"> -->\n                </div>\n\n                <hr class=\"my-4\">\n                <!-- <div class=\"row pad-10\"> -->\n                <mat-form-field>\n                    <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n                </mat-form-field>\n\n                <table mat-table [dataSource]=\"dataSource\" class=\"mat-elevation-z8\" matSort class=\"mat-elevation-z8\">\n\n                    <!-- CLAVE Column -->\n                    <ng-container matColumnDef=\"clave\">\n                        <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Clave </th>\n                        <td mat-cell *matCellDef=\"let element\"> <b>{{element.clave}}</b> </td>\n                    </ng-container>\n\n                    <!-- HORA Column -->\n                    <ng-container matColumnDef=\"hora\">\n                        <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Hora </th>\n                        <td mat-cell *matCellDef=\"let element\"> {{element.fecha | date:'shortTime'}}</td>\n                    </ng-container>\n\n                    <!-- USUARIO Column -->\n                    <ng-container matColumnDef=\"usuario\">\n                        <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Usuario </th>\n                        <td mat-cell *matCellDef=\"let element\">\n                            {{element.nombreUsuario}}\n                            <ng-container *ngIf=\"element.cliente\">\n                                {{ element.cliente }}\n                            </ng-container>\n\n                        </td>\n                    </ng-container>\n\n                    <!-- TIPO Column -->\n                    <ng-container matColumnDef=\"tipo\">\n                        <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Tipo </th>\n                        <td mat-cell *matCellDef=\"let element\">\n                            <ng-container *ngIf=\"element.tipo && element.abierto == null\">\n                                <img class=\"iconTi\" src=\"../../../../assets/imgs/tipo1.png\" title=\"Servicio pide lo que quieras.\">\n                            </ng-container>\n                            <ng-container *ngIf=\"element.abierto == 'Abierto plataforma'\">\n                                <img class=\"iconTi\" src=\"../../../../assets/imgs/tipo2.png\" title=\"Servicio de convenio.\">\n                            </ng-container>\n                        </td>\n                    </ng-container>\n\n                    <!-- ESTADO Column -->\n                    <ng-container matColumnDef=\"estatus\">\n                        <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef mat-sort-header> Estado </th>\n                        <td mat-cell *matCellDef=\"let element\">\n                            <ng-container *ngIf=\"element.estatus == 'Aceptado'\">\n                                <span class=\"badge badge-success\">Pedido Aceptado</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'AceptaBme'\">\n                                <span class=\"badge badge-primary\">Repartidor asignado</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'Gestando'\">\n                                <span class=\"badge badge-primary\">Se esta preparando el pedido</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'Notificando'\">\n                                <span class=\"badge badge-primary\">Buscando Repartidor</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'BuscandoBme'\">\n                                <span class=\"badge badge-primary\">Notificando al repartidor</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'Yendo'\">\n                                <span class=\"badge badge-success\">BMe dirigiendose por el pedido</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'Llevandolo'\">\n                                <span class=\"badge badge-success\">BMe dirigiendose al destino</span>\n                            </ng-container>\n                            <!-- Restaurante estados -->\n                            <ng-container *ngIf=\"element.estatus == 'Enviando'\">\n                                <span class=\"badge badge-primary\">Enviando pedido al domicilio</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'EnPuerta'\">\n                                <span class=\"badge badge-primary\">Esta en puerta</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'Pago'\">\n                                <span class=\"badge badge-primary\">Finalizando servicio</span>\n                            </ng-container>\n                            <ng-container *ngIf=\"element.estatus == 'Pagado'\">\n                                <span class=\"badge badge-primary\">Servicio finalizado</span>\n                            </ng-container>\n                        </td>\n                    </ng-container>\n\n                    <!-- DETALLE Column -->\n                    <ng-container matColumnDef=\"detalle\">\n                        <th style=\"text-align: center;\" mat-header-cell *matHeaderCellDef> Detalle </th>\n                        <td mat-cell *matCellDef=\"let element\">\n                            <button mat-mini-fab color=\"accent\" title=\"Detalle del pedido.\" routerLink=\"../../servicio-convenio/{{element.uid}}\">\n                                <mat-icon>list</mat-icon>\n                            </button>\n                        </td>\n                    </ng-container>\n                    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n                    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n                </table>\n                <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator>\n                <div style=\"padding-top: 3em;\">\n\n                </div>\n\n            </div>\n            <div class=\"tab-pane fade\" id=\"profile\">\n                <div class=\"secc1\">\n                    <ngx-alerts></ngx-alerts>\n                    <h1><span style=\"font-size: 1em; color: #007bff;\"><i class=\"fa fa-paper-plane-o\"></i></span> Nuevo pedido </h1>\n                    <form name=\"formPedido\" #formPedido=\"ngForm\" (ngSubmit)=\"onSavePedido(formPedido)\">\n                        <div class=\"form-group\">\n                            <label for=\"link_amazon\">Nombre del cliente</label>\n                            <input type=\"text\" name=\"cliente\" class=\"form-control\" placeholder=\"Nombre del cliente\" [(ngModel)]=\"this.selectedPedido.cliente\" required=\"true\">\n                        </div>\n                        <div class=\"form-group\">\n                            <!-- <label for=\"link_amazon\">Dirección de entrega</label> -->\n                            <div class=\"form-group\">\n                                <label>Ingrese dirección de entrega</label>\n                                <input type=\"text\" class=\"form-control\" (keydown.enter)=\"$event.preventDefault()\" placeholder=\"Buscar Dirección\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"off\" type=\"text\" required=\"true\" #search>\n                            </div>\n                            <agm-map [latitude]=\"latitude\" [longitude]=\"longitude\" [zoom]=\"zoom\">\n                                <agm-marker [latitude]=\"latitude\" [longitude]=\"longitude\" [markerDraggable]=\"true\" (dragEnd)=\"markerDragEnd($event)\"></agm-marker>\n                            </agm-map>\n                            <h5>Address: {{address}}</h5>\n                            <div>Latitude: {{latitude}}</div>\n                            <div>Longitude: {{longitude}}</div>\n                        </div>\n                        <!-- <input type=\"text\" name=\"entregaDir\" class=\"form-control\" placeholder=\"Mariano Escobedo #5409, Col. Los Rosales\" [(ngModel)]=\"this.selectedPedido.entregaDir\"> -->\n                        <div class=\"form-group\">\n                            <label for=\"tel\">Teléfono del cliente</label>\n                            <input type=\"number\" name=\"tel\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"this.selectedPedido.tel\" required=\"true\">\n                        </div>\n                        <div class=\"form-group\">\n                            <div class=\"form-group\">\n                                <label for=\"descripcion\" class=\"col-form-label\">Descripción de pedido:</label>\n                                <textarea class=\"form-control\" name=\"descripcion\" [(ngModel)]=\"this.selectedPedido.descripcion\"></textarea>\n                            </div>\n                        </div>\n                        <div class=\"form-group\">\n                            <label for=\"totalProductos\">Costo del pedido:</label>\n                            <input type=\"number\" name=\"totalProductos\" required class=\"form-control\" placeholder=\"\" [(ngModel)]=\"this.selectedPedido.totalProductos\" required>\n                        </div>\n                        <div class=\"form-group\">\n                            <div class=\"form-group\">\n                                <label for=\"notas\" class=\"col-form-label\">Notas :</label>\n                                <textarea class=\"form-control\" name=\"notas\" [(ngModel)]=\"this.selectedPedido.notas\"></textarea>\n                            </div>\n                        </div>\n\n                        <div class=\"modal-footer\">\n                            <button type=\"submit\" class=\"btn btn-primary\">Hacer Pedido</button>\n                            <button type=\"hidden\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button>\n                        </div>\n                    </form>\n                </div>\n\n            </div>\n\n        </div>\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/components/restaurante/list-pedidos/list-pedidos.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/components/restaurante/list-pedidos/list-pedidos.component.ts ***!
  \*******************************************************************************/
/*! exports provided: ListPedidosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPedidosComponent", function() { return ListPedidosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/pedidos/pedidos.service */ "./src/app/service/pedidos/pedidos.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");











// declare var google: any;
var ListPedidosComponent = /** @class */ (function () {
    function ListPedidosComponent(authService, afs, pedidoServ, mapsAPILoader, ngZone, alert) {
        this.authService = authService;
        this.afs = afs;
        this.pedidoServ = pedidoServ;
        this.mapsAPILoader = mapsAPILoader;
        this.ngZone = ngZone;
        this.alert = alert;
        this.displayedColumns = ['clave', 'hora', 'usuario', 'tipo', 'estatus', 'detalle'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTableDataSource"]();
        this.today = new Date();
        this.jsToday = '';
        this.uidConvenio = '';
        this.filterPost = '';
        this.filterPedido = '';
        this.selectedPedido = {};
        this.isAdmin = null;
        this.estatus = 'BuscandoBme';
        // user data login
        this.uidUser = null;
        this.providerId = 'null';
        this.usuario = [];
        this.pedido = {};
        this.producto = {};
        // maps
        this.title = 'AGM project';
        this.user = {
            uid: '',
            username: '',
            email: '',
            direccion: '',
            geolocalizacion: {},
            // photoUrl: '',
            roles: {}
        };
        this.servicioCollection = afs.collection('servicios');
        this.jsToday = Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["formatDate"])(this.today, 'dd-MM-yyyy', 'en-US');
    }
    ListPedidosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getAllPedidosHoy();
        this.getAllUsuarios();
        // MAPS
        // load Places Autocomplete
        this.mapsAPILoader.load().then(function () {
            _this.setCurrentLocation();
            _this.geoCoder = new google.maps.Geocoder;
            var autocomplete = new google.maps.places.Autocomplete(_this.searchElementRef.nativeElement, {
                types: ['address']
            });
            autocomplete.addListener('place_changed', function () {
                _this.ngZone.run(function () {
                    // get the place result
                    var place = autocomplete.getPlace();
                    // verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }
                    // set latitude, longitude and zoom
                    _this.latitude = place.geometry.location.lat();
                    _this.longitude = place.geometry.location.lng();
                    _this.zoom = 12;
                });
            });
        });
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.providerId = user.providerData[0].providerId;
                _this.afs.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuarios = data;
                    console.log('data', data);
                    _this.uidSucursal = data.uidSucursal;
                    _this.pedido = {
                        direccionLocal: data.address,
                        nombreLocal: data.username,
                        pedidoGeo: new firebase__WEBPACK_IMPORTED_MODULE_8__["firestore"].GeoPoint(data.direccion._lat, data.direccion._long)
                        // servicioID: this.uid
                    };
                });
            }
        });
    };
    ListPedidosComponent.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    ListPedidosComponent.prototype.getAllPedidosHoy = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidConvenio = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.afs.collection('servicios', function (ref) {
                    return ref
                        .where('uidRestaurante', '==', _this.uidConvenio).orderBy('fecha', 'desc');
                })
                    .valueChanges().subscribe(function (data) {
                    var serv = data;
                    var servicios = [];
                    serv.forEach(function (p) {
                        if (p.estatus !== 'Cancelado' &&
                            p.estatus !== 'Terminado' &&
                            p.estatus !== 'creando' &&
                            p.estatus !== 'Pagado') {
                            servicios.push(p);
                        }
                    });
                    // console.log("servicios_", servicios);
                    _this.pedidos = servicios;
                    _this.dataSource.data = servicios;
                });
                //   this.afs.collection('servicios', ref =>
                //     ref
                //       .where('uidRestaurante', '==', this.uidConvenio)).valueChanges().subscribe(data => {
                //         this.pedidos = data;
                //         console.log('query', data);
                //       });
            }
        });
    };
    ListPedidosComponent.prototype.getAllUsuarios = function () {
        var _this = this;
        this.pedidoServ.getAllUsuarios().subscribe(function (usuarios) {
            _this.usuarios = usuarios;
        });
    };
    // Get Current Location Coordinates
    ListPedidosComponent.prototype.setCurrentLocation = function () {
        var _this = this;
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                _this.latitude = position.coords.latitude;
                _this.longitude = position.coords.longitude;
                _this.zoom = 8;
                _this.getAddress(_this.latitude, _this.longitude);
            });
        }
    };
    ListPedidosComponent.prototype.markerDragEnd = function ($event) {
        console.log('markdrag', $event);
        this.latitude = $event.coords.lat;
        this.longitude = $event.coords.lng;
        this.getAddress(this.latitude, this.longitude);
    };
    ListPedidosComponent.prototype.getAddress = function (latitude, longitude) {
        var _this = this;
        this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, function (results, status) {
            console.log(results);
            console.log(status);
            if (status === 'OK') {
                if (results[0]) {
                    _this.zoom = 12;
                    _this.address = results[0].formatted_address;
                }
                else {
                    window.alert('No results found');
                }
            }
            else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    };
    ListPedidosComponent.prototype.onSavePedido = function (formPedido) {
        var _this = this;
        if (this.selectedPedido.cliente === '' || this.address === '' || this.selectedPedido.descripcion === '' ||
            this.selectedPedido.totalProductos === null || this.selectedPedido.notas === '') {
            this.alert.warning('Falta información en el formulario');
        }
        else {
            this.time = moment__WEBPACK_IMPORTED_MODULE_6__(this.today).format('x');
            this.authService.isAuth().subscribe(function (user) {
                if (user) {
                    _this.uidConvenio = _this.user.uid = user.uid;
                    // datos
                    formPedido.value.uidRestaurante = _this.uidConvenio;
                    formPedido.value.estatus = _this.estatus;
                    formPedido.value.fecha = _this.time;
                    // formPedido.value.uidRepartidor = this.repartidor;
                    formPedido.value.uidCliente = formPedido.value.cliente;
                    formPedido.value.tipo = 2;
                    formPedido.value.tipoTransporte = 'Motocicleta';
                    formPedido.value.entregaDir = _this.address;
                    formPedido.value.metodo_pago = 'Efectivo';
                    formPedido.value.abierto = 'Abierto plataforma';
                    formPedido.value.numTikets = 0;
                    formPedido.value.numPedidos = 1;
                    formPedido.value.uidSucursal = _this.uidSucursal;
                    formPedido.value.entregaGeo = new firebase__WEBPACK_IMPORTED_MODULE_8__["firestore"].GeoPoint(_this.latitude, _this.longitude);
                    var des_1 = formPedido.value.descripcion;
                    console.log('des', des_1);
                    var clave = moment__WEBPACK_IMPORTED_MODULE_6__().toArray();
                    console.log(clave[5], clave[6]);
                    var segundos = clave[5].toString();
                    var milisegundos = clave[6].toString();
                    var _clave_1 = segundos + milisegundos;
                    _this.afs.collection('servicios').add(formPedido.value)
                        .then(function (ref) {
                        console.log('id', _this.uid = ref.id);
                        _this.onPedido(_this.uid, des_1);
                        _this.afs.collection('servicios').doc(_this.uid).update({
                            uid: _this.uid,
                            clave: _clave_1
                        });
                    });
                    console.log('form', formPedido.value);
                    _this.btnClose.nativeElement.click();
                    formPedido.resetForm();
                    _this.btnClose.nativeElement.click();
                    _this.alert.success('Servicio solicitado.');
                }
            });
        }
    };
    ListPedidosComponent.prototype.onPedido = function (servicioID, des) {
        var _this = this;
        console.log('des', des);
        this.afs.collection('pedidos').add(this.pedido)
            .then(function (ref) {
            console.log('idPEdido', _this.pedidoID = ref.id);
            _this.onProducto(des, servicioID, _this.pedidoID);
            _this.afs.collection('pedidos').doc(_this.pedidoID).update({
                servicioID: servicioID
            });
        });
    };
    ListPedidosComponent.prototype.onProducto = function (desc, servicio, pedido) {
        var _this = this;
        this.producto = {
            cantidad: 1,
            descripcion: desc,
            servicioID: servicio,
            pedidoID: pedido
        };
        this.afs.collection('productos').add(this.producto)
            .then(function (ref) {
            console.log('productoID', _this.productoID = ref.id);
            _this.afs.collection('productos').doc(_this.productoID).update({
                productoID: _this.productoID
            });
        });
    };
    ListPedidosComponent.prototype.onCancelService = function (uid) {
        var _this = this;
        var confirmacion = confirm('¿Esta seguro de cancelar este servicio?');
        if (confirmacion) {
            this.afs.collection('servicios').doc(uid).update({
                estatus: 'Cancelado'
            }).then(function () {
                _this.afs.collection('servicios').doc(uid).update({
                    por: 'Convenio'
                });
            });
        }
    };
    ListPedidosComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginator"])
    ], ListPedidosComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSort"])
    ], ListPedidosComponent.prototype, "sort", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ListPedidosComponent.prototype, "btnClose", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('search'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ListPedidosComponent.prototype, "searchElementRef", void 0);
    ListPedidosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list-pedidos',
            template: __webpack_require__(/*! ./list-pedidos.component.html */ "./src/app/components/restaurante/list-pedidos/list-pedidos.component.html"),
            styles: [__webpack_require__(/*! ./list-pedidos.component.css */ "./src/app/components/restaurante/list-pedidos/list-pedidos.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            _service_pedidos_pedidos_service__WEBPACK_IMPORTED_MODULE_5__["PedidosService"],
            _agm_core__WEBPACK_IMPORTED_MODULE_7__["MapsAPILoader"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_9__["AlertService"]])
    ], ListPedidosComponent);
    return ListPedidosComponent;
}());



/***/ }),

/***/ "./src/app/components/restaurante/modal-menu/modal-menu.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/components/restaurante/modal-menu/modal-menu.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".oril {\n    text-align: right;\n}\n\n.img {\n    height: 75px;\n    width: 75px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZXN0YXVyYW50ZS9tb2RhbC1tZW51L21vZGFsLW1lbnUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtDQUNyQjs7QUFFRDtJQUNJLGFBQWE7SUFDYixZQUFZO0NBQ2YiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3Jlc3RhdXJhbnRlL21vZGFsLW1lbnUvbW9kYWwtbWVudS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm9yaWwge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4uaW1nIHtcbiAgICBoZWlnaHQ6IDc1cHg7XG4gICAgd2lkdGg6IDc1cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/restaurante/modal-menu/modal-menu.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/restaurante/modal-menu/modal-menu.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modalMenu\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"exampleModalLabel\">\n                    Nuevo Producto\n                </h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n            </div>\n            <div class=\"alert alert-dismissible alert-warning\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n                <h4 class=\"alert-heading\">Importante!</h4>\n                <p class=\"mb-0\">Las medidas estandar para las imágenes son 65 x 65 px.</p>\n            </div>\n            <ngx-alerts></ngx-alerts>\n            <div class=\"modal-body\">\n                <form name=\"formMenu\" #formMenu=\"ngForm\" (ngSubmit)=\"onSaveMenu(formMenu)\">\n                    <input type=\"hidden\" name=\"id\" [(ngModel)]=\"this.dataMenu.selectedMenu.id\">\n\n                    <input type=\"hidden\" name=\"userUid\" [value]=\"userUid\" [(ngModel)]=\"this.dataMenu.selectedMenu.userUid\">\n\n                    <div class=\"form-group\">\n                        <label for=\"nombre\">Nombre</label>\n                        <input type=\"text\" name=\"nombre\" class=\"form-control\" placeholder=\"Pizza\" [(ngModel)]=\"this.dataMenu.selectedMenu.nombre\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"precio\">Precio</label>\n                        <input type=\"number\" name=\"precio\" class=\"form-control\" placeholder=\"230.00\" [(ngModel)]=\"this.dataMenu.selectedMenu.precio\">\n                    </div>\n                    <div class=\"form-group\">\n                        <label for=\"descripcion\" class=\"col-form-label\">Descripción:</label>\n                        <textarea class=\"form-control\" name=\"descripcion\" [(ngModel)]=\"this.dataMenu.selectedMenu.descripcion\"></textarea>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <select class=\"custom-select\" name=\"estado\" [(ngModel)]=\"this.dataMenu.selectedMenu.estado\">\n                              <option selected=\"\">Selecciona el estado del producto</option>\n                              <option  value=\"1\">Activo</option>\n                              <option value=\"2\">Desactivado</option>\n                            </select>\n                    </div>\n                    <img *ngIf=\"this.dataMenu.selectedMenu.photourl\" class=\"img\" src=\"{{ this.dataMenu.selectedMenu.photourl }} \">\n                    <input type=\"hidden\" name=\"photourl\" [(ngModel)]=\"this.dataMenu.selectedMenu.photourl\">\n\n                    <div class=\"form-group\">\n                        <h5>Seleccionar imagen:</h5>\n                        <input class=\"hi\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                    </div>\n                    <div class=\"progress\">\n                        <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n                        </div>\n                    </div>\n                    <br>\n                    <!-- <input #imageUser type=\"hidden\" [value]=\"urlImage | async\"> -->\n                    <br>\n                    <div>\n                        <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Guardar</button> &nbsp;\n                    </div>\n\n                    <!-- <div class=\"oril\">\n                        <button type=\"hidden\" class=\"btn btn-secondary\" #btnClose data-dismiss=\"modal\">Cerrar</button>\n                    </div> -->\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/restaurante/modal-menu/modal-menu.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/restaurante/modal-menu/modal-menu.component.ts ***!
  \***************************************************************************/
/*! exports provided: ModalMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalMenuComponent", function() { return ModalMenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_restaurante_menu_menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/restaurante-menu/menu.service */ "./src/app/service/restaurante-menu/menu.service.ts");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");






var ModalMenuComponent = /** @class */ (function () {
    function ModalMenuComponent(dataMenu, alertService, storage) {
        this.dataMenu = dataMenu;
        this.alertService = alertService;
        this.storage = storage;
        // estaSobreElemento = false;
        this.archivos = [];
    }
    ModalMenuComponent.prototype.ngOnInit = function () {
        console.log('UserId', this.userUid);
        // console.log(this.dataMenu.selectedMenu);
    };
    ModalMenuComponent.prototype.onSaveMenu = function (formMenu) {
        console.log('id', formMenu.value.id);
        if (formMenu.value.id === null || formMenu.value.id === undefined) {
            // Es un nuevo producto
            this.dataMenu.selectedMenu.userUid = this.userUid;
            this.dataMenu.guardarMenu(this.urlImage1, this.dataMenu.selectedMenu.nombre, this.dataMenu.selectedMenu.estado, this.dataMenu.selectedMenu.precio, this.dataMenu.selectedMenu.descripcion, this.userUid);
            console.log('save', this.dataMenu.selectedMenu);
        }
        else {
            // Cuando se edita un producto.
            console.log('id', this.dataMenu.selectedMenu.id);
            this.dataMenu.editarMenu(this.urlImage1, formMenu.value);
        }
        formMenu.resetForm();
        this.btnClose.nativeElement.click();
    };
    ModalMenuComponent.prototype.onUpload = function (e) {
        var _this = this;
        console.log('subir', e.target.files[0]);
        var id = Math.random().toString(36).substring(2);
        var file = e.target.files[0];
        var filePath = "menus/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            return ref.getDownloadURL().subscribe(function (r) {
                _this.urlImage1 = _this.urlImage = r;
                console.log('foto', r);
            });
        }))
            .subscribe();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('btnClose'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ModalMenuComponent.prototype, "btnClose", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ModalMenuComponent.prototype, "userUid", void 0);
    ModalMenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-menu',
            template: __webpack_require__(/*! ./modal-menu.component.html */ "./src/app/components/restaurante/modal-menu/modal-menu.component.html"),
            styles: [__webpack_require__(/*! ./modal-menu.component.css */ "./src/app/components/restaurante/modal-menu/modal-menu.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_restaurante_menu_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"], ngx_alerts__WEBPACK_IMPORTED_MODULE_3__["AlertService"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_5__["AngularFireStorage"]])
    ], ModalMenuComponent);
    return ModalMenuComponent;
}());



/***/ }),

/***/ "./src/app/components/users/login/login.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/users/login/login.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-control.login-user {\n    border: 0 solid #fff !important;\n}\n\n.im {\n    height: 130px;\n    width: 150px;\n}\n\nhtml body head {\n    background-image: url('/assets/imgs/Blog.jpg') no-repeat center center fixed;\n    /* background: url('../assets/imgs/bringme.png')  */\n    background-size: cover;\n}\n\n.im[_ngcontent-c2] {\n    height: 100% !important;\n    width: 150px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy91c2Vycy9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZ0NBQWdDO0NBQ25DOztBQUVEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7O0FBRUQ7SUFDSSw2RUFBNkU7SUFDN0Usb0RBQW9EO0lBSXBELHVCQUF1QjtDQUMxQjs7QUFFRDtJQUNJLHdCQUF3QjtJQUN4Qix3QkFBd0I7Q0FDM0IiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3VzZXJzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9ybS1jb250cm9sLmxvZ2luLXVzZXIge1xuICAgIGJvcmRlcjogMCBzb2xpZCAjZmZmICFpbXBvcnRhbnQ7XG59XG5cbi5pbSB7XG4gICAgaGVpZ2h0OiAxMzBweDtcbiAgICB3aWR0aDogMTUwcHg7XG59XG5cbmh0bWwgYm9keSBoZWFkIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaW1ncy9CbG9nLmpwZycpIG5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIGZpeGVkO1xuICAgIC8qIGJhY2tncm91bmQ6IHVybCgnLi4vYXNzZXRzL2ltZ3MvYnJpbmdtZS5wbmcnKSAgKi9cbiAgICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmltW19uZ2NvbnRlbnQtYzJdIHtcbiAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICB3aWR0aDogMTUwcHggIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/users/login/login.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/users/login/login.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"login\" class=\"mb-5 mt-5\">\n    <div class=\"container\">\n        <!-- <img srcset=\"../assets/imgs/Blog-1200.png 1024w,\n        ../assets/imgs/Blog-927.png 468w\" sizes=\"(min-width: 1024px) 80vw, 100vw\" class=\"img-fluid img-responsive\" style=\"position:absolute; right: 0; top: 0; z-index: -100; opacity: 0.75;\"> -->\n        <div class=\"row\">\n            <div class=\"col-xs-12 col-sm-6 col-md-4 mx-auto\">\n                <div class=\"card_login\">\n                    <div class=\"card\">\n                        <div class=\"card-body text-center\">\n                            <form #formLogin=\"ngForm\" (submit)=\"onLogin()\">\n                                <div><img class=\"im\" src=\"../assets/imgs/lolo.png\" alt=\"\"></div>\n                                <h1 class=\"h3 mb-3 font-weight-normal\" *ngIf=\"!isError\">Login</h1>\n                                <ngx-alerts></ngx-alerts>\n                                <div class=\"form-group\">\n                                    <input [(ngModel)]=\"email\" type=\"email\" id=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Email\" required>\n                                </div>\n                                <div class=\"form-group\">\n                                    <input [(ngModel)]=\"password\" type=\"password\" id=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" required>\n                                </div>\n                                <button type=\"submit\" class=\"btn btn-lg btn-primary btn-block\">Login</button>\n                                <!-- <div class=\"form-group\">\n                                    <a routerLink=\"/user/register\" class=\"form-control login-user\">Are you new ?</a>\n                                </div> -->\n                                <!-- <div class=\"form-group\">\n                                    <button class=\"btn btn-block btn-social btn-facebook\" (click)=\"onLoginFacebook()\">\n                    <span class=\"fa fa-facebook\"></span>\n                    FACEBOOK\n                  </button>\n                                    <button class=\"btn btn-block btn-social btn-google\" (click)=\"onLoginGoogle()\">\n                    <span class=\"fa fa-google\"></span>\n                    GOOGLE\n                  </button>\n                                </div> -->\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/components/users/login/login.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/users/login/login.component.ts ***!
  \***********************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _service_administrador_administrador_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/administrador/administrador-data.service */ "./src/app/service/administrador/administrador-data.service.ts");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");







var LoginComponent = /** @class */ (function () {
    function LoginComponent(afAuth, router, authService, dataAdmn, alertService) {
        this.afAuth = afAuth;
        this.router = router;
        this.authService = authService;
        this.dataAdmn = dataAdmn;
        this.alertService = alertService;
        this.email = '';
        this.password = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
        // this.getListAdmn();
    };
    // getListAdmn () {
    //   this.dataAdmn.getAllAdministradores().subscribe( admins => {
    //     console.log('Admins', admins);
    //    this.admins = admins;
    //  });
    // }
    // onLoginGoogle() {
    //   this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    //   this.router.navigate(['admin/list-sucursale']);
    // }
    LoginComponent.prototype.onLogin = function () {
        var _this = this;
        // console.log('email', this.email);
        // console.log('pass', this.password);
        this.authService.loginEmailUser(this.email, this.password)
            .then(function (res) {
            // if (this.admins ) {
            _this.router.navigate(['admin/home-admin']);
            // }
        }).catch(function (err) { return _this.alertService.danger(err.message); });
    };
    //  getListAdmn() {
    //  }
    LoginComponent.prototype.logOut = function () {
        this.afAuth.auth.signOut();
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/users/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/users/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _service_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _service_administrador_administrador_data_service__WEBPACK_IMPORTED_MODULE_5__["AdministradorDataService"], ngx_alerts__WEBPACK_IMPORTED_MODULE_6__["AlertService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/users/profile/profile.component.css":
/*!****************************************************************!*\
  !*** ./src/app/components/users/profile/profile.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlcnMvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/users/profile/profile.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/users/profile/profile.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"profile\" class=\"container\">\n    <div class=\"card mb-3\">\n        <h3 class=\"card-header\">Perfil </h3>\n        <div class=\"card-body\">\n            <h5 *ngIf=\"usuario.roles.administrador == true\" class=\"card-title\">{{ usuario.username }} {{ usuario.lastname }} </h5>\n            <h5 *ngIf=\"usuario.roles.restaurante == true\" class=\"card-title\">{{ usuario.username }}</h5>\n            <h6 class=\"card-subtitle text-muted\">{{ usuario.email }} </h6>\n        </div>\n        <img *ngIf=\"usuario.roles.administrador == true\" style=\"text-align: center; height: auto; width: 30%; display: block\" [src]=\"user.photourl\" alt=\"Card image\">\n        <img *ngIf=\"usuario.roles.restaurante == true\" style=\"text-align: center; height: auto; width: 30%; display: block\" [src]=\"usuario.photourl\" alt=\"Card image\">\n        <ul class=\"list-group list-group-flush\">\n            <li class=\"list-group-item text-muted\">{{ usuario.uid }} </li>\n            <li class=\"list-group-item\">{{ usuario.address }} </li>\n            <li class=\"list-group-item\">{{ usuario.phone }} </li>\n            <li class=\"list-group-item\" *ngIf=\"usuario.roles.restaurante == true\"> <span class=\"badge-pill badge-success\">{{ usuario.pago }}</span> </li>\n            <li class=\"list-group-item\" *ngIf=\"usuario.roles.administrador == true\"> <span *ngIf=\"usuario.activo == true\" class=\"badge-pill badge-success\"> Activo </span> </li>\n        </ul>\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/components/users/profile/profile.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/users/profile/profile.component.ts ***!
  \***************************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");




var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(authService, db) {
        this.authService = authService;
        this.db = db;
        this.user = {
            username: '',
            email: '',
            // photoUrl: '',
            roles: {},
            geolocalizacion: {}
        };
        this.uidUser = null;
        this.usuario = {};
        this.providerId = 'null';
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.isAuth().subscribe(function (user) {
            if (user) {
                _this.uidUser = _this.user.uid = user.uid;
                _this.user.username = user.displayName;
                _this.user.email = user.email;
                _this.user.photourl = user.photoURL;
                _this.providerId = user.providerData[0].providerId;
                _this.db.doc("users/" + _this.uidUser).valueChanges().subscribe(function (data) {
                    _this.usuario = data;
                    console.log('usuer', _this.usuario);
                });
            }
        });
    };
    ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/components/users/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/components/users/profile/profile.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/components/users/register/register.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/users/register/register.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hi {\n    text-align: left;\n    font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy91c2Vycy9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0lBQ2pCLGdCQUFnQjtDQUNuQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlcnMvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oaSB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBmb250LXNpemU6IDEycHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/users/register/register.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/users/register/register.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"register\" class=\"mb-5 mt-5\">\n    <div class=\"container\">\n        <div class=\"row\">\n\n            <div class=\"col-xs-12 col-sm-6 col-md-4 mx-auto\">\n                <div class=\"card_register\">\n                    <div class=\"card\">\n                        <div class=\"card-body text-center\">\n                            <form #formRegister=\"ngForm\" (ngSubmit)=\"onAddUser()\">\n                                <h1 class=\"h3 mb-3 font-weight-normal\">Register</h1>\n                                <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"isError\">\n                                    {{msgError}}\n                                </div>\n                                <div class=\"form-group\">\n                                    <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Email\" [(ngModel)]=\"email\" required minlength=\"8\">\n                                </div>\n                                <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"email.touched && !email.valid\">\n                                    <div *ngIf=\"email.errors.required\">Email is required</div>\n                                    <div *ngIf=\"email.errors.minlength\">Email must be at least 8 characters</div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" [(ngModel)]=\"password\" required minlength=\"5\" maxlength=\"10\">\n                                </div>\n                                <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"password.touched && !password.valid\">\n                                    <div *ngIf=\"password.errors.required\">Password is required</div>\n                                    <div *ngIf=\"password.errors.minlength\">Password must be at least 5 characters</div>\n                                </div>\n                                <hr>\n                                <div class=\"form-group\">\n                                    <h5>Seleccionar image:</h5>\n                                    <input class=\"hi\" type=\"file\" accept=\".png, .jpg\" (change)=\"onUpload($event)\">\n                                </div>\n                                <div class=\"progress\">\n                                    <div [style.visibility]=\"(uploadPercent == 0) ? 'hidden' : 'visible'\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" [style.width]=\"(uploadPercent | async) +'%'\">\n\n                                        <!-- <span class=\"progressText\" *ngIf=\"urlImage | async\">\n                      Ok!!</span> -->\n                                    </div>\n                                </div>\n                                <br>\n                                <input #imageUser type=\"hidden\" [value]=\"urlImage | async\">\n                                <button *ngIf=\"urlImage | async; else btnDisabled\" type=\"submit\" class=\"btn btn-lg btn-primary btn-block\">Register</button>\n                                <ng-template #btnDisabled>\n                                    <button type=\"submit\" disabled=true class=\"btn btn-lg btn-secondary btn-block\">Register</button>\n                                </ng-template>\n                                <div class=\"form-group\">\n                                    <a routerLink=\"/user/login\" class=\"form-control login-user\">Do you have account?</a>\n                                </div>\n                                <!-- <div class=\"form-group\">\n                                    <button class=\"btn btn-block btn-social btn-facebook\" (click)=\"onLoginFacebook()\">\n                    <span class=\"fa fa-facebook\"></span>\n                    FACEBOOK\n                  </button>\n                                    <button class=\"btn btn-block btn-social btn-google\" (click)=\"onLoginGoogle()\">\n                    <span class=\"fa fa-google\"></span>\n                    GOOGLE\n                  </button>\n                                </div> -->\n                            </form>\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>"

/***/ }),

/***/ "./src/app/components/users/register/register.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/users/register/register.component.ts ***!
  \*****************************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");






var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(authService, router, storage) {
        this.authService = authService;
        this.router = router;
        this.storage = storage;
        this.email = '';
        this.password = '';
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    // onAddUser() {
    //   this.authService.registerUser(this.email, this.password)
    //     .then((res) => {
    //       this.authService.isAuth().subscribe(user => {
    //         if (user) {
    //           user.updateProfile({
    //             displayName: '',
    //             photoURL: this.inputImageUser.nativeElement.value
    //           }).then(() => {
    //             this.router.navigate(['admin/list-sucursal']);
    //           }).catch((error) => console.log('error', error));
    //         }
    //       });
    //     }).catch(err => console.log('err', err.message));
    // }
    RegisterComponent.prototype.onUpload = function (e) {
        var _this = this;
        // console.log('subir', e.target.files[0]);
        var id = Math.random().toString(36).substring(2);
        var file = e.target.files[0];
        var filePath = "uploads/profile_" + id;
        var ref = this.storage.ref(filePath);
        var task = this.storage.upload(filePath, file);
        this.uploadPercent = task.percentageChanges();
        task.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["finalize"])(function () { return _this.urlImage = ref.getDownloadURL(); })).subscribe();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('imageUser'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], RegisterComponent.prototype, "inputImageUser", void 0);
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/components/users/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/components/users/register/register.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_4__["AngularFireStorage"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/directives/ng-drop-files.directive.ts":
/*!*******************************************************!*\
  !*** ./src/app/directives/ng-drop-files.directive.ts ***!
  \*******************************************************/
/*! exports provided: NgDropFilesDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgDropFilesDirective", function() { return NgDropFilesDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_file_res__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/file-res */ "./src/app/models/file-res.ts");



var NgDropFilesDirective = /** @class */ (function () {
    function NgDropFilesDirective() {
        this.archivos = [];
        this.mouseSobre = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    NgDropFilesDirective.prototype.onDragEnter = function (event) {
        this.mouseSobre.emit(true);
        this._prevenirDetener(event);
    };
    NgDropFilesDirective.prototype.onDragLeave = function (event) {
        this.mouseSobre.emit(false);
    };
    NgDropFilesDirective.prototype.onDrop = function (event) {
        var transferencia = this._getTransferencia(event);
        if (!transferencia) {
            return;
        }
        this._extraerArchivos(transferencia.files);
        this._prevenirDetener(event);
        this.mouseSobre.emit(false);
    };
    NgDropFilesDirective.prototype._getTransferencia = function (event) {
        return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
    };
    NgDropFilesDirective.prototype._extraerArchivos = function (archivosLista) {
        // console.log( archivosLista);
        // tslint:disable-next-line:forin
        for (var propiedad in Object.getOwnPropertyNames(archivosLista)) {
            var archivoTemporal = archivosLista[propiedad];
            if (this._archivoPuedeSerCargado(archivoTemporal)) {
                var nuevoArchivo = new _models_file_res__WEBPACK_IMPORTED_MODULE_2__["FileRes"](archivoTemporal);
                this.archivos.push(nuevoArchivo);
            }
        }
        console.log(this.archivos);
    };
    // Validaciones
    NgDropFilesDirective.prototype._archivoPuedeSerCargado = function (archivo) {
        if (!this._archivoYaDropeado(archivo.name) && this._esImagen(archivo.type)) {
            return true;
        }
        else {
            return false;
        }
    };
    NgDropFilesDirective.prototype._prevenirDetener = function (event) {
        event.preventDefault();
        event.stopPropagation();
    };
    NgDropFilesDirective.prototype._archivoYaDropeado = function (nombreArchivo) {
        for (var _i = 0, _a = this.archivos; _i < _a.length; _i++) {
            var archivo = _a[_i];
            if (archivo.nombreArchivo === nombreArchivo) {
                console.log('El archivo' + nombreArchivo + ' ya esta agregado');
                return true;
            }
        }
        return false;
    };
    NgDropFilesDirective.prototype._esImagen = function (tipoArchivo) {
        return (tipoArchivo === '' || tipoArchivo === undefined) ? false : tipoArchivo.startsWith('image');
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], NgDropFilesDirective.prototype, "archivos", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], NgDropFilesDirective.prototype, "mouseSobre", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('dragover', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NgDropFilesDirective.prototype, "onDragEnter", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('draleave', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NgDropFilesDirective.prototype, "onDragLeave", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('drop', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], NgDropFilesDirective.prototype, "onDrop", null);
    NgDropFilesDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[appNgDropFiles]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NgDropFilesDirective);
    return NgDropFilesDirective;
}());



/***/ }),

/***/ "./src/app/guards/auth.guard.ts":
/*!**************************************!*\
  !*** ./src/app/guards/auth.guard.ts ***!
  \**************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var AuthGuard = /** @class */ (function () {
    function AuthGuard(afsAuth, router) {
        this.afsAuth = afsAuth;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        var _this = this;
        return this.afsAuth.authState
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["take"])(1))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (authState) { return !!authState; }))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (auth) {
            if (!auth) {
                _this.router.navigate(['/user/login']);
            }
        }));
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/material/material.module.ts":
/*!*********************************************!*\
  !*** ./src/app/material/material.module.ts ***!
  \*********************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");




var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDividerModule"],
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDividerModule"],
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/models/file-res.ts":
/*!************************************!*\
  !*** ./src/app/models/file-res.ts ***!
  \************************************/
/*! exports provided: Roles, FileRes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Roles", function() { return Roles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileRes", function() { return FileRes; });
var Roles = /** @class */ (function () {
    function Roles() {
    }
    return Roles;
}());

var FileRes = /** @class */ (function () {
    function FileRes(archivo) {
        this.archivo = archivo;
        this.nombreArchivo = archivo.name;
        this.estaSubiendo = false;
        this.progreso = 0;
    }
    return FileRes;
}());



/***/ }),

/***/ "./src/app/pipes/administrador/filter-administrador.pipe.ts":
/*!******************************************************************!*\
  !*** ./src/app/pipes/administrador/filter-administrador.pipe.ts ***!
  \******************************************************************/
/*! exports provided: FilterAdministradorPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterAdministradorPipe", function() { return FilterAdministradorPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterAdministradorPipe = /** @class */ (function () {
    function FilterAdministradorPipe() {
    }
    FilterAdministradorPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var administrador = value_1[_i];
            if (administrador.username.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(administrador);
            }
        }
        return resultPosts;
    };
    FilterAdministradorPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'filterAdministrador'
        })
    ], FilterAdministradorPipe);
    return FilterAdministradorPipe;
}());



/***/ }),

/***/ "./src/app/pipes/cliente/cliente-filter.pipe.ts":
/*!******************************************************!*\
  !*** ./src/app/pipes/cliente/cliente-filter.pipe.ts ***!
  \******************************************************/
/*! exports provided: ClienteFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClienteFilterPipe", function() { return ClienteFilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ClienteFilterPipe = /** @class */ (function () {
    function ClienteFilterPipe() {
    }
    ClienteFilterPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var cliente = value_1[_i];
            if (cliente.username.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(cliente);
            }
        }
        return resultPosts;
    };
    ClienteFilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'clienteFilter'
        })
    ], ClienteFilterPipe);
    return ClienteFilterPipe;
}());



/***/ }),

/***/ "./src/app/pipes/filter-repartidor.pipe.ts":
/*!*************************************************!*\
  !*** ./src/app/pipes/filter-repartidor.pipe.ts ***!
  \*************************************************/
/*! exports provided: FilterRepartidorPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterRepartidorPipe", function() { return FilterRepartidorPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterRepartidorPipe = /** @class */ (function () {
    function FilterRepartidorPipe() {
    }
    FilterRepartidorPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var repartidor = value_1[_i];
            if (repartidor.username.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(repartidor);
            }
        }
        return resultPosts;
    };
    FilterRepartidorPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'filterRepartidor'
        })
    ], FilterRepartidorPipe);
    return FilterRepartidorPipe;
}());



/***/ }),

/***/ "./src/app/pipes/filter-sucursal.pipe.ts":
/*!***********************************************!*\
  !*** ./src/app/pipes/filter-sucursal.pipe.ts ***!
  \***********************************************/
/*! exports provided: FilterSucursalPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterSucursalPipe", function() { return FilterSucursalPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterSucursalPipe = /** @class */ (function () {
    function FilterSucursalPipe() {
    }
    FilterSucursalPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var sucursal = value_1[_i];
            if (sucursal.estado.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(sucursal);
            }
        }
        return resultPosts;
    };
    FilterSucursalPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'filterSucursal'
        })
    ], FilterSucursalPipe);
    return FilterSucursalPipe;
}());



/***/ }),

/***/ "./src/app/pipes/historial-servicios/servicioh-repartidor.pipe.ts":
/*!************************************************************************!*\
  !*** ./src/app/pipes/historial-servicios/servicioh-repartidor.pipe.ts ***!
  \************************************************************************/
/*! exports provided: ServiciohRepartidorPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciohRepartidorPipe", function() { return ServiciohRepartidorPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ServiciohRepartidorPipe = /** @class */ (function () {
    function ServiciohRepartidorPipe() {
    }
    ServiciohRepartidorPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var repartidor = value_1[_i];
            if (repartidor.username.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(repartidor);
            }
        }
        return resultPosts;
    };
    ServiciohRepartidorPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'serviciohRepartidor'
        })
    ], ServiciohRepartidorPipe);
    return ServiciohRepartidorPipe;
}());



/***/ }),

/***/ "./src/app/pipes/historial-servicios/servicioh-usuario.pipe.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pipes/historial-servicios/servicioh-usuario.pipe.ts ***!
  \*********************************************************************/
/*! exports provided: ServiciohUsuarioPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciohUsuarioPipe", function() { return ServiciohUsuarioPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ServiciohUsuarioPipe = /** @class */ (function () {
    function ServiciohUsuarioPipe() {
    }
    ServiciohUsuarioPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var usuario = value_1[_i];
            if (usuario.username.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(usuario);
            }
        }
        return resultPosts;
    };
    ServiciohUsuarioPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'serviciohUsuario'
        })
    ], ServiciohUsuarioPipe);
    return ServiciohUsuarioPipe;
}());



/***/ }),

/***/ "./src/app/pipes/menu/filter-menu.pipe.ts":
/*!************************************************!*\
  !*** ./src/app/pipes/menu/filter-menu.pipe.ts ***!
  \************************************************/
/*! exports provided: FilterMenuPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterMenuPipe", function() { return FilterMenuPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterMenuPipe = /** @class */ (function () {
    function FilterMenuPipe() {
    }
    FilterMenuPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var menu = value_1[_i];
            if (menu.nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(menu);
            }
        }
        return resultPosts;
    };
    FilterMenuPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'filterMenu'
        })
    ], FilterMenuPipe);
    return FilterMenuPipe;
}());



/***/ }),

/***/ "./src/app/pipes/restaurante/filter-restaurante.pipe.ts":
/*!**************************************************************!*\
  !*** ./src/app/pipes/restaurante/filter-restaurante.pipe.ts ***!
  \**************************************************************/
/*! exports provided: FilterRestaurantePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterRestaurantePipe", function() { return FilterRestaurantePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterRestaurantePipe = /** @class */ (function () {
    function FilterRestaurantePipe() {
    }
    FilterRestaurantePipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var restaurante = value_1[_i];
            if (restaurante.username.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(restaurante);
            }
        }
        return resultPosts;
    };
    FilterRestaurantePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'filterRestaurante'
        })
    ], FilterRestaurantePipe);
    return FilterRestaurantePipe;
}());



/***/ }),

/***/ "./src/app/pipes/restaurante/pedido/pedido.pipe.ts":
/*!*********************************************************!*\
  !*** ./src/app/pipes/restaurante/pedido/pedido.pipe.ts ***!
  \*********************************************************/
/*! exports provided: PedidoPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PedidoPipe", function() { return PedidoPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PedidoPipe = /** @class */ (function () {
    function PedidoPipe() {
    }
    PedidoPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var pedido = value_1[_i];
            if (pedido.clave.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(pedido);
            }
        }
        return resultPosts;
    };
    PedidoPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'pedidoConFilter'
        })
    ], PedidoPipe);
    return PedidoPipe;
}());



/***/ }),

/***/ "./src/app/pipes/servicios/filter-servicios-dia.pipe.ts":
/*!**************************************************************!*\
  !*** ./src/app/pipes/servicios/filter-servicios-dia.pipe.ts ***!
  \**************************************************************/
/*! exports provided: FilterServiciosDiaPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterServiciosDiaPipe", function() { return FilterServiciosDiaPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FilterServiciosDiaPipe = /** @class */ (function () {
    function FilterServiciosDiaPipe() {
    }
    FilterServiciosDiaPipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var pedido = value_1[_i];
            if (pedido.clave.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(pedido);
            }
        }
        return resultPosts;
    };
    FilterServiciosDiaPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'filterServiciosDia'
        })
    ], FilterServiciosDiaPipe);
    return FilterServiciosDiaPipe;
}());



/***/ }),

/***/ "./src/app/pipes/transporte/transporte.pipe.ts":
/*!*****************************************************!*\
  !*** ./src/app/pipes/transporte/transporte.pipe.ts ***!
  \*****************************************************/
/*! exports provided: TransportePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransportePipe", function() { return TransportePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TransportePipe = /** @class */ (function () {
    function TransportePipe() {
    }
    TransportePipe.prototype.transform = function (value, arg) {
        // tslint:disable-next-line:curly
        if (arg === '' || arg.length < 3)
            return value;
        var resultPosts = [];
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var transporte = value_1[_i];
            if (transporte.placa.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
                resultPosts.push(transporte);
            }
        }
        return resultPosts;
    };
    TransportePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'transporte'
        })
    ], TransportePipe);
    return TransportePipe;
}());



/***/ }),

/***/ "./src/app/service/administrador/administrador-data.service.ts":
/*!*********************************************************************!*\
  !*** ./src/app/service/administrador/administrador-data.service.ts ***!
  \*********************************************************************/
/*! exports provided: AdministradorDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdministradorDataService", function() { return AdministradorDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");








var AdministradorDataService = /** @class */ (function () {
    function AdministradorDataService(afsAuth, afs, http, alertService) {
        this.afsAuth = afsAuth;
        this.afs = afs;
        this.http = http;
        this.alertService = alertService;
        this.administradoresCollection = this.afs.collection('users', function (ref) { return ref.where('tipo', '==', 1); });
        this.administradores = this.administradoresCollection.valueChanges();
    }
    AdministradorDataService.prototype.registerUser = function (username, lastname, address, phone, email, pass, uidsucursal) {
        var _this = this;
        // Configuramos una nueva variable de conexión
        var config = {
            apiKey: "AIzaSyAf1DuDZ8suCX2bqoG3QgiiRGAwUBeKPNU",
            authDomain: "lolo-1b8ff.firebaseapp.com",
            databaseURL: "https://lolo-1b8ff.firebaseio.com",
        };
        var secondaryApp = firebase__WEBPACK_IMPORTED_MODULE_5__["initializeApp"](config, 'Secondary');
        // Registramos al ADMIN usando la nueva variable de conexión
        return new Promise(function (resolve, reject) {
            console.log('pass', email, pass);
            secondaryApp.auth().createUserWithEmailAndPassword(email, pass)
                .then(function (userData) {
                resolve(userData),
                    _this.updateUserData(userData.user, username, lastname, address, phone, uidsucursal);
                _this.alertService.success('Se agrego correctamente el convenio');
            }).catch(function (err) { return _this.alertService.danger(err); });
        });
    };
    AdministradorDataService.prototype.updateUserData = function (user, username, lastname, address, phone, uidSucursal) {
        var userRef = this.afs.doc("users/" + user.uid);
        var data = {
            username: username,
            lastname: lastname,
            address: address,
            phone: phone,
            activo: true,
            email: user.email,
            geolocalizacion: {},
            // photoUrl: user.photoURL,
            roles: {
                administrador: true
            },
            tipo: 1,
            uid: user.uid,
            uidSucursal: uidSucursal
        };
        console.log('DATAadmin', data);
        return userRef.set(data, { merge: true });
    };
    AdministradorDataService.prototype.isAuth = function () {
        return this.afsAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (auth) { return auth; }));
    };
    AdministradorDataService.prototype.getAllAdministradores = function () {
        return this.administradores = this.administradoresCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    AdministradorDataService.prototype.deleteAdmin = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/delete';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('El usuario se elimino de SDK');
        });
        this.administradorDoc = this.afs.doc("users/" + uid);
        this.administradorDoc.delete();
        // this.alertService.info('El usuario se elimino correctamente');
    };
    AdministradorDataService.prototype.isUserAdministrador = function (userUid) {
        return this.afs.doc("users/" + userUid).valueChanges();
    };
    AdministradorDataService.prototype.desactivarAdmin = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/inhabilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('Se deshabilito del SDK');
        });
        // const idRepartidor = repartidor.uid;
        this.afs.collection('users').doc(uid).update({
            activo: false
        });
        // this.alertService.info('El usuario se inhabilito correctamente');
    };
    AdministradorDataService.prototype.activarAdmin = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/habilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('Se habilito del SDK');
        });
        // const idRepartidor = repartidor.uid;
        this.afs.collection('users').doc(uid).update({
            activo: true
        });
        // this.alertService.info('El usuario se habilito correctamente');
    };
    AdministradorDataService.prototype.getAdmins = function (uidSucursal) {
        return this.afs.collection('users', function (ref) {
            return ref.where('tipo', '==', 1).where('uidSucursal', '==', uidSucursal);
        }).valueChanges();
    };
    AdministradorDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            _angular_http__WEBPACK_IMPORTED_MODULE_7__["Http"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_6__["AlertService"]])
    ], AdministradorDataService);
    return AdministradorDataService;
}());



/***/ }),

/***/ "./src/app/service/auth.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/auth.service.ts ***!
  \*****************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");








var AuthService = /** @class */ (function () {
    function AuthService(afsAuth, afs, alertService, http) {
        this.afsAuth = afsAuth;
        this.afs = afs;
        this.alertService = alertService;
        this.http = http;
        this.data = {};
    }
    AuthService.prototype.newRegister = function (username, lastname, address, phone, marca, placa, email, pass, uidSucursal, img) {
        var _this = this;
        // Configuramos una nueva variable de conexión
        var config = {
            apiKey: "AIzaSyAf1DuDZ8suCX2bqoG3QgiiRGAwUBeKPNU",
            authDomain: "lolo-1b8ff.firebaseapp.com",
            databaseURL: "https://lolo-1b8ff.firebaseio.com",
        };
        var secondaryApp = firebase__WEBPACK_IMPORTED_MODULE_5__["initializeApp"](config, "Secondary");
        // Registramos al repartidor usando la nueva variable de conexión
        return new Promise(function (resolve, reject) {
            secondaryApp
                .auth()
                .createUserWithEmailAndPassword(email, pass)
                .then(function (userData) {
                resolve(userData),
                    _this.updateUserData(userData.user, username, lastname, address, phone, marca, placa, uidSucursal, img);
                _this.alertService.success("Se agrego correctamente el repartidor");
            })
                .catch(function (err) { return _this.alertService.danger(err); });
        });
    };
    // registerUser(username: string, lastname: string, address: string, phone: string,
    //   email: string, pass: string, uidSucursal: string, img: string) {
    //   return new Promise((resolve, reject) => {
    //     this.afsAuth.auth.createUserWithEmailAndPassword(email, pass)
    //       .then(userData => {
    //         resolve(userData),
    //           this.updateUserData(userData.user, username, lastname, address, phone, uidSucursal, img)
    //                 }).catch(err => console.log(reject(err)))
    //                   });
    // }
    AuthService.prototype.loginEmailUser = function (email, pass) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.afsAuth.auth.signInWithEmailAndPassword(email, pass).then(function (userData) { return resolve(userData); }, function (err) { return reject(err); });
        });
    };
    AuthService.prototype.logOut = function () {
        this.afsAuth.auth.signOut();
    };
    AuthService.prototype.isAuth = function () {
        return this.afsAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (auth) { return auth; }));
    };
    AuthService.prototype.updateUserData = function (user, username, lastname, address, phone, marca, placa, uidSucursal, img) {
        var userRef = this.afs.doc("users/" + user.uid);
        var data = {
            username: username,
            lastname: lastname,
            address: address,
            phone: phone,
            marca: marca,
            placa: placa,
            activo: true,
            email: user.email,
            uidSucursal: uidSucursal,
            roles: {
                repartidor: true,
            },
            tipo: 2,
            uid: user.uid,
            geolocalizacion: {},
            photourl: img,
        };
        return userRef.set(data, { merge: true });
    };
    AuthService.prototype.isUserAdmin = function (userUid) {
        return this.afs.doc("users/" + userUid).valueChanges();
    };
    AuthService.prototype.habil = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({
            "Content-Type": "application/json",
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        var url = "https://sdk-admin-bme.firebaseapp.com/habilitar";
        var data1 = JSON.stringify({
            uid: uid,
        });
        this.http.post(url, data1, options).subscribe(function (res) {
            console.log("Se habilito del SDK");
        });
        var userRef = this.afs.doc("users/" + uid);
        var data = {
            activo: true,
            pago: "Habil",
            roles: {
                restaurante: true,
            },
        };
        this.alertService.info("El convenio se habilito correctamente");
        return userRef.set(data, { merge: true });
    };
    AuthService.prototype.vencido = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({
            "Content-Type": "application/json",
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        var url = "https://sdk-admin-bme.firebaseapp.com/inhabilitar";
        var data1 = JSON.stringify({
            uid: uid,
        });
        this.http.post(url, data1, options).subscribe(function (res) {
            console.log("Se deshabilito del SDK");
        });
        var userRef = this.afs.doc("users/" + uid);
        var data = {
            activo: false,
            pago: "Vencido",
            roles: {
                restaurante: true,
            },
        };
        this.alertService.info("El convenio se inhabilito correctamente");
        return userRef.set(data, { merge: true });
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root",
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_6__["AlertService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_7__["Http"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/service/categoria/categoria-convenio.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/service/categoria/categoria-convenio.service.ts ***!
  \*****************************************************************/
/*! exports provided: CategoriaConvenioService, Categorias */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriaConvenioService", function() { return CategoriaConvenioService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Categorias", function() { return Categorias; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");




var CategoriaConvenioService = /** @class */ (function () {
    function CategoriaConvenioService(afs, alertService) {
        this.afs = afs;
        this.alertService = alertService;
        this.selectedCategoria = { id: null };
        this.data = {};
        this.categoriasCollection = afs.collection('categoria_critico');
        this.categorias = this.categoriasCollection.valueChanges();
    }
    CategoriaConvenioService.prototype.guardarCategoria = function (img, nombre, posicion, descripcion, uidSucursal) {
        var _this = this;
        if (img === '' || nombre === '' || posicion === '' || descripcion === '' || !img) {
            this.alertService.warning('No se guardo, algunos campos no fueron completados');
        }
        else {
            this.afs.collection('categoria_critico').add({
                nombre: nombre,
                posicion: posicion,
                descripcion: descripcion,
                uidSucursal: uidSucursal,
                imagen: img
            }).then(function (ref) {
                console.log('id', ref.id);
                _this.afs.collection('categoria_critico').doc(ref.id).update({
                    id: ref.id
                });
            });
            this.alertService.success('Se agrego la categoria');
        }
    };
    CategoriaConvenioService.prototype.editarCategoria = function (img, categoria) {
        var id = categoria.id;
        console.log('uid menu', categoria.id);
        // Cuando no existe una imagen
        if (!img) {
            this.afs.collection('categoria_critico').doc(id).update({
                nombre: categoria.nombre,
                posicion: categoria.posicion,
                descripcion: categoria.descripcion
            });
            this.alertService.info('Se edito el categoría correctamente');
            // Cuando existe una imagen
        }
        else {
            this.afs.collection('categoria_critico').doc(id).update({
                nombre: categoria.nombre,
                posicion: categoria.posicion,
                descripcion: categoria.descripcion,
                imagen: img
            });
            this.alertService.info('Se edito la categoría correctamente');
        }
    };
    CategoriaConvenioService.prototype.eliminar = function (id) {
        this.afs.collection('categoria_critico').doc(id).delete();
        this.alertService.info('Se elimino la categoría correctamente');
    };
    CategoriaConvenioService.prototype.getCategorias = function (uidSucursal) {
        return this.afs.collection('categoria_critico', function (ref) {
            return ref.where('uidSucursal', '==', uidSucursal).orderBy('posicion', 'asc');
        }).valueChanges();
    };
    CategoriaConvenioService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_3__["AlertService"]])
    ], CategoriaConvenioService);
    return CategoriaConvenioService;
}());

var Categorias = /** @class */ (function () {
    function Categorias() {
    }
    return Categorias;
}());



/***/ }),

/***/ "./src/app/service/cliente/cliente.service.ts":
/*!****************************************************!*\
  !*** ./src/app/service/cliente/cliente.service.ts ***!
  \****************************************************/
/*! exports provided: ClienteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClienteService", function() { return ClienteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");






var ClienteService = /** @class */ (function () {
    function ClienteService(afs, alertService, http) {
        this.afs = afs;
        this.alertService = alertService;
        this.http = http;
        this.clientesCollection = this.afs.collection('users', function (ref) { return ref.where('tipo', '==', 4); });
        // this.clientesPerfil = this.afs.collection<UserInterface>(`users/${perfil}`);
        this.clientes = this.clientesCollection.valueChanges();
    }
    ClienteService.prototype.getAllClientes = function () {
        return this.clientes = this.clientesCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    ClienteService.prototype.deleteClientes = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["RequestOptions"]({ headers: headers });
        var url = 'https://bringme-a412b.firebaseapp.com/eliminar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('El usuario se elimino de SDK');
        });
        this.clienteDoc = this.afs.doc("users/" + uid);
        this.clienteDoc.delete();
        this.alertService.info('El usuario se elimino correctamente');
    };
    ClienteService.prototype.desactivarCliente = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/inhabilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('Se deshabilito del SDK');
        });
        // const idRepartidor = repartidor.uid;
        this.afs.collection('users').doc(uid).update({
            activo: false
        });
        this.alertService.info('El usuario se inhabilito correctamente');
    };
    ClienteService.prototype.activarConvenio = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/habilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('Se habilito del SDK');
        });
        // const idRepartidor = repartidor.uid;
        this.afs.collection('users').doc(uid).update({
            activo: true
        });
        this.alertService.info('El usuario se habilito correctamente');
    };
    ClienteService.prototype.getClientes = function (uidSucursal) {
        return this.afs.collection('users', function (ref) {
            return ref.where('tipo', '==', 4).where('uidSucursal', '==', uidSucursal);
        }).valueChanges();
    };
    ClienteService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"], ngx_alerts__WEBPACK_IMPORTED_MODULE_4__["AlertService"], _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"]])
    ], ClienteService);
    return ClienteService;
}());



/***/ }),

/***/ "./src/app/service/config/banner_config/banner-config.service.ts":
/*!***********************************************************************!*\
  !*** ./src/app/service/config/banner_config/banner-config.service.ts ***!
  \***********************************************************************/
/*! exports provided: BannerConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BannerConfigService", function() { return BannerConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");




var BannerConfigService = /** @class */ (function () {
    function BannerConfigService(alertService, afs) {
        this.alertService = alertService;
        this.afs = afs;
        this.CARPETA_IMAGENES = 'banners';
        this.data = {};
    }
    // cargarImagenesFirebase( imagenes: any[], descripcion, fecha, estado, modulo, uidSucursal) {
    //   descripcion = descripcion;
    //   fecha = fecha;
    //   estado = estado;
    //   modulo = modulo;
    //   uidSucursal = uidSucursal;
    //   const storageRef = firebase.storage().ref();
    //   for ( const item of imagenes ) {
    //     item.estaSubiendo = true;
    //     if (item.progreso >= 100 ) {
    //       continue;
    //     }
    //     const uploadTask: firebase.storage.UploadTask =
    //       storageRef.child(`${ this.CARPETA_IMAGENES }/${ item.nombreArchivo}`)
    //         .put( item.archivo);
    //         uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
    //           (snapshot: firebase.storage.UploadTaskSnapshot) =>
    //            item.progreso = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
    //           (error) => console.error('Error al subir', error),
    //           () => {
    //            this.alertService.success('Bien! Se agrego el banner.');
    //            uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
    //            item.photourl = downloadURL;
    //            item.estaSubiendo = false;
    //           this.guardarImagen({
    //             descripcion: descripcion,
    //             fecha: fecha,
    //             estado: estado,
    //             modulo: modulo,
    //             uidSucursal: uidSucursal,
    //             photourl: item.photourl
    //           });
    //           });
    //           });
    //   }
    // }
    BannerConfigService.prototype.guardarImagen = function (photourl, descripcion, fecha, estado, modulo, uidSucursal, url) {
        var _this = this;
        this.afs.collection("/" + this.CARPETA_IMAGENES).add({
            descripcion: descripcion,
            fecha: fecha,
            estado: estado,
            modulo: modulo,
            uidSucursal: uidSucursal,
            photourl: photourl,
            url: url
        }).then(function (ref) {
            console.log('id', _this.uid = ref.id);
            _this.afs.collection('banners').doc(_this.uid).update({
                uid: _this.uid
            });
        });
        this.alertService.success('Se agrego correctamente el banner.');
    };
    // Delete
    BannerConfigService.prototype.deleteMenu = function (idB) {
        this.bannerDoc = this.afs.doc("banners/" + idB);
        this.bannerDoc.delete();
    };
    // Update
    BannerConfigService.prototype.updateEstadoBanner = function (uidB, estado) {
        this.data = {
            estado: estado
        };
        this.bannerDoc = this.afs.doc("banners/" + uidB);
        this.bannerDoc.update(this.data);
        console.log('update', this.data);
    };
    BannerConfigService.prototype.getBanner = function (uidSucursal) {
        return this.afs.collection('banners', function (ref) {
            return ref.where('uidSucursal', '==', uidSucursal);
        }).valueChanges();
    };
    BannerConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_alerts__WEBPACK_IMPORTED_MODULE_2__["AlertService"], _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]])
    ], BannerConfigService);
    return BannerConfigService;
}());



/***/ }),

/***/ "./src/app/service/config/servicios_config/servicios-config.service.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/service/config/servicios_config/servicios-config.service.ts ***!
  \*****************************************************************************/
/*! exports provided: ServiciosConfigService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosConfigService", function() { return ServiciosConfigService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");





var ServiciosConfigService = /** @class */ (function () {
    function ServiciosConfigService(afs, alert) {
        this.afs = afs;
        this.alert = alert;
    }
    // Servicio regular
    ServiciosConfigService.prototype.getCostoFijo = function (id) {
        this.costo_fijo = this.afs.collection('costos_fijos', function (ref) { return ref.where('uidSucursal', '==', id); });
        this.costo = this.costo_fijo.valueChanges();
        return this.costo = this.costo_fijo.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    //  Servicio Menu
    ServiciosConfigService.prototype.getCostoFijoMenu = function (id) {
        console.log('uidsucursal', id);
        this.costo_fijo_menu = this.afs.collection('costos_fijo_menu', function (ref) { return ref.where('uidSucursal', '==', id); });
        this.costo_menu = this.costo_fijo_menu.valueChanges();
        return this.costo_menu = this.costo_fijo_menu.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    // Servicios regulares
    ServiciosConfigService.prototype.updateArranqueService = function (costo, uid) {
        console.log('costoForm', costo.arranque);
        console.log('costoFormid', uid);
        this.afs.doc("costos_fijos/" + uid).update({
            arranque: costo.arranque
        });
        this.alert.success('Tarifa actualizada');
    };
    // Servicios menu
    ServiciosConfigService.prototype.updateArranqueServiceMenu = function (costo, uid) {
        console.log('costoForm', costo.arranque);
        console.log('costoFormid', uid);
        this.afs.doc("costos_fijo_menu/" + uid).update({
            arranque: costo.arranque
        });
        this.alert.success('Tarifa actualizada');
    };
    ServiciosConfigService.prototype.getTarifaService = function (id) {
        this.tarifaDoc = this.afs.collection('tarifas', function (ref) { return ref.where('uidSucursal', '==', id); });
        this.tarifa = this.tarifaDoc.valueChanges();
        return this.tarifa = this.tarifaDoc.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    ServiciosConfigService.prototype.updateTarifaService = function (tarifa, uid) {
        console.log('heyy', tarifa.distancia, tarifa.tiempo);
        this.afs.doc("tarifas/" + uid).update({
            distancia: tarifa.distancia,
            tiempo: tarifa.tiempo
        });
        this.alert.success('Tarifas actualizadas');
    };
    ServiciosConfigService.prototype.getPagoService = function (id) {
        this.pagoDoc = this.afs.collection('metodos_pagos', function (ref) { return ref.where('uidSucursal', '==', id); });
        this.pago = this.pagoDoc.valueChanges();
        return this.pago = this.pagoDoc.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    ServiciosConfigService.prototype.updatePagoService = function (efectivo, tarjeta, uid) {
        console.log('metodos', efectivo, tarjeta, uid);
        this.afs.doc("metodos_pagos/" + uid).update({
            Efectivo: efectivo,
            Tarjeta: tarjeta
        });
        this.alert.success('Se actualizo correctamente');
    };
    ServiciosConfigService.prototype.getTransporteService = function (id) {
        this.transporteDoc = this.afs.collection('transportes_config', function (ref) { return ref.where('uidSucursal', '==', id); });
        this.transporte = this.transporteDoc.valueChanges();
        return this.transporte = this.transporteDoc.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    ServiciosConfigService.prototype.updateTransporteService = function (moto, car, uid) {
        this.afs.doc("transportes_config/" + uid).update({
            moto: moto,
            car: car
        });
        this.alert.success('Se actualizo correctamente');
    };
    // Menu
    ServiciosConfigService.prototype.getTarifaServiceMenu = function (id) {
        this.tarifaMenuDoc = this.afs.collection('tarifas_menu', function (ref) { return ref.where('uidSucursal', '==', id); });
        this.tarifaMenu = this.tarifaMenuDoc.valueChanges();
        return this.tarifaMenu = this.tarifaMenuDoc.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    ServiciosConfigService.prototype.updateTarifaServiceMenu = function (tarifa, uid) {
        console.log('heyy', tarifa.distancia, tarifa.tiempo);
        this.afs.doc("tarifas_menu/" + uid).update({
            distancia: tarifa.distancia,
            tiempo: tarifa.tiempo
        });
        this.alert.success('Tarifas actualizadas');
    };
    // MenuMin
    ServiciosConfigService.prototype.getTarifaServiceMenuMin = function (id) {
        console.log('uid sucursa', id);
        this.tarifaMenuMinDoc = this.afs.collection('tarifas_menu_min', function (ref) { return ref.where('uidSucursal', '==', id); });
        this.tarifaMenuMin = this.tarifaMenuMinDoc.valueChanges();
        return this.tarifaMenuMin = this.tarifaMenuMinDoc.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    // Servicios menu
    ServiciosConfigService.prototype.updateArranqueServiceMenuMin = function (costo, uid) {
        console.log('costoFormid', uid);
        this.afs.doc("tarifas_menu_min/" + uid).update({
            costo: costo.costo
        });
        this.alert.success('Tarifa actualizada');
    };
    ServiciosConfigService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_4__["AlertService"]])
    ], ServiciosConfigService);
    return ServiciosConfigService;
}());



/***/ }),

/***/ "./src/app/service/corte/cortes.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/corte/cortes.service.ts ***!
  \*************************************************/
/*! exports provided: CortesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CortesService", function() { return CortesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var CortesService = /** @class */ (function () {
    function CortesService(afs) {
        this.afs = afs;
        this.uidConvenio = null;
        this.repartidoresCollection = afs.collection('users', function (ref) { return ref.where('tipo', '==', 2); });
        this.repartidores = this.repartidoresCollection.valueChanges();
        // Convenio
        this.convenioCollection = afs.collection('users', function (ref) { return ref.where('tipo', '==', 3); });
        this.convenios = this.convenioCollection.valueChanges();
        // Cortes
        this.cortesCollection = afs.collection('cortes');
        this.cortes = this.cortesCollection.valueChanges();
    }
    CortesService.prototype.getAllRepartidores = function () {
        return this.repartidores = this.repartidoresCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    CortesService.prototype.getAllConvenios = function () {
        return this.convenios = this.convenioCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    CortesService.prototype.getAllCortes = function () {
        return this.cortes = this.cortesCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    // Corte convenio
    CortesService.prototype.getCorteUidConvenio = function (uid) {
        this.uidConvenio = uid;
        console.log('uidConevenio', this.uidConvenio);
    };
    CortesService.prototype.getAllCortesConvenio = function (date1, date2) {
        var _this = this;
        this.getListConvenio();
        console.log('date1', date1);
        console.log('date2', date2);
        console.log('uidCorte', this.uidConvenio);
        this.convenioCorteCollection = this.afs.collection('servicios', function (ref) { return ref.orderBy('fecha', 'asc')
            .startAt(date1).endAt(date2).where('estatus', '==', 'Terminado')
            .where('uidRestaurante', '==', _this.uidConvenio); });
        this.conveniosCortes = this.convenioCorteCollection.valueChanges();
        return this.conveniosCortes = this.convenioCorteCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    CortesService.prototype.getListConvenio = function () {
        var _this = this;
        console.log('uidList', this.uidConvenio);
        this.convenioUserCollection = this.afs.collection('users', function (ref) { return ref.where('uid', '==', _this.uidConvenio); });
        this.conveniosUser = this.convenioUserCollection.valueChanges();
        return this.conveniosCortes = this.convenioUserCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        }));
    };
    CortesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], CortesService);
    return CortesService;
}());



/***/ }),

/***/ "./src/app/service/data-api.service.ts":
/*!*********************************************!*\
  !*** ./src/app/service/data-api.service.ts ***!
  \*********************************************/
/*! exports provided: DataApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataApiService", function() { return DataApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var DataApiService = /** @class */ (function () {
    function DataApiService(afs) {
        this.afs = afs;
        //
        this.selectedSucursal = {
            id: null
        };
        this.sucursalesCollection = afs.collection('sucursales');
        this.sucursales = this.sucursalesCollection.valueChanges();
    }
    DataApiService.prototype.getAllSucursales = function () {
        return this.sucursales = this.sucursalesCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            // recuperando id de la collección
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        }));
    };
    DataApiService.prototype.getOneSucursal = function (idSucursal) {
        this.sucursalDoc = this.afs.doc("sucursales/" + idSucursal);
        return this.sucursal = this.sucursalDoc.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.id = action.payload.id;
                return data;
            }
        }));
    };
    DataApiService.prototype.addSucursal = function (sucursal) {
        this.sucursalesCollection.add(sucursal);
    };
    DataApiService.prototype.updateSucursal = function (sucursal) {
        var idSucursal = sucursal.id;
        this.sucursalDoc = this.afs.doc("sucursales/" + idSucursal);
        this.sucursalDoc.update(sucursal);
    };
    DataApiService.prototype.deleteSucursal = function (idSucursal) {
        this.sucursalDoc = this.afs.doc("sucursales/" + idSucursal);
        this.sucursalDoc.delete();
    };
    DataApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], DataApiService);
    return DataApiService;
}());



/***/ }),

/***/ "./src/app/service/pedidos/pedidos.service.ts":
/*!****************************************************!*\
  !*** ./src/app/service/pedidos/pedidos.service.ts ***!
  \****************************************************/
/*! exports provided: PedidosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PedidosService", function() { return PedidosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var PedidosService = /** @class */ (function () {
    function PedidosService(afs) {
        this.afs = afs;
        this.pedidosCollection = afs.collection('servicios', function (ref) {
            return ref.where('estatus', '<', 'Terminado').where('estatus', '<', 'Terminado');
        });
        this.pedidos = this.pedidosCollection.valueChanges();
        // Historial pagados
        this.historialsCollectionPagados = afs.collection('servicios', function (ref) { return ref.where('estatus', '==', 'Terminado'); });
        this.historialsPagados = this.pedidosCollection.valueChanges();
        // Historial cancelados
        this.historialsCollectionCancelados = afs.collection('servicios', function (ref) { return ref.where('estatus', '==', 'Cancelado'); });
        this.historialsCancelados = this.pedidosCollection.valueChanges();
        // Clientes
        this.usuariosCollection = afs.collection('users');
        // Pedidos
        this.serviciosCollection = afs.collection('pedidos');
        // Menu
        this.menusCollection = afs.collection('menus');
    }
    // Historial de servicios pagados
    PedidosService.prototype.getAllHistorial = function () {
        return (this.historialsPagados = this.historialsCollectionPagados
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    // Historial de servicios cancelados
    PedidosService.prototype.getAllHistorialCancel = function () {
        return (this.historialsCancelados = this.historialsCollectionCancelados
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService.prototype.getAllPedidos = function () {
        return (this.pedidos = this.pedidosCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService.prototype.getAllUsuarios = function () {
        return (this.usuarios = this.usuariosCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService.prototype.getAllMenu = function () {
        return (this.menus = this.menusCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService.prototype.getOneServicio = function (idServicio) {
        this.pedidoDoc = this.afs.doc("servicios/" + idServicio);
        // this.pedidoDoc = this.afs.collection<Servicios>('servicios').doc(`/${idPedido}`).collection<Pedidos>('pedidos');
        return (this.pedido = this.pedidoDoc.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (action) {
            if (action.payload.exists === false) {
                return null;
            }
            else {
                var data = action.payload.data();
                data.uid = action.payload.id;
                return data;
            }
        })));
    };
    PedidosService.prototype.getAllServiciosP = function (idx) {
        this.serviciosP = this.afs.collection('rutas', function (ref) {
            return ref.where('idServicio', '==', idx).where('estatus', '==', 1).orderBy('fecha', 'asc');
        });
        this._serviciosP = this.serviciosP.valueChanges();
        return (this._serviciosP = this.serviciosP.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    // Update de consultas
    PedidosService.prototype.getPedidos = function (Id) {
        this.pedidoDoc2 = this.afs.collection('pedidos', function (ref) {
            return ref.where('servicioID', '==', Id);
        });
        this.pedido2 = this.pedidoDoc2.valueChanges();
        return (this.pedido2 = this.pedidoDoc2.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService.prototype.getProducto = function (Id) {
        this.productoCollection = this.afs.collection('productos', function (ref) {
            return ref.where('servicioID', '==', Id);
        });
        this.prodcuto = this.productoCollection.valueChanges();
        return (this.prodcuto = this.productoCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService.prototype.getCliente = function (Id) {
        this.clienteCollection = this.afs.collection('users', function (ref) {
            return ref.where('uid', '==', Id);
        });
        this.cliente = this.clienteCollection.valueChanges();
        return (this.cliente = this.clienteCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService.prototype.getRepartidor = function (Id) {
        this.repartidorCollection = this.afs.collection('users', function (ref) {
            return ref.where('uid', '==', Id);
        });
        this.repartidor = this.repartidorCollection.valueChanges();
        return (this.repartidor = this.repartidorCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService.prototype.getConvenio = function (Id) {
        this.convenioCollection = this.afs.collection('users', function (ref) {
            return ref.where('uid', '==', Id);
        });
        this.convenio = this.convenioCollection.valueChanges();
        return (this.convenio = this.convenioCollection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.$key = action.payload.doc.id;
                return data;
            });
        })));
    };
    PedidosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], PedidosService);
    return PedidosService;
}());



/***/ }),

/***/ "./src/app/service/repartidor/repartidor-data.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/service/repartidor/repartidor-data.service.ts ***!
  \***************************************************************/
/*! exports provided: RepartidorDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RepartidorDataService", function() { return RepartidorDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");






var RepartidorDataService = /** @class */ (function () {
    function RepartidorDataService(afs, http, alertService) {
        this.afs = afs;
        this.http = http;
        this.alertService = alertService;
        // public credentials: UserInterface;
        this.activo = 'false';
        this.repartidoresCollection = this.afs.collection('users', function (ref) { return ref.where('tipo', '==', 2); });
        this.repartidores = this.repartidoresCollection.valueChanges();
    }
    RepartidorDataService.prototype.getAllRepartidores = function (uidSucursal) {
        return (this.repartidores = this.repartidoresCollection
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            // Recuperando el id
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    RepartidorDataService.prototype.updateRepartidor = function (user, username, lastname, address, phone, marca, placa, uidSucursal, img) {
        var _this = this;
        var userRef = this.afs.doc("users/" + user);
        var data = {
            username: username,
            lastname: lastname,
            address: address,
            phone: phone,
            marca: marca,
            placa: placa,
            activo: true,
            uidSucursal: uidSucursal,
            roles: {
                repartidor: true
            },
            tipo: 2,
            uid: user,
            geolocalizacion: {},
            photourl: img
        };
        return userRef
            .update(data)
            .then(function () {
            return _this.alertService.success('Repartidor actualizado correctamente.');
        });
    };
    RepartidorDataService.prototype.deleteRepartidor = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/delete';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('El usuario se elimino de SDK');
        });
        // Eliminamos el usuario de la base de datos
        this.repartidorDoc = this.afs.doc("users/" + uid);
        this.repartidorDoc.delete();
        // this.alertService.info('El usuario se elimino correctamente');
    };
    RepartidorDataService.prototype.desactivarRepartidor = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/inhabilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('Se deshabilito del SDK');
        });
        // const idRepartidor = repartidor.uid;
        this.afs
            .collection('users')
            .doc(uid)
            .update({
            activo: false
        });
        this.alertService.info('El usuario se inhabilito correctamente');
    };
    RepartidorDataService.prototype.activarRepartidor = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/habilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('Se habilito del SDK');
        });
        // const idRepartidor = repartidor.uid;
        this.afs
            .collection('users')
            .doc(uid)
            .update({
            activo: true
        });
        this.alertService.info('El usuario se habilito correctamente');
    };
    RepartidorDataService.prototype.getRepartidores = function (uidSucursal) {
        return this.afs.collection('users', function (ref) {
            return ref.where('tipo', '==', 2).where('uidSucursal', '==', uidSucursal);
        }).valueChanges();
    };
    RepartidorDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_5__["AlertService"]])
    ], RepartidorDataService);
    return RepartidorDataService;
}());



/***/ }),

/***/ "./src/app/service/restaurante-menu/menu.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/service/restaurante-menu/menu.service.ts ***!
  \**********************************************************/
/*! exports provided: MenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuService", function() { return MenuService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");





var MenuService = /** @class */ (function () {
    function MenuService(afs, alertService) {
        this.afs = afs;
        this.alertService = alertService;
        this.selectedMenu = {};
        this.data = {};
        this.menusCollection = afs.collection('menus');
        this.menus = this.menusCollection.valueChanges();
    }
    // Get
    MenuService.prototype.getAllMenus = function () {
        return this.menus = this.menusCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                return data;
            });
        }));
    };
    MenuService.prototype.guardarMenu = function (img, nombre, estado, precio, descripcion, userUid) {
        if (img === '' || nombre === '' || estado === '' || precio === '' || descripcion === '' || !img) {
            this.alertService.warning('No se guardo, algunos campos no fueron completados');
        }
        else {
            this.afs.collection('menus').add({
                nombre: nombre,
                estado: estado,
                precio: precio,
                descripcion: descripcion,
                userUid: userUid,
                photourl: img,
                time: Date.now(),
                fecha: Date.now().toString()
            });
            this.alertService.success('Se agrego el producto.');
        }
    };
    MenuService.prototype.editarMenu = function (img, menu) {
        var idMenu = menu.id;
        console.log('uid menu', idMenu);
        // Cuando no existe una imagen
        if (!img) {
            this.afs.collection('menus').doc(idMenu).update({
                nombre: menu.nombre,
                estado: menu.estado,
                precio: menu.precio,
                descripcion: menu.descripcion
            });
            this.alertService.info('Se edito el producto correctamente');
            // Cuando existe una imagen
        }
        else {
            this.afs.collection('menus').doc(idMenu).update({
                nombre: menu.nombre,
                estado: menu.estado,
                precio: menu.precio,
                descripcion: menu.descripcion,
                photourl: img
            });
            this.alertService.info('Se edito el producto correctamente');
        }
    };
    // Delete
    MenuService.prototype.deleteMenu = function (idMenu) {
        this.menuDoc = this.afs.doc("menus/" + idMenu);
        this.menuDoc.delete();
        // this.alertService.info('Producto eliminado.');
    };
    MenuService.prototype.updateMenuDesactivar = function (uidMenu) {
        this.data = {
            estado: '2'
        };
        this.menuDoc = this.afs.doc("menus/" + uidMenu);
        this.menuDoc.update(this.data);
        console.log('update', this.data);
        // this.alertService.info('Producto desactivado');
    };
    MenuService.prototype.updateMenuActivar = function (uidMenu) {
        this.data = {
            estado: '1'
        };
        this.menuDoc = this.afs.doc("menus/" + uidMenu);
        this.menuDoc.update(this.data);
        console.log('update', this.data);
        // this.alertService.info('Producto activado.');
    };
    MenuService.prototype.getMenu = function (idUser) {
        return this.afs.collection('menus', function (ref) {
            return ref.where('userUid', '==', idUser);
        }).valueChanges();
    };
    MenuService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"], ngx_alerts__WEBPACK_IMPORTED_MODULE_4__["AlertService"]])
    ], MenuService);
    return MenuService;
}());



/***/ }),

/***/ "./src/app/service/restaurante/restaurante-data.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/service/restaurante/restaurante-data.service.ts ***!
  \*****************************************************************/
/*! exports provided: RestauranteDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RestauranteDataService", function() { return RestauranteDataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_alerts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-alerts */ "./node_modules/ngx-alerts/fesm5/ngx-alerts.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");








var RestauranteDataService = /** @class */ (function () {
    function RestauranteDataService(afsAuth, afs, alertService, http) {
        this.afsAuth = afsAuth;
        this.afs = afs;
        this.alertService = alertService;
        this.http = http;
        this.CARPETA_IMAGENES = 'users';
        this.restaurantesCollection = this.afs.collection('users', function (ref) { return ref.where('tipo', '==', 3); });
        this.restaurantes = this.restaurantesCollection.valueChanges();
    }
    // cargarImagenesFirebase( imagenes: FileRes[], email: string, password: string, username: string,
    //   contacto: string, address: string, phone: string, direccion, uidSucursal: string ) {
    //   // console.log( imagenes );
    //   email = email;
    //   password = password;
    //   username = username;
    //   contacto = contacto;
    //   address = address;
    //   phone = phone;
    //   direccion = direccion;
    //   uidSucursal = uidSucursal;
    //   const storageRef = firebase.storage().ref();
    //   for ( const item of imagenes ) {
    //     item.estaSubiendo = true;
    //     if (item.progreso >= 100 ) {
    //       continue;
    //     }
    //     const uploadTask: firebase.storage.UploadTask =
    //       storageRef.child(`${ this.CARPETA_IMAGENES }/${ item.nombreArchivo}`)
    //         .put( item.archivo);
    //         uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
    //           (snapshot: firebase.storage.UploadTaskSnapshot) =>
    //            item.progreso = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
    //           (error) => this.alertService.warning('Error al subir imagen.'),
    //           () => {
    //            this.alertService.success('Convenio guardado.');
    //            uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
    //            item.photourl = downloadURL;
    //            item.estaSubiendo = false;
    //           this.guardarImagen({
    //            nombre: item.nombreArchivo,
    //            photourl: item.photourl,
    //            email: email,
    //            password: password,
    //            username: username,
    //            contacto: contacto,
    //            address: address,
    //            phone: phone,
    //            roles: { restaurante: true },
    //            tipo: 3,
    //            activo: true,
    //            direccion: direccion,
    //            pago: 'Habil',
    //            uidSucursal: uidSucursal
    //            // uid: this.uid
    //           });
    //           });
    //           });
    //   }
    // }
    //  guardarImagen(  data: { nombre: string, photourl: string, email: string, password: string,
    //    username: string, contacto: string, address: string, phone: string, roles: { }, tipo: number,
    //    activo: boolean, direccion: {}, pago: string, uidSucursal: string}) {
    //      return new Promise(( resolve, reject ) => {
    //        this.afsAuth.auth.createUserWithEmailAndPassword(data.email, data.password)
    //        .then(userData => {
    //          resolve(userData),
    //          this.uid = userData.user.uid;
    //          console.log('uidsasas', this.uid);
    //           this.afs.collection(`/${ this.CARPETA_IMAGENES }`).doc(this.uid).set({
    //            nombre: data.nombre,
    //            username: data.username,
    //            photourl: data.photourl,
    //            email: data.email,
    //            contacto: data.contacto,
    //            address: data.address,
    //            phone: data.phone,
    //            roles: data.roles,
    //            tipo: data.tipo,
    //            activo: data.activo,
    //            uid: this.uid,
    //            direccion: data.direccion,
    //            pago: data.pago,
    //            uidSucursal: data.uidSucursal
    //           });
    //         // console.log('data', this.data);
    //       });
    //        });
    // }
    RestauranteDataService.prototype.newRegister = function (abrir, cerrar, email, password, username, contacto, address, phone, categoria, direccion, uidSucursal, img) {
        var _this = this;
        // Configuramos una nueva variable de conexión
        var config = {
            apiKey: "AIzaSyAf1DuDZ8suCX2bqoG3QgiiRGAwUBeKPNU",
            authDomain: "lolo-1b8ff.firebaseapp.com",
            databaseURL: "https://lolo-1b8ff.firebaseio.com",
        };
        var secondaryApp = firebase__WEBPACK_IMPORTED_MODULE_5__["initializeApp"](config, 'Secondary');
        // Registramos al repartidor usando la nueva variable de conexión
        return new Promise(function (resolve, reject) {
            secondaryApp
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then(function (userData) {
                resolve(userData),
                    _this.updateUserData(abrir, cerrar, userData.user, username, contacto, address, phone, categoria, direccion, uidSucursal, img);
                _this.alertService.success('Se agrego correctamente el convenio');
            })
                .catch(function (err) { return _this.alertService.danger(err); });
        });
    };
    RestauranteDataService.prototype.updateUserData = function (abrir, cerrar, user, username, contacto, address, phone, categoria, direccion, uidSucursal, img) {
        var userRef = this.afs.doc("users/" + user.uid);
        var data = {
            username: username,
            contacto: contacto,
            address: address,
            phone: phone,
            uidCategoria: categoria,
            hora_abierto: abrir,
            hora_cerrar: cerrar,
            direccion: direccion,
            activo: true,
            email: user.email,
            uidSucursal: uidSucursal,
            roles: {
                restaurante: true
            },
            tipo: 3,
            uid: user.uid,
            photourl: img,
            pago: 'Habil'
        };
        return userRef.set(data, { merge: true });
    };
    RestauranteDataService.prototype.updateConvenio = function (abrir, cerrar, user, username, contacto, address, phone, categoria, direccion, uidSucursal, img) {
        var _this = this;
        var userRef = this.afs.doc("users/" + user);
        var data = {
            username: username,
            contacto: contacto,
            address: address,
            phone: phone,
            uidCategoria: categoria,
            hora_abierto: abrir,
            hora_cerrar: cerrar,
            direccion: direccion,
            activo: true,
            uidSucursal: uidSucursal,
            roles: {
                restaurante: true
            },
            tipo: 3,
            uid: user,
            photourl: img,
            pago: 'Habil'
        };
        return userRef.update(data).then(function () { return _this.alertService.success('Convenio actualizado correctamente.'); });
    };
    RestauranteDataService.prototype.getAllRestaurantes = function () {
        return (this.restaurantes = this.restaurantesCollection
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.uid = action.payload.doc.id;
                return data;
            });
        })));
    };
    RestauranteDataService.prototype.deleteRestaurante = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/delete';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('El usuario se elimino de SDK');
        });
        this.restauranteDoc = this.afs.doc("users/" + uid);
        this.restauranteDoc.delete();
        // this.alertService.info('El convenio se elimino correctamente');
    };
    RestauranteDataService.prototype.isUserRestaurante = function (userUid) {
        return this.afs.doc("users/" + userUid).valueChanges();
    };
    RestauranteDataService.prototype.isAuth = function () {
        return this.afsAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (auth) { return auth; }));
    };
    RestauranteDataService.prototype.desactivarConvenio = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/inhabilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('Se deshabilito del SDK');
        });
        // const idRepartidor = repartidor.uid;
        this.afs
            .collection('users')
            .doc(uid)
            .update({
            activo: false
        });
        // this.alertService.info('El convenio se inhabilito correctamente');
    };
    RestauranteDataService.prototype.activarConvenio = function (uid) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["Headers"]({
            'Content-Type': 'application/json'
        });
        //
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_7__["RequestOptions"]({ headers: headers });
        var url = 'https://sdk-admin-bme.firebaseapp.com/habilitar';
        var data = JSON.stringify({
            uid: uid
        });
        this.http.post(url, data, options).subscribe(function (res) {
            console.log('Se habilito del SDK');
        });
        // const idRepartidor = repartidor.uid;
        this.afs
            .collection('users')
            .doc(uid)
            .update({
            activo: true
        });
        // this.alertService.info('El convenio se habilito correctamente');
    };
    RestauranteDataService.prototype.getConvenios = function (uidSucursal) {
        return this.afs.collection('users', function (ref) {
            return ref.where('tipo', '==', 3).where('uidSucursal', '==', uidSucursal);
        }).valueChanges();
    };
    RestauranteDataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"],
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"],
            ngx_alerts__WEBPACK_IMPORTED_MODULE_6__["AlertService"],
            _angular_http__WEBPACK_IMPORTED_MODULE_7__["Http"]])
    ], RestauranteDataService);
    return RestauranteDataService;
}());



/***/ }),

/***/ "./src/app/service/transporte/transporte.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/service/transporte/transporte.service.ts ***!
  \**********************************************************/
/*! exports provided: TransporteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransporteService", function() { return TransporteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var TransporteService = /** @class */ (function () {
    function TransporteService(afs) {
        this.afs = afs;
        this.selectedTransporte = { id: null };
        this.transportesCollection = afs.collection('transportes');
        this.transportes = this.transportesCollection.valueChanges();
    }
    TransporteService.prototype.addTransporte = function (transporte) {
        this.transportesCollection.add(transporte);
    };
    TransporteService.prototype.getAllTransportes = function () {
        return this.transportes = this.transportesCollection.snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (changes) {
            return changes.map(function (action) {
                var data = action.payload.doc.data();
                data.id = action.payload.doc.id;
                return data;
            });
        }));
    };
    TransporteService.prototype.deleteTransporte = function (idTransporte) {
        this.transporteDoc = this.afs.doc("transportes/" + idTransporte);
        this.transporteDoc.delete();
    };
    TransporteService.prototype.updateTransporte = function (transporte) {
        var idTransporte = transporte.id;
        this.transporteDoc = this.afs.doc("transportes/" + idTransporte);
        this.transporteDoc.update(transporte);
    };
    TransporteService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], TransporteService);
    return TransporteService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: true,
    firebaseConfig: {
        apiKey: "AIzaSyAf1DuDZ8suCX2bqoG3QgiiRGAwUBeKPNU",
        authDomain: "lolo-1b8ff.firebaseapp.com",
        databaseURL: "https://lolo-1b8ff.firebaseio.com",
        projectId: "lolo-1b8ff",
        storageBucket: "lolo-1b8ff.appspot.com",
        messagingSenderId: "901139849089",
        appId: "1:901139849089:web:7185b8a77aee749a0e1280",
        measurementId: "G-YPQSPVC2J1"
    }
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/dani/Desktop/lolo-admin/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map