import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCategoryConvenioComponent } from './list-category-convenio.component';

describe('ListCategoryConvenioComponent', () => {
  let component: ListCategoryConvenioComponent;
  let fixture: ComponentFixture<ListCategoryConvenioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCategoryConvenioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCategoryConvenioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
