import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDetailServiciosComponent } from './list-detail-servicios.component';

describe('ListDetailServiciosComponent', () => {
  let component: ListDetailServiciosComponent;
  let fixture: ComponentFixture<ListDetailServiciosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDetailServiciosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDetailServiciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
