import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCategoryConvenioComponent } from './modal-category-convenio.component';

describe('ModalCategoryConvenioComponent', () => {
  let component: ModalCategoryConvenioComponent;
  let fixture: ComponentFixture<ModalCategoryConvenioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCategoryConvenioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCategoryConvenioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
