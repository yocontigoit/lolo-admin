import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTransporteComponent } from './list-transporte.component';

describe('ListTransporteComponent', () => {
  let component: ListTransporteComponent;
  let fixture: ComponentFixture<ListTransporteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTransporteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTransporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
