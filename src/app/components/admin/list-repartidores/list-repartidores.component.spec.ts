import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRepartidoresComponent } from './list-repartidores.component';

describe('ListRepartidoresComponent', () => {
  let component: ListRepartidoresComponent;
  let fixture: ComponentFixture<ListRepartidoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRepartidoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRepartidoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
