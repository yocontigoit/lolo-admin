// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAf1DuDZ8suCX2bqoG3QgiiRGAwUBeKPNU",
    authDomain: "lolo-1b8ff.firebaseapp.com",
    databaseURL: "https://lolo-1b8ff.firebaseio.com",
    projectId: "lolo-1b8ff",
    storageBucket: "lolo-1b8ff.appspot.com",
    messagingSenderId: "901139849089",
    appId: "1:901139849089:web:7185b8a77aee749a0e1280",
    measurementId: "G-YPQSPVC2J1"
  }
};

